latexdiff -t BOLD --package=amsmath,endfloat,hyperref,biblatex  ~/mess/current/SN2019odp_old/paper/letter.tex letter.tex > letter_diff.tex
latexdiff -t BOLD --package=amsmath,endfloat,hyperref,biblatex  ~/mess/current/SN2019odp_old/paper/appendix.tex appendix.tex > appendix_diff.tex
latexmk -pdf main_diff.tex
