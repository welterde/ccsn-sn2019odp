import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import astropy.table as tbl



# Ic-BL
icbl_r = [
    ('1998bw', 0.54, 0.08, 13.0, 13.4),
    ('2002ap', 0.07, 0.01, 9.1, 12.2),
    ('2003jd', 0.43, 0.09, 11.9, 13.2),
    ('2005kz', 0.45, 0.09, 15.8, 21.1),
    ('2007ru', 0.41, 0.05, 6.8, 11.2),
    ('2009bb', 0.25, 0.04, 8.5, 10),
    ('2010bh', 0.17, 0.03, 4.6, 5)
]
icbl = tbl.Table(rows=icbl_r, names=('Name', 'Mni', 'e_Mni', 'tm_min', 'tm_max'))
icbl['e_tm'] = (icbl['tm_max'] - icbl['tm_min'])/2
icbl['tm'] = (icbl['tm_max'] + icbl['tm_min'])/2

plt.errorbar(icbl['Mni'], icbl['tm'], xerr=icbl['e_Mni'], yerr=icbl['e_tm'], alpha=0.5, ls='', label='Ic-BL', c='tab:green')
plt.text(0.54, 13.4, '1998bw')


# Ib
e_r = [
    ('1999dn', 0.1, 0.01, 13.9, 17.4),
    ('1999ex', 0.15, 0.04, 15.5, 16.9),
    ('2004dk', 0.22, 0.04, 15.9, 19.0),
    ('2004gq', 0.1, 0.05, 9.5, 12.7),
    ('2005bf', 0.07, 0.03, 9.2, 14.5),
    ('2005hg', 0.66, 0.1, 11.0, 13.4),
    ('2006ep', 0.06, 0.1, 10.8, 17.3),
    ('2007C', 0.17, 0.04, 8.7, 12.9),
    ('2007Y', 0.04, 0.01, 10.9, 14.9),
    ('2007uy', 0.28, 0.04, 12.5, 14.7),
    ('2008D', 0.09, 0.01, 15.2, 15.6),
    ('2009jf', 0.24, 0.03, 18.4, 21.8),
    ('iPTF13bvn', 0.06, 0.02, 12.5, 13.4)
]
e = tbl.Table(rows=e_r, names=('Name', 'Mni', 'e_Mni', 'tm_min', 'tm_max'))
e['e_tm'] = (e['tm_max'] - e['tm_min'])/2
e['tm'] = (e['tm_max'] + e['tm_min'])/2

plt.errorbar(e['Mni'], e['tm'], xerr=e['e_Mni'], yerr=e['e_tm'], alpha=0.5, ls='', label='Ib', c='tab:red')
plt.text(0.09, 15.4, '2008D')
plt.text(0.06, 12.7, 'iPTF13bvn')


# Ic
e_r = [
    ('1994I', 0.07, 0.01, 5.6, 7.3),
    ('2004aw', 0.2, 0.04, 14.6, 15.8),
    ('2004dn', 0.16, 0.03, 10.2, 14.4),
    ('2004fe', 0.23, 0.04, 9.3, 12.5),
    ('2005az', 0.24, 0.05, 13, 16.9),
    ('2005mf', 0.17, 0.06, 9.8, 13.5),
    ('2007gr', 0.08, 0.01, 10.9, 12.9),
    ('2011bm', 0.62, 0.09, 28.3, 30.4)
     
]
e = tbl.Table(rows=e_r, names=('Name', 'Mni', 'e_Mni', 'tm_min', 'tm_max'))
e['e_tm'] = (e['tm_max'] - e['tm_min'])/2
e['tm'] = (e['tm_max'] + e['tm_min'])/2

plt.errorbar(e['Mni'], e['tm'], xerr=e['e_Mni'], yerr=e['e_tm'], alpha=0.5, ls='', label='Ic', c='tab:purple')
plt.text(0.62, 28.5, '2011bm')


plt.errorbar(0.24, 18.6, xerr=0.01, yerr=2, lw=3, label='SN2019odp', ls='', c='tab:blue')



plt.legend()
plt.xlabel('Mni [Msol]')
plt.ylabel('Tm [d]')



plt.savefig('mni_tm.png')
