import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import astropy.table as tbl
import pandas as pd
import seaborn


# min   -> M(O) > 0.47
# LTE   -> M(O) > 3
# 5000K -> M(O) > 2.5

rows = [
    (18.1, 1.71, 3.95, 'Dessart+2021(bin)', 'min'),
    (35.7, 3.03, 5.32, 'Dessart+2021(bin)', 'min'),
    (35.7, 3.03, 5.32, 'Dessart+2021(bin)', 'LTE'),
    (35.7, 3.03, 5.32, 'Dessart+2021(bin)', '5000K'),

    (12.1, 0.57, 0, 'Laplace+2021(sngl)', 'min'),
    (13.0, 1.01, 0, 'Laplace+2021(sngl)', 'min'),
    (14.0, 1.23, 0, 'Laplace+2021(sngl)', 'min'),
    (15.0, 1.36, 0, 'Laplace+2021(sngl)', 'min'),
    (16.0, 1.68, 0, 'Laplace+2021(sngl)', 'min'),
    (17.0, 1.82, 0, 'Laplace+2021(sngl)', 'min'),
    (18.0, 2.10, 0, 'Laplace+2021(sngl)', 'min'),
    (19.0, 2.04, 0, 'Laplace+2021(sngl)', 'min'),
    (20.0, 2.06, 0, 'Laplace+2021(sngl)', 'min'),
    (21.0, 2.31, 0, 'Laplace+2021(sngl)', 'min'),
    (21.0, 2.31, 0, 'Laplace+2021(sngl)', '5000K'),
    
    (14.0, 0.49, 0, 'Laplace+2021(bin)', 'min'),
    (15.0, 0.72, 0, 'Laplace+2021(bin)', 'min'),
    (16.0, 1.04, 0, 'Laplace+2021(bin)', 'min'),
    (17.0, 1.34, 0, 'Laplace+2021(bin)', 'min'),
    (18.0, 1.56, 0, 'Laplace+2021(bin)', 'min'),
    (20.0, 1.68, 0, 'Laplace+2021(bin)', 'min'),
    (21.0, 1.98, 0, 'Laplace+2021(bin)', 'min'),

    (35, 2.39, 0, 'Woosley+1993(WR)', 'min'),
    (40, 2.42, 0, 'Woosley+1993(WR)', 'min'),
    (60, 1.51, 0, 'Woosley+1993(WR)', 'min'),
    (60, 2.02, 0, 'Woosley+1993(WR)', 'min'),
    (85, 4.51, 0, 'Woosley+1993(WR)', 'min'),
    (85, 3.96, 0, 'Woosley+1993(WR)', 'min'),
    
    (35, 2.39, 0, 'Woosley+1993(WR)', '5000K'),
    (40, 2.42, 0, 'Woosley+1993(WR)', '5000K'),
    (85, 4.51, 0, 'Woosley+1993(WR)', '5000K'),
    (85, 3.96, 0, 'Woosley+1993(WR)', '5000K'),
    (85, 4.51, 0, 'Woosley+1993(WR)', 'LTE'),
    (85, 3.96, 0, 'Woosley+1993(WR)', 'LTE'),

    (25, 1.53, 3.95, 'Dessart+2011(bin)', 'min'),
    (60, 2.65, 4.20, 'Dessart+2011(sngl)', 'min'),
    (60, 2.65, 4.20, 'Dessart+2011(sngl)', '5000K'),
    (25, 1.78, 4.97, 'Dessart+2011(bin)', 'min'),
    
    
    (20.8, 0.42, 3.56, 'Dessart+2020(bin)', 'min'),
    (23.3, 0.571, 4.07, 'Dessart+2020(bin)', 'min'),
    (25.7, 0.838, 4.55, 'Dessart+2020(bin)', 'min'),
    (27.9, 1.214, 5.0, 'Dessart+2020(bin)', 'min'),
    (29.7, 1.487, 5.29, 'Dessart+2020(bin)', 'min'),
    (31.7, 1.58, 5.2, 'Dessart+2020(bin)', 'min'),
    (33.7, 1.66, 5.25, 'Dessart+2020(bin)', 'min'),
    (35.7, 1.83, 5.43, 'Dessart+2020(bin)', 'min')
]

#for mzams in 



x = tbl.Table(rows=rows, names=('MZAMS', 'MO', 'Mej', 'paper', 'variant'))

lbls = list(np.unique(x['paper']))
matching = list(filter(lambda a: 'bin' in a, lbls))
#print(matching)

idx = np.zeros(len(x), dtype=np.bool)
for l in matching:
    idx = np.logical_or(idx, x['paper'] == l)


if False:
    df = x[idx].to_pandas()
    seaborn.stripplot(data=df, x='MZAMS', y='paper', hue='variant', dodge=True)
    plt.tight_layout()
    plt.savefig('mzams_bin.png')
else:
    df = x[~idx].to_pandas()
    seaborn.stripplot(data=df, x='MZAMS', y='paper', hue='variant', dodge=True)
    plt.tight_layout()
    plt.savefig('mzams_sngl.png')

#seaborn.boxplot(data=df, x='MZAMS', y='paper', hue='variant')

#seaborn.catplot(data=df, x='MZAMS', y='paper', hue='variant', dodge=True)







