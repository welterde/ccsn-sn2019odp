\documentclass{beamer}

\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{amsmath}


\title{SN2019odp}
\subtitle{Oxygen Mass Estimation}
\author{Tassilo Schweyer}
\institute{Stockholm University}
\date{2022-08}

\begin{document}

\frame{\titlepage}

\begin{frame}{Motivation}
  \centering
  \includegraphics[width=0.5\linewidth]{moi_mzams_ratio.png}
  
  Taken without permission from Kuncarayakti+2015
\end{frame}

\begin{frame}{Existing Methods}
  \begin{itemize}
  \item Uomoto+1986: $M(O I) = 10^8\cdot D^2\cdot F([O I]) \cdot \exp\left( 2.28/T_4 \right)$
    \begin{itemize}
    \item Often people assume $T_4 \approx 0.4$
    \end{itemize}
  \item Jerkstrand+2014: $M(O I) = \frac{L_{6300,6364}/\beta_{6300,6364}}{9.7\cdot10^{41} erg/s}\cdot\exp\left(22720/T\right)$
    \begin{itemize}
    \item Same-ish equation except for addition of $\beta$ (escape probability of the order $0.5$)
    \item Use $5577/6300,6364$ ratio to determine temperature instead of just assuming one
    \item Also includes $\beta_{5577}/\beta_{6300,6364}$ thats in the range of $1-2$
    \end{itemize}
  \item Both assume the ejecta to be completely optically thin
  \item No real treatment of uncertainties included in either
  \end{itemize}
  
\end{frame}

\begin{frame}{Observations/Data}
  \centering
  \includegraphics[width=0.55\linewidth]{../../paper/plots/specs/seq_nebular.png}
\end{frame}

\begin{frame}{Problems}
  \begin{itemize}
  \item Earlier spectra:
    \begin{itemize}
    \item Clearly visible recombination lines (suggests hotter/denser)
    \item 63xx complex shows extended red wing (explanation for that?)
    \item Quite strong calcium (influence on the oxygen lines?)
    \item Visible 5577 feature
    \item Ratio of peak at 6300 to 6364 is $\approx 1.7$ and not $3$ (thus not optically thin? 1.0 means completely optically thick)
    \end{itemize}
  \item Late Keck spectrum:
    \begin{itemize}
    \item No visible recombination lines
    \item No visible red wing
    \item No visible 5577 line... (doh!)
    \item Ratio is $\approx 2.7$.. quite close to 3
    \end{itemize}
    
  \end{itemize}
\end{frame}

  

\begin{frame}{J14 Result}
  \includegraphics[width=\linewidth]{j14_estimate.png}
\end{frame}

\begin{frame}{Needed Improvements}
  \begin{itemize}
  \item Must Have:
    \begin{itemize}
    \item Optical Depth as free parameter (no more treatment of 6300 and 6364 combined)
    \item Proper uncertainty propagation
    \end{itemize}
  \item Nice to have:
    \begin{itemize}
    \item Proper $\beta_{5577}/\beta_{6300}$ treatment (instead of just saying its 1-2)
    \item NLTE Effect Treatment of second excited state (5577 line)
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Updated Equations}
  \begin{eqnarray}
    \beta_\lambda = \frac{1 - \exp^{-\tau_\lambda}}{\tau_\lambda} \\
    \tau_{6364} = \frac{\tau_{6300}}{3} \\
    L_\lambda = \Lambda_{\lambda} \frac{\beta_\lambda M_\text{OI} }{\exp{T_\lambda / T}} \\
    \frac{\tau_{5577}}{\tau} = \frac{g_2}{g_1} d_2 \frac{A_{5577}}{A_{6300}} \frac{5577^3}{6300^3} \exp{\left(\frac{E_1}{k_B T}\right)} \frac{1- \exp{\frac{-\Delta E_{2\rightarrow1}}{k_B T}}  }{1-\exp{\frac{-E_1}{k_B T}}} \\
    \frac{L_{5577}}{L_{6300}} = d_2\, \frac{g_{u_2}}{g_{u_1}} \cdot \exp^{\frac{-\Delta E_{2 \rightarrow 1}}{k_BT}} \frac{A_{5577} \beta_{5577} (d_2, T, \tau)}{A_{6300} \beta_{6300}} \frac{6300\AA}{5577\AA}
  \end{eqnarray}

  Free Parameters: $M_\text{OI}$, $T$, $\tau \equiv \tau_{6300}$, NLTE $d_2$
\end{frame}

\begin{frame}{Problems I (Math)}
  \begin{itemize}
  \item Going from M, T, $\tau$ to $L_{5577}$, $L_{6300}$, $L_{6364}$ is easy
  \item The inverse is not, since the equations are highly non-linear
  \item Linear error propagation would be quite hard too
  \item Solution: Use Bayesian model with MCMC/nested sampling and can ``run'' the equations in the easy direction
  \item Get error propagation for free
  \item Downside: Takes couple hours of cpu-time per spectrum
  \end{itemize}
  
\end{frame}


\begin{frame}{Problems II (Line Profiles)}
  \begin{itemize}
  \item Updated more complicated equations are nice and all, but we still have to measure the line fluxes
  \item And we need to seperate 6300 and 6364 with them overlapping
  \item Strategy 1: Simple Gaussian (also tried Voigt and some other simple analytic profiles) $\rightarrow$ There is some extra flux on the blue wing that looks like additional component
  \item Strategy 2: Gaussian + second gaussian at that blue feature (plus 6364 MZ and matching 5577) $\rightarrow$ works perfectly for the late spectrum, but not really for the first spectra, where there is a lot more going on
  \item Strategy N: Use 7774 line as profile (only works on early spectra though)
  \end{itemize}
\end{frame}

\begin{frame}{Result Strategy 1 (Single Gaussian)}
  \includegraphics<1>[width=0.8\linewidth]{fit_diag_full_snglline_nlte0.1.png}
  \only<2>{
    \begin{columns}
      \begin{column}{0.5\textwidth}
        keck early
        \includegraphics[width=\linewidth]{../../playground/2022-07-18-oxygen-model-corner-tiny-keck_early-full_snglline_nlte0.1.png}
      \end{column}
      \begin{column}{0.5\textwidth}
        keck late
        \includegraphics[width=\linewidth]{../../playground/2022-07-18-oxygen-model-corner-tiny-keck_late-full_snglline_nlte0.1.png}
      \end{column}
    \end{columns}
  }
  \includegraphics<3>[width=0.8\linewidth]{../../playground/2022-07-18-temp-minmax-mass-full_snglline_nlte0.1.png}
  \includegraphics<4>[width=0.8\linewidth]{../../playground/2022-07-18-nlte-minmax-mass-full_snglline_nlte0.1.png}
\end{frame}

\begin{frame}{Result Strategy 2 (Two Gaussian)}
  \includegraphics<1>[width=0.8\linewidth]{../../playground/2022-07-18-oxygen-fit-diag-full_twoline_nlte0.1_mz.png}
  \only<2>{
    \begin{columns}
      \begin{column}{0.5\textwidth}
        keck early
        \includegraphics[width=\linewidth]{../../playground/2022-07-18-oxygen-model-corner-tiny-keck_early-full_twoline_nlte0.1_mz.png}
      \end{column}
      \begin{column}{0.5\textwidth}
        keck late
        \includegraphics[width=\linewidth]{../../playground/2022-07-18-oxygen-model-corner-tiny-keck_late-full_twoline_nlte0.1_mz.png}
      \end{column}
    \end{columns}
  }
  \includegraphics<3>[width=0.8\linewidth]{../../playground/2022-07-18-temp-minmax-mass-full_twoline_nlte0.1_mz.png}
  \includegraphics<4>[width=0.8\linewidth]{../../playground/2022-07-18-nlte-minmax-mass-full_twoline_nlte0.1_mz.png}
\end{frame}

\begin{frame}{Result Strategy 3 (2.5 Elongated Gaussian)}
  \includegraphics<1>[width=0.8\linewidth]{../../playground/2022-07-18-oxygen-fit-diag-full_multiline_nlte0.1_mz_shell.png}
  \only<2>{
    \begin{columns}
      \begin{column}{0.5\textwidth}
        keck early
        \includegraphics[width=\linewidth]{../../playground/2022-07-18-oxygen-model-corner-tiny-keck_early-full_multiline_nlte0.1_mz_shell.png}
      \end{column}
      \begin{column}{0.5\textwidth}
        keck late
        \includegraphics[width=\linewidth]{../../playground/2022-07-18-oxygen-model-corner-tiny-keck_late-full_multiline_nlte0.1_mz_shell.png}
      \end{column}
    \end{columns}
  }
  \includegraphics<3>[width=0.8\linewidth]{../../playground/2022-07-18-temp-minmax-mass-full_multiline_nlte0.1_mz_shell.png}
  \includegraphics<4>[width=0.8\linewidth]{../../playground/2022-07-18-nlte-minmax-mass-full_multiline_nlte0.1_mz_shell.png}
\end{frame}

\begin{frame}{Result Strategy N (mangled 7774 line + flattop-gaussian)}
  \only<1>{
    \includegraphics[width=0.8\linewidth]{scenario_n_lineprofile_fit.png}
    
    Unconstrained fit (Ratio fitting parameter)
  }
  \only<2>{
    \includegraphics[width=0.8\linewidth]{../../playground/2022-08-18-7774-mangle-unconstrained-corner.png}
    Unconstrained fit
  }
  % TODO 3: Add corner plot of the unconstrained fit
  \only<3>{
    \includegraphics[width=0.8\linewidth]{scenario_n_integrated_fit.png}
    
    Integrated fit (M, T, .. as fitting parameter)
  }
  \only<4>{
    \includegraphics[width=0.8\linewidth]{../../playground/2022-08-18-oxygen-7774-constrained-corner.png}
    %Integrated fit (M, T, .. as fitting parameter)
  }
  \only<5>{
    \includegraphics[width=0.8\linewidth]{scenario_n_sampled_temp_moimin.png}
    Unconstrained fit (sampled); 0.5Msol limit
  }
  \only<6>{
    \includegraphics[width=0.8\linewidth]{scenario_n_sampled_nlte_moi_shell_min.png}
    Unconstrained fit (sampled); $d_2 = 0.1$ corresponds $n_e \approx 2\cdot10^7$
  }
  \only<7>{
    \includegraphics[width=0.8\linewidth]{scenario_n_sampled_temp_moi_shell_min.png}
    
    Shell oxygen
  }
  % TODO 4: Add corner plot of constrained fit
  % TODO 1: best fit profiles keck_early + corner
\end{frame}

\begin{frame}{Further Constraints}
  \begin{itemize}
  \item Electron Density from Oxygen Recombination Lines (early spectra only; not as constraining as initially thought, but only upper limit on $n_e^2 f_O$)
    \begin{itemize}
    \item $n_e \sqrt{f_O}=$ $2.2\cdot 10^8$ (2500K) to $3.5\cdot10^8$ (7500K)
    \item Not quite useless though, gives us min temperature of $\approx 4500$K at early epoch
    \end{itemize}
  \item Electron Density from Line Volume (quite uncertain, but suggests $n_e$ in range $10^6$ to $7\cdot 10^7$)
  \end{itemize}
  
\end{frame}


\begin{frame}{Discussion}

\end{frame}






\end{document}
