#!/usr/bin/env python
import click, os, sys
import astropy.table as table
import h5py
import numpy as np
import astropy.coordinates as coord
import astropy.units as u

VEGA_TO_AB = {
    'J': 0.91,
    'H': 1.39,
    'K': 1.85,
    'g': 0,
    'r': 0,
    'i': 0,
    'z': 0
}


@click.command()
@click.option('-o', '--output')
@click.option('--extract-cal', is_flag=True)
@click.option('--dump', is_flag=True)
@click.argument('files', nargs=-1)
def main(files, output, dump, extract_cal):
    data = {}
    for band in 'grizJHK':
        data[band] = []
    
    for fname in files:
        band = os.path.basename(fname)[-4]
        if band in 'griz':
            cat = table.Table.read(fname, format='hdf5', path='/GROND/%s/phot/psf' % band)
            #print(cat)
        else:
            cat = table.Table.read(fname, format='hdf5', path='/GROND/%s/phot/aperture' % band)
        cat_coord = coord.SkyCoord(cat['RA'], cat['DEC'], unit=(u.deg, u.deg))

        if extract_cal:
            ref_coord = coord.SkyCoord(346.8139199381905, 13.846924974588713, unit=(u.deg, u.deg))
        else:
            ref_coord = coord.SkyCoord(346.829542, 13.85595, unit=(u.deg, u.deg))

        sep = cat_coord.separation(ref_coord).arcsec
        idx = sep < 2

        if np.count_nonzero(idx) == 0:
            print('Nothing found in %s' % fname)
            continue
        elif np.count_nonzero(idx) > 1:
            print('Found multiple (n=%d) in %s -> last_id=%d max_id=%d (len=%d)' % (np.count_nonzero(idx), fname, cat[idx][0]['ID'], cat[idx]['ID'].max(), len(cat)))

        # find the last row
        subidx = cat[idx]['ID'].argmax()
        last_row = cat[idx][subidx]
        #print(last_row)
        #sys.exit()
        f = h5py.File(fname, 'r')
        mjd = f['/GROND/%s' % band].attrs['mid_time_mjd']
        calib = f['/GROND/%s/calib' % band]
        offset = calib.attrs['offset']
        

        # perhaps convert VEGA to AB
        offset += VEGA_TO_AB[band]
        
        stddev = calib.attrs['stddev']
        if calib.attrs['calibration_source_number'] < 4:
            stddev += 0.7
        if band in 'griz':
            mag = last_row['MAG'] + offset
            mag_err = last_row['MERR'] + stddev
            mag_err_phot = last_row['MERR']
        else:
        #if True:
            mag = last_row['MAG1'] + offset
            mag_err = last_row['MERR1'] + stddev
            mag_err_phot = last_row['MERR1']
        mag_err_cal = stddev
        ncal = calib.attrs['calibration_source_number']
        data[band].append((mjd, mag, mag_err, mag_err_phot, mag_err_cal, ncal))

    columns = []
    names = ['mjd']
    for band in 'grizJHK':
        d = sorted(data[band], key=lambda a: a[0])
        if len(columns) == 0:
            mjd = [x[0] for x in d]
            columns.append(mjd)
        else:
            assert len(d) == len(columns[0])
        mag = [x[1] for x in d]
        merr = [x[2] for x in d]
        merr2 = [x[3] for x in d]
        merr3 = [x[4] for x in d]
        ncal = [x[5] for x in d]
        columns.append(mag)
        columns.append(merr)
        columns.append(merr2)
        columns.append(merr3)
        columns.append(ncal)
        names.append('mag_%s' % band)
        names.append('magerr_%s' % band)
        names.append('magerr_phot_%s' % band)
        names.append('magerr_cal_%s' % band)
        names.append('ncal_%s' % band)
    t = table.Table(columns, names=names)
    t.write(output)
    if dump:
        print(t)

if __name__ == '__main__':
    main()
