#!/usr/bin/env python3
import astropy.table as table
import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u
import click

from astropy.cosmology import Planck15 as cosmo
import astropy.constants as const



# TODO: refactor that into a common location
EXTINCTION_COEFF = {
    'g': 3.303,
    'r': 2.285,
    'i': 1.698,
    'z': 1.263,
    'J': 0.723,
    'H': 0.460,
    'K': 0.310
}
ebv = 0.165



GROND_BANDS = 'grizJHK'



# FIXME: that below is copy-pasted from elsewhere...

# lyman parameters from paper

lyman_pbc_params = np.array([
    0.168, -0.407, -0.608, # c0 c1 c2
    0.074 # rms
])

lyman_bc_params = np.array([
    0.054, -0.195, -0.719, # c0 c1 c2
    0.076 # rms
])

lyman_pbc_params_gi = np.array([
    0.051, -0.511, -0.195, # c0 c1 c2
    0.055 # rms
])

lyman_bc_params_gi = np.array([
    -0.029, -0.404, -0.230, # c0 c1 c2
    0.060 # rms
])

lyman_bc_cooling_params = np.array([
    -0.146, 0.479, 2.257, # c0 c1 c2
    0.078 # rms
])

COLOR_RANGES = {
    'pbc_gr': (-0.3, 1.0),
    'bc_gr': (-0.3, 1.0),
    'pbc_gi': (-0.8, 1.1),
    'bc_gi': (-0.8, 1.1)
}

VARIANTS = {
    'pbc_gr': ('g', 'r'),
    'bc_gr': ('g', 'r'),
    'pbc_gi': ('g', 'i'),
    'bc_gi': ('g', 'i')
}


# TODO: add support for other band configurations than just (g,r)
# TODO: add error estimation

def compute_bol_mag(mag_g, mag_r, method):
    if method == 'pbc_gr':
        c0, c1, c2, rms = lyman_pbc_params
    elif method == 'bc_gr':
        c0, c1, c2, rms = lyman_bc_params
    elif method == 'pbc_gi':
        c0, c1, c2, rms = lyman_pbc_params_gi
    elif method == 'bc_gi':
        c0, c1, c2, rms = lyman_bc_params_gi
    elif method == 'cooling':
        c0, c1, c2, rms = lyman_bc_cooling_params
    else:
        raise ValueError('Unknown method: %s' % method)

    dmag = mag_g - mag_r

    # validate color range
    color_min = dmag.min()
    color_max = dmag.max()

    if color_min < COLOR_RANGES[method][0]:
        print('* WARN: color in lc exceeds lyman (min lc = %f < %f)' % (color_min, COLOR_RANGES[method][0]))
    if color_max > COLOR_RANGES[method][1]:
        print('* WARN: color in lc exceeds lyman (max lc = %f > %f)' % (color_max, COLOR_RANGES[method][1]))
    
    bol_correction = c0 + c1*dmag + c2*dmag ** 2
    
    return mag_g + bol_correction, rms

def convert_luminosity(abs_mag):
    luminosity = (const.L_sun.cgs.value)*10 ** ((4.74 - abs_mag)/2.5)
    return luminosity


@click.command()
@click.option('--variant', default='bc_gr')
@click.option('--show', is_flag=True)
@click.option('--host-ebv', type=float, default=0)
@click.option('--overwrite', is_flag=True)
@click.argument('grond_lc')
@click.argument('dest_lc')
def main(variant, grond_lc, dest_lc, show, host_ebv, overwrite):
    grond_lc = table.Table.read(grond_lc)
    
    for band in GROND_BANDS:
        grond_lc['mag_%s' % band] -= EXTINCTION_COEFF[band] * (ebv+host_ebv)

    idx = grond_lc['magerr_g'] < 0.08
    grond_lc = grond_lc[idx]

    mjd = grond_lc['mjd']

    # load distance and convert to distance in cm
    redshift = table.Table.read('const/distance/current', format='ascii')

    dist = cosmo.luminosity_distance(redshift['redshift'][0]).to(u.cm)
    dist_cm = dist.value
    dist_pc = dist.to(u.pc).value
    dist_mod = 5*np.log10(dist_pc) - 5

    band_a, band_b = VARIANTS[variant]
    
    # now compute lyman qbol
    g_mags = np.array(grond_lc['mag_%s' % band_a], copy=True)
    i_mags = np.array(grond_lc['mag_%s' % band_b], copy=True)

    print('Using %s-%s mag' % (band_a, band_b))

    mag_err = np.sqrt(np.array(grond_lc['magerr_%s' % band_a], copy=True)**2 + np.array(grond_lc['magerr_%s' % band_b], copy=True)**2)

    lyman_mag, rms = compute_bol_mag(g_mags, i_mags, variant)
    abs_mag = lyman_mag - dist_mod
    #abs_mag_r = np.random.normal(abs_mag, rms/np.sqrt(2), size=1000)
    #lums_lyman = convert_luminosity(abs_mag)
    #lums_lyman = np.mean(lums_lyman_r)
    #lums_lyman_std = np.std(lums_lyman_r)
    lums_lyman_mean = []
    lums_lyman_std_total = []
    lums_lyman_std_method = []
    for row, phot_err in zip(abs_mag, mag_err):
        lums_lyman_mean.append(convert_luminosity(row))
        # calc the method error
        a = np.random.normal(row, rms+phot_err, size=100)
        l = convert_luminosity(a)
        
        lums_lyman_std_total.append(np.std(l))

        # calc method error
        a = np.random.normal(row, rms, size=100)
        l = convert_luminosity(a)
        lums_lyman_std_method.append(np.std(l))
    
    color = g_mags - i_mags

    # TODO: do MC to derive uncertainties

    # generate new table
    new_lc = table.Table([mjd, lums_lyman_mean, lums_lyman_std_total, lums_lyman_std_method, color], names=('MJD', 'Luminosity', 'Lums_Err', 'Lums_Err_Method', 'X-Color'))
    if show:
        new_lc.pprint()
    new_lc.write(dest_lc, overwrite=overwrite)


if __name__ == '__main__':
    main()
