#!/bin/sh
set -e

DESTFILE=$1

$(dirname $0)/imagesub_photometry.sh g data/phot/sci/ZTF19abqwtfu_20200826_NOT_g_wcs.fits $DESTFILE
$(dirname $0)/imagesub_photometry.sh g data/phot/sci/ZTF19abqwtfu_20200924_NOT_g_wcs.fits $DESTFILE

$(dirname $0)/imagesub_photometry.sh r data/phot/sci/ZTF19abqwtfu_20200826_NOT_r_wcs.fits $DESTFILE
$(dirname $0)/imagesub_photometry.sh r data/phot/sci/ZTF19abqwtfu_20200924_NOT_r_wcs.fits $DESTFILE
$(dirname $0)/imagesub_photometry.sh r data/phot/sci/ZTF19abqwtfu_20200927_NOT_r_wcs.fits $DESTFILE

$(dirname $0)/imagesub_photometry.sh i data/phot/sci/ZTF19abqwtfu_20200826_NOT_i_wcs.fits $DESTFILE
