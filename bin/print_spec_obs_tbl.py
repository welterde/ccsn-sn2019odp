import click, glob, re, sys, os
import astropy.table as table
import astropy.time as time
import astropy.io.fits as fits

try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../lib'))
import snelib19odp.const as const
import snelib19odp.dataloader as dataloader


TBL_HEADER = '''
\\begin{table*}
\\begin{tabular}{|c|c|c|c|c|c|c|c|}
\\hline
UT & MJD & $\\Delta t_\\text{expl}$ & $\\Delta t_g$ & Telescope/Instrument & Setup & Airmass & Exptime \\\\
& (days) & (days) & (days) & & & & (seconds) \\\\ 
\\hline
\\hline
'''

TBL_FOOTER = '''
\hline
\\end{tabular}
\\caption{Observation log of the spectroscopic observations.}
\\label{tab:obslog:spec}
\\end{table*}
'''



REQUIRED_HEADERS = ['DATE-OBS', 'EXPTIME']


@click.command()
@click.argument('destfile')
def main(destfile):
    fnames = sorted(glob.glob('data/specs/preproc_marshall/*.ascii'))

    # load extra metadata
    extra_meta = table.Table.read('data/specs_extra_metadata.txt', format='ascii')
    

    fo = open(destfile, 'w')
    fo.write(TBL_HEADER)

    for fname in fnames:
        # try to load metadata from the file
        meta = dataloader.get_meta(os.path.basename(fname))

        if not dataloader.check_headers(meta):
            sys.stderr.flush()
            continue

        # parse time
        obs_time = time.Time(meta['DATE-OBS'])
        
        ut_timestamp = obs_time.to_value('iso', 'date_hm')
        obs_mjd = obs_time.mjd
        delta_explosion = obs_mjd - const.explosion_epoch_mjd
        delta_phase = obs_mjd - const.sne_peak_mjd['g']
        
        telescope, setup = dataloader.guess_setup(os.path.basename(fname), meta)
        
        airmass = 0.0
        if 'AIRMASS' in meta:
            airmass = float(meta['AIRMASS'])

        exptime = meta['EXPTIME']

        fo.write(ut_timestamp)
        fo.write(' & ')
        fo.write('%.1f & ' % obs_mjd)
        fo.write('%.1f & ' % delta_explosion)
        fo.write('%.1f & ' % delta_phase)
        fo.write('%s & ' % telescope)
        fo.write('%s & ' % setup)
        fo.write('%.1f & ' % airmass)
        fo.write('%.0f \\\\\n' % float(exptime))

    fo.write(TBL_FOOTER)
    fo.close()
    



if __name__ == '__main__':
    main()
