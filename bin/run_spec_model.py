import click, glob, re, sys, os, collections, time, importlib.util
import pandas as pd
import numpy as np
import astropy.table as table

try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../lib'))
import snelib19odp.const as const

CONFIG_DIR = os.path.join(os.path.dirname(__file__), '../config')
PRODUCTS_DIR = os.path.join(os.path.dirname(__file__), '../products')

def import_helper(modname, moddir):
    for finder in sys.meta_path:
        spec = finder.find_spec(modname, [moddir])
        if spec is not None:
            mod = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(mod)
            return mod
    raise ImportError("Could not find module")


@click.command()
@click.option('--debug', is_flag=True)
@click.option('--print-results', is_flag=True)
@click.option('--print-lums', is_flag=True)
@click.option('--destfile')
@click.option('--nlive', type=int)
@click.option('--spec-module', default=f"{CONFIG_DIR}:models")
@click.argument('spec_name')
@click.argument('model_name')
def main(debug, spec_name, model_name, destfile, print_results, print_lums, nlive, spec_module):
    moddir, modname = spec_module.split(":", 2)
    models = import_helper(modname, moddir)
    model = models.make_model(model_name, spec_name)
    #sys.exit(1)
    #print(model)
    kwargs = {}
    if spec_name == 'not_late':
        kwargs['dynesty_method'] = 'rwalk'
    if nlive is not None:
        kwargs['nlive_init'] = nlive
        kwargs['dlogz_init'] = 1.0
    samples, result, sampler = model['fit_func'](**kwargs)
    
    # convert to table
    x = table.Table(rows=result.samples, names=model['labels'])
    x['weights'] = np.exp(result.logwt - result.logz[-1])
    x.meta['LABEL'] = f"{spec_name}_{model_name}"
    x.meta['SPEC_NAME'] = spec_name
    x.meta['MODEL_NAME'] = model_name
    if debug:
        print("Samples Table:")
        print(x)

    if print_results:
        y = table.Table(rows=samples, names=model['labels'])
        for label in model['labels']:
            p = np.percentile(y[label], [15, 50, 84])
            print(f"{label}\t{p[0]:.3e}\t{p[1]:.3e}\t{p[2]:.3e}\t{p[1]-p[0]:.2e}\t{p[2]-p[0]:.2e}")

    if print_lums:
        # TODO: implement this
        pass


    # make sure the destination directory exists
    dest_dir = os.path.join(PRODUCTS_DIR, 'spec_model', 'oxygen', spec_name, model_name)
    if not os.path.isdir(dest_dir):
        os.makedirs(dest_dir)

    if not destfile:
        destfile = os.path.join(dest_dir, '%d.fits' % int(time.time()))
    x.write(destfile, format='fits', overwrite=True)

if __name__ == '__main__':
    main()
