#!/usr/bin/env python3
import click, h5py, numba, collections, tqdm, dynesty
import numpy as np

import astropy.time as time
from dynesty import plotting as dyplot
import matplotlib.pyplot as plt

# of 19odp
MAX_EPOCH_MJD = time.Time('2019-09-09').mjd



GROUPS = [
    ('pre-peak', lambda a: a < -8),
    ('peak', lambda a: np.abs(a) < 8),
    ('post-peak', lambda a: a > 8)
]


def extract_total_metric(observed_phase, template_phase, metrics):
    results = {}
    for grp_name, func in GROUPS:
        obs_idx = np.array([func(a) for a in observed_phase], dtype=np.bool)
        template_idx = np.array([func(a) for a in template_phase], dtype=np.bool)
        
        best_l = -np.inf
        for i in np.arange(len(observed_phase))[obs_idx]:
            for j in np.arange(len(template_phase))[template_idx]:
                #print(metrics[i,j])
                #print(best_l)
                best_l = np.maximum(best_l, metrics[i,j])
        results[grp_name] = best_l
    return results


@click.command()
@click.option('--metric', default='snid_highpass')
@click.argument('template_lib')
@click.argument('corr_lib')
#@click.argument('sne_name')
def main(template_lib, corr_lib, metric):
    templates = h5py.File(template_lib, 'r')
    corr = h5py.File(corr_lib, 'r')

    # load the observed stuff..
    entries = []
    for grp_name in corr:
        grp = corr[grp_name]
        for ds_name in grp:
            ds = grp[ds_name]
            if 'DATE-OBS' not in ds.attrs:
                continue
            entries.append(ds)

    obs_times = np.array([time.Time(ds.attrs['DATE-OBS']).mjd for ds in entries])
    obs_phase = obs_times - MAX_EPOCH_MJD

    # group -> [(sne_name, best_likelihood)]
    results = {}
    for grp,func in GROUPS:
        results[grp] = []

    # sne_name -> type
    sne_type = {}
        
    for sne_name in tqdm.tqdm(templates):
        # now find full range of phase
        template_phase = []
        template_sn = templates[sne_name]

        if not isinstance(template_sn, h5py.Group):
            continue
        
        for ds_name in template_sn:
            ds = template_sn[ds_name]
            template_phase.append(ds.attrs['phase'])
        template_phase = np.array(template_phase)

        sne_type[sne_name] = template_sn.attrs['claimedtype']
        
        # compute metric matrix
        metrics = np.ones((len(obs_phase), len(template_phase)))*np.nan
        for i in range(len(obs_phase)):
            entry = entries[i]
            for j in range(len(template_phase)):
                idx = entry['template_sn'] == sne_name.encode('utf-8')
                idx = np.logical_and(idx, entry['phase'] == template_phase[j])
                #assert np.count_nonzero(idx) <= 1
                if np.count_nonzero(idx) == 1:
                    m = entry['%s_likelihood' % metric][idx] #+ 0.5np.log(entry['%s_overlap' % metric][idx])
                    #print(m)
                    metrics[i,j] = m


        data = extract_total_metric(obs_phase, template_phase, metrics)
        for grp,func in GROUPS:
            results[grp].append((sne_name, data[grp]))

    for grp,_func in GROUPS:
        best_l = -np.inf
        best_snn = None
        for sne_name, l in results[grp]:
            if l > best_l:
                best_l = l
                best_snn = sne_name
        print('%s => %s (%s) = %e' % (grp, best_snn, sne_type[best_snn], best_l))
    

if __name__ == '__main__':
    main()

