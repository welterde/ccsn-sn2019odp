#!/usr/bin/env python3
import click, h5py, numba, collections, tqdm
import numpy as np

import scipy.optimize as optimize
from scipy.interpolate import interp1d
from scipy.interpolate import splev, splrep
from numpy.lib import recfunctions as rfn

import matplotlib.pyplot as plt


Spec = collections.namedtuple('Spec', 'wave flux mask')

SN_TYPES=[
    ('Ibc', [b'Ib', b'Ic']),
    ('Ia', [b'Ia']),
    ('IIb', [b'IIb'])
]


SPLINE_MEAN_KNOTNUM = 17



#@numba.njit
def meanzero(flux):
    xknot = np.empty(len(flux))
    yknot = np.empty(len(flux))

    nw = len(flux)

    #print(flux[0])
    idx_nonzero = flux != 0.0
    assert np.count_nonzero(idx_nonzero) > 0
    l1 = np.min(np.arange(nw)[idx_nonzero])
    l2 = np.max(np.arange(nw)[idx_nonzero])
    assert (l2-l1) >= 3*SPLINE_MEAN_KNOTNUM

    # choose knots for spline
    nknot = 0
    kwidth = int(nw / SPLINE_MEAN_KNOTNUM)
    nave = 0
    wave = 0
    fave = 0
    istart = 0

    #print('Range: %d %d' % (l1, l2))
    #print('kwidth: %s' % repr(kwidth))
    #print('nw: %d' % nw)

    for i in range(nw):
        if i > l1 and i < l2:
            nave = nave + 1
            wave = wave + i-0.5
            fave = fave + flux[i]
        if (i-istart) % kwidth == 0:
            #print('nave: %s fave: %s' % (repr(nave), repr(fave)))
            if nave > 0 and fave > 0:
                #print(' -> knot')
                xknot[nknot] = wave / nave
                yknot[nknot] = np.log10(fave/nave)
                nknot = nknot + 1
            nave = 0
            wave = 0
            fave = 0

    assert nknot != 0

    xknot = xknot[0:nknot]
    yknot = yknot[0:nknot]
    #print('nknot: ', nknot)
    #print(xknot)
    #print(len(xknot))

    spline = splrep(xknot, yknot)

    flux_norm = np.zeros(nw)
    #if nknot % 2 == 0:
    #    interpolator = interp1d(xknot, yknot, kind=nknot-1)
    #else:
    #    interpolator = interp1d(xknot, yknot, kind=nknot)
    for i in range(l1, l2):
        #logspl = interpolator(i-0.5)
        logspl = splev(i-0.5, spline)
        flux_norm[i] = flux[i] / 10**logspl - 1.0
    #plt.plot(flux_norm[l1:l2])
    #plt.show()
    return flux_norm, (l1,l2)



@numba.njit
def snidbin(wlen, flux, nw, wlen0, dwlen_log, fnu=False):
    #wlen_bin = wlen0 * np.exp(np.arange(nw+1)*dwlen_log)
    #dwlen_bin = np.diff(wlen_bin)
    # mangle centers
    #for i in range(nw):
    #    wlen_bin[i] = 0.5*(wlen_bin[i]+wlen_bin[i+1])
    #wlen_bin = wlen_bin[0:(nw-2)]

    flux_bin = np.zeros(nw)

    for wlen_i in range(len(wlen)):
        if wlen_i == 0:
            s0 = 0.5*(3*wlen[wlen_i]-wlen[wlen_i+1])
            s1 = 0.5*(wlen[wlen_i]+wlen[wlen_i+1])
        elif wlen_i == len(wlen)-1:
            s0 = 0.5*(wlen[wlen_i-1]+wlen[wlen_i])
            s1 = 0.5*(3*wlen[wlen_i]-wlen[wlen_i-1])
        else:
            s0 = 0.5*(wlen[wlen_i-1]+wlen[wlen_i])
            s1 = 0.5*(wlen[wlen_i]+wlen[wlen_i+1])

        s0log = np.log(s0/wlen0)/dwlen_log
        s1log = np.log(s1/wlen0)/dwlen_log

        #print('log s0 = %f / log s1 = %f' % (s0log, s1log))

        if fnu:
            dw = (s1-s0)*2.99793e10/(wlen[wlen_i]*wlen[wlen_i]*1e-8)
        else:
            dw = s1-s0

        #print('dw = %e' % dw)
        
        for bin_i in range(int(s0log), int(s1log)+1):
            if bin_i >= 0 and bin_i < nw:
                alen = min([s1log, bin_i+1]) - max([s0log, bin_i])
                #print('alen = ', alen)
                #print('input flux = ', flux[wlen_i])
                flux_bin[bin_i] += flux[wlen_i] * alen/(s1log-s0log) * dw
                #print('%d -> %e' % (bin_i, flux_bin[bin_i]))

    #print(flux_bin*1e20)
    #print('Binned flux array size: %d' % len(flux_bin))

    #if True:
    #    wlen_bin = wlen0 * np.exp(np.arange(nw)*dwlen_log)
    #    plt.plot(wlen_bin, flux_bin)
    #    plt.show()
    return flux_bin


# port from https://github.com/nyusngroup/SESNspectraLib/blob/master/binspec.pro
@numba.njit
def binspec(wavelength, flux, wlen_start, wlen_end, wlen_bin):
    length = int((wlen_end - wlen_start)/wlen_bin) + 1
    out_wlen = np.arange(length)*wlen_bin + wlen_start
    out = np.empty(length, dtype=np.float64)

    interp_wlen = np.concatenate((wavelength, out_wlen))
    interp_wlen.sort()
    interp_wlen = np.unique(interp_wlen)

    interp_flux = np.interp(interp_wlen, wavelength, flux)

    for i in range(length):
        # XXX: off by one error here?
        w = np.logical_and(interp_wlen >= out_wlen[i], interp_wlen <= out_wlen[i+1])
        if np.count_nonzero(w) == 2:
            out[i] = 0.5*np.sum(interp_flux[w])*wlen_bin
        else:
            out[i] = np.trapz(interp_flux[w], interp_wlen[w])

    out[-1] = out[-2]

    out[np.logical_or(out_wlen > wavelength.max(), out_wlen < wavelength.min())] = 0.0
    return out_wlen, out/wlen_bin


def snidflat(spec, redshift, wlen_min, wlen_max):
    # wavelength bins (num, start, stop)
    nw = 1024
    w0 = 2500
    w1 = 10000

    # dwlog = np.log(w1/w0)/nw
    # wlen_log = w0*np.exp(np.arange(nw)*dwlog)
    # for i in range(nw-1):
    #     # half-wavelength-bins? snidbins??
    #     wlen_log[i] = 0.5*(wlen_log[i]+wlen_log[i+1])
    # wlen_log = wlen_log[:nw-1]
    # binsize = wlen_log[-1]-wlen_log[-2]
    # #wlog = np.array([0.5*(wlog[i]+wlog[i+1]) for i in range(nw-1)])

    # # de-redshift input spectrum
    wlen_z = spec.wave/(1+redshift)
    
    # # rebin onto log wavelength scale
    # 
    # log_flux_bin = snidbin(wlen_z[idx], flux[idx], wlen_log.min(), wlen_log.max(), binsize)

    dwlog = np.log(w1/w0)/nw
    wlen_log = w0*np.exp(np.arange(nw+1)*dwlog)
    
    idx = np.logical_and(spec.wave > wlen_min, spec.wave < wlen_max)
    flux_bin = snidbin(wlen_z[idx], spec.flux[idx], nw, w0, dwlog, fnu=True)
    
    
    for i in range(nw):
        # half-wavelength-bins? snidbins??
        wlen_log[i] = 0.5*(wlen_log[i]+wlen_log[i+1])
    wlen_log = wlen_log[:nw]

    #print('Len flux_bin = %d' % len(flux_bin))
    flux_bin_norm, (l1,l2) = meanzero(flux_bin)
    #print('Len flux_bin_norm = %d' % len(flux_bin_norm))

    #if do_filter:
    #    flux_bin_norm[l1:l2] = snidfilt(flux_bin_norm, k1=1, k2=4, k3=nw/12, k4=nw/10)[l1:l2]

    #print(flux_bin_norm)
        
    #print('wlen_flat: ', len(wlen_flat), ' flux_bin_norm ', len(flux_bin_norm))
    #flux_flat = np.interp(wlen_z, wlen_log, flux_bin_norm)

    #print(l1, l2)
    
    # this apodize thing from snid
    # if True:
    #     ftmp = np.array(flux_flat, copy=True)
    #     nsquash = min([nw*0.05, float((l2-l1)/2)])
    #     if nsquash >= 1:
    #         for i in range(int(nsquash)):
    #             arg = np.pi * i / (nsquash - 1)
    #             factor = 0.5 * (1 - np.cos(arg))
    #             ftmp[l1+i] = factor * flux_flat[l1+i]
    #             ftmp[l2-i] = factor * flux_flat[l2-i]
    #     flux_flat = ftmp
    
    # construct valid mask
    tmp_idx = np.arange(len(flux_bin_norm))
    idx = np.logical_and(np.logical_and(tmp_idx > l1, tmp_idx < l2), ~np.isnan(flux_bin_norm))
    
    return Spec(wlen_log, flux_bin_norm, idx)



def preprocess_spectrum(spec):
    spec = snidflat(spec, wlen_min=4000, wlen_max=9000, redshift=0.014353)

    return spec

def preprocess_template_spectrum(spec):
    spec = snidflat(spec, wlen_min=4000, wlen_max=9000, redshift=0.0)

    return spec



def to_spec(ds):
    wave = np.array(ds['wavelength'], copy=True)
    flux = np.array(ds['flux'], copy=True)
    #print(wave.dtype)
    mask = np.ones(len(wave), dtype=np.bool)
    return Spec(wave, flux, mask)


@click.command()
@click.option('--view-spec')
@click.option('--view-best', nargs=2)
@click.argument('template_lib')
@click.argument('specs_lib')
@click.argument('corr_lib')
def main(template_lib, specs_lib, corr_lib, view_spec, view_best):
    templates = h5py.File(template_lib, 'r')
    specs = h5py.File(specs_lib, 'r')
    corr = h5py.File(corr_lib, 'r')

    if view_spec:
        grp_name,ds_name = view_spec.split('/')
        ds = corr[grp_name][ds_name]
        
        for label, sn_types in SN_TYPES:
            idx = np.zeros(len(ds['sn_type']), dtype=np.bool)
            for sn_type in sn_types:
                #m_sn_type = sn_type + '\x00'*(10-len(sn_type))
                #print(m_sn_type)
                #print(ds['sn_type'][0])
                #print('\n\n\n\n')
                idx = np.logical_or(idx, np.logical_and(ds['sn_type'] == sn_type,
                                                        ds['snid_highpass_likelihood'] > -100)
                )
                
            print(label, np.count_nonzero(idx))
            plt.hist(ds['snid_highpass_likelihood'][idx], label=label, bins=25, histtype='step', density=True)
        plt.legend()
        plt.show()
    elif view_best:
        dscr, str_sn_type = view_best
        sn_type = str_sn_type.encode('utf-8')
        grp_name, ds_name = dscr.split('/')
        ds = corr[grp_name][ds_name]
        idx = ds['sn_type'] == sn_type
        b = ds['snid_highpass_likelihood'][idx]
        red_likelihood = ds['snid_highpass_likelihood'][idx]/ds['snid_highpass_overlap'][idx]
        #i = ds['snid_highpass_likelihood'][idx].argmax()
        i = red_likelihood.argmax()
        full_idx = np.arange(len(ds['sn_type']))[idx][i]

        sne_name = ds['template_sn'][full_idx]
        t_ds_name = ds['template_spec'][full_idx]

        print('Best Match: %s / %s' % (sne_name, t_ds_name))

        # load template spec
        t_ds = templates[sne_name][t_ds_name]
        t_spec = preprocess_template_spectrum(to_spec(t_ds))

        # load observed spec
        s_ds = specs[grp_name][ds_name]
        s_spec = preprocess_spectrum(to_spec(s_ds))

        plt.plot(t_spec.wave, t_spec.flux, label='Template')
        plt.plot(s_spec.wave, s_spec.flux, label='Observed')
        plt.legend()
        plt.show()
    
        


if __name__ == '__main__':
    main()
    
