#!/usr/bin/env python
import click
from ztfquery import fritz
import astropy.table as table
import astropy.time as time
import os
import sys


REF_TIME_MJD = time.Time('2021-04-06T12:15:30').mjd
REF_TIME_VAL = 1311.01


@click.command()
@click.option('--overwrite', is_flag=True)
@click.option('--type-filter', default='SN Ib')
@click.argument('target_csv')
@click.argument('destfile')
def main(target_csv, destfile, overwrite, type_filter):
    # load the target list csv
    targets = table.Table.read(target_csv, format='ascii')

    if os.path.isfile(destfile) and not overwrite:
        sys.stderr.write('Destination file %s already exists and not told to overwrite!\n')
        sys.exit(1)
    elif os.path.isfile(destfile) and overwrite:
        os.remove(destfile)

    objlist = []
    peak_mjd = []
    peak_flt = []
        
    for row in targets:
        if row['type'].strip() != type_filter:
            continue
        target_name = row['ZTFID']
        print(' * Downloading %s' % target_name)
        try:
            lc = fritz.download_lightcurve(target_name, get_object=True)
        except KeyError:
            print('\tDownload failed.. some error..')
            continue
        tab = table.Table.from_pandas(lc.data)
        # delete those columns since they cannot be easily be mapped to supported datatypes in HDF5
        del tab['groups']
        del tab['altdata']
        del tab['ra_unc']
        del tab['dec_unc']
        
        tab.write(destfile, path=target_name, append=True)
        peak_mjd.append(row['peakt'] - REF_TIME_VAL + REF_TIME_MJD)
        peak_flt.append(row['peakfilt'])
        objlist.append(target_name)
    t = table.Table({'Object': objlist, 'Peak_MJD': peak_mjd, 'Peak_Filter': peak_flt})
    t.write(destfile, path='objects', append=True)

if __name__ == '__main__':
    main()
