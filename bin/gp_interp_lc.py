#!/usr/bin/env python3
import click
import gp3.ztf.ingest as ztf_ingest
import numpy as np
import george
from george import kernels
import scipy.optimize as optimize
from astropy.table import Table, Column



@click.command()
#@click.option('--ztf-input', is_flag=True
@click.option('--p48-only', is_flag=True)
@click.argument('infile', type=click.Path(exists=True))
@click.argument('destfile', type=click.Path(exists=False))
def main(infile, destfile, p48_only):
    # ingest the main data
    df = ztf_ingest.ingest_growth_marshall_data(infile)
    df.rename(columns={'magpsf': 'MAG', 'sigmamagpsf': 'MAG_ERR'}, inplace=True)
    df = df[df['MAG'].notnull()]

    if p48_only:
        df = df.query('telescope == "P48"')

    # get the time range for which have data..
    min_mjd = df.index.get_level_values('MJD').min()
    max_mjd = df.index.get_level_values('MJD').max()
    mjd_pred = np.linspace(min_mjd, max_mjd, 500)

    interp_lcs = {}
    
    # construct a lightcurve per instrument/band
    for group_key, df_sel in df.groupby(level=(0,1,2), sort=True):
        print('\n', repr(group_key))
        (telescope, instrument, band) = group_key
        
        mjd = df_sel.index.get_level_values('MJD')
        mag = df_sel['MAG']
        mag_err = df_sel['MAG_ERR']

        kernel = 3 * kernels.ExpSquaredKernel(60)
        gp = george.GP(kernel, mean=19.27, fit_mean=True)
        gp.compute(mjd, mag_err)

        def nll(p):
            gp.set_parameter_vector(p)
            ll = gp.log_likelihood(mag, quiet=True)
            return -ll if np.isfinite(ll) else 1e25

        def grad_nll(p):
            gp.set_parameter_vector(p)
            return -gp.grad_log_likelihood(mag, quiet=True)

        p0 = gp.get_parameter_vector()
        results = optimize.minimize(nll, p0, jac=grad_nll, method="L-BFGS-B")

        # Update the kernel and print the final log-likelihood.
        gp.set_parameter_vector(results.x)
        print(gp.get_parameter_vector())

        pred, pred_var = gp.predict(mag, mjd_pred, return_var=True)

        interp_lcs[(telescope, instrument, band)] = (pred, pred_var)

    c1 = [Column(v[0], 'MAG_%s_%s_%s' % k) for k,v in interp_lcs.items()]
    c2 = [Column(v[1], 'MAGERR_%s_%s_%s' % k) for k,v in interp_lcs.items()]
    t = Table([Column(mjd_pred, 'MJD')] + c1 + c2)
    t.write(destfile, format='ascii')
        
    


if __name__ == '__main__':
    main()
