#!/usr/bin/env python
import click
import re
from datetime import datetime
import astropy.io.fits as fits
import astropy.table as table
import astropy.units as u
import astropy.time as time
from astropy.cosmology import Planck15 as cosmology
import numpy as np

import matplotlib.pyplot as plt


def load_distance(fname):
    params = table.Table.read(fname, format='ascii')
    z = params['redshift'][0]
    return cosmology.comoving_distance(z).to(u.cm).value


FILE_RE = re.compile(r'^.*_(\d+)_.*$')



def extract_time(fname):
    m = FILE_RE.match(fname)
    dat = m.groups(1)[0]
    #print(dat)
    d2 = datetime.strptime(dat, '%Y%m%d')
    #print(d2)
    #print(Time(d2).mjd)
    return time.Time(d2).mjd


@click.command()
@click.option('--qbol-format', is_flag=True)
@click.option('-o', '--output')
@click.option('--overwrite', is_flag=True)
@click.option('--plot', is_flag=True)
@click.option('--plot-dest')
@click.option('--wlen-min', type=float, default=4000)
@click.option('--wlen-max', type=float, default=9000)
# TODO: add monte carlo mode
@click.option('--distance', default="const/distance/current")
@click.argument('specfiles', nargs=-1)
def main(distance, specfiles, wlen_min, wlen_max, output, plot, plot_dest, qbol_format, overwrite):
    dist = load_distance(distance)
    #print('src_file_name\tmjd\tluminosity')
    data = []
    for specfile in specfiles:
        if 'P60' in specfile:
            continue
        mjd = extract_time(specfile)
        spec = table.Table.read(specfile, format='ascii')
        if 'col1' in spec.colnames:
            wlen = spec['col1']
            flux = spec['col2']
        else:
            wlen = spec['wavelength']
            flux = spec['flux']
        if wlen.min() > wlen_min:
            raise ValueError('Minimum Wavelength of supplied file %s does not extend far enough for wlen-min=%f (file has %f lower limit)' % (specfile, wlen_min, wlen.min()))
        if wlen.max() < wlen_max:
            raise ValueError('Maximum Wavelength of supplied file %s does not extend far enough for wlen-max=%f (file has %f upper limit)' % (specfile, wlen_max, wlen.max()))

        idx = np.logical_and(~np.isnan(flux), np.logical_and(wlen > wlen_min, wlen < wlen_max))
        
        lum = 4 * np.pi * dist**2 * np.trapz(flux[idx], wlen[idx])


        data.append((mjd, lum, specfile))
        
        #print('%s\t%f\t%e' % (specfile, mjd, lum))
    if qbol_format:
        t = table.Table(rows=data, names=('MJD', 'Luminosity', 'XSpecfile'))
        t.sort('MJD')
    else:
        t = table.Table(rows=data, names=('mjd', 'luminosity', 'specfile'))
        t.sort('mjd')

    if output:
        t.write(output, overwrite=overwrite)
    
    
    if plot:
        plt.figure(figsize=(10,6))
        if plot_dest:
            plt.rcParams.update({'font.size': 20})
        plt.plot(t['mjd'], np.log10(t['luminosity']))
        plt.xlabel('MJD [d]')
        plt.ylabel('log10 Luminosity [log erg/s/cm2]')
        plt.tight_layout()
        if plot_dest:
            plt.savefig(plot_dest)
        else:
            plt.show()

    
                 



if __name__ == '__main__':
    main()
