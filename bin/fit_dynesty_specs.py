#!/usr/bin/env python3
import click
import sys, os, functools, re, glob
import numpy as np
import astropy.units as u
from astropy.table import Table
import json as jsonlib
import numba
import tqdm
from scipy import optimize
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import astropy.time as time
import time as ostime
import h5py
import dynesty
from dynesty import plotting as dyplot
from dynesty import utils as dyfunc
import astropy.io.fits as fits
from astropy.cosmology import Planck15 as cosmo
import astropy.constants as aconst


try:
    import snelib19odp
except ImportError:
    sys.path.append('lib')
import snelib19odp.const as const
import snelib19odp.specsmooth as specsmooth

A_TO_CM = u.Angstrom.to(u.cm)

@numba.njit
def bb(temp, normalization, wavs):
    wavs_cm = wavs*A_TO_CM
    # cm^2 * erg / s
    flux_factor = 1.1910429526245744e-05
    # K * cm
    x_const = 1.4387773538277204
    pre_factor = flux_factor * normalization
    # cm^2 * erg / s / cm^5 = erg / s / cm^3
    B_lambda = pre_factor / wavs_cm**5 / (np.exp(x_const / wavs_cm / temp) - 1.0)
    # we want erg / s / cm^2 / A, so convert 1/cm to 1/A -> multiply by 1e-8
    # (1/u.cm).to(1/u.Angstrom) = 1e-8
    return B_lambda * 1e-8

@numba.njit
def likelihood(p, wave, obs_spec):
    temp, ln_radius = p

    sed = bb(temp, np.exp(ln_radius), wave)

    return -0.5 * np.nansum((sed - obs_spec)**2)
    #return -0.5 * np.nansum(np.abs(sed - obs_spec))


def dynesty_fit(spec, wlen_min, wlen_max, plot_fig, distance_factor):
    wave = spec['wavelength']
    flux = spec['flux'] * distance_factor
    mean_flux = np.nanmean(flux)
    flux /= mean_flux
    print('\tMean Flux: %e (log = %f)' % (mean_flux, np.log(mean_flux)))
    #print(flux)
    #print(bb(1e4, 1, np.linspace(1000, 9000)))
    idx = np.logical_and(wave > wlen_min, wave < wlen_max)

    args = (wave[idx], flux[idx])
    sampler = dynesty.NestedSampler(likelihood, prior_transform, 2, logl_args=args, nlive=4000)
    #sampler = dynesty.DynamicNestedSampler(likelihood, prior_transform, 2, logl_args=args)
    sampler.run_nested()
    results = sampler.results
    
    
    plot_fig(functools.partial(dyplot.cornerplot, results, labels=['Temp', 'ln R'], show_titles=True), 'corner')
    #plot_fig(functools.partial(dyplot.runplot, results), 'run')
    plot_fig(functools.partial(dyplot.traceplot, results, labels=['Temp', 'ln R'], show_titles=True), 'trace')

    
    samples, weights = results.samples, np.exp(results.logwt - results.logz[-1])
    mean, cov = dyfunc.mean_and_cov(samples, weights)
    print('\tMean: %s' % repr(mean))
    print('\n\t\t'.join(('\tCov: %s' % repr(cov)).split('\n')))

    new_samples = dyfunc.resample_equal(samples, weights)
    temp_q = np.quantile(new_samples[:,0], [0.05, 0.95])
    print('\tQuantiles T[K] : %s' % temp_q)
    norm_q = np.quantile(new_samples[:,1], [0.05, 0.95])
    print('\tQuantiles log N: %s' % norm_q)

    # calculate luminosity
    samples_temp = new_samples[:,0]
    samples_norm = new_samples[:,1] + np.log(mean_flux)
    samples_radius = np.sqrt(np.exp(samples_norm)/4/np.pi/np.pi)
    samples_l = 4*np.pi*(aconst.sigma_sb.cgs.value)*samples_radius**2 * samples_temp**4
    l_q = np.quantile(np.log10(samples_l), [0.05, 0.5, 0.95])

    print('\tQuantiles log L: %s' % l_q)

    def plot_spec():
        plt.plot(wave[idx], flux[idx], label='Observed')
        sed_mean = bb(mean[0], np.exp(mean[1]), wave[idx])
        plt.plot(wave[idx], sed_mean, label='Fit (mean)')
        sed_low =  bb(temp_q[0], np.exp(mean[1]), wave[idx])
        plt.plot(wave[idx], sed_low, label='Fit (low)', alpha=0.5)
        sed_high =  bb(temp_q[1], np.exp(mean[1]), wave[idx])
        plt.plot(wave[idx], sed_high, label='Fit (high)', alpha=0.5)
        for i in range(50):
            i = np.random.randint(10, new_samples.shape[0])
            sed = bb(new_samples[i,0], np.exp(new_samples[i,1]), wave[idx])
            plt.plot(wave[idx], sed, alpha=0.1, color='grey')
        plt.legend()
        plt.xlabel('Wavelength [A]')
        plt.ylabel('Flux')
    plot_fig(plot_spec, 'comparison')

    int_flux = np.trapz(flux[idx], wave[idx])
    sed = bb(mean[0], np.exp(mean[1]), wave[idx])
    int_gauss= np.trapz(sed, wave[idx])

    print('\tMatch: %f%%' % (int_gauss/int_flux*100))

    mean[1] += np.log(mean_flux)
    
    #return mean, cov, temp_q, results.logz[-1]
    return {
        'mean': mean,
        'cov': cov,
        'temp_q': temp_q,
        'logz': results.logz[-1],
        'int_flux_mismatch': int_gauss/int_flux,
        'norm_q': norm_q + np.log(mean_flux),
        'samples': new_samples,
        'lum_q': l_q
    }
    
    
@numba.njit
def prior_transform(u):
    x = np.empty(2)
    x[0] = 3e4*u[0] + 1000
    x[1] = 60*u[1] - 50
    return x



ASCII_META_RE = re.compile(r'\# (\S+)\: (\S+)')

REQUIRED_HEADERS = ['DATE-OBS']


def check_headers(meta):
    if 'DATE-OBS' not in meta:
        if 'UTC' in meta:
            meta['DATE-OBS'] = meta['UTC']
        elif 'UTSHUT' in meta:
            meta['DATE-OBS'] = meta['UTSHUT']
    for key in REQUIRED_HEADERS:
        if key not in meta:
            return False
    return True

def load_meta(fname, extra_meta):
    s_fname = os.path.basename(fname)
    fname = os.path.join('data', 'specs', 'preproc_marshall', s_fname)
    meta = {}
    with open(fname, 'r') as f:
        lines = f.readlines()
        for line in lines:
            if line[0] != '#':
                break
            m = ASCII_META_RE.match(line)
            if m:
                k,v = m.groups()
                meta[k] = v

    if not check_headers(meta):
        # try to read them from the fits file if it exists
        fits_fname = fname.replace('ascii', 'fits')
        if os.path.exists(fits_fname):
            fits_hdr = fits.getheader(fits_fname)
            for k in fits_hdr.keys():
                v = fits_hdr[k]
                meta[k] = v
        
    if not check_headers(meta):
        # try to read them from the extra metadata file
        short_fname = os.path.basename(fname)
        if short_fname in extra_meta['filename']:
            for k in extra_meta.colnames:
                if k == 'filename':
                    continue
                idx = extra_meta['filename'] == short_fname
                meta[k] = extra_meta[k][idx][0]
            

    if not check_headers(meta):
        sys.stderr.write('Still missing headers for %s\n' % fname)
        sys.stderr.flush()
        return None

    return meta


def resample(spec, target_dwlen):
    wlen_start = spec['wavelength'].min()
    wlen_stop = spec['wavelength'].max()
    new_wlen = np.arange(wlen_start, wlen_stop, target_dwlen)
    new_flux = specsmooth.spectres(new_wlen, spec['wavelength'], spec['flux'])
    return Table({'wavelength': new_wlen, 'flux': new_flux})


@click.command()
@click.option('--wlen-min', default=3500)
@click.option('--wlen-max', default=9000)
@click.option('--plot-dir', default='playground/bb-specs-dynesty')
@click.option('--resample-target', default=26)
@click.option('-o', '--out-file')
@click.option('--sample-file')
@click.argument('spec_dir')
def main(spec_dir, wlen_min, wlen_max, plot_dir, out_file, resample_target, sample_file):
    spec_files = glob.glob(os.path.join(spec_dir, '*.ascii'))
    specs = []

    run_id = str(int(ostime.time()))

    extra_meta = Table.read('data/specs_extra_metadata.txt', format='ascii')
    
    def plot_fig(fn, name, plot_dir):
        if not os.path.isdir(plot_dir):
            os.makedirs(plot_dir)
        plt.figure()
        fn()
        fname = os.path.join(plot_dir, '%s.png' % name)
        plt.savefig(fname)
        plt.close()
    
    for spec_fname in spec_files:
        spec = Table.read(spec_fname, format='ascii')

        # resample spectrum
        if resample_target != 0:
            spec = resample(spec, resample_target)

        # TODO: refactor the metadata loading
        meta = load_meta(spec_fname, extra_meta)
        if meta is None:
            continue

        # TODO: load the phase date from library
        phase = time.Time(meta['DATE-OBS']).jd - 2458735.8720
        
        specs.append((phase, spec, meta, os.path.basename(spec_fname)))
    specs = sorted(specs, key=lambda a: a[0])
    
    results = []

    # calculate distance factor
    dist = cosmo.luminosity_distance(const.redshift).to(u.cm)
    dist_cm = dist.value
    distance_factor = 4*np.pi*dist_cm**2
    
    for phase, spec, meta, fname in specs:
        date = meta['DATE-OBS']
        print('Fitting %s (phase=%f)' % (fname, phase))
        subdir = os.path.join(plot_dir, run_id, fname)
        
        result = dynesty_fit(spec, wlen_min, wlen_max, plot_fig=functools.partial(plot_fig, plot_dir=subdir), distance_factor=distance_factor)
        #results.append((phase, result, date, spec.meta['instrument']))
        results.append((phase, result, date, ''))
        #break

    cols = {
        'phase': [x[0] for x in results],
        'logz': [x[1]['logz'] for x in results],
        'temp_mean': [x[1]['mean'][0] for x in results],
        'temp_q5':  [x[1]['temp_q'][0] for x in results],
        'temp_q95':  [x[1]['temp_q'][1] for x in results],
        'norm_mean': [x[1]['mean'][1] for x in results],
        'norm_q5':  [x[1]['norm_q'][0] for x in results],
        'norm_q95':  [x[1]['norm_q'][1] for x in results],
        'log_lum_q5':  [x[1]['lum_q'][0] for x in results],
        'log_lum_q50':  [x[1]['lum_q'][1] for x in results],
        'log_lum_q95':  [x[1]['lum_q'][2] for x in results],
        
        'radius_mean': [np.sqrt(np.exp(x[1]['mean'][1])/4/np.pi/np.pi) for x in results],
        'radius_q5':  [np.sqrt(np.exp(x[1]['norm_q'][0])/4/np.pi/np.pi) for x in results],
        'radius_q95':  [np.sqrt(np.exp(x[1]['norm_q'][1])/4/np.pi/np.pi) for x in results],
        'date': [x[2] for x in results],
        'instrument': [x[3] for x in results],
        'int_flux_mismatch': [x[1]['int_flux_mismatch'] for x in results]
    }
    t = Table(cols)
    print(t)
    if out_file:
        t.write(out_file, overwrite=True)
    



if __name__ == '__main__':
    main()
