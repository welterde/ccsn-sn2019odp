#!/usr/bin/env python3
import click
import sys
import speclite
import speclite.filters
import numpy as np
import astropy.units as u
from astropy.table import Table
import json
from astropy.time import Time
import re
from datetime import datetime
import matplotlib.pyplot as plt


@click.command()
@click.option('--bands', type=str)
@click.option('--compare-telescopes', type=str)
@click.argument('infile')
def main(bands, compare_telescopes, infile):
    lc = Table.read(infile, format='ascii')

    if compare_telescopes:
        tel1, tel2 = compare_telescopes.split(',')
        
        times = lc['MJD']
        for band in bands:
            
            mag1 = lc['MAG_%s_%s' % (tel1, band)]
            mag2 = lc['MAG_%s_%s' % (tel2, band)]
            err1 = lc['MAGERR_%s_%s' % (tel1, band)]
            err2 = lc['MAGERR_%s_%s' % (tel2, band)]
            
            mag = mag1 - mag2
            err = np.sqrt(err1 + err2)
            plt.fill_between(times, mag - 3*err, mag + 3*err, label=band, alpha=0.4)
        plt.xlabel('Time MJD [d]')
        plt.ylabel('%s - %s [mag]' % (tel1, tel2))
        plt.legend()
    plt.show()



if __name__ == '__main__':
    main()
