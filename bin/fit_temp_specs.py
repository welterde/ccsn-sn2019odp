#!/usr/bin/env python3
import click
import sys
import numpy as np
import astropy.units as u
from astropy.table import Table
import json as jsonlib
import numba
import tqdm
from scipy import optimize
import matplotlib.pyplot as plt
import astropy.time as time
import h5py


A_TO_CM = u.Angstrom.to(u.cm)

@numba.njit
def bb(temp, phot_radius, wavs):
    wavs_cm = wavs*A_TO_CM
    flux_factor = 1.1910429526245744e-05
    x_const = 1.4387773538277204
    pre_factor = flux_factor * (phot_radius ** 2)
    return pre_factor / wavs_cm**5 / (np.exp(x_const / wavs_cm / temp) - 1.0)


@numba.njit
def likelihood(p, wave, obs_spec):
    temp, ln_radius = p

    sed = bb(temp, np.exp(ln_radius), wave)/1e20

    #return -0.5 * np.nansum((sed - obs_spec)**2)
    return -0.5 * np.nansum(np.abs(sed - obs_spec))


def evo_fit(spec, wlen_min, wlen_max):
    wave = spec['wavelength']
    flux = spec['flux']
    idx = np.logical_and(wave > wlen_min, wave < wlen_max)

    bounds = [
        (2000, 2e4),
        (np.log(1e-20), np.log(1e3))
    ]

    f = lambda p: -likelihood(p, wave[idx], flux[idx])
    result = optimize.differential_evolution(f, bounds=bounds)
    return result.x
    
    

@click.command()
@click.option('--wlen-min', default=4000)
@click.option('--wlen-max', default=9000)
@click.option('--plot', type=float)
@click.argument('sseq_file')
def main(sseq_file, wlen_min, wlen_max, plot):
    f = h5py.File(sseq_file, 'r')
    for x in f:
        for y in f[x]:
            spec = Table.read(sseq_file, format='hdf5', path='%s/%s' % (x,y))
            phase = time.Time(spec.meta.get('DATE-OBS', spec.meta['date'])).jd - 2458735.8720
            temp, ln_radius = evo_fit(spec, wlen_min=wlen_min, wlen_max=wlen_max)
            print('%f\t%f\t%f' % (phase, temp, ln_radius))
            if plot and np.abs(phase - plot) < 1.0:
                plt.plot(spec['wavelength'], spec['flux'], label='Observed')
                sed = bb(temp, np.exp(ln_radius), spec['wavelength'])/1e20
                plt.plot(spec['wavelength'], sed, label='Best Fit BB')
                plt.legend()
                plt.xlabel('Wavelength [A]')
                plt.ylabel('Flux')
                plt.show()



if __name__ == '__main__':
    main()
