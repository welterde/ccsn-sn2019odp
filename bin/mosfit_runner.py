#!/usr/bin/env python3
import os, sys, json, click, sexpdata, logging, time

try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../lib'))
import snelib19odp.dataset as dataset
import snelib19odp.utils as utils
import snelib19odp.mftools.sim as sim

import mosfit.fitter as fitter



ROOT_DIR = os.path.join(os.path.dirname(__file__), '..')

DEFAULT_PRODUCT_DIR = os.path.join(ROOT_DIR, 'products/mosfit_runs')


# TODO: maybe move to dedicated module


                


@click.command()
@click.option('--config-file', default=os.path.join(ROOT_DIR, 'config/mosfit.scm'))
@click.option('--products-dir')
@click.option('--mpi', is_flag=True)
@click.option('--debug', is_flag=True)
@click.argument('sim_name')
def main(config_file, sim_name, products_dir, mpi, debug):
    # setup logging
    if debug:
        logging.basicConfig(level=1)
    else:
        logging.basicConfig(level=logging.INFO)
    
    
    
    simulator = sim.get_sim(sim_name, config_file=config_file, products_dir=products_dir, mpi=mpi)
    simulator.run()



if __name__ == '__main__':
    main()




