#!/usr/bin/env python3
import astropy.table as table
import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u
import click

from astropy.cosmology import Planck15 as cosmo
import astropy.constants as const

from superbol.fbol import integrate_fqbol as fqbol_trapezoidal




# TODO: refactor that into a common location
EXTINCTION_COEFF = {
    'g': 3.303,
    'r': 2.285,
    'i': 1.698,
    'z': 1.263,
    'J': 0.723,
    'H': 0.460,
    'K': 0.310
}
ebv = 0.165


# (band, wavelength, AB zeropoint flux)
# taken from http://svo2.cab.inta-csic.es/svo/theory//fps3/index.php?id=LaSilla/GROND.J&&mode=browse&gname=LaSilla&gname2=GROND#filter
GROND_INS = [
    ('g', 4586.85, 5.36482e-9),
    ('r', 6220.09, 2.92735e-9),
    ('i', 7640.69, 1.88225e-9),
    ('z', 8989.58, 1.36524e-9),
    ('J', 12399.17, 7.25809e-10),
    ('H', 16468.80, 4.08192e-10),
    ('K', 21705.48, 2.34388e-10)
]
GROND_BANDS = [x[0] for x in GROND_INS]
GROND_CENT_WAVELENGTHS = [x[1] for x in GROND_INS]
GROND_ZERO_FLUXES = [x[2] for x in GROND_INS]



@click.command()
@click.option('--show', is_flag=True)
@click.option('--host-ebv', type=float, default=0)
@click.option('--overwrite', is_flag=True)
@click.argument('grond_lc')
@click.argument('dest_lc')
def main(grond_lc, dest_lc, show, host_ebv, overwrite):
    grond_lc = table.Table.read(grond_lc)

    for band in 'grizJHK':
        grond_lc['mag_%s' % band] -= EXTINCTION_COEFF[band] * (ebv+host_ebv)

    idx = grond_lc['magerr_g'] < 0.15
    grond_lc = grond_lc[idx]

    # extract data from LC
    mjd = grond_lc['mjd']
    fluxes = []
    flux_errs = []
    for i in range(len(grond_lc)):
        a = []
        e = []
        for band in GROND_BANDS:
            a.append(grond_lc['mag_%s' % band][i])
            e.append(grond_lc['magerr_%s' % band][i])
        # convert to fluxes
        fluxes.append(GROND_ZERO_FLUXES*10**(-0.4*np.array(a)))
        flux_errs.append(fluxes[-1] * np.abs(0.4*np.log(10) * (np.array(e)+0.05)))

    # now integrate each point
    fluxes_qbol = np.empty(len(grond_lc))
    fluxes_qbol_err = np.empty(len(grond_lc))
    for i in range(len(grond_lc)):
        #f_qbol = np.trapz(fluxes[i], GROND_CENT_WAVELENGTHS)
        f_qbol, f_qbol_err = fqbol_trapezoidal(GROND_CENT_WAVELENGTHS, fluxes[i], flux_errs[i])
        fluxes_qbol[i] = f_qbol
        fluxes_qbol_err[i] = f_qbol_err

    # load distance and convert to distance in cm
    redshift = table.Table.read('const/distance/current', format='ascii')

    dist = cosmo.luminosity_distance(redshift['redshift'][0]).to(u.cm)
    dist_cm = dist.value
    dist_pc = dist.to(u.pc).value
    dist_mod = 5*np.log10(dist_pc) - 5

    # convert to luminosity
    lums_qbol = 4*np.pi*dist_cm**2*fluxes_qbol
    lums_qbol_err = 4*np.pi * dist_cm**2 * fluxes_qbol_err

    # generate new table
    new_lc = table.Table([mjd, lums_qbol, lums_qbol_err], names=('MJD', 'Luminosity', 'Lums_Err'))
    if show:
        new_lc.pprint()
    new_lc.write(dest_lc, overwrite=overwrite)



if __name__ == '__main__':
    main()
