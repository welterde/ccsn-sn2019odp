#!/usr/bin/env python3
import os, sys, glob
import click
from astroquery.vizier import Vizier


Vizier.ROW_LIMIT = -1


@click.command()
@click.option('--overwrite', is_flag=True)
@click.argument('catalog')
@click.argument('destfile')
def main(catalog, destfile, overwrite):
    Vizier.ROW_LIMIT = -1
    x = Vizier(catalog=catalog, row_limit=-1)
    y = x.query_constraints()
    print(y)
    y[0].write(destfile, overwrite=overwrite)



if __name__ == '__main__':
    main()
