#!/usr/bin/env python3
import click, sys, os, re, json, glob
import astropy.table as table
import numpy as np

def load_json_phot(fname):
    with open(fname, 'r') as f:
        data = json.load(f)
    sne = data[list(data.keys())[0]]
    phot = sne['photometry']
    def check(x):
        if 'band' not in x:
            return False
        if 'e_magnitude' not in x:
            return False
        return True
    times = [float(x['time']) for x in phot if check(x)]
    mag = [float(x['magnitude']) for x in phot if check(x)]
    mag_err = [float(x['e_magnitude']) for x in phot if check(x)]
    band = [x['band'] for x in phot if check(x)]
    t = table.Table({'time': times, 'mag': mag, 'band': band, 'mag_err': mag_err})
    return t


@click.command()
@click.option('--require-class')
@click.option('--solo-class', is_flag=True)
@click.option('--require-sloan-bands')
@click.option('--require-legacy-bands')
@click.argument('cat_folders', nargs=-1)
def main(cat_folders, require_class, solo_class, require_sloan_bands, require_legacy_bands):
    for cat_dir in cat_folders:
        files = glob.glob('%s/*.json' % cat_dir)
        for path in files:
            with open(path, 'r') as f:
                data = json.load(f)
            sn_name = list(data.keys())[0]
            snd = data[sn_name]
            if 'claimedtype' not in snd:
                continue
            if 'photometry' not in snd:
                continue
            classes = [x['value'] for x in snd['claimedtype']]
            if require_class:
                if solo_class and len(classes) > 0:
                    continue
                if require_class not in classes:
                    continue
            # extract photometry
            phot = snd['photometry']
            def check(x):
                if 'band' not in x:
                    return False
                if 'e_magnitude' not in x:
                    return False
                if 'time' not in x:
                    return False
                try:
                    float(x['time'])
                except TypeError:
                    return False
                return True
            times = [float(x['time']) for x in phot if check(x)]
            mag = [float(x['magnitude']) for x in phot if check(x)]
            mag_err = [float(x['e_magnitude']) for x in phot if check(x)]
            band = [x['band'] for x in phot if check(x)]
            t = table.Table({'time': times, 'mag': mag, 'band': band})

            has_all_bands = True
            if not require_sloan_bands:
                require_sloan_bands = []
            for band in require_sloan_bands:
                idx1 = t['band'] == band
                idx2 = t['band'] == ("%s'" % band)
                idx = np.logical_or(idx1, idx2)
                if np.count_nonzero(idx) < 30:
                    has_all_bands = False
            if not require_legacy_bands:
                require_legacy_bands = []
            for band in require_legacy_bands:
                idx = t['band'] == band
                if np.count_nonzero(idx) < 30:
                    has_all_bands = False

            if not has_all_bands:
                continue

            print(sn_name, repr(classes), np.count_nonzero(idx))


if __name__ == '__main__':
    main()
