#!/usr/bin/env python3
import astropy.table as table
import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u
import click
import superbol

from superbol.fit_blackbody import bb_fit_parameters, bb_flux_nounits
from superbol.fbol import (ir_correction, uv_correction_blackbody,
                           uv_correction_linear)
from superbol.fbol import integrate_fqbol as fqbol_trapezoidal

from astropy.cosmology import Planck15 as cosmo
import astropy.constants as const

# TODO: refactor that into a common location
EXTINCTION_COEFF = {
    'g': 3.303,
    'r': 2.285,
    'i': 1.698,
    'z': 1.263,
    'J': 0.723,
    'H': 0.460,
    'K': 0.310
}
ebv = 0.165


# (band, wavelength, AB zeropoint flux)
# taken from http://svo2.cab.inta-csic.es/svo/theory//fps3/index.php?id=LaSilla/GROND.J&&mode=browse&gname=LaSilla&gname2=GROND#filter
GROND_INS = [
    ('g', 4586.85, 5.36482e-9),
    ('r', 6220.09, 2.92735e-9),
    ('i', 7640.69, 1.88225e-9),
    ('z', 8989.58, 1.36524e-9),
    ('J', 12399.17, 7.25809e-10),
    ('H', 16468.80, 4.08192e-10),
    ('K', 21705.48, 2.34388e-10)
]
GROND_BANDS = [x[0] for x in GROND_INS]
GROND_CENT_WAVELENGTHS = [x[1] for x in GROND_INS]
GROND_ZERO_FLUXES = [x[2] for x in GROND_INS]

# TODO: refactor all that into common location too





@click.command()
@click.option('--show', is_flag=True)
@click.option('--host-ebv', type=float, default=0)
@click.option('--overwrite', is_flag=True)
@click.option('--nir-correction-only', is_flag=True)
@click.argument('grond_lc')
@click.argument('dest_lc')
def main(grond_lc, dest_lc, show, host_ebv, overwrite, nir_correction_only):
    grond_lc = table.Table.read(grond_lc)
    
    for band in GROND_BANDS:
        grond_lc['mag_%s' % band] -= EXTINCTION_COEFF[band] * (ebv+host_ebv)

    idx = grond_lc['magerr_g'] < 0.08
    grond_lc = grond_lc[idx]

    mjd = grond_lc['mjd']

    # TODO: refactor that into utility function
    # load distance and convert to distance in cm
    redshift = table.Table.read('const/distance/current', format='ascii')

    dist = cosmo.luminosity_distance(redshift['redshift'][0]).to(u.cm)
    dist_cm = dist.value
    dist_pc = dist.to(u.pc).value
    dist_mod = 5*np.log10(dist_pc) - 5
    
    lums = []
    lum_errs = []
    for i in range(len(grond_lc)):
        a = []
        e = []
        for band in GROND_BANDS:
            a.append(grond_lc['mag_%s' % band][i])
            e.append(grond_lc['magerr_%s' % band][i])
        # convert to fluxes
        fluxes = GROND_ZERO_FLUXES*10**(-0.4*np.array(a))
        flux_errs = np.abs(fluxes * -0.4 * np.log(10) * (np.array(e)+0.05))
        
        # TODO: move all this stuff to utility module
        
        # do superbol integration
        f_qbol_sb, f_qbol_err = fqbol_trapezoidal(GROND_CENT_WAVELENGTHS, fluxes, flux_errs)

        # now do the superbol thing
        temperature, angular_radius, perr = bb_fit_parameters(GROND_CENT_WAVELENGTHS, fluxes, flux_errs)
        #temps[i] = temperature
        temperature_err = perr[0]
        angular_radius_err = perr[1]
        longest_wl = np.amax(GROND_CENT_WAVELENGTHS)
        shortest_wl = np.amin(GROND_CENT_WAVELENGTHS)
        shortest_flux = np.amin(fluxes)
        shortest_flux_err = np.amin(flux_errs)
        ir_corr, ir_corr_err = ir_correction(temperature, temperature_err, angular_radius, angular_radius_err, longest_wl)
        U_wl = GROND_CENT_WAVELENGTHS[0]
        U_flux = fluxes[0]
        if U_flux < bb_flux_nounits(U_wl, temperature, angular_radius):
            uv_corr, uv_corr_err = uv_correction_linear(
                shortest_wl, shortest_flux, shortest_flux_err)
        else:
            uv_corr, uv_corr_err = uv_correction_blackbody(
                temperature, temperature_err, angular_radius,
                angular_radius_err, shortest_wl)
        if nir_correction_only:
            lum_superbol = 4*np.pi*dist_cm**2*(f_qbol_sb + ir_corr)
            fbol_err = np.sqrt(np.sum(x * x for x in [f_qbol_err, ir_corr_err]))
        else:
            lum_superbol = 4*np.pi*dist_cm**2*(f_qbol_sb + ir_corr + uv_corr)
            fbol_err = np.sqrt(np.sum(x * x for x in [f_qbol_err, ir_corr_err, uv_corr_err]))

        lumerr_superbol = 4*np.pi*dist_cm**2 * fbol_err
        
        lums.append(lum_superbol)
        lum_errs.append(lumerr_superbol)

    # generate new table
    new_lc = table.Table([mjd, lums, lum_errs], names=('MJD', 'Luminosity', 'Lums_Err'))
    if show:
        new_lc.pprint()
    new_lc.write(dest_lc, overwrite=overwrite)
        
    

if __name__ == '__main__':
    main()
