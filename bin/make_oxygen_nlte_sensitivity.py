import numpy as np

if __name__ == '__main__':
    x = np.logspace(-2, 0, 30)
    for nlte in x:
        dest_file = "products/diags/oxygen_nlte_sensitivity_7774/%.4f.fits" % nlte
        print("python3 bin/run_oxygen_fit.py --transient SN2019odp products/spec_model/oxygen/keck_early/7774 --nlte-prior %f --additive-shell --print-results  --target-samples=123000 --nlive=9000 --dest-file %s" % (nlte, dest_file))

    
                    
