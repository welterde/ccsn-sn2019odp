import tqdm
import json
import glob
import h5py
import astropy.time as time
import numpy as np
import click
import os
from numpy.lib import recfunctions as rfn


def parse_date(x):
    try:
        return float(x)
    except:
        return time.Time(x.replace('/', '-')).mjd

EXTRA_ATTRS = [
    'claimedtype'
]
    
@click.command()
@click.option('--min-spectra', default=12)
@click.option('--dest-file')
@click.argument('cat_dirs', nargs=-1)
#@click.argument('dest_file')
def main(cat_dirs, dest_file, min_spectra):
    dest = h5py.File(dest_file, 'w')

    files = []
    for cat_dir in cat_dirs:
        files.extend(glob.glob(os.path.join(cat_dir, '*.json')))
    
    for fn in tqdm.tqdm(files):
        with open(fn, 'r') as f:
            dat = json.load(f)
            sne_name = list(dat.keys())[0]

            if 'spectra' not in dat[sne_name]:
                continue

            if len(dat[sne_name]['spectra']) < min_spectra:
                continue

            # extract maximum epoch
            #if not 'maxdate' in dat[sne_name]:
            #    continue

            if 'maxdate' in dat[sne_name]:
                max_mjd = parse_date(dat[sne_name]['maxdate'][0]['value'])
            else:
                #print('No maxdate')
                continue
            
            
            grp = dest.create_group(sne_name)

            for attr in EXTRA_ATTRS:
                if attr in dat[sne_name]:
                    v = dat[sne_name][attr][0]['value']
                    grp.attrs[attr] = v
            
            specs = dat[sne_name]['spectra']
            for spec in specs:
                if 'time' not in spec:
                    continue
                t = parse_date(spec['time'])
                phase = t - max_mjd

                idx = 0
                while '%d_%d' % (int(t), idx) in grp:
                    idx += 1
                ds_name = '%d_%d' % (int(t), idx)
                wave = np.array([float(x[0]) for x in spec['data']])
                flux = np.array([float(x[1]) for x in spec['data']])

                # ignore specs that are mostly UV or NIR
                if np.count_nonzero(np.logical_and(wave > 4000, wave < 9000)) < 100:
                    continue
                
                arr = np.vstack((wave, flux)).T
                dt = np.dtype([('wavelength', 'f4'), ('flux', 'f4')])
                arr2 = rfn.unstructured_to_structured(arr, dt)
                
                ds = grp.create_dataset(ds_name, data=arr2)
                ds.attrs['phase'] = phase
                
                
                


if __name__ == '__main__':
    main()



