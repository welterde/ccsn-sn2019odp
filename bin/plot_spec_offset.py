import os
import re
import numpy as np
#import matplotlib.pyplot as plt
from astropy.table import Table
import click
import spectres
import datetime
from scipy import interpolate
import matplotlib.cm as cm
from scipy.ndimage import gaussian_filter1d
import matplotlib.colors as colors
import spectres
import h5py
import astropy.time as time
import scipy.signal as signal

DATE_RE = re.compile(r'.+_(\d+)_.+')

@click.command()
@click.option('--wlen-center', type=float)
@click.option('--wlen-width', type=float)
@click.option('--mean-window', type=float, default=3.0)
@click.option('--redshift', type=float, default=0.0)
@click.option('--phase-stop', type=float, default=99999)
@click.option('-o', '--out-file')
@click.argument('sseq_file')
def main(sseq_file, redshift, wlen_center, wlen_width, phase_stop, mean_window, out_file):
    if out_file:
        import matplotlib
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import matplotlib.ticker as ticker
    if out_file:
        plt.rcParams.update({'font.size': 20})
    fig = plt.figure(figsize=(15,22))
    ax = fig.add_subplot(111)
    f = h5py.File(sseq_file, 'r')
    specs = []
    
    for x in f:
        for y in f[x]:
            spec = Table.read(sseq_file, format='hdf5', path='%s/%s' % (x,y))
            phase = time.Time(spec.meta.get('DATE-OBS', spec.meta['date'])).jd - 2458735.8720
            spec.meta['phase'] = phase
            spec.meta['jd'] = time.Time(spec.meta.get('DATE-OBS', spec.meta['date'])).jd
            specs.append(spec)
                
    specs = sorted(specs, key=lambda a: a.meta['phase'])

    last_phase = -9999
    spec_phases = []
    for spec in specs:
        phase = spec.meta['phase']
        if np.abs(last_phase - phase) < 3.0:
            continue
        last_phase = phase
        if phase > phase_stop:
            break
        wave = spec['wavelength']/(1+redshift)
        if wlen_center:
            idx = np.abs(wave - wlen_center) < wlen_width
            mean_idx = np.abs(wave - wlen_center) < wlen_width*mean_window
        else:
            idx = np.ones(len(wave), dtype=np.bool)
            mean_idx = idx
        flux = spec['flux'][idx]
        #if len(spec['flux'] > 200):
        #    flux -= signal.medfilt(spec['flux'], 101)[idx]
        #else:
        #    flux -= signal.medfilt(spec['flux'], 33)[idx]
        #print(np.nanmean(flux))
        #flux *= 1e15
        flux -= 0.9*np.nanmean(spec['flux'][mean_idx])
        #flux /= np.nanstd(spec['flux'][mean_idx])
        #flux /= np.nanmax(spec['flux'][mean_idx]) - np.nanmean(spec['flux'][mean_idx])
        print(np.nanmean(flux))
        #flux *= 1e15
        #flux = np.nanstd(flux))
        flux /= 0.5*np.nanstd(flux)
        flux *= -1
        ax.plot(wave[idx], flux+phase , color='black', alpha=0.5)
        #ax.plot(wave[idx]*1.01, phase*np.ones(len(wave[idx])), '--', color='grey', alpha=0.4)
        wave_end = np.nanmax(wave[idx])
        s = '%d' % phase
        if int(phase) >= 0:
            s = '+%d' % phase
        ax.text(x=wave_end+100, y=phase, s=s)
        spec_phases.append(phase)

    ax.yaxis.set_minor_locator(ticker.FixedLocator(spec_phases))
    ax.tick_params(which='minor', axis='y', direction='in', width=3, length=6)
    ax.tick_params(which='major', axis='y', direction='out', width=4, length=6)

    ax.set_ylabel('Phase [d]')
    ax.set_xlabel('Wavelength')
    ax.invert_yaxis()
    if out_file:
        fig.tight_layout()
        fig.savefig(out_file)
    else:
        plt.show()


                  
                    

if __name__ == '__main__':
    main()
