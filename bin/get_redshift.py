#!/usr/bin/env python3
import sys
import numpy as np
try:
    import snelib19odp
except ImportError:
    sys.path.append('lib')
import snelib19odp.const as const



def main(target_name):
    if target_name in ['sn2019odp', 'ztf19abqwtfu']:
        redshift = const.redshift
    elif '13bvn' in target_name:
        redshift = 0.00449
    else:
        raise ValueError('Unknown target "%s"' % target_name)
    print('%f' % redshift)



if __name__ == '__main__':
    main(sys.argv[1].lower())
