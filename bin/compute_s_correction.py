#!/usr/bin/env python3
import click
import sys
import speclite
import speclite.filters
import numpy as np
import astropy.units as u
from astropy.table import Table
import json as jsonlib
from astropy.time import Time
import re
from datetime import datetime
import pandas as pd
import matplotlib.pyplot as plt





# ZTF filters
def load_filter(inst, band):
    data = Table.read('const/filters/%s.%s.dat' % (inst, band), format='ascii')
    wave = []
    transmission = []
    for entry in data:
        if len(wave) > 0 and wave[-1] == entry['col1']:
            continue
        wave.append(entry['col1'])
        if entry['col2'] < 0.01 or entry['col1'] < 3790:
            transmission.append(0.0)
        else:
            transmission.append(entry['col2'])
    wave = np.array(wave)*u.Angstrom
    transmission = np.array(transmission)

    f = speclite.filters.FilterResponse(wavelength=wave, response=transmission, meta=dict(group_name='ZTF', band_name=band))
    #print(f)
    return f

FILE_RE = re.compile(r'^.*_(\d+)_.*$')


def extract_time(fname):
    m = FILE_RE.match(fname)
    dat = m.groups(1)[0]
    #print(dat)
    d2 = datetime.strptime(dat, '%Y%m%d')
    #print(d2)
    #print(Time(d2).mjd)
    return Time(d2).mjd


FILTERS = {
    'ZTF_g': load_filter('Palomar_ZTF', 'g'),
    'ZTF_r': load_filter('Palomar_ZTF', 'r'),
    'GROND_g': load_filter('LaSilla_GROND', 'g'),
    'GROND_r': load_filter('LaSilla_GROND', 'r'),
    'NOT_g': load_filter('NOT_SDSS', 'g'),
    'NOT_r': load_filter('NOT_SDSS', 'r'),
    'NOT_i': load_filter('NOT_SDSS', 'i'),
}


@click.command()
#@click.option('--json', is_flag=True)
@click.option('--plot')
@click.argument('input_files', nargs=-1)
def main(input_files, plot):
    times = []
    phot_system_a = []
    phot_system_b = []
    mag_system_a = []
    mag_system_b = []
    phot_filter = []
    for spec_fname in input_files:
        # 1) load spectrum
        spec = Table.read(spec_fname, format='ascii')
        wlen = spec['col1']*u.Angstrom
        flux = spec['col2']
        mjd = extract_time(spec_fname)

        for band in 'gr':
            side_a = 'ZTF'
            for side_b in ['GROND']:
                filter_a = '%s_%s' % (side_a, band)
                filter_b = '%s_%s' % (side_b, band)

                # bookeeping
                times.append(mjd)
                phot_system_a.append(side_a)
                phot_system_b.append(side_b)
                phot_filter.append(band)

                mag_a = FILTERS[filter_a].get_ab_magnitude(np.array(flux, copy=True), wlen)
                mag_b = FILTERS[filter_b].get_ab_magnitude(np.array(flux, copy=True), wlen)

                mag_system_a.append(mag_a)
                mag_system_b.append(mag_b)

    df = pd.DataFrame(data={
        'mjd': times,
        'filter': phot_filter,
        'phot_system_a': phot_system_a,
        'phot_system_b': phot_system_b,
        'mag_a': mag_system_a,
        'mag_b': mag_system_b
        })

    if plot is not None:
        plt.figure(figsize=(11,6))
        for band in 'gr':
            # TODO: subgroup by system_b
            df_c = df.query('filter == "%s"' % band)
            plt.plot(df_c['mjd'], df_c['mag_b'] - df_c['mag_a'], label='%s-band' % band)
        #plt.ylabel('%s-band %s - %s [mag]' % (band, 'GROND', 'ZTF'))
        plt.ylabel('GROND - ZTF [mag]')
        plt.xlabel('MJD [d]')
        plt.legend()
        plt.savefig(plot)
    
    print(df)

if __name__ == '__main__':
    main()

        
