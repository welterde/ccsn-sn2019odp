#!/usr/bin/env python3
import os, sys, json

import click
import numpy as np
import astropy.table as table

import gp3.ztf.ingest as ingest
import gp3.astrocat.dump as dump



TGT_NAME = 'SN2019odp'

@click.command()
@click.argument('input_file')
@click.argument('destfile')
def main(input_file, destfile):
    gen = dump.CatGen(transient_name=TGT_NAME)

    # load ZTF photometry
    df = ingest.ingest_growth_marshall_data(input_file)
    df = df.query('instrument == "ZTF"')
    # TODO: switch to the ztflc lc?
    # get rid of multi-index and convert them to columns
    df = df.reset_index()
    for idx, row in df.iterrows():
        mjd = row['mjd']
        band = row['filter'][1]
        instrument = row['instrument'] + '-Cam'
        telescope = row['telescope']
        mag = row['mag']
        mag_err = row['magerr']
        mag_system = 'AB'
        upper_lim_mag = row['mag_limiting']
        if np.isnan(mag):
            mag_err = None
            mag = upper_lim_mag
        gen.add_photometry_point(mjd=mjd, telescope=telescope, instrument=instrument, band=band, mag=mag, mag_err=mag_err, mag_system=mag_system)

    # load the UVOT photometry
    uvot = table.Table.read(os.path.join(os.path.dirname(__file__), '../data/uvot_phot.txt'), format='ascii')
    uvot['mjd'] = uvot['JD'] - 2400000.5
    for row in uvot:
        mjd = row['mjd']
        telescope = 'Swift'
        instrument = 'UVOT'
        band = row['FILTER']
        if row['AB_MAG'] < row['AB_MAG_LIM']:
            mag = row['AB_MAG']
            mag_err = row['AB_MAG_ERR']
        else:
            mag = row['AB_MAG_LIM']
            mag_err = None
        mag_system = 'AB'
        gen.add_photometry_point(mjd=mjd, telescope=telescope, instrument=instrument, band=band, mag=mag, mag_err=mag_err, mag_system=mag_system)

    # add GROND photometry
    grond = table.Table.read(os.path.join(os.path.dirname(__file__), '../data/grond_lc/current.fits'))
    idx = grond['magerr_g'] < 0.08
    for band in 'izJH':
        for row in grond[idx]:
            mjd = row['mjd']
            mag = row['mag_%s' % band]    
            mag_err = row['magerr_%s' % band]
            if band in 'JHK':
                mag_err += 0.05
            instrument = 'GROND'
            telescope = 'LaSilla'
            mag_system = 'AB'
            gen.add_photometry_point(mjd=mjd, telescope=telescope, instrument=instrument, band=band, mag=mag, mag_err=mag_err, mag_system=mag_system)

    # TODO: load redshift and error and input that into the file
    
    gen.dump_json(destfile)

    


if __name__ == '__main__':
    main()
