#!/usr/bin/env python
import re, json, os, glob, json
import click
import numpy as np
import astropy.table as table


PHASE_RE = re.compile(r'.*_([\-\d]+)\..*')

def extract(data_arr, data_quantiles, quantile):
    for d, q in zip(data_arr, data_quantiles):
        if q == quantile:
            return d
    raise ValueError('Quantile %f not found' % quantile)

@click.command()
@click.option('--phase', type=int)
@click.option('--print-tab', is_flag=True)
@click.argument('results_dir')
@click.argument('destfile')
def main(results_dir, destfile, phase, print_tab):
    obs_phase = phase
    result_files = glob.glob(os.path.join(results_dir, '*.json'))

    # load template offset table
    tmpl_offsets = table.Table.read("playground/mean_FeII5169_vabs_comb.dat", format='ascii')
    

    offsets = {}
    phases = []
    for row in tmpl_offsets:
        fname = row["template_name"]
        phase = int(PHASE_RE.match(fname).groups(1)[0])
        phases.append(phase)
        offsets[phase] = (row['template_vel'], row['mean_vel_err'])
    tmpl_offsets['PHASE'] = phases
    #print(tmpl_offsets)

    rows = []
    for fname in result_files:
        # extract phase
        phase = int(PHASE_RE.match(fname).groups(1)[0])

        # load data
        with open(fname, 'r') as f:
            data = json.load(f)
            #print(data)

        fit_blueshift = extract(data['blueshift'], data['quantiles'], 50)
        fit_broadening = extract(data['broadening'], data['quantiles'], 50)

        template_offset = offsets[phase][0]

        rows.append((phase, 1000*fit_blueshift+template_offset, 1000*fit_broadening))

    t = table.Table(rows=rows, names=('phase', 'velocity', 'broadening'))
    t.sort('phase')
    if print_tab:
        print(t)

    if obs_phase:
        idx = np.abs(t['phase'] - obs_phase) < 5
        print('\n\n\n')
        print('Velocity: %f km/s' % np.mean(t['velocity'][idx]))
        print('Uncertainty: %f km/s' % np.std(t['velocity'][idx]))
        


if __name__ == '__main__':
    main()
