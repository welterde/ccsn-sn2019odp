#!/usr/bin/env python3
import click, sys, os, functools, dynesty, numba, collections, json, logging
from dynesty import plotting as dyplot
from dynesty import utils as dyfunc
import numpy as np
import astropy.table as table
from multiprocessing import Pool
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import astropy.constants as aconst
import astropy.units as u
import sexpdata

try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../lib'))
import snelib19odp.specsmooth as specsmooth
import snelib19odp.absfit as absfit
import snelib19odp.specds as specds


C_KM_S = aconst.c.to(u.km/u.s).value

log = logging.getLogger('main')

def load_overrides(fname):
    with open(fname, 'r') as f:
        cfg = sexpdata.load(f)

    items = []
    for entry in cfg:
        assert len(entry) > 1
        assert isinstance(entry[0], str)
        items.append((entry[0], entry[1:]))
    return dict(items)


def apply_overrides(setup, overrides):
    kwargs = {}
    skip = False

    for rule in overrides:
        assert len(rule) > 0
        assert isinstance(rule[0], sexpdata.Symbol)
        cmd = rule[0].value()

        if cmd == 'nlive':
            kwargs['nlive'] = rule[1]
        elif cmd == 'skip':
            if len(rule) == 1:
                skip = True
            elif rule[1] == setup.line_center:
                skip = True
        elif cmd == 'dlogz-init':
            kwargs['dlogz_init'] = rule[1]
        elif cmd == 'enlarge':
            kwargs['enlarge'] = rule[1]
        elif cmd == 'method':
            kwargs['method'] = rule[1]
        elif cmd == 'walks':
            kwargs['walks'] = rule[1]
        elif cmd == 'update-interval':
            kwargs['update_interval'] = rule[1]
        

    return setup, kwargs, skip


def estimate_scalefactor(spec):
    idx = np.logical_and(spec['obswave'] > 3000, spec['obswave'] < 8000)
    spec_diff = np.diff(spec['flux'][idx])
    std = np.nanstd(spec_diff)/np.sqrt(2)
    return 1/std


@click.command()
@click.option('--debug', is_flag=True)
@click.option('--spec-dataset', default='SN2019odp')
@click.option('--line', default=5876)
@click.option('--wlen-min', default=-30000)
@click.option('--wlen-max', default=+3000)
@click.option('--fit-wlen-min', default=-20000)
@click.option('--fit-wlen-max', default=-800)
@click.option('--plot-dir', default='playground/abs-line-fit')
@click.option('--resample-target', default=26)
@click.option('--smp', default=1)
@click.option('--scalefactor', default=-1.0)
@click.option('--specname')
@click.option('--rev', default=1)
@click.option('--skip-existing', is_flag=True)
@click.option('--pcyg', is_flag=True)
@click.option('--timelimit', default=1800)
@click.option('--nlive', default=8000)
@click.option('--min-phase', default=-1000)
@click.option('--overrides')
def main(debug, spec_dataset, specname, wlen_min, wlen_max, fit_wlen_min, fit_wlen_max, plot_dir, line, resample_target, smp, scalefactor, rev, skip_existing, pcyg, timelimit, overrides, nlive, min_phase):
    if debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    
    sds = specds.load_dataset(spec_dataset)

    # load overrides if specified
    if overrides is not None:
        overrides = load_overrides(overrides)
    else:
        overrides = {}

    # convert the commandline specifications to velocities
    wlen_min = wlen_min / C_KM_S * line + line
    wlen_max = wlen_max / C_KM_S * line + line
    fit_wlen_min = fit_wlen_min / C_KM_S * line + line
    fit_wlen_max = fit_wlen_max / C_KM_S * line + line
    

    for spec in sds.specs:
        # filter 
        if specname is not None and spec.meta['name'] != specname:
            continue

        if spec.meta['phase'] < min_phase:
            continue

        # skip spectra that contain no pixels in the fitting region
        if np.count_nonzero(np.logical_and(spec['restwave'] > wlen_min, spec['restwave'] < wlen_max)) < 10:
            log.info('Skipping %s because contains fewer than 10 pixels in fitting window.', spec.meta['name'])
            continue
        
        plot_dir_full = os.path.join(plot_dir, spec_dataset + ('_%.0f' % line) , spec.meta['name'])
        destfile = sds.get_absfit_path(spec.meta['name'], fit_rev=rev, line=line)

        if skip_existing and os.path.isfile(destfile):
            log.info('Skipping %s because already present.', spec.meta['name'])
            continue

        # resample spectrum
        if resample_target != 0:
            spec = specsmooth.resample(spec, resample_target)

        # make sure target dests exist
        if not os.path.isdir(plot_dir_full):
            os.makedirs(plot_dir_full)
        if not os.path.isdir(os.path.dirname(destfile)):
            os.makedirs(os.path.dirname(destfile))
    
    
        def plot_fig(fn, name, autocreates_fig=False):
            if not autocreates_fig:
                fig = plt.figure()
                fn(fig=fig)
            else:
                fig, _axs = fn()
            fname = os.path.join(plot_dir_full, '%s.png' % name)
            fig.savefig(fname)
            plt.close()

        spec_scalefactor = scalefactor
        if scalefactor < 0:
            spec_scalefactor = estimate_scalefactor(spec)
            
        setup = absfit.AbsFitSetup(
            wlen_min=wlen_min,
            wlen_max=wlen_max,
            fit_wlen_min=fit_wlen_min,
            fit_wlen_max=fit_wlen_max,
            scalefactor=spec_scalefactor,
            line_center=line,
            pcyg=pcyg
        )

        # apply overrides
        setup, kwargs, skip = apply_overrides(setup, overrides.get(spec.meta['name'], []))
        if 'nlive' not in kwargs:
            kwargs['nlive'] = nlive

        if skip:
            log.info('Skipping spectrum %s', spec.meta['name'])
            continue

        log.info('Fitting spectrum %s', spec.meta['name'])

        result = absfit.nested_fit(spec, setup, plot_fig=plot_fig, smp=smp, timelimit=timelimit, **kwargs)
        result.meta['source_spec_dataset'] = spec_dataset
        result.meta['source_spec_name'] = spec.meta['name']
        result.meta['source_file_name'] = spec.meta['file_name']
        
        if not os.path.isdir(os.path.dirname(destfile)):
            os.makedirs(os.path.dirname(destfile))
        result.save(destfile, overwrite=True)
    
    
if __name__ == '__main__':
    main()
