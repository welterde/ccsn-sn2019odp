#!/usr/bin/env python
import pandas as pd
import click
import matplotlib
import numpy as np

import astropy.table as table
import astropy.units as u
import astropy.constants as const
from astropy.cosmology import Planck15 as cosmology

print('loading PTF12os..')
import snelib19odp.datasets.ptf12os as ptf12os
print('loading PTF13bvn..')
import snelib19odp.datasets.ptf13bvn as ptf13bvn
print('loading SN1993J..')
import snelib19odp.datasets.sn1993j as sn1993j
print('loading SN2008D..')
import snelib19odp.datasets.sn2008d as sn2008d

import snelib19odp.utils as utils
import snelib19odp.extinction as extinction

from scipy.optimize import differential_evolution


# TODO: move the 19odp parameters somewhere global
ODP_EBV = ('uniform', 0.1361, 0.1989)
ODP_REDSHIFT = ('gaussian', 0.014353, 0.000013)
ODP_HOST_EBV = ('uniform', 0, 0.2)


SAMPLE_NUM = 1000

TARGET_PEAK = -16.5

def load_sn19odp(band):
    print('loading SN19odp..')
    # load the raw interpolated dataset
    df = pd.read_hdf('products/lc_interpolated_per_ins.h5')
    # FIXME: load a completed bolometric LC

    return df.rename(columns={
        'mag_"%s"_ZTF' % band: 'mag',
        'magerr_"%s"_ZTF' % band: 'mag_err'
    })


def sample(spec, samples):
    if spec[0] == 'gaussian':
        return np.random.normal(spec[1], spec[2], size=samples)
    elif spec[0] == 'pos_gaussian':
        return np.abs(np.random.normal(spec[1], spec[2], size=samples))
    elif spec[0] == 'uniform':
        return np.random.uniform(spec[1], spec[2], size=samples)
    else:
        raise ValueError('Unknown distribution spec %s' % spec[0])




    
def fit_rise(mjd, mag, mag_err, time_range):
    # cut down to first $time_range days
    idx = mjd < mjd.min() + time_range
    mjd = mjd[idx]
    mag = mag[idx]
    mag_err = mag_err[idx]
    #print(mag)

    flux = 10**(mag/-2.5)

    def fitfunc(p, x=mjd):
        amp, t0, beta = p[0], p[1], p[2]
        return amp*(x-t0)**beta

    def scorefunc(p):
        f = fitfunc(p)
        d = (f-flux)**2
        #if np.count_nonzero(np.isnan(d)):
        #    return 1e3*np.count_nonzero(np.isnan(d))
        return np.nansum(d)

    bounds = [(1e-6, 1e9), (mjd.min()-20, mjd.min()-0.5), (0.7, 2)]

    result = differential_evolution(scorefunc, bounds)
    print(result)
    grid_mjd = np.linspace(mjd.min(), mjd.max())
    fit_flux = fitfunc(result.x, x=grid_mjd)
    fit_mag = -2.5*np.log10(fit_flux)
    t0 = result.x[1]
    return grid_mjd, fit_mag, t0

    
@click.command()
@click.option('-b', '--band', default='g')
@click.option('-o', '--output')
@click.argument('datasets', nargs=-1)
def main(output, datasets, band):
    if output:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    if output:
        plt.rcParams.update({'font.size': 20})
    fig = plt.figure(figsize=(15,9))
    ax = fig.add_subplot(111)

    data = []
    
    for dataset in datasets:
        dist_lum = None
        if dataset == 'PTF12os':
            df = ptf12os.lc_interp
            df.rename(inplace=True, columns={
                '%s_mag' % band: 'mag',
                '%s_mag_err' % band: 'mag_err'
                })
            ebv_mw = ptf12os.ebv_mw_dist
            ebv_host = ptf12os.ebv_host_dist
            redshift = ptf12os.redshift_dist
        elif dataset == 'PTF13bvn':
            df = ptf13bvn.lc_interp
            df.rename(inplace=True, columns={
                '%s_mag' % band: 'mag',
                '%s_mag_err' % band: 'mag_err'
                })
            ebv_mw = ptf13bvn.ebv_mw_dist
            ebv_host = ptf13bvn.ebv_host_dist
            redshift = ptf13bvn.redshift_dist
        elif dataset in ['SN2019odp', '19odp']:
            df = load_sn19odp(band)
            # skip first few points for now
            idx = df.index > df.index.min()+2
            df = df[idx]
            redshift = ODP_REDSHIFT
            ebv_mw = ODP_EBV
            ebv_host = ODP_HOST_EBV
        elif dataset == 'SN1993J':
            df = sn1993j.r_lc.rename(columns={
                'r_mag': 'mag',
                'r_mag_err': 'mag_err'
            })
            ebv_mw = sn1993j.ebv_mw_dist
            ebv_host = sn1993j.ebv_host_dist
            dist_lum = sn1993j.distance_modulus_dist
            redshift = None
        elif dataset == 'SN2008D':
            df = sn2008d.r_lc.rename(columns={
                'r_mag': 'mag',
                'r_mag_err': 'mag_err'
            })
            ebv_mw = sn2008d.ebv_mw_dist
            ebv_host = sn2008d.ebv_host_dist
            redshift = sn2008d.redshift_dist
        else:
            raise ValueError('Unknwon dataset %s' % dataset)

        # calc distance modulus
        if redshift:
            redshift_dist = sample(redshift, SAMPLE_NUM)
            dist = cosmology.luminosity_distance(redshift_dist).to(u.pc).value
            dist_mod = 5*np.log10(dist) - 5
        elif dist_lum:
            dist_mod = sample(dist_lum, SAMPLE_NUM)
        else:
            raise ValueError('Need either redshift of distance modulus')

        
        dist_std = np.std(dist_mod)
        dist_mean = np.mean(dist_mod)
        mjd = df.index
        mag = df['mag'] - dist_mean
        mag_err = df['mag_err'] + dist_std

        diff_peak = mag.min() - TARGET_PEAK
        
        fit_mjd, fit_mag, t0 = fit_rise(mjd, mag, mag_err, 10)

        idx = mjd < mjd.min() + 20
        #ax.plot((mjd - t0)[idx], mag[idx] - diff_peak, label=dataset)
        ax.errorbar((mjd - t0)[idx], mag[idx], yerr=mag_err[idx], fmt='o', label=dataset)
        #ax.plot(fit_mjd - t0, fit_mag - diff_peak, label=(dataset + ' Fit'))
        ax.plot(fit_mjd - t0, fit_mag, label=(dataset + ' Fit'))
        print(fit_mjd)
        print(fit_mag)
        
    ax.legend()
    ax.invert_yaxis()
    fig.tight_layout()
    
    if output:
        fig.savefig(output)
    else:
        plt.show()



if __name__ == '__main__':
    main()
