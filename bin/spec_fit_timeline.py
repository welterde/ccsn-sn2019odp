#!/usr/bin/env python3
import click, h5py, numba, collections, tqdm, dynesty
import numpy as np

import astropy.time as time
from dynesty import plotting as dyplot
import matplotlib.pyplot as plt

# of 19odp
MAX_EPOCH_MJD = time.Time('2019-09-09').mjd





def fit_spec_curve(observed_phase, template_phase, metrics):
    
    @numba.njit
    def likelihood(p, observed_phase, template_phase, metrics):
        log_stretch, offset = p

        adj_phase = np.exp(log_stretch)*template_phase + offset

        found_specs = 0
        result_l = 0
        for i in range(len(observed_phase)):
            # now find the closest match for each observed spectrum
            diff = np.abs(adj_phase - observed_phase[i])
            min_idx = diff.argmin()

            if diff[min_idx] > 30:
                continue

            m = metrics[i, min_idx]
            if np.isnan(m):
                continue
            
            found_specs += 1
            result_l += m

        result_l += np.log(found_specs)
        if found_specs < 3:
            return -np.inf
        return result_l

    @numba.njit
    def prior_transform(p):
        x = np.empty(2)
        x[0] = 4*p[0]-2
        x[1] = 40*p[1]-20
        return x

    args = (observed_phase, template_phase, metrics)
    sampler = dynesty.NestedSampler(likelihood, prior_transform, 2, logl_args=args, nlive=32000)
    #sampler = dynesty.DynamicNestedSampler(likelihood, prior_transform, 2, logl_args=args)
    sampler.run_nested()
    results = sampler.results
    print(results)

    fig, axes = dyplot.traceplot(results, show_titles=True)
    #dyplot.cornerplot(results, show_titles=True)
    plt.show()
        

#@numba.njit
#def calc_




@click.command()
@click.option('--metric', default='snid_highpass')
@click.argument('template_lib')
@click.argument('corr_lib')
@click.argument('sne_name')
def main(template_lib, corr_lib, sne_name, metric):
    templates = h5py.File(template_lib, 'r')
    corr = h5py.File(corr_lib, 'r')

    # load the observed stuff..
    entries = []
    for grp_name in corr:
        grp = corr[grp_name]
        for ds_name in grp:
            ds = grp[ds_name]
            if 'DATE-OBS' not in ds.attrs:
                continue
            entries.append(ds)

    obs_times = np.array([time.Time(ds.attrs['DATE-OBS']).mjd for ds in entries])
    obs_phase = obs_times - MAX_EPOCH_MJD

    # now find full range of phase
    template_phase = []
    template_sn = templates[sne_name]
    for ds_name in template_sn:
        ds = template_sn[ds_name]
        template_phase.append(ds.attrs['phase'])
    template_phase = np.array(template_phase)


    # compute metric matrix
    metrics = np.ones((len(obs_phase), len(template_phase)))*np.nan
    for i in range(len(obs_phase)):
        entry = entries[i]
        for j in range(len(template_phase)):
            idx = entry['template_sn'] == sne_name.encode('utf-8')
            idx = np.logical_and(idx, entry['phase'] == template_phase[j])
            #assert np.count_nonzero(idx) <= 1
            if np.count_nonzero(idx) == 1:
                m = entry['%s_likelihood' % metric][idx] + np.log(entry['%s_overlap' % metric][idx])
                #print(m)
                metrics[i,j] = m

    fit_spec_curve(obs_phase, template_phase, metrics)

if __name__ == '__main__':
    main()

