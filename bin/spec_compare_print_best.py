#!/usr/bin/env python3
import click, h5py, numba, collections, tqdm, dynesty, pandas
import numpy as np

import astropy.table as table
import astropy.time as time
from dynesty import plotting as dyplot
import matplotlib.pyplot as plt

# of 19odp
MAX_EPOCH_MJD = time.Time('2019-09-09').mjd

@click.command()
@click.option('--metric', default='snid_highpass')
#@click.argument('template_lib')
@click.argument('corr_lib')
#@click.argument('sne_name')
def main(corr_lib, metric):
    #templates = h5py.File(template_lib, 'r')
    corr = h5py.File(corr_lib, 'r')

    # load the observed stuff..
    entries = []
    for grp_name in corr:
        grp = corr[grp_name]
        for ds_name in grp:
            ds = grp[ds_name]
            if 'DATE-OBS' not in ds.attrs:
                continue
            entries.append(ds)

    obs_times = np.array([time.Time(ds.attrs['DATE-OBS']).mjd for ds in entries])
    obs_phase = obs_times - MAX_EPOCH_MJD

    #sn_types = np.unique([a for a in entries[0]['sn_type']
    rows = []
    for phase, entry in zip(obs_phase, entries):
        for sn_type in np.unique(entry['sn_type']):
            idx = entry['sn_type'] == sn_type
            score = entry['%s_likelihood' % metric] + 0.5*np.log(entry['%s_overlap' % metric])
            best_idx = np.arange(len(entry['template_sn']))[idx][np.argmax(score[idx])]

            #print('%e\t%s\t%e\t%s' % (phase, sn_type.decode('utf-8'), score[best_idx], entry['template_sn'][best_idx].decode('utf-8')))
            rows.append((phase, sn_type.decode('utf-8'), score[best_idx], entry['template_sn'][best_idx].decode('utf-8')))

    t = table.Table(rows=rows, names=('phase', 'sn_type', 'score', 'sn_name'))
    t.sort(['phase', 'score'])
    t.pprint_all()
    
if __name__ == '__main__':
    main()

