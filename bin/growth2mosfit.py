#!/usr/bin/env python3
import gp3.ztf.ingest as ingest
import gp3.astrocat.dump as dump
import json
import click
import sys

TGT_NAME = 'SN2019odp'

@click.command()
@click.argument('input_file')
def main(input_file):
    df = ingest.ingest_growth_marshall_data(input_file)
    df = df.query('instrument == "ZTF-Cam"')
    #print(df)
    model = dump.convert_mosfit(df, target_name=TGT_NAME, mag_column='mag', magerr_column='magerr', upper_lim_mag_column='mag_limiting')
    json.dump(model, sys.stdout)

    


if __name__ == '__main__':
    main()
