#!/usr/bin/env python3
import click
import sys, glob, re
import speclite
import speclite.filters
import numpy as np
import astropy.units as u
from astropy.table import Table
from astropy.time import Time
from datetime import datetime
import matplotlib.pyplot as plt

FILE_RE = re.compile(r'^.*_(\d+)_.*$')



def extract_time(entry):
    m = FILE_RE.match(entry['fname'])
    dat = m.groups(1)[0]
    #print(dat)
    d2 = datetime.strptime(dat, '%Y%m%d')
    #print(d2)
    #print(Time(d2).mjd)
    return Time(d2).mjd


def main():
    spec_files = glob.glob('data/specs/preproc_marshall/*.ascii')

    for fname in filter(lambda a: 'P60' not in a, spec_files):
        print(fname)
    



if __name__ == '__main__':
    main()
