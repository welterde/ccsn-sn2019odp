#!/usr/bin/env python3
import click, h5py, numba, collections, tqdm, dynesty
import numpy as np

import astropy.table as table
import astropy.time as time
from dynesty import plotting as dyplot
import matplotlib.pyplot as plt

# of 19odp
MAX_EPOCH_MJD = time.Time('2019-09-09').mjd


@click.command()
@click.option('--metric', default='snid_highpass')
@click.argument('template_lib')
@click.argument('corr_lib')
#@click.argument('sne_name')
def main(template_lib, corr_lib, metric):
    templates = h5py.File(template_lib, 'r')
    corr = h5py.File(corr_lib, 'r')

    # load the observed stuff..
    entries = []
    for grp_name in corr:
        grp = corr[grp_name]
        for ds_name in grp:
            ds = grp[ds_name]
            if 'DATE-OBS' not in ds.attrs:
                continue
            entries.append(ds)

    obs_times = np.array([time.Time(ds.attrs['DATE-OBS']).mjd for ds in entries])
    obs_phase = obs_times - MAX_EPOCH_MJD

    # sne_name -> [best_likelihood]
    results = {}

    # sne_name -> type
    sne_type = {}

    rows = []
    
    for sne_name in templates:
        template_sn = templates[sne_name]
        sne_type[sne_name] = template_sn.attrs['claimedtype']

        best_ls = np.empty(len(entries))
        for i, entry in enumerate(entries):
            idx = entry['template_sn'] == sne_name.encode('utf-8')
            if np.count_nonzero(idx) < 1:
                continue
            # find the best match
            #score = entry['%s_likelihood' % metric] + np.log(entry['%s_overlap' % metric])
            #score = entry['%s_likelihood' % metric] + entry['%s_overlap' % metric]
            score = entry['%s_likelihood' % metric] + 0.5*np.sqrt(entry['%s_overlap' % metric])
            best_idx = np.arange(len(entry['template_sn']))[idx][np.argmax(score[idx])]
            #best_ls[i] = entry['%s_likelihood' % metric][best_idx]
            best_ls[i] = score[best_idx]

        total_l = np.sum(best_ls)
        #print('%s\t%e\t%s' % (sne_name, total_l, template_sn.attrs['claimedtype']))
        rows.append((sne_name, total_l, template_sn.attrs['claimedtype']))

    t = table.Table(rows=rows, names=('supernova', 'total_likelihood', 'type'))
    t.sort('total_likelihood')
    #idx = t['type'] == 'Ib'
    #print(np.count_nonzero(idx))
    #print(t[idx])
    t.pprint_all()

if __name__ == '__main__':
    main()

