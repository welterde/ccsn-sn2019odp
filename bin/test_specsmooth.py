#import snelib19odp.specsmooth as specsmooth
import astropy.table as table
import sys


import numba
import numpy as np
import matplotlib.pyplot as plt

import scipy.optimize as optimize
from scipy.interpolate import interp1d
from scipy.interpolate import splev, splrep

import astropy.units as units
import astropy.constants as const




SPLINE_MEAN_KNOTNUM = 17


#@numba.jit
def meanzero(flux):
    xknot = np.empty(len(flux))
    yknot = np.empty(len(flux))

    nw = len(flux)
    
    idx_nonzero = flux != 0.0
    assert np.count_nonzero(idx_nonzero) > 0
    l1 = np.min(np.arange(nw)[idx_nonzero])
    l2 = np.max(np.arange(nw)[idx_nonzero])
    assert (l2-l1) >= 3*SPLINE_MEAN_KNOTNUM

    # choose knots for spline
    nknot = 0
    kwidth = int(nw / SPLINE_MEAN_KNOTNUM)
    nave = 0
    wave = 0
    fave = 0
    istart = 0

    print('Range: %d %d' % (l1, l2))
    print('kwidth: %s' % repr(kwidth))
    print('nw: %d' % nw)

    for i in range(nw):
        if i > l1 and i < l2:
            nave = nave + 1
            wave = wave + i-0.5
            fave = fave + flux[i]
        if (i-istart) % kwidth == 0:
            print('nave: %s fave: %s' % (repr(nave), repr(fave)))
            if nave > 0 and fave > 0:
                print(' -> knot')
                xknot[nknot] = wave / nave
                yknot[nknot] = np.log10(fave/nave)
                nknot = nknot + 1
            nave = 0
            wave = 0
            fave = 0

    assert nknot != 0

    xknot = xknot[0:nknot]
    yknot = yknot[0:nknot]
    print('nknot: ', nknot)
    #print(xknot)
    #print(len(xknot))

    spline = splrep(xknot, yknot)

    flux_norm = np.zeros(nw)
    #if nknot % 2 == 0:
    #    interpolator = interp1d(xknot, yknot, kind=nknot-1)
    #else:
    #    interpolator = interp1d(xknot, yknot, kind=nknot)
    for i in range(l1, l2):
        #logspl = interpolator(i-0.5)
        logspl = splev(i-0.5, spline)
        flux_norm[i] = flux[i] / 10**logspl - 1.0
    #plt.plot(flux_norm[l1:l2])
    #plt.show()
    return flux_norm, (l1,l2)

#@numba.njit
def snidbin(wlen, flux, nw, wlen0, dwlen_log, fnu=False):
    #wlen_bin = wlen0 * np.exp(np.arange(nw+1)*dwlen_log)
    #dwlen_bin = np.diff(wlen_bin)
    # mangle centers
    #for i in range(nw):
    #    wlen_bin[i] = 0.5*(wlen_bin[i]+wlen_bin[i+1])
    #wlen_bin = wlen_bin[0:(nw-2)]

    flux_bin = np.zeros(nw)

    for wlen_i in range(len(wlen)):
        if wlen_i == 0:
            s0 = 0.5*(3*wlen[wlen_i]-wlen[wlen_i+1])
            s1 = 0.5*(wlen[wlen_i]+wlen[wlen_i+1])
        elif wlen_i == len(wlen)-1:
            s0 = 0.5*(wlen[wlen_i-1]+wlen[wlen_i])
            s1 = 0.5*(3*wlen[wlen_i]-wlen[wlen_i-1])
        else:
            s0 = 0.5*(wlen[wlen_i-1]+wlen[wlen_i])
            s1 = 0.5*(wlen[wlen_i]+wlen[wlen_i+1])

        s0log = np.log(s0/wlen0)/dwlen_log
        s1log = np.log(s1/wlen0)/dwlen_log

        print('log s0 = %f / log s1 = %f' % (s0log, s1log))

        if fnu:
            dw = (s1-s0)*2.99793e10/(wlen[wlen_i]*wlen[wlen_i]*1e-8)
        else:
            dw = s1-s0

        #print('dw = %e' % dw)
        
        for bin_i in range(int(s0log), int(s1log)+1):
            if bin_i >= 0 and bin_i < nw:
                alen = min([s1log, bin_i+1]) - max([s0log, bin_i])
                #print('alen = ', alen)
                #print('input flux = ', flux[wlen_i])
                flux_bin[bin_i] += flux[wlen_i] * alen/(s1log-s0log) * dw
                #print('%d -> %e' % (bin_i, flux_bin[bin_i]))

    #print(flux_bin*1e20)
    print('Binned flux array size: %d' % len(flux_bin))

    if True:
        wlen_bin = wlen0 * np.exp(np.arange(nw)*dwlen_log)
        plt.plot(wlen_bin, flux_bin)
        plt.show()
    return flux_bin


def snidflat(wlen, flux, redshift, wlen_min, wlen_max, do_filter=True, do_plot=False):
    # wavelength bins (num, start, stop)
    nw = 1024
    w0 = 2500
    w1 = 10000

    # dwlog = np.log(w1/w0)/nw
    # wlen_log = w0*np.exp(np.arange(nw)*dwlog)
    # for i in range(nw-1):
    #     # half-wavelength-bins? snidbins??
    #     wlen_log[i] = 0.5*(wlen_log[i]+wlen_log[i+1])
    # wlen_log = wlen_log[:nw-1]
    # binsize = wlen_log[-1]-wlen_log[-2]
    # #wlog = np.array([0.5*(wlog[i]+wlog[i+1]) for i in range(nw-1)])

    # # de-redshift input spectrum
    wlen_z = wlen/(1+redshift)
    
    # # rebin onto log wavelength scale
    # 
    # log_flux_bin = snidbin(wlen_z[idx], flux[idx], wlen_log.min(), wlen_log.max(), binsize)

    dwlog = np.log(w1/w0)/nw
    wlen_log = w0*np.exp(np.arange(nw+1)*dwlog)
    
    idx = np.logical_and(wlen > wlen_min, wlen < wlen_max)
    flux_bin = snidbin(wlen_z[idx], flux[idx], nw, w0, dwlog, fnu=True)
    
    
    for i in range(nw):
        # half-wavelength-bins? snidbins??
        wlen_log[i] = 0.5*(wlen_log[i]+wlen_log[i+1])
    wlen_log = wlen_log[:nw]

    print('Len flux_bin = %d' % len(flux_bin))
    flux_bin_norm, (l1,l2) = meanzero(flux_bin)
    print('Len flux_bin_norm = %d' % len(flux_bin_norm))

    if do_filter:
        flux_bin_norm[l1:l2] = snidfilt(flux_bin_norm, k1=1, k2=4, k3=nw/12, k4=nw/10)[l1:l2]

    print(flux_bin_norm)
        
    #print('wlen_flat: ', len(wlen_flat), ' flux_bin_norm ', len(flux_bin_norm))
    flux_flat = np.interp(wlen_z, wlen_log, flux_bin_norm)

    # this apodize thing from snid
    if True:
        ftmp = np.array(flux_flat, copy=True)
        nsquash = min([nw*0.05, float((l2-l1)/2)])
        if nsquash >= 1:
            for i in range(int(nsquash)):
                arg = np.pi * i / (nsquash - 1)
                factor = 0.5 * (1 - np.cos(arg))
                ftmp[l1+i] = factor * flux_flat[l1+i]
                ftmp[l2-i] = factor * flux_flat[l2-i]
        flux_flat = ftmp
    
    # construct valid mask
    tmp_idx = np.arange(len(flux_flat))
    idx = np.logical_and(tmp_idx > l1, tmp_idx < l2)
    
    if do_plot:
        #plt.plot(wlen, flux_flat*1e58)
        plt.plot(wavelength, flux_flat)
        #plt.plot(wlen, flux)
        plt.show()
        
    return flux_flat, idx, wlen_z
    
#@numba.jit
def snidfilt(flux_bin, k1, k2, k3, k4):
    n = len(flux_bin)
    print('n = ', n)
    xft = np.fft.fft(flux_bin)
    print(xft)
    print(xft.shape)
    for j in range(1,n):
        if j < n/2 + 1:
            num = j - 1
        else:
            num = j - n -1
        num_abs = np.abs(num)
        if num_abs < k1 or num_abs > k4:
            factor = 0.0
        elif num_abs < k2:
            arg = np.pi * float(num_abs - k1)/float(k2-k1)
            factor = 0.5 * (1-np.cos(arg))
        elif num_abs > k3:
            arg = np.pi * float(num_abs - k3)/float(k4-k3)
            factor = 0.5 * (1+np.cos(arg))
        else:
            factor = 1.0
        xft[j-1] = xft[j-1] * factor

    flux_filtered = np.fft.ifft(xft)
    return np.real(flux_filtered)




# port from https://github.com/nyusngroup/SESNspectraLib/blob/master/binspec.pro
@numba.njit
def binspec(wavelength, flux, wlen_start, wlen_end, wlen_bin):
    length = int((wlen_end - wlen_start)/wlen_bin) + 1
    out_wlen = np.arange(length)*wlen_bin + wlen_start
    out = np.empty(length, dtype=np.float64)

    interp_wlen = np.concatenate((wavelength, out_wlen))
    interp_wlen.sort()
    interp_wlen = np.unique(interp_wlen)

    interp_flux = np.interp(interp_wlen, wavelength, flux)

    for i in range(length):
        # XXX: off by one error here?
        w = np.logical_and(interp_wlen >= out_wlen[i], interp_wlen <= out_wlen[i+1])
        if np.count_nonzero(w) == 2:
            out[i] = 0.5*np.sum(interp_flux[w])*wlen_bin
        else:
            out[i] = np.trapz(interp_flux[w], interp_wlen[w])

    out[-1] = out[-2]

    out[np.logical_or(out_wlen > wavelength.max(), out_wlen < wavelength.min())] = 0.0
    return out_wlen, out/wlen_bin

    


C_KMS = const.c.to(units.km/units.s).value

MAX_SN_VELOCITY = 1e5






def specFFTsmooth(wavelength, flux, min_velocity, stddev_width=100):
    wlen_log = np.log(wavelength)

    num = len(wlen_log)
    binsize = wlen_log[-1]-wlen_log[-2]
    #print('Binsize: ', binsize)
    
    
    #flux /= np.mean(flux)
    wlen_bin, flux_bin = binspec(wlen_log, flux, wlen_log.min(), wlen_log.max(), binsize)
    num_bin = len(wlen_bin)
    #print(np.diff(wlen_bin))

    #print(flux_bin)
    #flux_bin_ft = np.fft.fft(flux_bin)
    flux_bin_ft = np.fft.fft(flux_bin)*num_bin
    #print(flux_bin_ft)

    #freq_idx = np.arange(int((num_bin-1)/2)+1)
    #if num_bin % 2 == 0:
    #    freq = np.concatenate((freq_idx, np.array([num_bin/2]), -num_bin/2 + freq_idx))/num_bin
    #else:
    #    freq = np.concatenate((freq_idx, -(num_bin/2 + 1) + freq_idx))/num_bin
    freq = np.fft.fftfreq(num_bin)
    
    # find the range indexes that are min_velocity < velocity < MAX_SN_VELOCITY in a really stupid way
    idx_arr = np.arange(len(freq))
    num_upper = np.max(idx_arr[1:(num_bin-1)][1.0/freq[1:(num_bin-1)] * C_KMS * binsize >= min_velocity])
    num_lower = np.max(idx_arr[1:(num_bin-1)][1.0/freq[1:(num_bin-1)] * C_KMS * binsize >= MAX_SN_VELOCITY])

    snid_k2 = 1/MAX_SN_VELOCITY*C_KMS*binsize
    print('SNID k2 = %f' % snid_k2)
    
    #print('Found %d %d' % (num_upper, num_lower))
    #print(idx_arr[1:(num_bin-1)][1.0/freq[1:(num_bin-1)] * C_KMS * binsize >= MAX_SN_VELOCITY])
    #print(1/freq[num_lower:num_upper]*C_KMS*binsize)

    # calculate average signal for the valid velocity region
    signal_avg = np.mean(np.abs(flux_bin_ft[num_lower:num_upper]))

    # fit power law to mags in valid region
    g = np.polyfit(np.log10(freq[num_lower:num_upper]), np.log10(flux_bin_ft[num_lower:num_upper]), 1)
    print(g)

    p0 = [10e1**(-g[0]/g[1]), g[1]]
    def func(x, a, b):
        return (x/a)**b
    print(p0)
    p,pcov = optimize.curve_fit(func, freq[num_lower:int(num_bin/2)], np.abs(flux_bin_ft[num_lower:int(num_bin/2)]))
    print(p)

    delta = (freq[1:int(num_bin/2)]/p[0])**p[1] - signal_avg

    #plt.plot(freq[1:int(num_bin/2)], np.abs(flux_bin_ft[1:int(num_bin/2)]))
    #plt.plot(freq[1:int(num_bin/2)], (freq[1:int(num_bin/2)]/p[0])**p[1])
    #plt.axhline(np.abs(signal_avg))
    #plt.show()

    print('Delta > 0 Count: %d' % np.count_nonzero(delta <= 0))
    
    # stupid way of doing things
    num_sep = np.min(idx_arr[1:int(num_bin/2)][delta <= 0])

    sep_vel = 1.0/freq[num_sep] * C_KMS * binsize
    print(sep_vel)

    flux_bin_ft_smooth = flux_bin_ft/num_bin
    i = 1
    #flux_bin_ft_smooth[0] = 0.0
    while i < num_bin:
        if i < num_bin/2+1:
            number = i-1
        else:
            number = i - num_bin - 1
        numa = abs(number)
        if numa >= num_sep:
            flux_bin_ft_smooth[i-1] = 0
        #if numa < num_lower:
        #    flux_bin_ft_smooth[i-1] = 0
        i += 1
    flux_bin_smooth = np.fft.ifft(flux_bin_ft_smooth)

    wlen_smooth = np.exp(wlen_bin)
    flux_smooth = np.interp(wavelength, wlen_smooth, flux_bin_smooth)

    flux_residual = flux - flux_smooth

    if True:
        plt.plot(wavelength, flux, label='Before')
        plt.plot(wavelength, np.real(flux_smooth), label='Smoothed')
        plt.legend()
        plt.show()

    stddev_binsize = int(stddev_width/(wavelength[1]-wavelength[0]))
    stddev = np.empty(len(flux_residual))
    for i in range(len(flux_residual)):
        if i < stddev_binsize/2:
            stddev[i] = np.std(flux_residual[0:stddev_binsize])
        else:
            stddev[i] = np.std(flux_residual[(i-int(stddev_binsize/2)):(i+int(stddev_binsize/2))])

    #plt.plot(wavelength, stddev)
    #plt.show()

    return flux_smooth, stddev
    
    #print(1/freq*C_KMS*binsize)
    #plt.plot(freq, np.abs(flux_bin_ft))
    #plt.yscale('log')
    #plt.plot(1/freq[1:]*C_KMS*binsize)
    #plt.xlim(0, 30e6)
    #3plt.xlim(0,0.5)
    #plt.show()
    #plt.plot(wlen_bin, flux_bin)
    #plt.show()

    




#t = table.Table.read("data/specs/preproc_marshall/ZTF19abqwtfu_20190830_NOT_v1.ascii", format='ascii')
#t = table.Table.read("data/specs/preproc_marshall/ZTF19abqwtfu_20191003_NOT_v1.ascii", format='ascii')
#fname = 'data/specs/preproc_marshall/ZTF19abqwtfu_20190827_P200_v1.ascii'
#fname = 'data/specs/preproc_marshall/ZTF19abqwtfu_20190830_NOT_v1.ascii'
#fname = 'data/specs/preproc_marshall/ZTF19abqwtfu_20191003_NOT_v1.ascii'
fname = sys.argv[1]
t = table.Table.read(fname, format='ascii')
wavelength = t['col1']
flux = t['col2']

#idx = wavelength < 8500
#wavelength = wavelength[idx]
#flux = flux[idx]

#p = np.polyfit(wavelength, flux, 3)
#p = result[0]
#print(result)
#print(p)
#continuum = p[0]*wavelength**3 + p[1]*wavelength**2 + p[2]*wavelength**1 + p[3]

#p = np.polyfit(wavelength, flux, 2)
#p = result[0]
#print(result)
#print(p)
#continuum = p[0]*wavelength**2 + p[1]*wavelength**1 + p[2]

#p = np.polyfit(wavelength, flux, 1)
#p = result[0]
#print(result)
#print(p)
#continuum = p[0]*wavelength**1 + p[1]

#if True:
#    flux, flux_mask, wavelength = snidflat(wavelength, flux, wlen_min=4000, wlen_max=9000, redshift=0.014353, do_plot=True, do_filter=False)
    #flux, flux_mask, wavelength = snidflat(wavelength, flux, wlen_min=4000, wlen_max=9000, redshift=0.0, do_plot=False, do_filter=False)

if False:
    plt.plot(wavelength, flux)
    plt.plot(wavelength, continuum)
    plt.plot(wavelength, flux - continuum)
    plt.show()


#flux_smooth, stddev = specFFTsmooth(wavelength, (flux - continuum)*1e16, 1000)
#flux_smooth, stddev = specFFTsmooth(wavelength, flux, 1000)

if flux[0] < 1e-15:
    flux *= 1e18

flux_smoothed, stddev = specFFTsmooth(wavelength, flux, 1000)

flux_smoothed = np.real(flux_smoothed)

flatflux, flatflux_mask, new_wavelength = snidflat(wavelength, flux, wlen_min=4000, wlen_max=9000, redshift=0.014353, do_plot=False, do_filter=False)
flatflux_smoothed, flatflux_mask, new_wavelength = snidflat(wavelength, flux_smoothed, wlen_min=4000, wlen_max=9000, redshift=0.014353, do_plot=False, do_filter=False)

flux_residual = flatflux - flatflux_smoothed

stddev_width = 100

stddev_binsize = int(stddev_width/(wavelength[1]-wavelength[0]))
stddev = np.empty(len(flux_residual))
for i in range(len(flux_residual)):
    if i < stddev_binsize/2:
        stddev[i] = np.std(flux_residual[0:stddev_binsize])
    else:
        stddev[i] = np.std(flux_residual[(i-int(stddev_binsize/2)):(i+int(stddev_binsize/2))])

import astropy.table as table
idx = np.logical_and(new_wavelength > 4300, new_wavelength < 8200)
idx = np.ones(len(new_wavelength), dtype=np.bool)
t = table.Table([new_wavelength[idx], flatflux[idx], flatflux_smoothed[idx], stddev[idx]], names=['wavelog_input', 'flatflux_input', 'flatflux_input_sm', 'flatflux_err_input'])
#t.write('test.csv', format='ascii')
t.write(sys.argv[2], format='ascii', overwrite=True)

