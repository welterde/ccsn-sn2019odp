#!/usr/bin/env python3
import click, sys, os, json
import numpy as np
import astropy.table as table


@click.command()
@click.argument('srcfile')
@click.argument('dstdir')
def main(srcfile, dstdir):
    if not os.path.isdir(dstdir):
        os.makedirs(dstdir)
    # load the input data
    with open(srcfile, 'r') as f:
        data = json.load(f)
    sne_name = list(data.keys())[0]
    sne_data = data[sne_name]
    for spec in sne_data['spectra']:
        #if 'filename' not in spec:
        #    continue
        if 'data' not in spec:
            continue
        if 'time' not in spec:
            continue
        t = int(float(spec['time']))
        fname = '%s_mjd%d.dat' % (sne_name, t)
        #fname = spec['filename']
        dest_fname = os.path.join(dstdir, fname)
        wave = [float(x[0]) for x in spec['data']]
        flux = [float(x[1]) for x in spec['data']]
        d = table.Table({'wavelength': wave, 'flux': flux})
        d.write(dest_fname, format='ascii')


if __name__ == '__main__':
    main()
