#!/usr/bin/env python3
import click
import sys, os, re, json
import speclite
import speclite.filters
import numpy as np
import astropy.units as u
from astropy.table import Table
import astropy.table as table
import json as jsonlib
from datetime import datetime
from astropy.time import Time
import matplotlib.pyplot as plt
import pandas as pd
import george
from george import kernels
import astropy.table as table
import scipy.optimize as optimize

# TODO: read that from somewhere else
PHASE_CENTER_MYSN = 58740

MJD_CUTOFF_MYSN = 1e12

def detect_peak(df, col):
    x = df[col].idxmin()
    print(x)
    return x


def interpolate_lc(mjd, mag, mag_err, mjd_grid):
    #mjd_grid = np.arange(grid_start, grid_end, grid_step)

    kernel = 10 * kernels.ExpSquaredKernel(150)
    gp = george.GP(kernel, mean=np.mean(mag), fit_mean=True)
    gp.compute(mjd, mag_err+0.09)
    
    # Define the objective function (negative log-likelihood in this case).
    def nll(p):
        gp.set_parameter_vector(p)
        ll = gp.log_likelihood(mag, quiet=True)
        return -ll if np.isfinite(ll) else 1e25
    
    # And the gradient of the objective function.
    def grad_nll(p):
        gp.set_parameter_vector(p)
        return -gp.grad_log_likelihood(mag, quiet=True)
        
    p0 = gp.get_parameter_vector()
    results = optimize.minimize(nll, p0, jac=grad_nll, method="L-BFGS-B")
    
    # Update the kernel and print the final log-likelihood.
    gp.set_parameter_vector(results.x)
    pred, pred_var = gp.predict(mag, mjd_grid, return_var=True)

    return pred, pred_var

# ZTF filters
def load_filter(inst, band):
    data = Table.read('const/filters/%s.%s.dat' % (inst, band), format='ascii')
    wave = []
    transmission = []
    for entry in data:
        if len(wave) > 0 and wave[-1] == entry['col1']:
            continue
        wave.append(entry['col1'])
        if entry['col2'] < 0.01 or entry['col1'] < 3790:
            transmission.append(0.0)
        else:
            transmission.append(entry['col2'])
    wave = np.array(wave)*u.Angstrom
    transmission = np.array(transmission)
    transmission[0] = 0
    transmission[-1] = 0

    f = speclite.filters.FilterResponse(wavelength=wave, response=transmission, meta=dict(group_name='ZTF', band_name=band))
    #print(f)
    return f


FILTERS = {
    'ZTF_g': load_filter('Palomar_ZTF', 'g'),
    'ZTF_r': load_filter('Palomar_ZTF', 'r'),
    'GROND_g': load_filter('LaSilla_GROND', 'g'),
    'GROND_r': load_filter('LaSilla_GROND', 'r')
}
for color in 'BVRI':
    FILTERS['Bes_%s' % color] = load_filter('NOT_ALFOSC', 'Bes_%s' % color)

VEGA_TO_AB = {
    'Bes_V': 0.02,
    'Bes_R': 0.21,
    'ZTF_g': 0,
    'ZTF_r': 0
}

    
FILE_RE = re.compile(r'^.*_(\d+)_.*$')
def extract_time(fname):
    m = FILE_RE.match(fname)
    dat = m.groups(1)[0]
    #print(dat)
    d2 = datetime.strptime(dat, '%Y%m%d')
    #print(d2)
    #print(Time(d2).mjd)
    return Time(d2).mjd

def load_json_phot(fname, fake_error=False):
    with open(fname, 'r') as f:
        data = json.load(f)
    sne = data[list(data.keys())[0]]
    phot = sne['photometry']
    def check(x):
        if 'band' not in x:
            return False
        if 'e_magnitude' not in x and not fake_error:
            return False
        if 'time' not in x:
            return False
        return True
    times = [float(x['time']) for x in phot if check(x)]
    mag = [float(x['magnitude']) for x in phot if check(x)]
    if fake_error:
        mag_err = [float(x.get('e_magnitude', 0.1)) for x in phot if check(x)]
    else:
        mag_err = [float(x['e_magnitude']) for x in phot if check(x)]
    band = [x['band'] for x in phot if check(x)]
    t = table.Table({'time': times, 'mag': mag, 'band': band, 'mag_err': mag_err})
    return t

FAKE_ERR_OBJS = [
    'SN2008D'
]

@click.command()
@click.option('--filter-a', default='Bes_V')
@click.option('--filter-b', default='Bes_R')
@click.option('--max-phase', default=100)
@click.option('--plot-comparison')
@click.option('--plot-ztf-color', is_flag=True)
@click.option('--p60-filter', default='all')
@click.option('--title')
@click.argument('input_files', nargs=-1)
def main(input_files, filter_a, filter_b, plot_comparison, max_phase, plot_ztf_color, p60_filter, title):
    flt_a = FILTERS[filter_a]
    flt_b = FILTERS[filter_b]

    odp_dataset = []
    for input_file in input_files:
        fname = os.path.basename(input_file)
        if 'P60' in fname and p60_filter == 'no':
            continue
        elif 'P60' not in fname and p60_filter == 'only':
            continue
        # 1) load spectrum
        spec = Table.read(input_file, format='ascii')
        wlen = spec['wavelength']*u.Angstrom
        flux = spec['flux']

        mag_a = flt_a.get_ab_magnitude(np.array(flux, copy=True), wlen)
        mag_b = flt_b.get_ab_magnitude(np.array(flux, copy=True), wlen)

        mag = mag_a - mag_b

        # extract date
        t = extract_time(fname)

        #print('%f\t%f\t%s' % (t, mag, fname))
        odp_dataset.append((t, mag, fname, 'P60' in fname))

    odp = table.Table(rows=odp_dataset, names=('mjd', 'color', 'fname', 'p60'))
    #print(odp)
    plt.plot(odp['mjd'] - PHASE_CENTER_MYSN, odp['color'], 'o', label='SN2019odp')
    if plot_comparison is None:
        plot_comparison = ''
    
    for comparison_obj in plot_comparison.split(','):
        if comparison_obj == '':
            continue
        print(comparison_obj)
        phot = load_json_phot('data/comparison/%s.json' % comparison_obj, comparison_obj in FAKE_ERR_OBJS)
        print('Bands: %s' % repr(np.unique(phot['band'])))
        t_min = 0
        t_max = 1e9
        #t_peaks = []
        for band in 'VR':
            idx = np.logical_and(phot['band'] == band, phot['time'] < phot['time'].min()+150)
            #idx = phot['band'] == band
            min_t, max_t = np.min(phot['time'][idx]), np.max(phot['time'][idx])
            print('\t', band, min_t, max_t)
            if min_t > t_min:
                t_min = min_t
            if max_t < t_max:
                t_max = max_t
            if band == 'V':
                i_peak = phot['mag'][idx].argmin()
                t_peak = phot['time'][idx][i_peak]
        print('\t%f %f' % (t_min, t_max))
        #t_peak = np.mean(t_peaks)
        t_grid = np.linspace(t_min, min(t_max, t_min+150))
        #idx = phot['band'] == 'V'
        idx = np.logical_and(phot['band'] == 'V', phot['time'] < phot['time'].min()+150)
        #mag_a_interp = np.interp(t_grid, phot['time'][idx], phot['mag'][idx])
        mag_a_interp, mag_a_var = interpolate_lc(phot['time'][idx], phot['mag'][idx], phot['mag_err'][idx], t_grid)
        mag_a_max = mag_a_interp - mag_a_var + VEGA_TO_AB['Bes_V']
        mag_a_min = mag_a_interp + mag_a_var + VEGA_TO_AB['Bes_V']
        idx = np.logical_and(phot['band'] == 'R', phot['time'] < phot['time'].min()+150)
        #mag_b_interp = np.interp(t_grid, phot['time'][idx], phot['mag'][idx])
        mag_b_interp, mag_b_var = interpolate_lc(phot['time'][idx], phot['mag'][idx], phot['mag_err'][idx], t_grid)
        mag_b_max = mag_b_interp - mag_b_var + VEGA_TO_AB['Bes_R']
        mag_b_min = mag_b_interp + mag_b_var + VEGA_TO_AB['Bes_R']
        #mag = mag_a_interp - mag_b_interp
        color_max = mag_a_max - mag_b_min
        color_min = mag_a_min - mag_b_max
        
        phase = t_grid - t_peak
        idx = phase < max_phase
        #plt.plot(phase[idx], mag[idx], label=comparison_obj)
        plt.fill_between(phase[idx], color_max[idx], color_min[idx], alpha=0.4, label=comparison_obj)

    if plot_ztf_color:
        df_interpolated = pd.read_hdf('products/lc_interpolated_combined.h5', '/lc')
        #phase_center = detect_peak(df_interpolated, 'mag_"r"_global')
        phase_center = PHASE_CENTER_MYSN
        
        interp_idx = df_interpolated.index.get_level_values('mjd') < MJD_CUTOFF_MYSN
        df_interp_sel = df_interpolated.iloc[interp_idx]
        interp_key = '"g"_global'
        mag_g_max = df_interp_sel['mag_%s' % interp_key] - 1*df_interp_sel['magerr_%s' % interp_key]
        mag_g_min = df_interp_sel['mag_%s' % interp_key] + 1*df_interp_sel['magerr_%s' % interp_key]
        interp_key = '"r"_global'
        mag_r_max = df_interp_sel['mag_%s' % interp_key] - 1*df_interp_sel['magerr_%s' % interp_key]
        mag_r_min = df_interp_sel['mag_%s' % interp_key] + 1*df_interp_sel['magerr_%s' % interp_key]
        
        color_max = mag_g_max - mag_r_min
        color_min = mag_g_min - mag_r_max

        plt.fill_between(df_interp_sel.index - phase_center, color_max, color_min, alpha=0.4, label='SN2019odp ZTF g-r')
        plt.ylabel('g - r Color [magAB]')
    else:
        plt.ylabel('V - R Color [magAB]')
    plt.ylim((-0.2,1.5))
    plt.xlabel('Phase [d]')
    if title:
        plt.title(title)
    
    plt.legend()
    plt.show()
             
    



if __name__ == '__main__':
    main()
