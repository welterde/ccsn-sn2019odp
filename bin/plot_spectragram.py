import os
import re
import numpy as np
import matplotlib.pyplot as plt
from astropy.table import Table
import click
import spectres
import datetime
from scipy import interpolate
import matplotlib.cm as cm
from scipy.ndimage import gaussian_filter1d
import matplotlib.colors as colors
import spectres
import h5py
import astropy.time as time
import astropy.constants as const
import astropy.units as u

DATE_RE = re.compile(r'.+_(\d+)_.+')

@click.command()
@click.option('--wlen-center', type=float)
@click.option('--wlen-width', type=float)
@click.option('--wlen-resample-width', type=float)
@click.option('--redshift', type=float, default=0.0)
@click.option('--max-phase', type=int, default=999)
#@click.option('--velocity', is_flag=True)
@click.argument('sseq_file')
def main(sseq_file, redshift, wlen_center, wlen_width, wlen_resample_width, max_phase):
    f = h5py.File(sseq_file, 'r')
    specs = []

    ref_grid = None
    for x in f:
        for y in f[x]:
            spec = Table.read(sseq_file, format='hdf5', path='%s/%s' % (x,y))
            phase = time.Time(spec.meta.get('DATE-OBS', spec.meta['date'])).jd - 2458735.8720
            spec.meta['phase'] = phase
            spec.meta['jd'] = time.Time(spec.meta.get('DATE-OBS', spec.meta['date'])).jd
            if phase > max_phase:
                continue
            specs.append(spec)
            if spec.meta['instrument'] == 'P60':
                wave = spec['wavelength']/(1+redshift)
                idx = np.abs(wave - wlen_center) <= wlen_resample_width
                ref_grid = wave[idx]
                
    specs = sorted(specs, key=lambda a: a.meta['phase'])

    def resample(s):
        print(ref_grid)
        print(s['wavelength'])
        new_flux = spectres.spectres(ref_grid, s['wavelength']/(1+redshift), s['flux'])
        #new_flux = new_flux / np.nanmean(new_flux)
        new_flux = new_flux - 0.9*np.nanmean(new_flux)
        subidx = np.abs(ref_grid - wlen_center) < wlen_width
        return Table([ref_grid[subidx], new_flux[subidx]], names=('wavelength', 'flux'))
    res_specs = list(map(resample, specs))

    days = [a.meta['jd'] for a in specs]
    #print('Days: %s' % repr(days))
    day_diffs = list(map(lambda a: int(a - days[0]), days))
    
    day_grid = np.arange(day_diffs[-1]+1, dtype=np.double)
    data = np.zeros((len(res_specs[0]['wavelength']), int(day_diffs[-1]+1)), dtype=np.double)
    fluxes = np.empty((len(res_specs[0]['wavelength']), len(day_diffs)), dtype=np.double)

    for i in range(len(day_diffs)):
        print('Day Diff = %d' % i)
        #fluxes[:,i] = gaussian_filter1d(res_specs[i]['flux'], 3)
        #fluxes[:,i] = res_specs[i]['flux']/np.quantile(res_specs[i]['flux'], 0.95)
        if i > 10:
            continue
        fluxes[:,i] = res_specs[i]['flux']

    day_grid = np.arange(day_diffs[-1]+1, dtype=np.double)
    for w_i in range(len(res_specs[0]['wavelength'])):
        inter = interpolate.interp1d(day_diffs, fluxes[w_i, :], kind='linear')
        data[w_i,:] = inter(day_grid)

    real_x = ref_grid
    real_y = day_grid
    dx = (real_x[1]-real_x[0])/2.
    dy = 1000*(real_y[1]-real_y[0])/2.
    extent = [real_x[0]-dx, real_x[-1]+dx, real_y[0]-dy, real_y[-1]+dy]

    extent = [real_x[0], real_x[-1], real_y[-1], real_y[0]]


    vmin, vmax = np.nanpercentile(data, [1,95])

    fig = plt.figure()
    ax = plt.subplot(111)
    print(vmin, vmax)
    im = ax.imshow(data.T, extent=extent, aspect='auto', cmap='rainbow', vmin=vmin, vmax=vmax)#norm=colors.LogNorm(vmin=vmin, vmax=vmax))
    #im = ax.imshow(data.T, aspect='auto', cmap='rainbow', vmin=vmin, vmax=vmax)#norm=colors.LogNorm(vmin=vmin, vmax=vmax))
    fig.colorbar(im)
    for dt in day_diffs:
        ax.axhline(y=dt)


    def ang2vel(x):
        diff = x - wlen_center # [A]
        z = diff/wlen_center
        v = z*(const.c.to(u.km/u.s).value)
        return v

    def vel2ang(v):
        z = v/(const.c.to(u.km/u.s).value)
        diff = z*wlen_center + wlen_center
        return diff

    # add velocity axis
    secax = ax.secondary_xaxis('top', functions=(ang2vel, vel2ang))
    secax.set_xlabel('Velocity [km/s]')

    ax.set_ylabel('Days since first Spectrum')
    ax.set_xlabel('Wavelength [A]')
    #plt.gca().set_xticks(range(len(real_x)))
    #plt.gca().set_yticks(range(len(real_x)))
    #plt.gca().set_xticklabels(real_x)
    #plt.gca().set_yticklabels(real_y)
    #plt.tight_layout()
    plt.show()


                    
                    

if __name__ == '__main__':
    main()
