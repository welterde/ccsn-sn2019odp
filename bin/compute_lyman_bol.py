#!/usr/bin/env python3
import re
import click
from datetime import datetime
import astropy.time as time
import astropy.table as table
import astropy.units as u
import astropy.constants as const
from astropy.cosmology import Planck15 as cosmology
import numpy as np
import pandas as pd


# TODO: include extinction uncertainty + correct for MW extinction
EXTINCTION_COEFF = {
    'g': 3.303,
    'r': 2.285,
    'i': 1.698,
    'z': 1.263,
    'J': 0.723,
    'H': 0.460,
    'K': 0.310
}
ebv = 0.165



# we use pBC from lyman(2014)
# pBC_g = \sum_i c_i (g-r)**i
lyman_gr_pbc_params = np.array([
    0.168, -0.407, -0.608, # c0 c1 c2
    0.074 # rms
])

lyman_gr_bc_params = np.array([
    0.054, -0.195, -0.719, # c0 c1 c2
    0.076 # rms
])

lyman_gr_bc_cooling_params = np.array([
    -0.146, 0.479, 2.257, # c0 c1 c2
    0.078 # rms
])

lyman_gi_bc_params = np.array([
    -0.029, -0.404, -0.230, # c0 c1 c2
    0.060 # rms
])


SAMPLE_NUM=300


def load_distance(fname):
    params = table.Table.read(fname, format='ascii')
    z = params['redshift'][0]
    z_err = params['err_redshift'][0]
    z_sample = np.random.normal(z, z_err, size=SAMPLE_NUM)
    dist = cosmology.luminosity_distance(z).to(u.pc).value
    dist_sample = cosmology.luminosity_distance(z_sample).to(u.pc).value
    return dist, dist_sample


@click.command()
@click.option('--distance-file', default='const/distance/current')
@click.option('--variant', default='bc_gr')
@click.argument('photfile')
@click.argument('destfile')
def main(photfile, destfile, distance_file, variant):
    df = pd.read_hdf(photfile).dropna()

    if variant == 'bc_gr':
        mag_x = df['mag_"g"_global'] - EXTINCTION_COEFF['g'] * ebv
        magerr_x = df['magerr_"g"_global']
        mag_y = df['mag_"r"_global'] - EXTINCTION_COEFF['r'] * ebv
        magerr_y = df['magerr_"r"_global']
    elif variant == 'bc_gi':
        mag_x = df['mag_"g"_global'] - EXTINCTION_COEFF['g'] * ebv
        magerr_x = df['magerr_"g"_global']
        mag_y = df['mag_"i"_global'] - EXTINCTION_COEFF['i'] * ebv
        magerr_y = df['magerr_"i"_global']
    else:
        raise ValueError('Unknown variant %s' % variant)
    
    dmag = mag_x - mag_y
    dmagerr = np.sqrt(magerr_x ** 2 + magerr_y ** 2)

    if variant == 'bc_gr':
        c0, c1, c2, rms = lyman_gr_bc_params
    elif variant == 'bc_gi':
        c0, c1, c2, rms = lyman_gi_bc_params
    else:
        raise ValueError('Unknown variant %s' % variant)

    # pBC_x = M_bol - M_x
    pbc = c0 + c1*dmag + c2*dmag ** 2
    pbc_err = np.abs(c1)*dmagerr + np.abs(c2)*dmagerr + rms

    mag_bol_x = mag_x + pbc
    magerr_bol_x = magerr_x + pbc_err

    
    # convert to luminosity
    dist, dist_sample = load_distance(distance_file)
    dist_mod = 5*np.log10(dist) - 5
    dist_mod_sample = 5*np.log10(dist_sample) - 5
    

    abs_mag = mag_bol_x - dist_mod
    # XXX: include the error from the distance in this

    #luminosity = (const.L_bol0.cgs.value)*10 ** ((4.74 - abs_mag)/2.5)
    luminosity = (const.L_sun.cgs.value)*10 ** ((4.74 - abs_mag)/2.5)

    lum_err = []
    for mag_bol_x_i, magerr_bol_x_i in zip(mag_bol_x, magerr_bol_x):
        mag_bol_sample = np.random.normal(mag_bol_x_i, magerr_bol_x_i, size=SAMPLE_NUM)
        abs_mag_sample = mag_bol_sample - dist_mod_sample

        lum_sample = (const.L_sun.cgs.value)*10 ** ((4.74 - abs_mag_sample)/2.5)
        
        lum_err.append(np.std(lum_sample))
    
    
    
    mjd = df.index.get_level_values('mjd')

    t = table.Table([mjd, luminosity, lum_err, mag_bol_x, magerr_bol_x, pbc, pbc_err], names=('MJD', 'Luminosity', 'Lums_Err', 'mag_bol_g', 'magerr_bol_g', 'pbc_mag', 'pbc_magerr'))
    print(t)
    t.write(destfile, overwrite=True)

    
    

if __name__ == '__main__':
    main()
