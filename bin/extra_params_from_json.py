import json
import click
import tqdm
import numpy as np
import astropy.table as table


@click.command()
@click.option('--band', default='r')
@click.argument('inputs', nargs=-1)
def main(inputs, band):
    for fname in tqdm.tqdm(inputs):
        with open(fname, 'r') as f:
            data = json.load(f)
            sne_data = data[list(data.keys())[0]]
            if 'photometry' not in sne_data:
                continue
            photometry = sne_data['photometry']
            # only consider ZTF..
            lc = []
            for lc_p in filter(lambda a: a.get('instrument') == 'ZTF-Cam', photometry):
                if lc_p['band'] != band:
                    continue
                if lc_p.get('upperlimit'):
                    continue
                mjd = lc_p['time']
                mag = lc_p['magnitude']
                if 'e_magnitude' not in lc_p:
                    continue
                mag_err = lc_p['e_magnitude']
                lc.append((mjd, mag, mag_err))
            if len(lc) < 10:
                continue
            t = table.Table(rows=lc, names=['mjd', 'mag', 'mag_err'])
            t.sort('mjd')
            print(len(t))
if __name__ == '__main__':
    main()
