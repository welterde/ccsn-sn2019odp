#!/usr/bin/env python3
import sys, click, os, json
#import logging
import warnings
warnings.filterwarnings("ignore", module="dynesty.sampling")
try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../lib'))
from snelib19odp.dataset import load_dataset

import matplotlib
matplotlib.use('Agg')
#logging.basicConfig(level=logging.DEBUG)

STATUS_DIR = os.path.join(os.path.dirname(__file__), '../.dsstatus')

RUN_LOG = os.path.join(os.path.dirname(__file__), '../products/lc/fit_run.log')


def load_run_log():
    log = []
    with open(RUN_LOG, 'r') as f:
        for line in f.readlines():
            dat = json.loads(line)
            log.append(dat)
    return log


@click.command()
@click.option('--bands', default='all')
@click.option('--interpolated', is_flag=True)
@click.argument('dataset')
def main(dataset, bands, interpolated):
    if not os.path.isdir(STATUS_DIR):
        os.makedirs(STATUS_DIR)
    status_file = os.path.join(STATUS_DIR, dataset)
    status = {}
    if os.path.isfile(status_file):
        try:
            with open(status_file, 'r') as f:
                status = json.load(f)
        except:
            pass

    if dataset == 'all':
        # load the diag file
        run_log = reversed(load_run_log())
        already_done = set()
        for entry in run_log:
            ds_name = entry['ds_name']
            band = entry['band']
            key = (ds_name, band)
            if key in already_done:
                continue
            print(' * %s (band=%s)' % key)
            ds = load_dataset(ds_name)
            if not interpolated:
                lc = ds.get_combined_lc(band)
            else:
                lc = ds.get_interpolated(band)
            status[band] = ds.rev
            already_done.add(key)
    else:
        ds = load_dataset(dataset)
        if bands == 'all':
            raise NotImplemented()
        else:
            bands = bands.split(',')

        for band in bands:
            if not interpolated:
                lc = ds.get_combined_lc(band)
            else:
                lc = ds.get_interpolated(band)
            status[band] = ds.rev

    # write status file
    with open(status_file, 'w') as f:
        json.dump(status, f)
    


if __name__ == '__main__':
    main()
