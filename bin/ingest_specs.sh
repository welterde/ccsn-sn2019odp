#!/bin/sh

DEST=$1

test -f $DEST && rm $DEST

for f in data/specs/preproc_marshall/*.ascii
do
    date=$(echo $f|ruby -rdate -ne 'm=/_(\d+)_(\S+)_/.match($_); d=DateTime.strptime(m.captures[0], "%Y%m%d"); puts "#{d.year}-#{d.month}-#{d.day}"')
    instrument=$(echo $f|ruby -ne 'm=/_(\d+)_(\S+)_/.match($_); puts m.captures[1]')
    gp3-sseq-ingest --meta date $date --meta instrument $instrument $DEST $f
done
