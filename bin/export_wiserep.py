#!/usr/bin/env python3
import click, sys, os, functools, dynesty, numba, collections, json, logging
import numpy as np
from astropy.table import Table
from astropy.io import ascii

try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../lib'))
import snelib19odp.specds as specds
import snelib19odp.dataloader as dataloader



WISEREP_INSTRUMENT_IDS = {
    'P60/SEDM': 149,
    'ALFOSC': 41,
    'NOT/ALFOSC': 41,
    'P200/DBSP': 1,
    'Keck-I/LRIS': 3,
}

WISEREP_WAVE_ID = 3

WISEREP_WAVE_MEDIUM_ID = 1 # Air

def lookup_instrument_id(spec, meta):
    fname = spec.meta["file_name"]
        
    tel_ins, setup = dataloader.guess_setup(os.path.basename(fname), meta)
    
    return WISEREP_INSTRUMENT_IDS[tel_ins.strip()]


def make_tsv(ds, dest_file, objname, group_id, bibcode, skip_objs):
    rows = []

    # set the values to NULL if not present
    if not group_id:
        group_id = 'NULL'
    if not bibcode:
        bibcode = 'NULL'

    for spec_name in ds.names:
        if spec_name in skip_objs:
            continue
        
        spec = ds[spec_name]
        meta = spec.meta
        fmeta = dataloader.get_meta(os.path.basename(meta["file_name"]))

        if not dataloader.check_headers(fmeta):
            sys.stderr.flush()
            sys.exit(1)

        entry = []

        entry.append(('Obj. IAU-name', objname))
        entry.append(('Ascii-filename', meta["file_name"]))
        entry.append(('FITS-filename', "NULL"))

        t = dataloader.get_spec_obs_time(meta["file_name"])
        entry.append(('Obs-date [YYYY-MM-DD HH:MM:SS]', t.iso))

        entry.append(('Instrument-Id', str(lookup_instrument_id(spec, fmeta))))
        entry.append(('Exptime', fmeta["EXPTIME"]))

        #entry.append(('WL Units-id', WISEREP_WAVE_ID))
        entry.append(('WL Medium-Id', WISEREP_WAVE_MEDIUM_ID))
        #entry.append(('Flux Unit Coeff', WISEREP_FLUX_UNIT_COEFF))
        #entry.append(('Flux Units-Id', WISEREP_FLUX_UNIT_ID))
        entry.append(('Source Group-Id', str(group_id)))

        if 'FC:PHOT' in ds.flags:
            # flux calibration by photometry
            entry.append(('Flux Calib. By-Id', '2'))
        else:
            # we default to flux calibration by stdstar
            entry.append(('Flux Calib. By-Id', '1'))

        # object spectrum
        entry.append(('Spec Type-Id', '10'))

        # add the publication bibcode 
        entry.append(('Publish (bibcode)', bibcode))

        # possible additions:
        # Grating (NOT/ALFOSC)
        # Dichroic (P200)
        

        rows.append(dict(entry))

    t = Table(rows=rows, meta={'comments': ['TSV-type:\tspectra']})
    print(t)

    # create the table and write the TSV file
    # t = Table(rows=rows, names=[
    #     'Ascii-filename*',
    #     'FITS-filename*',
    #     'Obs-date* [YYYY-MM-DD HH:MM:SS] / JD',
    #     'Instrument-Id*',
    #     'Exp-time (sec)',
    #     'WL Units-id',
    #     'WL Medium-Id',
    #     'Flux Unit Coeff',
    #     'Flux Units-Id',
    #     'Flux Calib. By-Id',
    #     'Extinction-Corrected-Id',
    #     'Observer/s      ',
    #     'Reducer/s   ',
    #     'Reduction-date [YYYY-MM-DD HH:MM:SS] / JD',
    #     'Aperture (Slit)',
    #     'Dichroic',
    #     'Grism',
    #     'Grating',
    #     'Blaze',
    #     'Airmass',
    #     'Hour Angle',
    #     'Spec Type-Id',
    #     'Spec Quality-Id',
    #     'Spec. Prop-period value',
    #     'Prop-period units',
    #     'Assoc. Groups',
    #     'Spec-Remarks',
    #     'Publish (bibcode)',
    #     'Contrib',
    #     'Related-file1',
    #     'RF1 Comments',
    #     'Related-file2',
    #     'RF2 Comments'
    # ], meta={'comments': ['TSV-type:\tspectra']})

    # use the lower-level interface to add the second line with the defaults
    writer = ascii.get_writer(ascii.Tab, fast_writer=False, comment='',
                              fill_values=[('None', 'NULL'), ('', 'NULL'), ('UNKNOWN', 'NULL')])
    lines = writer.write(t)
    #lines.insert(2, '\t\t\t\tNULL\t[default=11 (Angstrom)]\t[default=1 (Air)]\t[default=1.0]\t[default=6]\tNULL\tNULL\t[Unknown]\tNULL\tNULL\tNULL\tNULL\tNULL\tNULL\tNULL\tNULL\tNULL\t[default=10=Object]\tNULL\tNULL\t[days/months/years]\t[Comma delim.]\tNULL\tNULL\tNULL\tNULL\tNULL\tNULL\tNULL')
    with open(dest_file, 'w') as f:
        f.write('\n'.join(lines))
        f.write('\n')
    

SKIP_EXTINCTION_CORRECTION = set([
    'extinction:mw',
    'extinction:host'
])
        

@click.command()
@click.option('--debug', is_flag=True)
@click.option('--group-id', type=int)
@click.option('--bibcode')
@click.option('--skip-obj', multiple=True)
@click.option("--spec-dataset")
@click.option("--phot-dataset")
@click.option("--include-extinction-correction", is_flag=True)
@click.argument("destdir")
def main(debug, spec_dataset, phot_dataset, destdir, bibcode, group_id, skip_obj, include_extinction_correction):
    """
    Program to dump all spectra into wiserip uploadable format
    """
    if debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    
    # 0. ensure the destination directory does not exists
    if os.path.exists(destdir):
        sys.stderr.write("* ERROR: Destination directory already exists!\n")
        sys.exit(2)

    os.makedirs(destdir)
    
    # 1. spectrum export
    if spec_dataset:
        # 1.1 load spectral dataset
        skip_steps = SKIP_EXTINCTION_CORRECTION
        if include_extinction_correction:
            skip_steps = None
        sds = specds.load_dataset(spec_dataset)

        objname = sds.transient.name

        # 1.2 calculate destination file
        dest_file = os.path.join(destdir, objname + '.tsv')

        # 1.3 write summary file
        make_tsv(sds, dest_file, objname, group_id, bibcode, skip_obj)

        # 1.4 write out the spectra in ASCII/FITS format
        for spec_name in sds.names:
            if spec_name in skip_obj:
                continue

            spec = sds[spec_name]
            meta = spec.meta

            if np.count_nonzero(~np.isnan(spec['fluxerr'])) > 10:
                spec = spec['obswave', 'flux', 'fluxerr']
            else:
                spec = spec['obswave', 'flux']

            # 1.4.1 Write ASCII output
            dest_fname = os.path.join(destdir, spec_name + '.asci')
            spec.write(dest_fname, format='ascii')
            

    # 2. photometry export
    if phot_dataset:
        # TODO: implement this
        pass


if __name__ == '__main__':
    main()
