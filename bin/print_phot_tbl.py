import click, glob, re, sys, os, collections
import pandas as pd
import numpy as np
import astropy.table as table
import astropy.time as time
import astropy.io.fits as fits
import astropy.table as table

try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../lib'))
import snelib19odp.const as const

TBL_HEADER = '''
\\begin{table*}
\\centering
\\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|}
\\hline
UT & MJD & $\\Delta t_\\text{expl}$ & $\\Delta t_g$ & Filter & Telescope/Instrument & $m$ & $\\Delta m$ & $m_\\text{lim}$ & $F$ & $\\Delta F$ \\\\
& (d) & (d) & (d) & & & (mag) & (mag) & (mag) & ($\\mu$uJy) & ($\\mu$Jy) \\\\ 
\\hline
\\hline
'''

TBL_FOOTER = '''
\hline
\\end{tabular}
\\caption{Listing of the full forced photometry dataset obtained for SN\\,2019odp.
ZTF photometry is based on image subtraction forced photometry and contains pre-explosion epochs.
Both magnitudes as well as fluxes are given.
The full photometry table will be available at location TODO.}
\\label{tab:obslog:phot}
\\end{table*}
'''



def digitize_mjd(mjd):
    return int(mjd*24*3600)


Entry = collections.namedtuple('Entry', 'mjd instrument band mag magerr maglim flux fluxerr')

# TODO: move that to lib maybe? maybe snelib19odp.dirs ?
products_dir = os.path.join(os.path.dirname(__file__), '../products')
data_dir = os.path.join(os.path.dirname(__file__), '../data')


@click.command()
@click.option('--min-mjd', default=58700)
@click.option('--max-entries', default=40)
@click.argument('destfile')
def main(destfile, max_entries, min_mjd):
    # load the ztflc lightcurve
    ztflc = table.Table.read(os.path.join(data_dir, 'ztflc_forcefit.h5'))
    ztflc['mag'] = -2.5*np.log10(ztflc['ampl']) + ztflc['magzp']

    #########################################
    # crossmatch by MJD between the two lists
    # MJD rounded to 300s precision is the key of the dict
    # ((time_key, instrument, band) -> Entry)
    entries = {}

    # ingest the ztflc lightcurve
    for row in ztflc:
        mjd = row['obsmjd']
        band = row['filter'][-1]
        key = (digitize_mjd(mjd), 'P48/ZTF', band)
        
        mag = row['mag']
        # FIXME: load correct error
        magerr = np.nan
        maglim = row['maglim']

        # TODO: factor out this conversion code to utility function in library
        scalezp = 0
        f0coef = 3631e6 * 10 ** (-(row["magzp"] - scalezp) / 2.5)
        flux = f0coef*row['ampl']
        fluxerr = f0coef*row['ampl.err']

        if flux < 0:
            mag = np.nan
            magerr = np.nan
        else:
            # TODO: bring this in line with the normal alert packet error..
            magerr = fluxerr/flux*1.087 + row['magzprms']

        # ignore objects that are beyond the limiting magnitude?
        #if maglim < mag:
        #    mag = np.nan
        #    magerr = np.nan

        if key in entries:
            sys.stderr.write('Duplicate ztflc photometry detected!\n')
            entries[key] = entries[key]._replace(mag=mag, magerr=magerr, maglim=maglim, flux=flux, fluxerr=fluxerr)
        else:
            entries[key] = Entry(mjd=mjd, instrument='P48/ZTF', band=band, mag=mag, magerr=magerr, maglim=maglim, flux=flux, fluxerr=fluxerr)

    # ingest the UVOT lightcurve
    uvot = table.Table.read(os.path.join(data_dir, 'uvot_phot.txt'), format='ascii')
    for row in uvot:
        mjd = row['JD'] - 2400000.5
        band = row['FILTER']
        instrument = 'Swift/UVOT'
        key = (digitize_mjd(mjd), instrument, band)

        mag = row['AB_MAG']
        magerr = row['AB_MAG_ERR']
        maglim = row['AB_MAG_LIM']
        
        flux = 3631e6 * 10 ** (-row['AB_MAG']/2.5)
        fluxerr = row['AB_MAG_ERR'] * flux / 1.087

        if key in entries:
            sys.stderr.write('Duplicate UVOT photometry detected!\n')
            entries[key] = entries[key]._replace(mag=mag, magerr=magerr, maglim=maglim, flux=flux, fluxerr=fluxerr)
        else:
            entries[key] = Entry(mjd=mjd, instrument=instrument, band=band, mag=mag, magerr=magerr, maglim=maglim, flux=flux, fluxerr=fluxerr)

    # ingest the ATLAS lightcurve
    atlas = table.Table.read(os.path.join(data_dir, 'forced_photometry/job19862.txt'), format='ascii')
    atlas['mjd'] = atlas['##MJD']
    for row in atlas:
        mjd = row['mjd']
        band = row['F']
        instrument = 'ATLAS'
        key = (digitize_mjd(mjd), instrument, band)

        if row['m'] > 0:
            mag = row['m']
            magerr = row['dm']
        else:
            mag = np.nan
            magerr = np.nan
        # TODO: double-check if that is the correct column...
        maglim = row['mag5sig']

        flux = row['uJy']
        fluxerr = row['duJy']

        if key in entries:
            sys.stderr.write('Duplicate ATLAS photometry detected!\n')
            entries[key] = entries[key]._replace(mag=mag, magerr=magerr, maglim=maglim, flux=flux, fluxerr=fluxerr)
        else:
            entries[key] = Entry(mjd=mjd, instrument=instrument, band=band, mag=mag, magerr=magerr, maglim=maglim, flux=flux, fluxerr=fluxerr)
    
    with open(destfile, 'w') as f:
        f.write(TBL_HEADER)
        ctr = 0
        for key in sorted(entries.keys()):
            entry = entries[key]

            if entry.mjd < min_mjd:
                continue
            
            if ctr > max_entries and max_entries > 0:
                break
            ctr += 1
            
            obs_time = time.Time(entry.mjd, format='mjd')
            ut_timestamp = obs_time.to_value('iso', 'date_hm')
            obs_mjd = obs_time.mjd
            delta_explosion = obs_mjd - const.explosion_epoch_mjd
            delta_phase = obs_mjd - const.sne_peak_mjd['g']

            f.write(ut_timestamp)
            f.write(' & ')
            f.write('%.1f & ' % obs_mjd)
            f.write('%.1f & ' % delta_explosion)
            f.write('%.1f & ' % delta_phase)
            f.write('%s & ' % entry.band)
            # ztf lc
            f.write('%s & ' % entry.instrument)
            f.write('%.2f & %.2f & %.2f & %.1f & %.1f \\\\\n' % (entry.mag, entry.magerr, entry.maglim, entry.flux, entry.fluxerr))
        f.write(TBL_FOOTER)
            

if __name__ == '__main__':
    main()
