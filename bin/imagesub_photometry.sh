#!/bin/sh
set -e

BAND=$1
NOT_IMAGE=$PWD/$2
DESTFILE=$PWD/$3

IMG_ID=$(basename $2)

REF_SRC=ps1
SCI_PIXSCALE=0.2142

REF_IMAGE=$PWD/$(dirname $0)/../data/ref_images/$REF_SRC.$BAND.fits

WORK_DIR=$PWD/$(dirname $0)/../products/imagesub/$IMG_ID/$REF_SRC

PHOT_SCRIPT=$PWD/$(dirname $0)/app_phot.py

# create working directories
mkdir -p $WORK_DIR

# create downsampled image
SWarp -IMAGEOUT_NAME $WORK_DIR/ref.fits -CENTER_TYPE MANUAL -CENTER "23:07:19.09,+13:51:21.5" $REF_IMAGE -RESAMPLE Y  -PIXELSCALE_TYPE MANUAL -PIXEL_SCALE $SCI_PIXSCALE -IMAGE_SIZE 1000,1000 -SUBTRACT_BACK N -SATLEV_DEFAULT 14000

# create image cutout from sci frame
SWarp -IMAGEOUT_NAME $WORK_DIR/obs.fits -WEIGHTOUT_NAME $WORK_DIR/obs.weight.fits -CENTER_TYPE MANUAL -CENTER "23:07:19.09,+13:51:21.5" $NOT_IMAGE -RESAMPLE Y -IMAGE_SIZE 1000,1000 -SUBTRACT_BACK N

cd $WORK_DIR
#/home/welterde/Work/SU/sw/hotpants/hotpants -inim obs.fits -tmplim ref.fits -outim diff.fits -oni noise.fits -tl -50 -il -50 -v 1 -tg 1.05986 -ig 0.16 -tr 42.41547 -ir 4.3 -iuk 15000 -iu 15000 -tuk 15000 -tu 15000 -c t -nsx 5 -nsy 5 -ng  3 6 3.1 4 6.2 2 12.4 -n t
/home/welterde/Work/SU/sw/hotpants/hotpants -inim obs.fits -tmplim ref.fits -outim diff.fits -oni noise.fits -nrx 1 -nry 1 -nsx 5 -nsy 5 -r 15 -rss 20 -iu 60000 -iuk 54000 -il -100.0 -tl -1000.0 -tu 87613 -tuk 78852 -ko 1 -bgo 1 -ft 20 -ng  3 6 3.1 4 6.2 2 12.4  -sconv -n t


$PHOT_SCRIPT --band=$BAND --use-cog $NOT_IMAGE $REF_IMAGE diff.fits noise.fits $DESTFILE
