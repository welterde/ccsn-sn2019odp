#!/usr/bin/env python3
import click
import sys
import speclite
import speclite.filters
import numpy as np
import astropy.units as u
from astropy.table import Table
import json
from astropy.time import Time
import re
from datetime import datetime
import matplotlib.pyplot as plt

FILE_RE = re.compile(r'^.*_(\d+)_.*$')



def extract_time(entry):
    m = FILE_RE.match(entry['fname'])
    dat = m.groups(1)[0]
    #print(dat)
    d2 = datetime.strptime(dat, '%Y%m%d')
    #print(d2)
    #print(Time(d2).mjd)
    return Time(d2).mjd



@click.command()
@click.option('--bands', type=str)
@click.option('--compare-instruments', type=str)
@click.option('--plot-lc', type=str)
@click.option('--plot-color', type=str)
@click.option('--plot-real-lc', type=click.Path(exists=True))
@click.option('--plot-real-prefix', type=str)
@click.option('--plot-real-prefixerr', type=str)
@click.argument('infiles', nargs=-1)
def main(infiles, compare_instruments, bands, plot_lc, plot_color, plot_real_lc, plot_real_prefix, plot_real_prefixerr):
    raw_lc = []
    for fn in infiles:
        with open(fn, 'r') as f:
            raw_lc.append(json.load(f))


            
    print(raw_lc)
    if compare_instruments:
        inst1, inst2 = compare_instruments.split(',')
        lc = {}
        for entry in raw_lc:
            for sub_entry in entry:
                band = sub_entry['band']
                if band not in lc:
                    lc[band] = ([], [], [])
                lc[band][0].append(extract_time(sub_entry))
                lc[band][1].append(sub_entry[inst1])
                lc[band][2].append(sub_entry[inst2])
        for band in bands:
            times, inst1_lc, inst2_lc = lc[band]
            plt.plot(times, np.array(inst1_lc) - np.array(inst2_lc), 'o', label=band)
            #print(lc[band])
        plt.title('Synthetic Photometry SN2019odp')
        plt.xlabel('MJD [d]')
        plt.ylabel('%s - %s [mag]' % (inst1, inst2))
        plt.legend()
        plt.show()
    elif plot_lc:
        inst = plot_lc
        
        lc = {}
        for entry in raw_lc:
            for sub_entry in entry:
                band = sub_entry['band']
                if band not in lc:
                    lc[band] = ([], [])
                lc[band][0].append(extract_time(sub_entry))
                lc[band][1].append(sub_entry[inst])

        if bands and not plot_color:
            for band in bands:
                times, inst_lc = lc[band]
                plt.plot(times, np.array(inst_lc), 'o', label=band)
        elif plot_color:
            band1, band2 = plot_color.split(',')
            times = lc[band1][0]
            mag = np.array(lc[band1][1]) - np.array(lc[band2][1])
            plt.plot(times, mag)

        if plot_real_lc:
            # load the interpolated lightcurve
            real_lc = Table.read(plot_real_lc, format='ascii')

            if plot_color:
                band1, band2 = plot_color.split(',')
                times = real_lc['MJD']
                mag = real_lc[plot_real_prefix + band1] - real_lc[plot_real_prefix + band2]
                magerr = np.sqrt(real_lc[plot_real_prefixerr + band1] + real_lc[plot_real_prefixerr + band])
                plt.fill_between(times, mag - 3*magerr, mag + 3*magerr, alpha=0.4, label='Observed lightcurve')
                #plt.plot(times, mag, label='Observed lightcurve')

        plt.title('Synthetic Photometry SN2019odp')
        plt.xlabel('MJD [d]')
        if not plot_color:
            plt.ylabel('%s [mag]' % inst)
        else:
            plt.ylabel('%s - %s [%s mag]' % (band1, band2, inst))
        plt.gca().invert_yaxis()
        plt.legend()
        plt.show()
    
                

if __name__ == '__main__':
    main()
