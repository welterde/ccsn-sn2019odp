#!/usr/bin/env python
from scipy import optimize
import numpy as np
import subprocess
import json
import click
import astropy.units as u
import matplotlib.pyplot as plt
import pandas as pd
import numba
import dynesty
from multiprocessing.pool import ThreadPool
import zmq
import threading
from dynesty import plotting as dyplot
from dynesty import utils as dyfunc
import time
import statsd
import emcee
import corner

import snelib19odp.extinction as extinction
import snelib19odp.bolometric.lyman as lyman
from astropy.cosmology import Planck15 as cosmology
import snelib19odp.utils as utils


SOLAR_TO_CGS = u.solMass.to(u.g)


stats_client = statsd.StatsClient('localhost', port=8125)

def run_susuc(ej_mass, ej_velocity, opt_opacity, gam_opacity, nickel_fraction, susuc_path):
    #print('RUN [mass=%f, velocity=%f, opt_op=%f, gam_op=%f, ni%%=%f]' % (
    #    ej_mass, ej_velocity, opt_opacity, gam_opacity, nickel_fraction*100
    #    ))
    cfg = {
        'ejMass': SOLAR_TO_CGS*np.exp(ej_mass),
        'ejVelocity': np.exp(ej_velocity)*1e5,  # km/s -> cm/s
        'optOpacity': np.exp(opt_opacity),
        'gamOpacity': np.exp(gam_opacity),
        'nickelFraction': np.exp(nickel_fraction)
    }
    s = json.dumps(cfg)

    ipc_method, path = susuc_path
    stats_client.incr('active_ipc_req')
    before_t = time.time()
    if ipc_method == 'proc':
        proc = subprocess.Popen([path, '/dev/stdin'], stdout=subprocess.PIPE, stdin=subprocess.PIPE)
        data = proc.communicate(input=s.encode('utf-8'))[0]
    elif ipc_method == 'zmq':
        threadLocal = threading.local()
        if not hasattr(threadLocal, 'zmq_socket'):
            ctx = zmq.Context.instance()
            threadLocal.zmq_ctx = ctx
            socket = ctx.socket(zmq.REQ)
            socket.connect(path)
            threadLocal.zmq_socket = socket
        socket = threadLocal.zmq_socket
        socket.send(s.encode('utf-8'))
        data = socket.recv().decode('utf-8')
    else:
        raise ValueError('Unknown IPC method %s' % ipc_method)
    stats_client.decr('active_ipc_req')
    
    after1_t = time.time()
    arr = np.fromstring(data, sep=' ', dtype=np.float64)
    arr2 = arr.reshape((int(len(arr)/2), 2))
    after2_t = time.time()
    stats_client.timing('ipc_call', (after1_t-before_t)*1000)
    stats_client.timing('parse', (after2_t-after1_t)*1000)
    return arr2


def readjust_peak(a):
    lum = a[:,1]
    lum[np.isinf(lum)] = np.nan
    peak_idx = np.nanargmax(lum)
    t_offset = a[peak_idx,0]
    #print('Offseting by %f (idx %d = %e)' % (t_offset, peak_idx, lum[peak_idx]))
    phase = (a[:,0] - t_offset)/3600/24

    return phase, lum


def int_lc(phase, lum):
    idx = ~np.logical_or(np.isnan(lum), np.isinf(lum))
    return np.trapz(lum[idx]/1e42, phase[idx])


def fit_fun(p, susuc_path, obs_lc_phase, obs_lc_lum, likelihood=True):
    ejMass = p[0]
    ejVelocity = p[1]
    if len(p) > 2:
        nickelFraction = p[2]
    else:
        nickelFraction = 0.2
    if len(p) > 3:
        opt_opacity = p[3]
    else:
        opt_opacity = 0.2
    if len(p) > 4:
        gam_opacity = p[4]
    else:
        gam_opacity = 10.0

    a = run_susuc(ejMass, ejVelocity, opt_opacity, gam_opacity, nickelFraction, susuc_path)

    # find peak lum
    phase, lum = readjust_peak(a)
    idx = ~np.isnan(lum)
    
    # extract the syn values at the obs_lc_phase times
    synlum_interp = np.interp(obs_lc_phase, xp=phase[idx], fp=lum[idx], left=0.0)

    total_energy_obs = int_lc(obs_lc_phase, obs_lc_lum)
    total_energy_syn = int_lc(obs_lc_phase, synlum_interp)
    
    if likelihood:
        return -0.5*np.sum((synlum_interp/1e42 - obs_lc_lum/1e42)**2) + -0.5*(total_energy_obs - total_energy_syn)**2
        #return -0.5*np.sum(np.abs(np.log10(synlum_interp) - np.log10(obs_lc_lum))) + -0.5*(total_energy_obs - total_energy_syn)**2
    else:
        return np.sum((synlum_interp - obs_lc_lum)**2)



def load_lc():
    df = pd.read_hdf('products/lc_interpolated_combined.h5')
    # params..
    ebv = 0.165
    redshift = 0.014353

    dist = cosmology.luminosity_distance(redshift).to(u.pc).value
    dist_mod = 5*np.log10(dist) - 5
    
    extinction.correct_extinction_columns(df, ebv, 3.1, column_band_mapping={
        'mag_"g"_global': 'SDSS g',
        'mag_"r"_global': 'SDSS r'
    })

    
    lc = lyman.compute_bol_mag(df['mag_"g"_global'], df['mag_"r"_global'], 'bc')
    lc -= dist_mod
    
    lc = utils.convert_luminosity(lc)
    
    return calc_phase(lc, 'auto', True)
    
    
def calc_phase(df, phase_method, luminosity):
    if phase_method == 'auto':
        if luminosity:
            idx_max = df.argmax()
        else:
            idx_max = df.argmin()
        t_max = df.index[idx_max]
        new_df = df.copy(deep=True)
        new_df.index = new_df.index - t_max
        return new_df
    else:
        raise ValueError('Unknown phase method %s' % phase_method)


def fit(lc_df, susuc_path):
    bounds = [
        (np.log(0.01), np.log(100)), # ejecta mass
        (np.log(100), np.log(100e3)), # ejecta velocity
        (np.log(1e-4), np.log(0.5)), # nickel fraction
        (np.log(0.02), np.log(0.3)), # optical opacity
        #(5,40) # gamma opacity
    ]
    args = (susuc_path, lc_df.index, lc_df, False)
    result = optimize.differential_evolution(fit_fun, args=args, bounds=bounds, maxiter=1000, polish=False)
    print(result)
    return result.x


#############################
### Nested Sampling Stuff ###
@numba.njit
def prior_transform(u):
    x = np.empty(5)

    # ejecta mass [solMass]
    #x[0] = 100*u[0] + 0.01
    x[0] = 5*u[0]

    # ejecta velocity [km/s]
    #x[1] = 130e3*u[1] + 100
    x[1] = 10.4*u[1]

    # nickel fraction 
    if len(u) > 2:
        #x[2] = 50*u[2] + 0.01
        x[2] = 7*u[2]-7.5
    else:
        x[2] = np.log(0.1)

    # optical opacity
    if len(u) > 3:
        #x[3] = 0.6*u[3]+0.02
        x[3] = 5.0*u[3]-5.8
    else:
        x[3] = 0.2

    # gamma opacity
    if len(u) > 4:
        #x[4] = 35*u[4]+5
        x[4] = 2.7*u[4]+1.0
    else:
        x[4] = np.log(5.0)

    return x


def dynesty_run(lc_df, susuc_path, queue_size):
    args = (susuc_path, lc_df.index, lc_df, True)
    pool = ThreadPool(queue_size)
    sampler = dynesty.NestedSampler(fit_fun, prior_transform, 4, logl_args=args, nlive=4000, pool=pool, queue_size=queue_size)
    sampler.run_nested()
    results = sampler.results
    results.summary()
    dyplot.cornerplot(results, labels=['log Mej', 'log vej', 'log Ni%', 'log optOp', 'gamOp'], dims=[0,1,2,3], show_titles=True)
    #plt.show()
    samples, weights = results.samples, np.exp(results.logwt - results.logz[-1])
    mean, cov = dyfunc.mean_and_cov(samples, weights)
    
    return mean

def dynesty_dynamic_run(lc_df, susuc_path, queue_size):
    args = (susuc_path, lc_df.index, lc_df, True)
    pool = ThreadPool(queue_size)
    sampler = dynesty.DynamicNestedSampler(fit_fun, prior_transform, 5, logl_args=args, nlive_init=3000, nlive_batch=700, maxiter_init=1000, maxiter_batch=500, pool=pool, queue_size=queue_size, use_pool={'prior_transform': False, 'propose_point': False, 'update_bound': False})
    sampler.run_nested()
    results = sampler.results
    dyplot.cornerplot(results, labels=['log Mej', 'log vej', 'log Ni%', 'log optOp', 'gamOp'], dims=[0,1,2,3,4], show_titles=True)
    #plt.show()
    samples, weights = results.samples, np.exp(results.logwt - results.logz[-1])
    mean, cov = dyfunc.mean_and_cov(samples, weights)
    
    return mean


def fit_emcee(lc_df, susuc_path, queue_size):
    args = (susuc_path, lc_df.index, lc_df, True)

    @numba.njit
    def log_prior(u):
        c = np.logical_and(u >= 0, u <= 1)
        if np.count_nonzero(c) == len(c):
            return 0.0
        return -np.inf

    def log_probability(u, *args):
        lp = log_prior(u)
        if not np.isfinite(lp):
            return -np.inf
        p = prior_transform(u)
        l = fit_fun(p, *args)
        return lp + l

    #x0 = np.array([ 2.62303634, 11.90951209, -5.10122928,  0.29513243,  5.        ])
    #x0 = np.array([0.5, 0.5, 0.5, 0.1])*(1+1e-4*np.random.randn(32,4))
    #x0 = np.array([-0.95775744,  6.5800858 , -1.42175298, -3.63343356])*(1+1e-4*np.random.randn(32,4))
    x0 = np.random.randn(32,4)
    nwalkers, ndim = x0.shape

    pool = ThreadPool(queue_size)
    
    sampler = emcee.EnsembleSampler(nwalkers, ndim, log_probability, args=args, pool=pool)
    sampler.run_mcmc(x0, 7000, progress=True)

    flat_samples = sampler.get_chain(discard=1000, flat=True)

    labels=['log Mej', 'log vej', 'log Ni%', 'log optOp']
    corner.corner(
        flat_samples, labels=labels
    )
    plt.show()
    
    return np.nanmean(flat_samples, axis=0)
    

@click.command()
@click.option('-m', '--method', type=click.Choice(['none', 'nested', 'nested-dynamic', 'emcee', 'differential-evolution']), default='none')
@click.option('--zmq-bind')
@click.option('--queue-size', default=10)
@click.option('--susuc-bol-path')
def main(susuc_bol_path, method, zmq_bind, queue_size):
    # load bolometric LC
    lc = load_lc()
    print(int_lc(lc.index, lc))
    
    if not zmq_bind:
        susuc_path = ('proc', susuc_bol_path)
    else:
        context = zmq.Context.instance()
        socket = context.socket(zmq.DEALER)
        socket.bind(zmq_bind)

        socket_int = context.socket(zmq.ROUTER)
        socket_int.bind('inproc://#1')
        def int_proxy():
            zmq.proxy(socket_int, socket)
        thread = threading.Thread(target=int_proxy, daemon=True)
        thread.start()
        
        susuc_path = ('zmq', 'inproc://#1')
    
    if method == 'none':
        x = np.array([ 2.62303634, 11.90951209, -5.10122928,  0.29513243,  5.        ])
    elif method == 'nested':
        x = dynesty_run(lc, susuc_path, queue_size=queue_size)
    elif method == 'nested-dynamic':
        x = dynesty_dynamic_run(lc, susuc_path, queue_size=queue_size)
    elif method == 'differential-evolution':
        x = fit(lc, susuc_path)
    elif method == 'emcee':
        x = fit_emcee(lc, susuc_path, queue_size=queue_size)
    else:
        # shouldnt happen anyway (click shouldnt allow it)
        raise ValueError('Unsupported method %s' % method)
    
    print('Fit result: %s' % repr(x))
    ejMass = 1.4
    if len(x) > 0:
        ejMass = x[0]
    ejVelocity = 3.06649615e+04
    if len(x) > 1:
        ejVelocity = x[1]
    nickelFraction = 4.30459341e-02
    if len(x) > 2:
        nickelFraction = x[2]
    optical_opacity = 0.2
    if len(x) > 3:
        optical_opacity = x[3]
    gamma_opacity = 5.0
    if len(x) > 4:
        gamma_opacity = x[4]
    
    a = run_susuc(ejMass, ejVelocity, optical_opacity, gamma_opacity, nickelFraction, susuc_path=susuc_path)
    phase, lum = readjust_peak(a)
    #plt.plot(a[:,0]/3600/24, a[:,1], label='SUSUC')
    plt.figure()
    plt.plot(lc, label='19odp')
    plt.plot(phase, lum, label='SUSUC')
    for optical_opacity in [0.1, 0.2, 0.3, 0.5]:
        a = run_susuc(ejMass, ejVelocity, np.log(optical_opacity), gamma_opacity, nickelFraction, susuc_path=susuc_path)
        phase, lum = readjust_peak(a)
        plt.plot(phase, lum, label='Optical Opacity = %f' % optical_opacity)
    #for ejMass in [1,10,50]:
    #    for ejVelocity in [10e3, 30e3, 100e3]:
    #        a = run_susuc(ejMass, ejVelocity, 0.2, 10.0, 0.1, susuc_path=susuc_bol_path)
    #        plt.plot(a[:,0]/3600/24, a[:,1], label='ejMass=%d / ejVelocity=%.0f' % (ejMass, ejVelocity))
    plt.legend()
    plt.xlabel('MJD [d]')
    plt.show()




if __name__ == '__main__':
    main()
