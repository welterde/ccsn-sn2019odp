#!/usr/bin/env python3
import os
import click
import numpy as np
import h5py
import astropy.io.fits as fits
import astropy.time as time
from photutils.aperture import CircularAperture, CircularAnnulus, aperture_photometry
from photutils.detection import DAOStarFinder
from astropy.stats import sigma_clipped_stats

def load_image(fname):
    hdul = fits.open(fname)
    hdul.info()
    return hdul[0].data


def sigma_clip_background(diff_data, noise_data, aperture_size, pixelscale):
    pos = diff_data.shape[0]/2, diff_data.shape[1]/2

    radius = aperture_size / pixelscale
    aperture = CircularAperture([pos], r=radius)
    annulus_aperture = CircularAnnulus([pos], r_in=(aperture_size+1)/pixelscale, r_out=(aperture_size+4)/pixelscale)

    annulus_masks = annulus_aperture.to_mask(method='center')
    annulus_data = annulus_masks[0].multiply(diff_data)
    mask = annulus_masks[0].data
    annulus_data_1d = annulus_data[mask > 0]
    print(annulus_data_1d.shape)
    mean_sigclip, median_sigclip, std_sigclip = sigma_clipped_stats(annulus_data_1d)

    background = median_sigclip
    background_err = std_sigclip

    return background, background_err


def measure(diff_data, noise_data, aperture_size, pixelscale, zeropoint):
    pos = diff_data.shape[0]/2, diff_data.shape[1]/2

    radius = aperture_size / pixelscale
    aperture = CircularAperture([pos], r=radius)
    annulus_aperture = CircularAnnulus([pos], r_in=(aperture_size+1)/pixelscale, r_out=(aperture_size+4)/pixelscale)
    phot_table = aperture_photometry(diff_data, [aperture, annulus_aperture], error=noise_data)
    #print(phot_table)

    bkg_mean = phot_table['aperture_sum_1'] / annulus_aperture.area
    bkg_mean_err = phot_table['aperture_sum_err_1'] / annulus_aperture.area
    

    #bkg_mean, bkg_mean_err = sigma_clip_background(diff_data, noise_data, aperture_size, pixelscale)
    
    #print('Background: %f \pm %f' % (bkg_mean, bkg_mean_err))
    
    bkg_sum = bkg_mean * aperture.area
    bkg_err = bkg_mean_err * aperture.area
    final_sum = phot_table['aperture_sum_0'] - bkg_sum
    final_err = np.sqrt(phot_table['aperture_sum_err_0']**2 + bkg_err**2)

    print('Background[aperture=%.1f]: %f \pm %f' % (aperture_size, qbkg_mean, bkg_mean_err))
    
    mag = -2.5*np.log10(final_sum[0]) + zeropoint
    mag_err = 2.5/np.log(10) * final_err[0]/final_sum[0]
    snr = final_sum[0] / (final_err[0])

    return mag, mag_err, snr


def curve_of_growth(diff_data, noise_data, pixelscale, zeropoint, aperture_min, aperture_max):
    aperture = np.linspace(aperture_min, aperture_max)

    r_mag = np.empty_like(aperture)
    r_mag_err = np.empty_like(aperture)
    r_snr = np.empty_like(aperture)
    
    for i,aperture_size in enumerate(aperture):
        mag, mag_err, snr = measure(diff_data, noise_data, aperture_size, pixelscale, zeropoint)
        r_mag[i] = mag
        r_mag_err[i] = mag_err
        r_snr[i] = snr

    return aperture, r_mag, r_mag_err, r_snr


@click.command()
@click.option('--src-img-hdr-extn', default=0)
@click.option('--template-img-hdr-extn', default=0)
@click.option('--aperture-size', default=1.6)
@click.option('--pixelscale', default=0.2142)
@click.option('--band')
@click.option('--plot-cog', is_flag=True)
@click.option('--use-cog', is_flag=True)
@click.argument('src_img')
@click.argument('template_img')
@click.argument('diff_img')
@click.argument('noise_img')
@click.argument('destfile')
def main(src_img, template_img, diff_img, noise_img, destfile, src_img_hdr_extn, template_img_hdr_extn, pixelscale, aperture_size, band, plot_cog, use_cog):
    src_fname = os.path.basename(src_img)
    # 1) load neccessary metadata from the src image
    hdr_src = fits.getheader(src_img, ext=src_img_hdr_extn)
    hdr_template = fits.getheader(template_img, ext=template_img_hdr_extn)
    
    # read zeropoint from template image
    zeropoint = hdr_template['HIERARCH FPA.ZP'] + 2.5*np.log10(hdr_template['EXPTIME'])

    # mjd of the observations
    mjd = time.Time(hdr_src['DATE-OBS']).mjd

    # 2) load the difference image and the noise image
    diff_data = load_image(diff_img)
    noise_data = load_image(noise_img)
    
    # 3) Perform aperture photometry on the difference image

    if plot_cog:
        aperture, mags, mag_err, snr = curve_of_growth(diff_data, noise_data, pixelscale, zeropoint, aperture_min=0.5, aperture_max=3)
        import matplotlib.pyplot as plt
        plt.errorbar(aperture, mags, yerr=mag_err)
        idx = np.argmin(mag_err)
        plt.axvline(aperture[idx])
        #plt.plot(aperture, snr)
        plt.show()
        return
    if use_cog:
        aperture, mags, mag_errs, snrs = curve_of_growth(diff_data, noise_data, pixelscale, zeropoint, aperture_min=1.0, aperture_max=3)
        mag_errs[mag_errs <= 0] = 1e3
        idx = np.argmin(mag_errs)
        mag = mags[idx]
        mag_err = mag_errs[idx]
        snr = snrs[idx]
        print('Using aperture=%.1f' % aperture[idx])
    else:
        mag, mag_err, snr = measure(diff_data, noise_data, aperture_size, pixelscale, zeropoint)
    print('Magnitude Estimate: %f \pm %f (SNR %f)' % (mag, mag_err, snr))
    
    # 4) write result to row in the destination file
    f = h5py.File(destfile, mode='a')
    # create the table if it doesnt exist yet
    if 'lc' not in f:
        lc_dt = np.dtype({'names': ['mjd', 'band', 'mag', 'mag_err', 'file_name'],
                          'formats': ['f8', 'S4', 'f8', 'f8', 'S40']})
        f.create_dataset('lc', (0,), dtype=lc_dt, maxshape=(None,))
        print(f['lc'])
        
    print('Band', band)
    entry = (mjd, band, mag, mag_err, src_fname.encode('utf-8'))
    
    ds = f['lc']
    if ds.shape is not None:
        print(ds['file_name'])
        idx = ds['file_name'] == src_fname.encode('utf-8')
    else:
        idx = np.zeros(0, dtype=np.bool)
    if np.count_nonzero(idx) == 0:
        cur_len = 0
        if ds.shape is not None:
            cur_len = ds.shape[0]
        ds.resize(cur_len+1, axis=0)
        idx = -1
    ds[idx] = entry
                         
    f.close()




if __name__ == '__main__':
    main()
    
