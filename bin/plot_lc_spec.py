import re
import click
from datetime import datetime
import astropy.time as time
import astropy.table as table
import astropy.units as u
import astropy.constants as const
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


FILE_RE = re.compile(r'^.*_(\d+)_.*$')



def extract_time(fname):
    m = FILE_RE.match(fname)
    dat = m.groups(1)[0]
    #print(dat)
    d2 = datetime.strptime(dat, '%Y%m%d')
    #print(d2)
    #print(Time(d2).mjd)
    return time.Time(d2).mjd



@click.command()
@click.option('--phot-filter', default='r')
@click.argument('photfile')
@click.argument('specfile', nargs=-1)
def main(photfile, specfile, phot_filter):
    phot = pd.read_hdf(photfile).dropna()
    phot_sel = phot[phot.index.get_level_values('filter') == '"%s"' % phot_filter]

    fig, axs = plt.subplots(2)
    axs[0].plot(phot_sel.index.get_level_values('mjd'), phot_sel['mag'], 'o')
    axs[0].set_xlabel('MJD [d]')
    axs[0].set_ylabel('Mag')
    axs[0].invert_yaxis()

    axs[1].set_xlabel('Wavelength [A]')
    
    for fname in specfile:
        mjd = extract_time(fname)
        spec = table.Table.read(fname, format='ascii')
        wlen = spec['wavelength']
        flux = spec['flux']

        axs[1].plot(wlen, flux, label='%d' % mjd)
        axs[0].axvline(x=mjd, label='%d' % mjd)

    axs[0].legend()
    axs[1].legend()
        
    plt.show()


if __name__ == '__main__':
    main()
