#!/usr/bin/env python
import re
import click
from datetime import datetime
import astropy.time as time
import astropy.table as table
import astropy.units as u
import astropy.constants as const
import numpy as np
import pandas as pd


FILE_RE = re.compile(r'^.*_(\d+)_.*$')


def extract_time(fname):
    m = FILE_RE.match(fname)
    dat = m.groups(1)[0]
    #print(dat)
    d2 = datetime.strptime(dat, '%Y%m%d')
    #print(d2)
    #print(Time(d2).mjd)
    return time.Time(d2).mjd


def load_filter_curve(instrument='ZTF', band='r'):
    if instrument == 'ZTF':
        dat = table.Table.read('const/filters/Palomar_ZTF.%s.dat' % band, format='ascii')
        return dat['col1'], dat['col2']
    

@click.command()
#@click.option('--phot-filter', default='r')
@click.option('--overwrite', is_flag=True)
@click.argument('photfile')
@click.argument('specfile_in')
@click.argument('specfile_out')
def main(photfile, specfile_in, specfile_out, overwrite):
    spec = table.Table.read(specfile_in, format='ascii')
    wlen = spec['col1']
    flux = spec['col2']

    phot_all = pd.read_hdf(photfile)#.dropna()
    
    dmags = []
    for phot_filter in 'gri':
        filter_wave, filter_response = load_filter_curve(band=phot_filter)
        filter_response[0] = 0
        filter_response[-1] = 0
        filter_response[filter_response < 0] = 0
        
        #filter_factor = np.log10(3.631e-20 * (const.c.cgs.value) * np.trapz(filter_response/filter_wave, filter_wave))
    
        

        phot = phot_all.dropna(subset=['mag_"%s"_ZTF' % phot_filter])
        
        mjd = extract_time(specfile_in)

        phot_mjd = phot.index
        phot_sel = phot['mag_"%s"_ZTF' % phot_filter]
        
        
        #print(phot_sel)
        dmjd_idx = ((phot_mjd - mjd)**2).argmin()
        mag = phot_sel.iloc[dmjd_idx]

        if np.abs(phot_mjd - mjd).min() > 8:
            print('Filter %s does not have good photometry for that time period' % phot_filter)
            continue
        
        spec_valid = ~np.isnan(flux)
        flux_fixed = np.interp(wlen, wlen[spec_valid], flux[spec_valid])
        
        # interpolate the filter response onto the observed spec grid
        filter_response_interp = np.interp(wlen, filter_wave, filter_response)
        
        #mphot = np.log10(ZTF_ZEROPOINTS[phot_filter]) - 0.4*mag
        
        pivot_factor2 = np.trapz(wlen*filter_response_interp, wlen)/np.trapz(filter_response_interp/wlen, wlen)
        
        spec_flux = np.trapz(wlen*flux_fixed*filter_response_interp, wlen)/np.trapz(wlen*filter_response_interp, wlen)
        
        mint = -2.5*np.log10(spec_flux) - 2.5*np.log10(pivot_factor2/(const.c.to(u.Angstrom/u.s).value)) - 48.6
        
        #fint = np.log10(np.trapz(wlen*flux*filter_response_interp, wlen))
        #mint = np.log10(np.trapz(flux_fixed*filter_response_interp, wlen))
        
        print('Computed spectroscopy mag: %f' % mint)
        print('Spec sum: %e' % np.sum(flux_fixed))

        #print(mag)
        #print(fint)
        #print(filter_factor)
        #dmag = mag + fint + filter_factor
        dmag = (mint - mag)/2.5
        #print(dmag)
        print('Computed correction factor %s-band: %f' % (phot_filter, dmag))
        dmags.append(dmag)

    if len(dmags) == 0:
        dmag = 0
        print('Skipping flux-calibration because of missing photometry!')
    else:
        dmag = np.mean(dmags)

        print('Mean Correction Factor: %f' % np.mean(dmags))
        print('Min/Max/Std: %f %f %f' % (np.min(dmags), np.max(dmags), np.std(dmags)))
    
    new_flux = flux * 10 ** dmag

    new_spec = table.Table([wlen, new_flux], names=['wavelength', 'flux'])
    new_spec.write(specfile_out, format='ascii.commented_header', overwrite=overwrite)



if __name__ == '__main__':
    main()
