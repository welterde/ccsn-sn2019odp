#!/usr/bin/env python3
import click
import sys
import speclite
import speclite.filters
import numpy as np
import astropy.units as u
from astropy.table import Table
import json as jsonlib
import numba
import tqdm
from scipy import optimize
import matplotlib.pyplot as plt
from astropy.cosmology import Planck15 as cosmology

import dynesty
from dynesty import plotting as dyplot
from dynesty import utils as dyfunc
import pandas as pd

try:
    import snelib19odp
except ImportError:
    sys.path.append('lib')
import snelib19odp.blackbody as blackbody
import snelib19odp.const as sneconst
import time
import astropy.constants as aconst


peak_mjd = sneconst.sne_peak_mjd['g']




# ZTF filters
def load_filter(inst, band):
    data = Table.read('const/filters/%s.%s.dat' % (inst, band), format='ascii')
    wave = []
    transmission = []
    for entry in data:
        if len(wave) > 0 and wave[-1] == entry['col1']:
            continue
        wave.append(entry['col1'])
        if entry['col2'] < 0.01 or entry['col1'] < 3790:
            transmission.append(0.0)
        else:
            transmission.append(entry['col2'])
    wave = np.array(wave)*u.Angstrom
    transmission = np.array(transmission)

    f = speclite.filters.FilterResponse(wavelength=wave, response=transmission, meta=dict(group_name='ZTF', band_name=band))
    #print(f)
    return f


FILTERS = {
    'ZTF_g': load_filter('Palomar_ZTF', 'g'),
    'ZTF_r': load_filter('Palomar_ZTF', 'r'),
    'ZTF_i': load_filter('Palomar_ZTF', 'i')
}
for band in 'grizJHK':
    FILTERS['GROND_' + band] = load_filter('LaSilla_GROND', band)


WAVS = np.arange(3500, 25000, 1)*1.0

#@numba.njit
def likelihood(p, obs_mags, obs_mags_errs, filter_curves, distance_cm):
    temp, ln_radius = p

    sed = blackbody.bb(temp, np.exp(ln_radius), WAVS)
    syn_mags = []
    for f in filter_curves:
        syn_mags.append(f.get_ab_magnitude(sed/4/np.pi/distance_cm**2, WAVS*u.Angstrom))
    #print(syn_mags)
    #return
    sigma2 = obs_mags_errs ** 2
    return -0.5 * np.sum((syn_mags - obs_mags)**2 / sigma2)


@numba.njit
def prior_transform(u):
    x = np.empty(2)
    x[0] = 9e4*u[0] + 1000
    x[1] = 30*u[1]+50
    return x



def dynesty_fit(mags, mags_err, filter_curves, distance_cm):
    args = (mags, mags_err, filter_curves, distance_cm)
    sampler = dynesty.NestedSampler(likelihood, prior_transform, 2, logl_args=args)
    sampler.run_nested()
    results = sampler.results

    samples, weights = results.samples, np.exp(results.logwt - results.logz[-1])
    mean, cov = dyfunc.mean_and_cov(samples, weights)
    print('\tMean: %s' % repr(mean))
    print('\n\t\t'.join(('\tCov: %s' % repr(cov)).split('\n')))

    new_samples = dyfunc.resample_equal(samples, weights)
    temp_q = np.quantile(new_samples[:,0], [0.05, 0.5, 0.95])
    print('\tQuantiles T[K] : %s' % temp_q)
    norm_q = np.quantile(new_samples[:,1], [0.05, 0.5, 0.95])
    print('\tQuantiles log N: %s' % norm_q)

    # calculate luminosity
    samples_temp = new_samples[:,0]
    samples_norm = new_samples[:,1]
    samples_radius = np.sqrt(np.exp(samples_norm)/4/np.pi/np.pi)
    samples_l = 4*np.pi*(aconst.sigma_sb.cgs.value)*samples_radius**2 * samples_temp**4
    l_q = np.quantile(np.log10(samples_l), [0.05, 0.5, 0.95])

    print('\tQuantiles log L: %s' % l_q)

    #dyplot.cornerplot(results, labels=['T', 'ln R'], show_titles=True)
    #plt.show()

    return temp_q, np.sqrt(norm_q/4/np.pi**2), l_q


@click.command()
#@click.argument('phot_file')
def main():
    flux = 3631/WAVS**2/3.34e4
    
    #print(FILTERS['ZTF_r'].get_ab_magnitude(flux, WAVS))
    #print(blackbody.bb(1000, 1, WAVS))
    #sed = blackbody.bb(1000, 1e12, WAVS)
    #redshift = 0.014353
    #r = cosmology.luminosity_distance(redshift).to(u.cm).value
    #print(FILTERS['ZTF_r'].get_ab_magnitude(sed/4/np.pi/r**2*u.erg/u.s/u.cm/u.cm/u.Angstrom, WAVS*u.Angstrom))
    
    
    #sys.exit(1)
    # load photometry
    #phot = Table.read('data/grond_lc_v3.fits')
    lc = pd.read_hdf('products/lc_interpolated_per_ins.h5')
    lc['mjd'] = lc.index

    redshift = 0.014353
    dist = cosmology.luminosity_distance(redshift).to(u.pc).value
    dist_mod = 5*np.log10(dist) - 5
    distance_cm = cosmology.luminosity_distance(redshift).to(u.cm).value
    #print(dist_mod)
    #sys.exit(1)

    #
    temps_q5 = []
    temps_q50 = []
    temps_q95 = []
    radii_q5 = []
    radii_q50 = []
    radii_q95 = []
    lums_q5 = []
    lums_q50 = []
    lums_q95 = []
    phases = []
    mjds = []
    
    for i in range(len(lc)):
    #for row in phot:
        row = lc.iloc[i]
        #print(row.index)
        phase = row['mjd'] - peak_mjd
        
        if phase > 30:
            continue
        mjds.append(row['mjd'])
        phases.append(phase)
        #
        #row = phot[i]
        mags = [(row['mag_"%s"_ZTF' % band]) for band in 'gr']
        mags_err = [row['magerr_"%s"_ZTF' % band] for band in 'gr']
        #mags = [row['mag_%s' % band] for band in 'grizJHK']
        #mags_err = [row['magerr_%s' % band] for band in 'grizJHK']
        filter_curves = [FILTERS['ZTF_' + band] for band in 'gr']
        #filter_curves = [FILTERS['GROND_' + band] for band in 'grizJHK']
        temp, radius, lums = dynesty_fit(np.array(mags), np.array(mags_err), filter_curves, distance_cm)
        temps_q5.append(temp[0])
        temps_q50.append(temp[1])
        temps_q95.append(temp[2])
        radii_q5.append(radius[0])
        radii_q50.append(radius[1])
        radii_q95.append(radius[2])
        lums_q5.append(lums[0])
        lums_q50.append(lums[1])
        lums_q95.append(lums[2])

    t = Table({'mjd': mjds, 'phase': phases, 'temp_q5': temps_q5, 'temp_q50': temps_q50, 'temp_q95': temps_q95, 'radius_q5': radii_q5, 'radius_q50': radii_q50, 'radius_q95': radii_q95, 'lum_q5': lums_q5, 'lum_q95': lums_q95, 'lum_q50': lums_q50})
    t.pprint()

    x = int(time.time())
    fname = 'playground/2021-05-25-phot-bbfit-%d.fits' % x
    
    t.write(fname)
        #

        #print(mags)
        #temps, radii = mc_diff_evo(mags, mags_err, filter_curves, nsamples=100)

        #print('%f\t%f\%f\t%f\%f' % (row['mjd'], np.mean(temps), np.std(temps), np.mean(radii), np.std(radii)))
        
        #print('temp', np.mean(temps), np.std(temps))
        #print('radii', np.mean(radii), np.std(radii))

    
    #sed = bb(5000, 1e14, WAVS)/1e42
    #print(sed)
    #plt.plot(WAVS, sed)
    #sed = bb(6000, 1.1e14, WAVS)
    #plt.plot(WAVS, sed)
    #plt.show()
    #print(filter_curves[0].get_ab_magnitude(sed, WAVS))
    


if __name__ == '__main__':
    main()
