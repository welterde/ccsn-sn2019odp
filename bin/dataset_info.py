#!/usr/bin/env python3
import click, sys, os, logging


try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../lib'))
import snelib19odp.specds as specds
import snelib19odp.dataset as dataset




@click.command()
@click.argument('cmd', type=click.Choice(['rev']))
@click.argument('dataset_type', type=click.Choice(['phot', 'spec']))
@click.argument('dataset_name')
def main(cmd, dataset_type, dataset_name):
    # load dataset
    if dataset_type == 'phot':
        ds = dataset.load_dataset(dataset_name)
    else:
        ds = specds.load_dataset(dataset_name)

    if cmd == 'rev':
        print(ds.rev)
    
    


if __name__ == '__main__':
    main()

