#!/usr/bin/env python
import click, os, sys
import astropy.table as table

import snelib19odp.phot_sed as phot_sed




@click.command()
@click.argument('lcfile')
def main(lcfile):
    lc = table.Table.read(lcfile).to_pandas().set_index('mjd')

    phot_sed.mc_flux_from_phot(lc, bands='grizJHK')
    





if __name__ == '__main__':
    main()
