import click, glob, re, sys, os, collections, time
import matplotlib
matplotlib.use('Agg')
import pandas as pd
import numpy as np
import astropy.table as table
import astropy.units as u
from dynesty import utils as dyfunc

try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../lib'))
import snelib19odp.const as const
import snelib19odp.analysis.oxygen_lum as oxygen_lum
import snelib19odp.transient as transientlib
import snelib19odp.utils as utils

CONFIG_DIR = os.path.join(os.path.dirname(__file__), '../config')
PRODUCTS_DIR = os.path.join(os.path.dirname(__file__), '../products')

MPC_TO_CM = u.Mpc.to(u.cm)


def load_trace_data(input_file):
    # in case the input_file is actually a directory load the "newest" file
    if os.path.isdir(input_file):
        files = glob.glob(os.path.join(input_file, "*.fits"))
        if len(files) == 0:
            return None
        input_file = sorted(files)[-1]

    return table.Table.read(input_file), input_file





@click.command()
@click.option('--debug', is_flag=True)
@click.option('--transient')
@click.option('--dest-dir')
@click.option('--dest-file')
@click.option('--dist-range', type=float, nargs=2)
@click.option('--dist-gaussian', is_flag=True)
@click.option('--oxygen-mass-prior', type=float, nargs=2)
@click.option('--temp-prior', type=float, nargs=2)
@click.option('--nlte-prior', type=float)
@click.option('--additive-shell', is_flag=True)
@click.option('--print-results', is_flag=True)
@click.option('--print-lums', is_flag=True)
@click.option('--target-samples', type=int)
@click.option('--nlive', type=int)
@click.argument('input_file')
def main(debug, transient, input_file, dest_dir, oxygen_mass_prior, temp_prior, dist_range, dist_gaussian, dest_file, nlte_prior, additive_shell, print_results, print_lums, target_samples, nlive):
    # for now hardcoded
    kwargs = {'mode': 'mv_normal', 'additive_shell': additive_shell}
    
    # set the dist_range prior
    if transient:
        trans = transientlib.load_transient(transient)
        if dist_gaussian:
            kwargs['dist_gaussian'] = True
            kwargs['dist_range'] = (trans.dist_mpc*MPC_TO_CM, trans.dist_mpc_err*MPC_TO_CM)
        else:
            kwargs['dist_gaussian'] = False
            kwargs['dist_range'] = ((trans.dist_mpc-trans.dist_mpc_err)*MPC_TO_CM, (trans.dist_mpc+trans.dist_mpc_err)*MPC_TO_CM)
    elif dist_range:
        if dist_gaussian:
            kwargs['dist_gaussian'] = True
        else:
            kwargs['dist_gaussian'] = False
        kwargs['dist_range'] = (dist_range[0]*MPC_TO_CM, dist_range[1]*MPC_TO_CM)
    else:
        sys.stderr.write('Distance range required (either --transient or --dist-range)!\n')
        sys.stderr.flush()
        sys.exit(1)

    if oxygen_mass_prior:
        kwargs['oxygen_mass_min'], kwargs['oxygen_mass_max'] = oxygen_mass_prior

    if temp_prior:
        kwargs['temp_min'], kwargs['temp_max'] = temp_prior

    if nlte_prior:
        kwargs['nlte_departure_max'] = nlte_prior

    if nlive:
        kwargs['dynesty_nlive_init'] = nlive
    
    # load the trace data
    lum_trace, input_file = load_trace_data(input_file)
    kwargs['line_profile_fit_result'] = lum_trace

    if print_lums:
        # we get the full monty.. we need to resample it using the weights
        samples = np.column_stack((lum_trace['log_F6300'].data, lum_trace['R6364'].data, lum_trace['R5577'].data))
        weights = lum_trace['weights']
        new_samples = dyfunc.resample_equal(samples, weights)
        
        if kwargs['dist_gaussian']:
            dist_dist = np.random.normal(kwargs['dist_range'][0], kwargs['dist_range'][1], size=len(lum_trace))
        else:
            dist_dist = np.random.uniform(kwargs['dist_range'][0], kwargs['dist_range'][1], size=len(lum_trace))

        flux_6300 = np.exp(new_samples[:,0])
        flux_6364 = flux_6300 * new_samples[:,1]
        flux_5577 = flux_6300 * new_samples[:,2]
        #flux_6300 = np.exp(lum_trace['log_F6300'])
        #flux_6364 = flux_6300 * lum_trace['R6364']
        #flux_5577 = flux_6300 * lum_trace['R5577']

        lum_6300 = utils.flux2luminosity(flux_6300, dist_dist)/1e38
        lum_6364 = utils.flux2luminosity(flux_6364, dist_dist)/1e38
        lum_5577 = utils.flux2luminosity(flux_5577, dist_dist)/1e38

        print('Label\tQ15\tQ50\tQ84\tNegErr\tPosErr\tMin')
        for label, lum in [('5577', lum_5577), ('6300', lum_6300), ('6364', lum_6364)]:
            p = np.percentile(lum, [15, 50, 84, 0.1])
            print(f"{label}\t{p[0]:.3e}\t{p[1]:.3e}\t{p[2]:.3e}\t{p[1]-p[0]:.2e}\t{p[2]-p[0]:.2e}\tMin={p[3]:.2e}")
        sys.exit(0)
        

    fit_setup = oxygen_lum.make_setup(**kwargs)

    result, trace, sampler = oxygen_lum.run_dynesty_fit(fit_setup, target_samples=target_samples)

    weights = np.exp(result.logwt - result.logz[-1])
    mean, cov = dyfunc.mean_and_cov(result.samples[:,(0,1,3,4)], weights)
    print('Mean: ', mean)
    print('Cov: ', cov)

    if print_results:
        y = table.Table(rows=trace, names=oxygen_lum.LABELS)
        for label in oxygen_lum.LABELS:
            p = np.percentile(y[label], [15, 50, 84, 0.1])
            print(f"{label}\t{p[0]:.3e}\t{p[1]:.3e}\t{p[2]:.3e}\t{p[1]-p[0]:.2e}\t{p[2]-p[0]:.2e}\tMin={p[3]:.2e}")

    if dest_dir:
        dest_file = os.path.join(dest_dir, os.path.basename(input_file))
    elif not dest_file:
        dest_file = os.path.join(PRODUCTS_DIR, 'oxygen_phys', lum_trace.meta['SPEC_NAME'], lum_trace.meta['MODEL_NAME'], os.path.basename(input_file))

    dest_dir = os.path.dirname(dest_file)
    if not os.path.isdir(dest_dir):
        os.makedirs(dest_dir)
        
    x = table.Table(rows=trace, names=oxygen_lum.LABELS)
    #x['weights'] = np.exp(result.logwt - result.logz[-1])
    x.write(dest_file, format='fits', overwrite=True)
    

if __name__ == '__main__':
    main()
