(("iPTF13bvn"
  (load:json "data/comparison/iPTF13bvn.json")
  (filters
   ((match:key "telescope" "P48")
    (set:system_prefix "SDSS ")
    )
   ((match:key "telescope" "P60")
    (set:system_prefix "SDSS ")
    )
   ((match:unmatched)
    (delete)
    )
   )
					; MW extinction
  (extinction
   (ebv:uniform 0.0421 0.0448)
   )
					; host extinction
  (extinction
   (ebv:gaussian 0.08 0.07)
   )

					; Distance taken from NED
  (distance
   (Mpc:gaussian 26.8 2.6)
   )
  )

 ("SN2008D"
  (load:json "data/comparison/SN2008D.json")
  (filters
   ((match:key "telescope" "CfA3_KEP")
    (match:key "band" "r'" "i'")
    (set:system_prefix "SDSS ")
    )
   ((match:key "telescope" "CfA3_KEP")
    (match:key "band" "U" "B" "V")
    (set:system_prefix "Landolt ")
    )
   ((match:unmatched)
    (delete)
    )
   )

					; MW extinction
  (extinction
   (ebv:uniform 0.0193 0.0002)
   )
					; host extinction
  (extinction
   (ebv:gaussian 0.6 0.1)
   )

					; taken from NED
  (distance
   (Mpc:gaussian 33.69 2.36)
   )
  )
  
 ("SN1998bw"
  (load:table "data/comparison/SN1998bw-Clocchiatti-2011-table2.h5")
  (table:transpose
   (bands "U" "B" "V" "Rc" "Ic")
   (columns
					; stupid non-jd or mjd format..
    (mjd "JD" (offset 50000.5))
    (mag "%smag")
    (mag_err "e_%smag")
    )
   )
   
   
  (filters
   ((match:key "band" "U" "B" "V" "Rc" "Ic")
    (set:system_prefix "CTIO ")
    )
   ((match:unmatched)
    (delete)
    )
   )

  (distance
   (Mpc:gaussian 38 3)
   )

  )

 ("SN2002ap"
  (load:json "data/comparison/SN2002ap.json")
  (filters
   ((match:key "band" "U" "B" "V" "R" "I")
    (set:system_prefix "CTIO ")
    )
   ((match:unmatched)
    (delete)
    )
   )

  (distance
   (Mpc:gaussian 9 4)
   )
  )
   
	     

 
 )
