

# Description of Directories

data/  Contains the raw (or as raw as possible) input data before any paper-specific processing steps are undertaken.
const/ Contains globally assumed constants such as filter curves, distances or extinction values (in case values change over time we use sub-directories per constant with the current symlink pointing to the currently used value).

# Description of the build system

Build.hs is based on the haskell shake build system.



# Lightcurve Data Flow

1) Compute qbol lightcurve -> products/qbol/<method>.fits
2) - Fit model products -> products/models/<model>/...
   - Model code goes into lib/snelib19odp/models/<model>.py ?
   - Or perhaps into models/<model>/...

# Figures

* fig. 1: Finder Chart

  - script: [plots/supernova_field.py](plots/supernova_field.py)

* fig. 2: Overview of the observed multi-color LC

  - Script: [plots/lc/overview.py](plots/lc/overview.py)

* fig. 3: Zoom onto early phase of multi-color LC

  - Script: [plots/lc/overview.py](plots/lc/overview.py)
  - Option: ``--early``

* fig. 4-7: Spectral sequence (split into several distinct phases of the supernova)

  - Script: [plots/spec/seq.py](plots/spec/seq.py)
  - Configuration files:
    + [Plateau Phase](config/plots/seq/plateau.scm)
    + [Main Peak](config/plots/seq/photospheric.scm)
    + [Pre-nebular phase](config/plots/seq/prenebular.scm)
    + [Nebular phase](config/plots/seq/nebular.scm)

* fig. 8: LC Comparison with comparison objects

  - Script: [plots/lc_abs_comparison.py](plots/lc_abs_comparison.py)

* fig. 9: Color Evolution Comparison

  - Script: [plots/lc/color_evo.py](plots/lc/color_evo.py)

* fig. 10: BB-Parameter time-evolution comparison

  - Script: [plots/photbb.py](plots/photbb.py)

* fig. 11: Spectral comparison geared towards change of spectral class

  - Script: [plots/classification_change.py](plots/classification_change.py)

* fig. 12: Velocity Evolution of HeI absorption lines

  - Script: [plots/spec/absline.py](plots/spec/absline.py)
  - Fitting Script: [bin/fit_abs_line.py](bin/fit_abs_line.py)

* fig. 14: Posterior distribution of oxygen analysis

  - Script: [plots/spec/oxygen_model.py](plots/spec/oxygen_model.py)
  - Fitting Scripts:
    + Stage 1: [bin/run_spec_model.py](bin/run_spec_model.py)
    + Stage 2: [bin/run_oxygen_fit.py](bin/run_oxygen_fit.py)

* fig. C.1: Oxygen Mass Sensitivity on NLTE prior

* fig. C.2 - C.4: Oxygen Spectral Fit Diagnostics

  - Script: [plots/spec/spec_model_diag.py](plots/spec/spec_model_diag.py)

* fig. C.5 - C.7: Oxygen Spectral Fit Corner Plots

  - Script: [plots/spec/spec_model_diag.py](plots/spec/spec_model_diag.py)
  - Option: ``--corner``

* fig. C.8 - C.10: Oxygen Analysis Corner Plots

  - Script: [plots/spec/oxygen_model.py](plots/spec/oxygen_model.py)
  - Option: ``--corner``
  - Uses same input files as fig. 9

* fig. D.1: BB Parameter Filter Sensitivity Comparison

  - Notebook: [playground/2022-07-29-phot-new-bbfits.ipynb](playground/2022-07-29-phot-new-bbfits.ipynb)


# Tables

* tab. 1: List of photometric observations

  - Script: [bin/print_phot_tbl.py](bin/print_phot_tbl.py)
  - Some manual edits mostly related to caption/header

* tab. 2: Observation log of spectroscopic observations

  - Script: [bin/print_spec_obs_tbl.py](bin/print_spec_obs_tbl.py)
  - Some manual edits to fix issues with some informative headers

* tab. 3: Adopted comparison transient parameters

  - Manually created

* tab. 4: Basic observables for SN2019odp and comparison transients

  - Script: [tables/lc_observables.py](tables/lc_observables.py)
  - Some manual edits to fix Latex issues and sign-errors in the upper/lower-limit logic

* tab. 5: Arnett Model Parameter Estimates

  - Manually summarized from the model result files







* tab. 6: Physical constants for oxygen analysis

  - Manually created

* tab. 7: Priors for oxygen mass estimation

  - Manually created

* tab. 8: Inferred oxygen line luminosities

  - Manually created

* tab. 9: Overview of used photometric systems

  - Manually created

* tab. A.1: Nuclear decay constants

  - Manually created

* tab. A.2: Priors for Arnett model

  - Manually created

* tab. B.1: List of used analytic LC models as mean kernel of GP interpolation

  - Manually created
