{ buildPythonPackage
, lib
, pythonOlder
, fetchPypi
, numpy
, scipy
, pybind11
, enum34
}:

buildPythonPackage rec {
  pname = "george";
  version = "0.4.0";

  src = fetchPypi {
    inherit pname version;
    sha256 = "sha256-PIgwS0qBbEGwEZjAhOMQe6ewq0OIHGDSSYKesjISRbE=";
  };

  propagatedBuildInputs = [
    pybind11
    numpy
    scipy
  ];

  # checks use bazel; should be revisited
  doCheck = false;

  meta = {
    description = "Fast and flexible Gaussian Process regression in Python";
    homepage = "https://github.com/dfm/george";
    license = lib.licenses.mit;
    maintainers = with lib.maintainers; [ ];
  };
}