{ buildPythonPackage
, lib
, pythonOlder
, fetchPypi
, six
, scipy
, numpy
, matplotlib
, enum34
}:

buildPythonPackage rec {
  pname = "dynesty";
  version = "1.0.1";

  src = fetchPypi {
    inherit pname version;
    sha256 = "sha256-3a6uNdNZq7TLscJ3UArKtVtU70+xSTLKrLS4pvcZ968=";
  };

  propagatedBuildInputs = [
    numpy
    scipy
    matplotlib
  ];

  # checks use bazel; should be revisited
  doCheck = false;

  meta = {
    description = "A Dynamic Nested Sampling package for computing Bayesian posteriors and evidence";
    homepage = "https://github.com/joshspeagle/dynesty";
    license = lib.licenses.mit;
    maintainers = with lib.maintainers; [ ];
  };
}