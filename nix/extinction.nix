{ buildPythonPackage
, lib
, pythonOlder
, fetchPypi
, six
, numpy
, cython
, enum34
}:

buildPythonPackage rec {
  pname = "extinction";
  version = "0.4.6";

  src = fetchPypi {
    inherit pname version;
    sha256 = "sha256-0W3sE6BATgTDYx84Yo6zsjOsB96sKVASndE22o9+RDg=";
  };

  propagatedBuildInputs = [
    cython
    numpy
  ];

  # checks use bazel; should be revisited
  doCheck = false;

  meta = {
    description = "Fast interstellar dust extinction laws in Python";
    homepage = "https://github.com/kbarbary/extinction";
    license = lib.licenses.mit;
    maintainers = with lib.maintainers; [ ];
  };
}