#!/usr/bin/env python3
import click, h5py, numba, collections, tqdm, dynesty, pandas, os, sys
import numpy as np

import astropy.table as table
import astropy.time as time
from dynesty import plotting as dyplot
import matplotlib.pyplot as plt
import scipy.optimize as optimize
from scipy.interpolate import interp1d
from scipy.interpolate import splev, splrep
from numpy.lib import recfunctions as rfn
import extinction

try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../lib'))
import snelib19odp.dataloader as dataloader
import snelib19odp.spec as spectools
from snelib19odp.plot_tools import plot_spec



    

@click.command()
@click.option('--show', is_flag=True)
@click.option('-o', '--output')
def main(show, output):
    fig, axs = plt.subplots(1,2,figsize=(12,7))

    
    # load the two key comparison spectra from SN2019odp
    plateau = dataloader.load_obs_spec('ZTF19abqwtfu_20190823_NTT_v1.ascii', fluxcal=True)
    peak = dataloader.load_obs_spec('ZTF19abqwtfu_20191003_NOT_v1.ascii', fluxcal=True)

    # load the comparison objects
    # TODO: factor out this phase0 information into a global file in data/
    sn1998bw = dataloader.load_comparison_json_specs('SN1998bw.json', phase0=50946)
    sn2008d = dataloader.load_comparison_json_specs('SN2008D.json', phase0=54492)
    ptf13bvn = dataloader.load_comparison_json_specs('iPTF13bvn.json', phase0=56474)

    # Plateau subplot
    idx = 0
    plot_spec(axs[0], ptf13bvn[idx], label='PTF13bvn (Ib) %.0fd' % ptf13bvn[idx].meta['phase'], offset=1.5, blue_cut=4000)

    idx = 9
    plot_spec(axs[0], sn2008d[idx], label='SN2008D (Ib) %.0fd' % sn2008d[idx].meta['phase'], offset=+0.5, ebv_mw=0.6, blue_cut=4000)
    
    plot_spec(axs[0], plateau, label='SN2019odp -16d', ebv_mw=0.14)
    
    idx = 2
    plot_spec(axs[0], sn1998bw[idx], label='SN1998bw (Ic-BL) %.0fd' % sn1998bw[idx].meta['phase'], offset=-0.5)

    axs[0].legend()
    axs[0].set_ylabel('Normalized Flux + Offset')
    axs[0].set_xlabel(r'Rest Wavelength [$\AA$]')
    axs[0].set_title('Early Pre-Peak Phase')

    
    # Post-Peak subplot
    idx = 22
    plot_spec(axs[1], ptf13bvn[idx], label='PTF13bvn (Ib) %+.0fd' % ptf13bvn[idx].meta['phase'], offset=+1.2, scale=0.5)

    idx = 35
    plot_spec(axs[1], sn2008d[idx], label='SN2008D (Ib) %+.0fd' % sn2008d[idx].meta['phase'], offset=+0.8, ebv_mw=0.6, scale=0.5)
    
    plot_spec(axs[1], peak, label='SN2019odp +26d', ebv_mw=0.14, blue_cut=4000)
    idx = 17
    plot_spec(axs[1], sn1998bw[idx], label='SN1998bw (Ic-BL) %+.0fd' % sn1998bw[idx].meta['phase'], offset=-0.5)
    
    axs[1].legend()
    axs[1].set_xlabel(r'Rest Wavelength [$\AA$]')
    axs[1].set_title('Post Peak Phase')
                               

    if show:
        plt.show()
    else:
        fig.savefig(output)


if __name__ == '__main__':
    main()
