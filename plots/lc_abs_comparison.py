#!/usr/bin/env python3
import click, h5py, numba, collections, tqdm, dynesty, pandas, os, sys
import numpy as np

import astropy.table as table
import astropy.time as time
from dynesty import plotting as dyplot
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import matplotlib.ticker as ticker
import scipy.optimize as optimize
from scipy.interpolate import interp1d
from scipy.interpolate import splev, splrep
from numpy.lib import recfunctions as rfn
import extinction

try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../lib'))
import snelib19odp.dataloader as dataloader
import snelib19odp.const as const
import snelib19odp.lcproc as lcproc
import snelib19odp.dataset as dataset




O2C = {
    'SN2019odp': 'tab:blue',
    'SN2008D': 'tab:red',
    'iPTF13bvn': 'tab:pink',
    'SN2002ap': 'tab:brown',
    'SN1998bw': 'tab:orange'
}



@click.command()
@click.option('--show', is_flag=True)
@click.option('--phase-max', default=9999)
@click.option('--sample-number', default=30)
@click.option('-o', '--output')
def main(show, output, phase_max, sample_number):
    fig, axs = plt.subplots(1,3,figsize=(12,7),sharey=True)
    # re-enable the axis labels that were disabled by sharey
    for ax in axs:
        ax.tick_params(labelleft=True)
    for i, label in enumerate(('(A)', '(B)', '(C)')):
        ax = axs[i]
        ax.text(0.1, 1.05, label, transform=ax.transAxes,
                fontsize=16, fontweight='bold', va='top', ha='right')
    # set the tight layout engine so the text doesn't overlap the previous graph
    fig.set_tight_layout(True)
    ztflc = table.Table.read(os.path.join(os.path.dirname(__file__), '../data/ztflc_forcefit.h5'))
    ztflc['mag'] = -2.5*np.log10(ztflc['ampl']) + ztflc['magzp'] - 34
    ztflc.sort('obsmjd')
    ztflc['phase'] = ztflc['obsmjd'] - const.sne_peak_mjd['g']
    ztflc['band'] = np.array([x[-1] for x in ztflc['filter']])
    ztflc['mag_err'] = ztflc['ampl.err']/ztflc['ampl']*1.087 + ztflc['magzprms']
    ztflc = ztflc[ztflc['sigma'] > 3]
    ztflc = ztflc[ztflc['phase'] > -21]

    marshal = table.Table.read("data/marshal_raw_lc_v2.csv")
    print(marshal)
    idx1 = np.logical_and(marshal['instrument'] == 'P60+SEDM', marshal['magpsf'] < 99)
    marshal = marshal[idx1]
    marshal['phase'] = marshal['jdobs'] - 2400000.5 - const.sne_peak_mjd['g']
    marshal['mag'] = marshal['absmag']
    marshal['mag_err'] = marshal['sigmamagpsf']
    marshal['band'] = marshal['filter']
    print(marshal)


    lc_proc = lcproc.COMPARISON_PROCESSOR

    sn2019odp = dataset.load_dataset('SN2019odp_phot')
    ptf13bvn = dataset.load_dataset('iPTF13bvn')
    sn2008d = dataset.load_dataset('SN2008D')
    sn1998bw = dataset.load_dataset('SN1998bw')
    sn2002ap = dataset.load_dataset('SN2002ap')

    #bolometric_method = 'lyman'

    def plot_obj(ds, ax, band, dataname=None, offset=0, alpha=1.0, bolometric=False, bolometric_method='lyman', max_phase=120, label=None, color=None):
        if dataname is None:
            dataname = ds.transient.name
        if label is None:
            label = dataname
        if color is None:
            color = O2C[dataname]
        z_factor = 1/(1+ds.transient.redshift)
        
        ds.get_combined_lc(band)
        lc = ds.get_combined_lc(band)

        diff_factor = np.nanmean(lc['absmag']-lc['mag'])
        
        ds.get_interpolated(band)
        lcc = ds.get_interpolator(band).get_corrected_obs_dataset()
        t = lcc['mjd']
        yabs = lcc['mag'] + diff_factor
        yerr = lcc['mag_err']
        
        
        #lc['phase'] = lc['mjd'] - ds.transient.prior_t0
        #idx = lc['mag_err'] < 0.6
        idx = yerr < 0.6
        phase = t - ds.transient.prior_t0
        idx = np.logical_and(idx, phase < phase_max)
        #idx = np.logical_and(idx, lc['phase'] < 120)
        idx = np.logical_and(idx, phase < 120)
        if not bolometric:
            #ax.errorbar(lc['phase'][idx], lc['absmag'][idx], yerr=lc['mag_err'][idx], ls='', fmt='.', label='%s %s' % (dataname, band), alpha=alpha, color=O2C[dataname])
            ax.errorbar(phase[idx]*z_factor, yabs[idx], yerr=yerr[idx], ls='', fmt='.', label='%s %s' % (label, band), alpha=alpha, color=color)
        #else:
            #interpolator = ds.get_interpolator
            # TODO: estimate error on peak bolometric lc

        if bolometric:
            bol_t_min, bol_t_max = ds.get_bolometric(bolometric_method).validity_range
            t_min = bol_t_min
        else:
            #t_min = ds.get_combined_lc(band)['mjd'].min()
            t_min = ds.first_detection_mjd_band(band)

        # calculate the maximum phase for the interpolation grid to use
        # -> stop at the last photometry point in the max_phase window that is not a upper_limit
        lc_idx_sel = np.logical_and(~lc['upper_limit'], lc['mjd'] - ds.transient.prior_t0 < max_phase)
        t_max = lc['mjd'][lc_idx_sel].max()
            
        #t_grid = np.linspace(t_min, min(120, max_phase) + ds.transient.prior_t0, 280)
        t_grid = np.linspace(t_min, t_max, 280)
        sample_num = sample_number
        if bolometric:
            sample_num = sample_number*3
        
        

        if bolometric:
            peak_mags = np.empty(200)
            t_grid_peak = np.linspace(-5, 5, 50) + ds.transient.prior_t0
            print('Sampling peak magnitude')
            for i in range(200):
                peak_mags[i] = np.nanmin(ds.sample_abs_lc(method=bolometric_method, t_grid=t_grid_peak, tweaks=['correlated-error']))
            q32, q50, q68 = np.percentile(peak_mags, [32, 50, 68])
            
            print(dataname, q32, q50, q68)
            if phase_max >= 0:
                ax.errorbar(0, q50, yerr=[[q50-q32], [q68-q50]], color=color, label=label, marker='o', capsize=3)

        print('Sampling lightcurve')
        for i in range(sample_num):
            if not bolometric:
                lc = ds.get_interpolated(band, t_grid=t_grid, parametric=False, sample='random')
                idx = (t_grid - ds.transient.prior_t0)*z_factor < phase_max
                ax.plot(((t_grid - ds.transient.prior_t0)*z_factor)[idx], (lc+diff_factor)[idx], color=color, alpha=0.01)
            else:
                lc = ds.sample_abs_lc(method=bolometric_method, t_grid=t_grid, tweaks=['no-correlated-error'])
                idx = t_grid > t_min
                idx = np.logical_and(idx, t_grid > bol_t_min)
                idx = np.logical_and(idx, t_grid < bol_t_max)
                idx = np.logical_and(idx, (t_grid - ds.transient.prior_t0)*z_factor < phase_max)
                ax.plot((t_grid[idx] - ds.transient.prior_t0)*z_factor, lc[idx], color=color, alpha=0.03)
        

    plot_obj(ptf13bvn, axs[0], 'g', 'iPTF13bvn', offset=-0.5)
    plot_obj(sn2008d, axs[0], 'V', 'SN2008D', offset=-0.5)
    plot_obj(sn2019odp, axs[0], 'g', 'SN2019odp')
    plot_obj(sn1998bw, axs[0], 'V', 'SN1998bw', offset=1.6)
    plot_obj(sn2002ap, axs[0], 'V', 'SN2002ap', alpha=0.3, max_phase=30)

    plot_obj(ptf13bvn, axs[1], 'r', 'iPTF13bvn', offset=-0.5)
    plot_obj(sn2008d, axs[1], 'r', 'SN2008D', offset=-0.7)
    plot_obj(sn2019odp, axs[1], 'r', 'SN2019odp')
    plot_obj(sn1998bw, axs[1], 'Rc', 'SN1998bw', offset=1.4)
    plot_obj(sn2002ap, axs[1], 'R', 'SN2002ap', offset=-0.2, alpha=0.3, max_phase=30)

    plot_obj(ptf13bvn, axs[2], 'i', 'iPTF13bvn', offset=-0.5, bolometric=True)
    plot_obj(sn2008d, axs[2], 'i', 'SN2008D', offset=-0.5, bolometric=True)
    plot_obj(sn2019odp, axs[2], 'i', 'SN2019odp', bolometric=True, label='SN2019odp')
    #plot_obj(sn2019odp, axs[2], 'i', 'SN2019odp', bolometric=True, bolometric_method='lyman-gi', label='SN2019odp (Lyman g;i)', color='aqua')
    plot_obj(sn1998bw, axs[2], 'Ic', 'SN1998bw', offset=1.4, bolometric=True)
    #plot_obj(sn1998bw, axs[2], 'Ic', 'SN1998bw', offset=1.4, bolometric=True, bolometric_method='lyman-vr', label='SN1998bw (Lyman V;R)', color='peru')
    plot_obj(sn2002ap, axs[2], 'I', 'SN2002ap', alpha=0.3, offset=-0.2, bolometric=True, max_phase=30)

    # create the inset plots
    #axin_g = inset_axes(axs[0], width="44%", height="15%", loc=3, borderpad=1)
    axin_g = axs[0].inset_axes([0.01, 0.01, 0.44, 0.15], ylim=(-15.5, -16.3), xticklabels=[], yticklabels=[], xticks=[], yticks=[])
    #axin_r = inset_axes(axs[1], width="45%", height="15%", loc='lower center')
    axin_r = axs[1].inset_axes([0.1, 0.01, 0.44, 0.15], ylim=(-15.3, -16.3), xticklabels=[], yticklabels=[], xticks=[], yticks=[])
    
    # override the phase max
    phase_max=-11

    # override the sample number (since now smaller view.. so we need more for them to be visible)
    sample_number *= 2

    plot_obj(ptf13bvn, axin_g, 'g', 'iPTF13bvn', offset=-0.5)
    plot_obj(sn2008d, axin_g, 'V', 'SN2008D', offset=-0.5)
    plot_obj(sn2019odp, axin_g, 'g', 'SN2019odp')
    #plot_obj(sn1998bw, axin_g, 'V', 'SN1998bw', offset=1.6)
    #plot_obj(sn2002ap, axin_g, 'V', 'SN2002ap', alpha=0.3, max_phase=30)

    plot_obj(ptf13bvn, axin_r, 'r', 'iPTF13bvn', offset=-0.5)
    plot_obj(sn2008d, axin_r, 'r', 'SN2008D', offset=-0.7)
    plot_obj(sn2019odp, axin_r, 'r', 'SN2019odp')
    #plot_obj(sn1998bw, axin_r, 'Rc', 'SN1998bw', offset=1.4)
    #plot_obj(sn2002ap, axin_r, 'R', 'SN2002ap', offset=-0.2, alpha=0.3, max_phase=30)
    
    for ax in axs:
        #ax.set_xlabel('Phase $t-t^{peak}_g$ [d]')
        ax.set_xlabel('Restframe Phase [d]')
        #ax.axvline(0, alpha=0.3, ls='--', color='black')
        #ax.grid()
        ax.invert_yaxis()
        ax.legend()
        ax.yaxis.set_minor_locator(ticker.MultipleLocator(0.25))

    # configure insets
    # axin_g.grid()
    # axin_g.invert_yaxis()
    #axin_g.set_ylim(-15.5, -16.3)
    # #axin_g.set_xticks([-20])
    # axin_g.set_yticks([-16])
    #axin_g.axes.xaxis.set_ticklabels([])
    #axin_g.axes.yaxis.set_ticklabels([])
    # axin_g.axes.xaxis.tick_top()
    # axin_g.axes.yaxis.tick_right()
    # axin_g.axes.xaxis.set_tick_params(labelsize='small')
    # axin_g.axes.yaxis.set_tick_params(labelsize='small')
    # axin_g.axes.xaxis.set_major_formatter('{x}d')
    # axin_g.yaxis.set_minor_locator(ticker.MultipleLocator(0.25))
    
    # axin_r.grid()
    # axin_r.invert_yaxis()
    #axin_r.set_ylim(-15.3, -16.3)
    # #axin_r.set_xticks([-20])
    # axin_r.set_yticks([-16])
    #axin_r.axes.xaxis.set_ticklabels([])
    #axin_r.axes.yaxis.set_ticklabels([])
    # axin_r.axes.xaxis.tick_top()
    # axin_r.axes.yaxis.tick_right()
    # axin_r.axes.xaxis.set_major_formatter('{x} d')
    # axin_r.yaxis.set_minor_locator(ticker.MultipleLocator(0.25))

    axs[0].indicate_inset_zoom(axin_g, edgecolor="black")
    # lines[0].set_visible(True)
    # lines[3].set_visible(True)
    rect, lines = axs[1].indicate_inset_zoom(axin_r, edgecolor="black")
    for i in range(3):
        lines[i].set_visible(False)
    lines[1].set_visible(True)
    lines[3].set_visible(True)
    
    axs[0].set_ylabel('Blue Filter Absolute Magnitude')
    axs[1].set_ylabel('Red Filter Absolute Magnitude')
    axs[2].set_ylabel('Pseudobolometric Magnitude')

    if show:
        plt.show()
    else:
        fig.savefig(output)




if __name__ == '__main__':
    main()
