#!/usr/bin/env python
import pandas as pd
import click
import matplotlib
import numpy as np

import astropy.table as table
import astropy.units as u
import astropy.constants as const


DATASETS = {
    'grond_direct_int': {'lc_f': 'products/lc/qbol_comp/grond_direct_int.fits', 'label': 'GROND Integrated'},
    'grond_lb_superbol': {'lc_f': 'products/lc/qbol_comp/grond_lb_superbol.fits', 'label': 'GROND SuperBoL'},
    'grond_direct_int_0mag2': {'lc_f': 'products/lc/qbol_comp/grond_direct_int_0mag2.fits', 'label': 'GROND Integrated EBV+0.2mag'},
    'grond_lyman_gr': {'lc_f': 'products/lc/qbol_comp/grond_lyman_gr.fits', 'label': 'GROND+Lyman g-r'},
    'grond_lyman_gr_0mag2': {'lc_f': 'products/lc/qbol_comp/grond_lyman_gr_0mag2.fits', 'label': 'GROND+Lyman g-r EBV+0.2mag'},
    'grond_lyman_gi': {'lc_f': 'products/lc/qbol_comp/grond_lyman_gi.fits', 'label': 'GROND+Lyman g-i'},
    'specs_direct_int': {'lc_f': 'products/lc/qbol_comp/spec_direct_int.fits', 'label': 'Spectra Integrated'},
    'ztf_lyman_gr': {'lc_f': 'products/lc/pbol/ztf_lyman_gr.fits', 'label': 'ZTF Lyman g-r', 'shade': True},
    'ztf_lyman_gi': {'lc_f': 'products/lc/pbol/ztf_lyman_gi.fits', 'label': 'ZTF Lyman g-i', 'shade': True}
    
}


@click.command()
@click.option('-o', '--output')
@click.option('--min-mjd', type=float, default=0)
@click.option('--max-mjd', type=float, default=1e20)
@click.option('--ratio-plot')
@click.argument('datasets', nargs=-1)
def main(output, datasets, min_mjd, max_mjd, ratio_plot):
    if output:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    if output:
        plt.rcParams.update({'font.size': 20})
    fig = plt.figure(figsize=(15,9))
    ax = fig.add_subplot(111)

    if ratio_plot:
        conf = DATASETS[ratio_plot]
        ratio_lc = table.Table.read(conf['lc_f'])
        ref_mjd = ratio_lc['MJD']
        ref_lum = ratio_lc['Luminosity']
        ref_label = conf['label']
        if 'Lums_Err_Method' in ratio_lc.columns:
            print(' * Using method error for reference')
            ref_err = ratio_lc['Lums_Err_Method']
        else:
            ref_err = ratio_lc['Lums_Err']
        min_lum = ratio_lc['Luminosity'] - ref_err
        max_lum = ratio_lc['Luminosity'] + ref_err
        ax.fill_between(ref_mjd, min_lum/ref_lum, max_lum/ref_lum, alpha=0.3, label='Reference Uncertainty')
        ax.axhline(1.0, label='Reference', color='red')
    
        

    #ax.set_xlim((min_mjd, max_mjd))
    
    for ds in datasets:
        print(' * %s' % ds)
        conf = DATASETS[ds]
        lc_f = conf['lc_f']
        label = conf['label']
        
        lc = table.Table.read(lc_f)
        lc.pprint()

        t = lc['MJD']
        lum = lc['Luminosity']
        

        if ratio_plot:
            lum_err = lc['Lums_Err']
            # produce interpolation grid
            min_mjd = max(np.min(t), np.min(ref_mjd))
            max_mjd = min(np.max(t), np.max(ref_mjd))
            if len(t) > len(ref_mjd):
                grid_mjd = t
            else:
                grid_mjd = ref_mjd
            lum_interp_ref = np.interp(grid_mjd, ref_mjd, ref_lum)
            lum_interp_ds = np.interp(grid_mjd, t, lum)
            lum_interp_ds_min = np.interp(grid_mjd, t, lum-lum_err)
            lum_interp_ds_max = np.interp(grid_mjd, t, lum+lum_err)

            lum_ratio = lum_interp_ds/lum_interp_ref
            lum_ratio_max = lum_interp_ds_max/lum_interp_ref
            lum_ratio_min = lum_interp_ds_min/lum_interp_ref
            lum_ratio_err = (lum_ratio_max - lum_ratio_min)/2

            ax.errorbar(grid_mjd, lum_ratio, yerr=lum_ratio_err, capsize=10, label=label)
            
        else:
        
            #ax.plot(t[idx], lum[idx], label=label)
            idx = np.logical_and(t > min_mjd-5, t < max_mjd+8)
            print('Matched %d points for dataset %s' % (np.count_nonzero(idx), ds))
            if 'Lums_Err' in lc.columns:
                lum_err = lc['Lums_Err']
                if 'shade' in conf:
                    ax.fill_between(t[idx], lum[idx] - lum_err[idx]/2, lum[idx] + lum_err[idx]/2, alpha=0.5)
                    ax.plot(t[idx], lum[idx], label=label)
                else:
                    ax.errorbar(t[idx], lum[idx], yerr=lum_err, label=label)
            else:
                ax.plot(t[idx], lum[idx], label=label)
            #ax.plot(t, lum, label=label)

    ax.set_xlabel('MJD [d]')
    if ratio_plot:
        ax.set_ylabel('Luminosity Ratio versus %s' % ref_label)
    else:
        ax.set_ylabel('Luminosity [erg/s/cm2]')
    ax.legend()

    if output:
        fig.savefig(output)
    else:
        plt.show()

if __name__ == '__main__':
    main()
