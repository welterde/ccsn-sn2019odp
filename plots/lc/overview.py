#!/usr/bin/env python
import os, sys, glob, tqdm
import pandas as pd
import click
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import h5py
import astropy.time as time
import numpy as np
import matplotlib.ticker as ticker
from matplotlib.lines import Line2D
import tqdm
import astropy.table as table

try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../../lib'))
import snelib19odp.const as const
import snelib19odp.dataset as dataset
import snelib19odp.dataloader as dataloader
import snelib19odp.colors as colors




PHASE_CENTER = const.sne_peak_mjd['g']

PLOT_CFG = [
    ('u', 1e12, 'blue', 'Blues', 0.2),
    ('g', 1e12, 'green', 'Greens', 0.1),
    ('r', 1e12, 'red', 'Reds', 0.1),
    ('i', 58792, 'purple', 'Purples', 0.1),
    ('z', 58792, 'orange', 'Oranges', 0.1),
    ('J', 58792, 'red', 'Reds', 0.1)
]

SUN_GAP_START = 58880.0
#SUN_GAP_END = SUN_GAP_START + 80

products_dir = os.path.join(os.path.dirname(__file__), '../products')
data_dir = os.path.join(os.path.dirname(__file__), '../data')


def plot_main(ds, ax, mark_peak_bands):
    max_per_band = {}

    late_window_start = SUN_GAP_START + 120
    late_window_end = SUN_GAP_START
    instruments = set()
    for band, mjd_cutoff, _color, color_cmap, magerr_limit in PLOT_CFG:
        combined = ds.get_corrected_dataset(band)
        idx = combined['mjd'] > SUN_GAP_START
        if band in 'gri':
            late_window_start = min(late_window_start, combined['mjd'][idx].min())
            late_window_end = max(late_window_end, combined['mjd'][idx].max())
        for instrument in np.unique(combined['instrument']):
            instruments.add(instrument)

    instrument2marker = dict(zip(sorted(list(instruments)), 'ospP*Xdx+'))

    color_legend = []

    for band, mjd_cutoff, _color, color_cmap, magerr_limit in PLOT_CFG:
        color = colors.BAND_COLOR_MAP[band]

        # setup the color legend
        color_legend.append(Line2D([0], [0], color=color, lw=4, label=band))
        
        combined = ds.get_corrected_dataset(band, extinct_correct=True)
        #print(combined)
        instruments = np.unique(combined['instrument'])
        #print(combined.colnames)
        max_per_band[band] = combined['mag'][combined['mag_err'] < magerr_limit].min()

        first_detection_mjd = ds.first_detection_mjd_band(band)
        last_detection_mjd = combined['mjd'].max()
        interpolator = ds.get_interpolator(band)
        
        # plot the interpolated lightcurve
        mjd_grid = np.linspace(first_detection_mjd, last_detection_mjd, 500)
        for i in tqdm.tqdm(range(300)):
            lc = interpolator.sample_lc(mjd_grid, sample='random')
            idx = ~np.isnan(lc)
            # FIXME: quick hack for now to compensate for broken interpolation function
            idx = np.logical_and(idx, lc > 14)
            ax.plot(mjd_grid[idx], lc[idx], alpha=0.01, color=color, lw=1)

        idx = np.ones(len(combined), dtype=np.bool)
        #idx = combined['mjd'] < SUN_GAP_START
        idx = np.logical_and(idx, combined['mag_err'] < 0.5)
        idx = np.logical_and(idx, ~np.isnan(combined['mag']))
        idx = np.logical_and(idx, ~combined['upper_limit'])
        for instrument in instruments:
            s_idx = np.logical_and(idx, combined['instrument'] == instrument)
            ax.errorbar(combined['mjd'][s_idx], combined['mag'][s_idx], yerr=combined['mag_err'][s_idx], label='%s-band' % band, marker=instrument2marker[instrument], ms=10, color=color, ls='')

        #print(combined['mag'][idx])
        print(np.count_nonzero(np.isnan(combined['mag'][idx])))
        
        # plot late time
        #idx = combined['mjd'] > SUN_GAP_START
        #idx = np.logical_and(idx, combined['mag_err'] < 0.5)
        #ax_late.errorbar(combined['mjd'][idx], combined['mag'][idx], yerr=combined['mag_err'][idx], label='%s-band' % band, fmt='o', color=color)
        #mjd_grid = np.linspace(late_window_start, late_window_end, 400)
        #for i in range(300):
        #    lc = interpolator.sample_lc(mjd_grid, sample='random')
        #    ax_late.plot(mjd_grid, lc, alpha=0.01, color=color)
        
    # manual line at phase_t0
    ax.axvline(ds.transient.prior_t0, color='grey', ls='--', alpha=0.5)

    ## manually construct the legend
    # the per-band legend
    legend1 = ax.legend(handles=color_legend, loc='lower left', bbox_to_anchor=(0.1,0,1,1))
    # the per-instrument legend
    instrument_legend = []
    for instrument, marker in instrument2marker.items():
        instrument_legend.append(Line2D([0], [0], color='black', marker=marker, markersize=10, label=instrument, ls=''))
    ax.legend(handles=instrument_legend)
    ax.add_artist(legend1)

    ax.set_ylabel('Apparent Magnitude [mag]')
    ax.invert_yaxis()
    #ax_late.set_ylabel('Apparent Magnitude [mag]')
    #ax_late.invert_yaxis()

    yticks = []
    for band in mark_peak_bands:
        yticks.append(max_per_band[band])
    for x in np.arange(16.5, 22, 0.5):
        collision = False
        for y in yticks:
            if abs(x-y) < 0.25:
                collision = True
        if not collision:
            yticks.append(x)
    print(yticks)
    #ax.set_yticks(yticks)

    ax.set_ylim((22.9, 15.5))
    ax.set_xlim((58713, 59130))



    
def plot_early(ds, ax, end_time=58721):
    # TODO: refactor this loading stuff to some utility functions?
    # load the ztflc lc
    #ztflc = table.Table.read(os.path.join(data_dir, 'ztflc_forcefit.h5'))
    #ztflc['mag'] = -2.5*np.log10(ztflc['ampl']) + ztflc['magzp']
    ztflc = ds.get_sub_dataset("ZTF")

    # load the swift-uvot observations
    #uvot = table.Table.read(os.path.join(data_dir, 'uvot_phot.txt'), format='ascii')
    #uvot['mjd'] = uvot['JD'] - 2400000.5
    #idx = uvot['AB_MAG'] < uvot['AB_MAG_LIM']-3*uvot['AB_MAG_ERR']


    # plot ztflc
    scalezp = 0
    for band in 'gri':
        color = colors.BAND_COLOR_MAP[band]
        interpolator = ds.get_interpolator(band)

        extinction_correction = dataset.compute_extinction_correction(ds, 'SDSS %s' % band)
        
        idx = np.logical_and(ztflc['band'] == band, np.logical_and(ztflc['obsmjd'] > 58710, ztflc['obsmjd'] < end_time))
        print(ztflc[idx])
        f0coef = 3631e6 * 10 ** (-(ztflc["magzp"][idx] - scalezp) / 2.5)
        ax.errorbar(ztflc['obsmjd'][idx], f0coef*ztflc['ampl'][idx], yerr=f0coef*ztflc['ampl.err'][idx], fmt='o', label='ZTF %s' % band, capsize=2, alpha=0.7, color=color, ms=10)

        mjd_grid = np.linspace(58715, 58720.7, 300)
        for i in tqdm.tqdm(range(1000)):
            lc = interpolator.sample_lc(mjd_grid, sample='random')
            # de-apply the extinction correction (we dont want extinction corrected here!)
            lc += extinction_correction
            flux = 3631e6 * 10 ** (-lc/2.5)
            ax.plot(mjd_grid, flux, alpha=0.01, color=color, lw=0.5)

    # plot UVOT detections
    #idx = np.logical_and(uvot['AB_MAG'] < uvot['AB_MAG_LIM']-uvot['AB_MAG_ERR'], uvot['mjd'] < end_time)
    #for i, band in enumerate(['U', 'B', 'V']):
    #    idx2 = np.logical_and(idx, uvot['FILTER'] == band)
    #    if np.count_nonzero(idx2) == 0:
    #        continue
    #    flux = 3631e6 * 10 ** (-uvot['AB_MAG']/2.5)
    #    flux_err = uvot['AB_MAG_ERR'] * flux / 1.087
        #ax.errorbar(uvot['mjd'][idx2]+(i-1)*0.02, flux[idx2], yerr=flux_err[idx2], fmt='o', label='UVOT %s' % band, alpha=0.7, capsize=2)

    # plot UVOT non-detections
    #idx = np.logical_and(uvot['AB_MAG'] >= uvot['AB_MAG_LIM']-uvot['AB_MAG_ERR'], uvot['mjd'] < end_time)
    #for i, band in enumerate(np.unique(uvot['FILTER'])):
    #    print(repr(band))
    #    if band == 'B':
    #        continue
    #    idx2 = np.logical_and(idx, uvot['FILTER'] == band)
    #    if np.count_nonzero(idx2) == 0:
    #        continue
    #    flux = 3631e6 * 10 ** (-uvot['AB_MAG_LIM']/2.5)
    #    #flux_err = uvot['AB_MAG_ERR'] * flux / 1.087
    #    #ax.errorbar(uvot['mjd'][idx2]+(i-3)*0.02, flux[idx2], fmt='v', label='UVOT %s' % band)

    ax.axhline(0, ls='--', alpha=0.5, color='black')

    ax.set_ylabel(r'Flux [$\mu$Jy]')
    ax.set_ylim((-20, 220))
    
    



@click.command()
@click.option('--dataset-name', default='SN2019odp_phot')
@click.option('--early', is_flag=True)
@click.option('--mark-peak-bands', default='gr')
@click.argument('destfile')
def main(destfile, early, mark_peak_bands, dataset_name):
    if early:
        plt.rcParams.update({'font.size': 15})
        fig = plt.figure(figsize=(9,6))
        ax = fig.add_subplot(111)
    else:
        plt.rcParams.update({'font.size': 20})
        fig = plt.figure(figsize=(15,9))
        ax = fig.add_subplot(111)
    

    ds = dataset.load_dataset(dataset_name)
    
    spec_mjds = dataloader.load_spec_mjds(fluxcal=False)

    if not early:
        plot_main(ds, ax, mark_peak_bands)
    else:
        plot_early(ds, ax)    
        ax.legend()
    
    ax.set_xlabel('MJD [d]')
    

    def abs2rel(x):
        return x - ds.transient.prior_t0

    def rel2abs(x):
        return x + ds.transient.prior_t0

    #ax.grid(linestyle = '--', linewidth = 0.5)
        
    sec_ax = ax.secondary_xaxis('top', functions=(abs2rel, rel2abs))
    sec_ax.set_xlabel('Phase [d]')

    spec_ax = ax.secondary_xaxis(1.2)
    spec_ax.set_xlabel('Spectroscopy')
    #spec_ax.plot(spec_mjds, np.ones(len(spec_mjds)), 'v')
    spec_ax.xaxis.set_major_locator(ticker.FixedLocator(spec_mjds))
    spec_ax.xaxis.set_ticklabels([])
    spec_ax.tick_params(width=2, length=12, direction='inout')
    # from https://matplotlib.org/examples/axes_grid/demo_parasite_axes2.html
    #par2 = ax.twinx()
    #offset = 60
    #print(repr(par2.axis))
    #par2.axis['top'] = par2.get_grid_helper().new_fixed_axis(loc='top',
    #                                                         axes=par2,
    #                                                         offset=(0, offset))
    #par2.axis["top"].toggle(all=True)
    
    
    plt.tight_layout()
    fig.savefig(destfile)




if __name__ == '__main__':
    main()
