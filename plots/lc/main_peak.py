import os, sys, warnings
import click
import numpy as np
try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../../lib'))
from snelib19odp.dataset import load_dataset
import snelib19odp.lc.observables as observables
import snelib19odp.dataloader as dataloader
import matplotlib
import seaborn




def plot_dataset_slope(ax, ds_name, band, label=None, obs=None, kde=True, dist_correct=None):
    if label is None:
        label = ds_name
    if obs is None:
        ds = load_dataset(ds_name)
        obs = observables.Observables(ds).basic(band)
    if kde:
        seaborn.kdeplot(x=obs['delta_m15'], y=obs['delta_m-10'], label='%s %s' % (label, band), ax=ax)
    else:
        ax.errorbar(obs['delta_m15'], obs['delta_m-10'], xerr=obs['delta_m15_err'], yerr=obs['delta_m-10_err'], label='%s %s' % (label, band), ls='')


def plot_dataset_peak(ax, ds_name, band, label=None, obs=None, kde=True, dist_correct=True):
    if label is None:
        label = ds_name
    if obs is None:
        ds = load_dataset(ds_name)
        obs = observables.Observables(ds).basic(band)
        if dist_correct:
            combined_lc = ds.get_combined_lc(band)
            dmag = np.nanmean(combined_lc['absmag'] - combined_lc['mag'])
            dmag_err = np.nanmean(combined_lc['mag_err_correlated'])
            obs['peak_mag'] += np.random.normal(dmag, dmag_err, size=len(obs))
            #print(np.random.normal(dmag, dmag_err, size=len(obs)))
    if kde:
        seaborn.kdeplot(x=obs['linear_slope'], y=obs['peak_mag'], label='%s %s' % (label, band), ax=ax)
    else:
        ax.errorbar(obs['linear_slope'], obs['peak_mag'], xerr=obs['linear_slope_err'], yerr=obs['peak_mag_err'], label='%s %s' % (label, band), ls='')

def plot_dataset_plateau(ax, ds_name, band, label=None, dist_correct=True):
    if label is None:
        label = ds_name
    ds = load_dataset(ds_name)
    obs = observables.Observables(ds).basic(band)
    if dist_correct:
        combined_lc = ds.get_combined_lc(band)
        dmag = np.nanmean(combined_lc['absmag'] - combined_lc['mag'])
        dmag_err = np.nanmean(combined_lc['mag_err_correlated'])
        obs['plateau_level'] += np.random.normal(dmag, dmag_err, size=len(obs))
        #print(np.random.normal(dmag, dmag_err, size=len(obs)))
    seaborn.kdeplot(x=obs['linear_slope'], y=obs['plateau_level'], label='%s %s' % (label, band), ax=ax)

def plot_dataset_plateau_rel(ax, ds_name, band, label=None, dist_correct=True):
    if label is None:
        label = ds_name
    ds = load_dataset(ds_name)
    obs = observables.Observables(ds).basic(band)
    if dist_correct:
        combined_lc = ds.get_combined_lc(band)
        #dmag = np.nanmean(combined_lc['absmag'] - combined_lc['mag'])
        #dmag_err = np.nanmean(combined_lc['mag_err_correlated'])
        #obs['plateau_level'] += np.random.normal(dmag, dmag_err, size=len(obs))
        obs['plateau_level'] -= obs['peak_mag']
        #print(np.random.normal(dmag, dmag_err, size=len(obs)))
    seaborn.kdeplot(x=obs['linear_slope'], y=obs['plateau_level'], label='%s %s' % (label, band), ax=ax)

def plot_dataset_rise(ax, ds_name, band, label=None):
    if label is None:
        label = ds_name
    ds = load_dataset(ds_name)
    obs = observables.Observables(ds).basic(band)
    seaborn.kdeplot(x=obs['peak_width'], y=obs['rise_timescale'], label='%s %s' % (label, band), ax=ax)



@click.command()
@click.option('--output')
@click.option('--taddia-2019', is_flag=True)
@click.option('--taddia-2015', is_flag=True)
@click.argument('mode', default='slope')
def main(mode, output, taddia_2019, taddia_2015):
    warnings.filterwarnings("ignore", module="dynesty.sampling")
    if output:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt

    fig, ax = plt.subplots()

    if mode == 'slope':
        func = plot_dataset_slope
    elif mode == 'peak':
        func = plot_dataset_peak
    elif mode == 'plateau':
        func = plot_dataset_plateau
    elif mode == 'plateau_rel':
        func = plot_dataset_plateau_rel
    elif mode == 'rise':
        func = plot_dataset_rise
    
    func(ax, 'SN2019odp_phot', 'r', label='SN2019odp')
    func(ax, 'SN2008D', 'r')
    func(ax, 'SN1998bw', 'Rc')
    func(ax, 'SN2002ap', 'R')
    func(ax, 'iPTF13bvn', 'r')

    if taddia_2019 and mode != 'plateau':
        ds = dataloader.load_aa_table(os.path.join(os.path.dirname(__file__), '../../data/comparison/Taddia-2019-AA621.txt'))
        #ax.plot(
        ds['delta_m15'] = ds['dm15']
        ds['delta_m-10'] = ds['dm−10']
        ds['delta_m15_err'] = ds['dm15_err']
        ds['delta_m-10_err'] = ds['dm−10_err']
        ds['linear_slope'] = ds['LinearSlope']*1e3
        ds['linear_slope_err'] = ds['LinearSlope_err']*1e3
        ds['peak_mag'] = ds['Mrmax']
        ds['peak_mag_err'] = ds['Mrmax_err']
        func(ax, 'Taddia+2019', 'r', obs=ds, kde=False, dist_correct=False)

    if taddia_2015 and mode == 'slope':
        ds = dataloader.load_aa_table(os.path.join(os.path.dirname(__file__), '../../data/comparison/Taddia-2015-AA574-Tbl3.txt'))
        print(ds)
        #ax.plot(
        ds['delta_m15'] = ds['dm15']
        ds['delta_m-10'] = ds['dm−10']
        ds['delta_m15_err'] = ds['dm15_err']
        ds['delta_m-10_err'] = ds['dm−10_err']
        func(ax, 'Taddia+2015', 'r', obs=ds, kde=False, dist_correct=False)

    if mode == 'slope':
        ax.set_xlabel(r'$\Delta m_{15} (r,R)$ [mag]')
        ax.set_ylabel(r'$\Delta m_{-10} (r,R)$ [mag]')
    elif mode == 'peak':
        ax.set_xlabel('Linear Slope [mmag/d]')
        ax.set_ylabel('Peak Mag (r,R) [mag]')
        ax.invert_yaxis()
    elif mode == 'plateau':
        ax.set_xlabel('Linear Slope [mmag/d]')
        ax.set_ylabel('Plateau Mag (r,R) [mag]')
        ax.invert_yaxis()
    ax.legend()
    


    if not output:
        plt.show()
    else:
        fig.savefig(output)


if __name__ == '__main__':
    main()
