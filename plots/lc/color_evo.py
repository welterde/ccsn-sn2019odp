#!/usr/bin/env python
import click, sys, os, tqdm, functools
import matplotlib
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
import numpy as np

import astropy.table as table
import scipy.optimize as optimize
try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../../lib'))
from snelib19odp.dataset import load_dataset
import snelib19odp.specds as specds
import snelib19odp.synphot as synphot



def plot_color(ax, ds, color_a, color_b, legend=None, plot_color=None, samples=800, alpha=0.02, color_limit=(-2,2), max_phase=100):
    color_min, color_max = color_limit
    
    int_a = ds.get_interpolator(color_a)
    int_b = ds.get_interpolator(color_b)

    label = '%s %s-%s' % (ds.transient.name, color_a, color_b)

    mjd_min = max(ds.first_detection_mjd_band(color_a), ds.first_detection_mjd_band(color_b))
    mjd_max = ds.transient.prior_t0 + max_phase
    mjd_grid = np.linspace(mjd_min, mjd_max, 500)

    print('Using grid: %d - %d' % (mjd_min, mjd_max))

    if legend is not None:
        patch = mpatches.Patch(color=plot_color, label=label)
        legend.append(patch)
    
    for i in tqdm.trange(samples, desc=ds.name, disable=None):
        lc_a = int_a.sample_lc(mjd_grid, sample='random')
        lc_b = int_b.sample_lc(mjd_grid, sample='random')

        color = lc_a - lc_b
        idx = np.logical_and(color > color_min, color < color_max)
        ax.plot((mjd_grid - ds.transient.prior_t0)[idx], color[idx], alpha=alpha, color=plot_color, lw=0.75)


def make_synphot_lc(sds, filters):
    data = {'mjd': []}
    for band in 'gri':
        data[band] = []
    for name in sds.names:
        spec = sds[name]
        if spec.meta['obs_mjd'] > 58900:
            continue
        data['mjd'].append(spec.meta['obs_mjd'])
        for band in 'gri':
            mag = synphot.spec2mag(spec, filters[band])
            data[band].append(mag)
    t = table.Table(data)
    t.sort('mjd')
    return t


@click.command()
@click.option('--output', '-o')
@click.option('--samples', default=800)
@click.option('--synphot-ds', multiple=True, default=[])
@click.option('--max-phase', default=100)
@click.argument('mode')
def main(output, mode, samples, synphot_ds, max_phase):
    # load the filter curves for synphot LC (even if not needed)
    filters = {}
    for band in 'gri':
        filters[band] = synphot.load_filter('Palomar_ZTF', band, return_format='table')

    def plot_synphots(ax, band1, band2, legend, max_phase):
        for ds_name in synphot_ds:
            sds = specds.load_dataset(ds_name)
            slc = make_synphot_lc(sds, filters)
            phase = slc['mjd'] - sds.transient.prior_t0
            idx = phase < max_phase
            ax.plot(phase[idx], (slc[band1] - slc[band2])[idx], marker='x', ls='', color='black', markersize=15)
            if legend is not None:
                #patch = mpatches.Patch(color='black', label='Spectra %s-%s' % (band1, band2))
                patch = mlines.Line2D([0], [0], marker='x', color='black', label='Spectra %s-%s' % (band1, band2), ls='', markersize=15)
                legend.append(patch)
            
    if output:
        matplotlib.use('Agg')
    
    import matplotlib.pyplot as plt
    plt.rcParams.update({'font.size': 20})
    
    if mode == 'combined':
        fig = plt.figure(figsize=(15,15))
        ax1 = fig.add_subplot(211)
        ax2 = fig.add_subplot(212, sharex=ax1)
        axs = [ax1, ax2]
    else:
        fig = plt.figure(figsize=(15,9))
        ax = fig.add_subplot(111)
        axs = [ax]

    legend = []

    #sn2019odp = load_dataset('SN2019odp_phot')
    sn2019odp = load_dataset('SN2019odp_phot_short')
    sn2008d = load_dataset('SN2008D')
    iptf13bvn = load_dataset('iPTF13bvn')
    sn2002ap = load_dataset('SN2002ap')
    sn1998bw = load_dataset('SN1998bw')

    plot_func = functools.partial(plot_color, samples=samples, max_phase=max_phase)
    
    if mode == 'blue_green':
        plot_func(ax, sn2019odp, 'g', 'r', plot_color='tab:Purple', legend=legend)
        plot_func(ax, sn2008d, 'B', 'r', plot_color='tab:Green', legend=legend, alpha=0.03)
        plot_func(ax, iptf13bvn, 'g', 'r', plot_color='tab:Blue', legend=legend)
        plot_func(ax, sn2002ap, 'B', 'R', plot_color='tab:Cyan', legend=legend)
        plot_func(ax, sn1998bw, 'B', 'Rc', plot_color='tab:Brown', legend=legend)
        plot_synphots(ax, 'g', 'r', legend=legend, max_phase=max_phase)
        ax.legend(handles=legend)
    elif mode == 'green_red':
        plot_func(ax, sn2019odp, 'r', 'i', plot_color='tab:Purple', legend=legend, alpha=0.04)
        plot_func(ax, sn2008d, 'r', 'i', plot_color='tab:Green', legend=legend, alpha=0.04)
        plot_func(ax, iptf13bvn, 'r', 'i', plot_color='tab:Blue', legend=legend)
        plot_func(ax, sn2002ap, 'R', 'I', plot_color='tab:Cyan', legend=legend)
        plot_func(ax, sn1998bw, 'Rc', 'Ic', plot_color='tab:Brown', legend=legend)
        plot_synphots(ax, 'r', 'i', legend=legend, max_phase=max_phase)
        ax.legend(handles=legend)
    elif mode == 'blue_red':
        plot_func(ax, sn2019odp, 'g', 'i', plot_color='tab:Purple', legend=legend, color_limit=(-1.5, 2.3))
        plot_func(ax, sn2008d, 'B', 'i', plot_color='tab:Green', legend=legend, alpha=0.03, color_limit=(-1.5, 2.3))
        plot_func(ax, iptf13bvn, 'g', 'i', plot_color='tab:Blue', legend=legend, color_limit=(-1.5, 2.3))
        plot_func(ax, sn2002ap, 'B', 'I', plot_color='tab:Cyan', legend=legend, color_limit=(-1.5, 2.3))
        plot_func(ax, sn1998bw, 'B', 'Ic', plot_color='tab:Brown', legend=legend, color_limit=(-1.5, 2.3))
        plot_synphots(ax, 'g', 'i', legend=legend, max_phase=max_phase)
        ax.legend(handles=legend)
    elif mode == 'uv_blue':
        plot_func(ax, sn2019odp, 'u', 'g', plot_color='tab:Purple', legend=legend, color_limit=(-1.5, 2.3))
        ax.legend(handles=legend)
    elif mode == 'combined':
        plot_func(ax1, sn2019odp, 'g', 'r', plot_color='tab:Purple', legend=legend)
        plot_func(ax1, sn2008d, 'B', 'r', plot_color='tab:Green', legend=legend, alpha=0.03)
        plot_func(ax1, iptf13bvn, 'g', 'r', plot_color='tab:Blue', legend=legend)
        plot_func(ax1, sn2002ap, 'B', 'R', plot_color='tab:Cyan', legend=legend)
        plot_func(ax1, sn1998bw, 'B', 'Rc', plot_color='tab:Brown', legend=legend)
        plot_synphots(ax1, 'g', 'r', legend=legend, max_phase=max_phase)
        ax1.legend(handles=legend, loc='lower center', ncol=2)

        legend = []
        plot_func(ax2, sn2019odp, 'r', 'i', plot_color='tab:Purple', legend=legend, alpha=0.04)
        plot_func(ax2, sn2008d, 'r', 'i', plot_color='tab:Green', legend=legend, alpha=0.04)
        plot_func(ax2, iptf13bvn, 'r', 'i', plot_color='tab:Blue', legend=legend)
        plot_func(ax2, sn2002ap, 'R', 'I', plot_color='tab:Cyan', legend=legend)
        plot_func(ax2, sn1998bw, 'Rc', 'Ic', plot_color='tab:Brown', legend=legend)
        plot_synphots(ax2, 'r', 'i', legend=legend, max_phase=max_phase)
        ax2.legend(handles=legend)

    for ax in axs:
        
        ax.set_xlabel('Phase [d]')
        ax.set_ylabel('Color Index [mag]')

    if output:
        fig.savefig(output)
    else:
        plt.show()
    


if __name__ == '__main__':
    main()
