#!/usr/bin/env python
import click
import aplpy






@click.command()
@click.option('--fits-file', default='')
@click.option('--sne-ra', default=346.829542)
@click.option('--sne-dec', default=13.85595)
@click.option('--fov', default=3.0)
@click.option('--pmax', default=98.0)
@click.option('-o', '--output')
def main(fits_file, output, sne_ra, sne_dec, fov, pmax):
    gc = aplpy.FITSFigure(fits_file)

    gc.show_grayscale(pmax=pmax)
    gc.recenter(sne_ra, sne_dec, radius=fov/60)
    
    gc.show_rectangles(sne_ra-3.5/3600, sne_dec, width=3/3600, height=0.5/3600, facecolor='red')
    gc.show_rectangles(sne_ra, sne_dec+3/3600, width=0.5/3600, height=3/3600, facecolor='red')
    
    gc.add_grid()
    # essentially just inverts the image:
    gc.set_theme('publication')

    if not output:
        import matplotlib.pyplot as plt
        plt.show()
    else:
        gc.save(output)
    




if __name__ == '__main__':
    main()
