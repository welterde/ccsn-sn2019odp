import os, sys, logging, click, sexpdata
import numpy as np
import matplotlib
import matplotlib.transforms as mtransforms

try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../../lib'))
from snelib19odp.specds import load_dataset
from snelib19odp.utils import convert_dict

CFG_DIR = os.path.join(os.path.dirname(__file__), '../../config/plots/seq')

def load_plot_config(name):
    cfg_fname = os.path.join(CFG_DIR, name + '.scm')
    with open(cfg_fname, 'r') as f:
        return sexpdata.load(f)

def load_specs(cfg):
    spec_cfg = None
    for rule in cfg:
        if rule[0].value() == 'specs':
            spec_cfg = rule[1:]
    assert spec_cfg is not None
    ds = None
    specs_select = None
    for rule in spec_cfg:
        if rule[0].value() == 'dataset':
            ds = load_dataset(rule[1])
            specs_select = np.ones(len(ds.specs), dtype=np.bool)
        elif rule[0].value() == 'select:phase':
            assert ds is not None
            phase_s = rule[2]
            
            for i,s in enumerate(ds.specs):
                if rule[1].value() == '>':
                    specs_select[i] &= s.meta['phase'] > phase_s
                else:
                    specs_select[i] &= s.meta['phase'] < phase_s
        elif rule[0].value() == 'if':
            assert ds is not None
            
            cond_rule = rule[1]
            apply_rule = rule[2]

            for i,s in enumerate(ds.specs):
                matched = False
                if cond_rule[0].value() == 'specname':
                    matched = s.meta['name'] == cond_rule[1]
                #print(s.meta['name'])
                if matched:
                    #print(s)
                    if apply_rule[0].value() == 'wavelength:max':
                        s.meta['wavelength:max'] = apply_rule[1]
                    elif apply_rule[0].value() == 'wavelength:min':
                        s.meta['wavelength:min'] = apply_rule[1]
    ret_specs = []
    for i,s in enumerate(ds.specs):
        if specs_select[i]:
            ret_specs.append(s)
    return ret_specs, ds.transient.redshift


def plot_atom(ax, cfg, trans):
    name = cfg[0]

    color = None
    
    wave_grid = np.linspace(3000, 9000, 1000)
    
    for rule in cfg[1:]:
        cmd = rule[0].value()
        if cmd == 'line':
            line = rule[1]
            line_cfg = convert_dict(rule[2:])
            ax.axvline(line, alpha=0.6, ls='--', color=color)
            ax.text(line+line_cfg.get('offset:text', [0])[0], 0.94, '%s %d' % (name, line), transform=trans, rotation='vertical', fontsize=16)
        elif cmd == 'color':
            color = rule[1]
        elif cmd == 'shade':
            #idx = np.logical_or(idx1, np.logical_or(idx2, np.logical_or(idx3, idx4)))
            idx = np.logical_and(wave_grid > rule[1], wave_grid < rule[2])
            ax.fill_between(wave_grid, 0, 1, where=idx, facecolor=color, alpha=0.25, transform=trans)

def plot_telluric(ax, telluric, trans, redshift):
    color = 'grey'
    if telluric == 'O2-A':
        # A-band of O_2
        # wavelength range taken from https://arxiv.org/pdf/2011.10845.pdf
        wave_grid = np.linspace(7596, 7713)
        ax.fill_between(wave_grid/(1+redshift), 0, 1, facecolor=color, alpha=0.25, transform=trans)
        ax.text(7596/(1+redshift), 0.96, '$\\bigoplus$', transform=trans, rotation='vertical', fontsize=12)
            

@click.command()
@click.option('--debug', is_flag=True)
@click.option('-o', '--output')
#@click.option('--dataset', default='SN2019odp')
@click.argument('plot_name')
def main(debug, output, plot_name):
    if debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    
    if output:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    if output:
        plt.rcParams.update({'font.size': 20})

    # load the configuration
    cfg = load_plot_config(plot_name)

    fig = plt.figure(figsize=(15,22))
    ax = fig.add_subplot(111)

    # transform to match the whole y-axis
    trans = mtransforms.blended_transform_factory(ax.transData, ax.transAxes)

    # load the spectra
    specs, redshift = load_specs(cfg)

    for i, spec in enumerate(specs):
        wave = spec['restwave']
        flux = spec['flux']*wave
        phase = spec.meta['phase']

        flux -= 0.9*np.nanmean(flux)
        flux /= 0.5*np.nanstd(flux)
        flux *= -1
        

        idx = np.abs(flux) < 10

        if 'wavelength:max' in spec.meta:
            idx = np.logical_and(idx, spec['obswave'] < spec.meta['wavelength:max'])
        if 'wavelength:min' in spec.meta:
            idx = np.logical_and(idx, spec['obswave'] > spec.meta['wavelength:min'])

        ax.plot(wave[idx], flux[idx]+5*i, color='black')
        if phase < 100:
            label = '%.1fd' % phase
        else:
            label = '%.0fd' % phase
        flux_level_end = np.percentile(flux[idx][-10:], 50)
        ax.text(x=wave[idx].max()+75, y=flux_level_end+5*i, s=label)

    ax.set_xlabel(r'Rest Wavelength [$\AA$]')
    ax.set_ylabel('Normalized Flux + Offset')
    ax.invert_yaxis()

    ### mark the various lines in the spectrum
    

    # now process the remaining config rules
    for rule in cfg:
        cmd = rule[0].value()
        if cmd == 'specs':
            # no longer relevant at this stage
            continue
        elif cmd == 'atom':
            # overlay some lines for an atom
            plot_atom(ax, rule[1:], trans=trans)
        elif cmd == 'telluric':
            # overlay some shaded region for telluric absorption
            plot_telluric(ax, rule[1], trans=trans, redshift=redshift)
        elif cmd == 'plot:wave-range':
            print('Old limits: %s' % repr(ax.get_xlim()))
            print('New limits: %s' % repr(rule[1], rule[2]))
            ax.set_xlim((rule[1], rule[2]))
    
    # draw the He I lines
    # idx1 = np.logical_and(wave_grid > 5600, wave_grid < 5876-70)
    # ax.text(5670, 0.94, 'He I 5876', transform=trans, rotation='vertical', fontsize=12)
    # ax.axvline(5876, alpha=0.3, ls='--', color='green')
    
    # idx2 = np.logical_and(wave_grid > 6300, wave_grid < 6678-70)
    # ax.text(6370, 0.94, 'He I 6678', transform=trans, rotation='vertical', fontsize=12)
    # ax.axvline(6678, alpha=0.3, ls='--', color='green')
    
    # idx3 = np.logical_and(wave_grid > 6700, wave_grid < 7065-70)
    # ax.text(6770, 0.94, 'He I 7065', transform=trans, rotation='vertical', fontsize=12)
    # ax.axvline(7065, alpha=0.3, ls='--', color='green')
    
    # idx4 = np.logical_and(wave_grid > 4150, wave_grid < 4472-70)
    # ax.text(4220, 0.94, 'He I 4472', transform=trans, rotation='vertical', fontsize=12)
    # ax.axvline(4472, alpha=0.3, ls='--', color='green')
    
    # idx = np.logical_or(idx1, np.logical_or(idx2, np.logical_or(idx3, idx4)))
    # ax.fill_between(wave_grid, 0, 1, where=idx, facecolor='green', alpha=0.25, transform=trans)

    if output:
        fig.tight_layout()
        fig.savefig(output)
    else:
        plt.show()
    


if __name__ == '__main__':
    main()
