import os, sys, logging, click
import numpy as np
import matplotlib
import astropy.table as table
import seaborn
import astropy.constants as aconst
import astropy.units as u

try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../../lib'))
from snelib19odp.specds import load_dataset
import snelib19odp.absfit as absfit


C_KM_S = aconst.c.to(u.km/u.s).value

DATA_DIR = os.path.join(os.path.dirname(__file__), '../../data')


def load_measurements(sds, entry, rev, line):
    fname = sds.get_absfit_path(entry, fit_rev=rev, line=line)
    #print(fname)
    if not os.path.isfile(fname):
        # TODO: log a warning?
        return None, None
    
    result = absfit.AbsFitResult.load(fname)

    ret = {'fname': entry}

    # ret['logz'] = result.meta['logz']

    for i,label in enumerate(result.parameter_names):
        q = np.quantile(result.trace[:,i], [0.025, 0.5, 0.975, 0.841, 0.159])
        ret['%s_q2.5' % label] = q[0]
        ret['%s_q50' % label] = q[1]
        ret['%s_q97.5' % label] = q[2]
        ret['%s_q84.1' % label] = q[3]
        ret['%s_q15.9' % label] = q[4]

    # extract velocities
    center_idx = result.parameter_names.index('center')

    velocities = (np.array(result.trace[:,center_idx], dtype=np.float) - line)/line * C_KM_S
    label = 'velocity'
    q = np.quantile(velocities*-1, [0.025, 0.5, 0.975, 0.841, 0.159])
    ret['%s_q2.5' % label] = q[0]
    ret['%s_q50' % label] = q[1]
    ret['%s_q97.5' % label] = q[2]
    ret['%s_q84.1' % label] = q[3]
    ret['%s_q15.9' % label] = q[4]

    # calculate the equivalent width
    amplitude_idx = result.parameter_names.index('amplitude')
    continuum_idx = result.parameter_names.index('continuum')
    ew = np.exp(result.trace[:, amplitude_idx]) / np.abs(result.trace[:, continuum_idx])
    label = 'EW'
    q = np.quantile(ew, [0.025, 0.5, 0.975, 0.841, 0.159])
    ret['%s_q2.5' % label] = q[0]
    ret['%s_q50' % label] = q[1]
    ret['%s_q97.5' % label] = q[2]
    ret['%s_q84.1' % label] = q[3]
    ret['%s_q15.9' % label] = q[4]

    # extract emission velocites
    if 'center_offset_em' in result.parameter_names:
        em_center_offset_idx = result.parameter_names.index('center_offset_em')

        em_velocities = (np.array(result.trace[:,center_idx]+result.trace[:, em_center_offset_idx], dtype=np.float) - line)/line * C_KM_S
        label = 'velocity_em'
        q = np.quantile(em_velocities*-1, [0.025, 0.5, 0.975, 0.841, 0.159])
        ret['%s_q2.5' % label] = q[0]
        ret['%s_q50' % label] = q[1]
        ret['%s_q97.5' % label] = q[2]
        ret['%s_q84.1' % label] = q[3]
        ret['%s_q15.9' % label] = q[4]
    
    # TODO: extract more interesting params
    
    return ret, velocities


# from  https://stackoverflow.com/questions/21844024/weighted-percentile-using-numpy
def weighted_quantile(values, quantiles, sample_weight=None, 
                      values_sorted=False, old_style=False):
    """ Very close to numpy.percentile, but supports weights.
    NOTE: quantiles should be in [0, 1]!
    :param values: numpy.array with data
    :param quantiles: array-like with many quantiles needed
    :param sample_weight: array-like of the same length as `array`
    :param values_sorted: bool, if True, then will avoid sorting of
        initial array
    :param old_style: if True, will correct output to be consistent
        with numpy.percentile.
    :return: numpy.array with computed quantiles.
    """
    values = np.array(values)
    quantiles = np.array(quantiles)
    if sample_weight is None:
        sample_weight = np.ones(len(values))
    sample_weight = np.array(sample_weight)
    assert np.all(quantiles >= 0) and np.all(quantiles <= 1), \
        'quantiles should be in [0, 1]'

    if not values_sorted:
        sorter = np.argsort(values)
        values = values[sorter]
        sample_weight = sample_weight[sorter]

    weighted_quantiles = np.cumsum(sample_weight) - 0.5 * sample_weight
    if old_style:
        # To be convenient with numpy.percentile
        weighted_quantiles -= weighted_quantiles[0]
        weighted_quantiles /= weighted_quantiles[-1]
    else:
        weighted_quantiles /= np.sum(sample_weight)
    return np.interp(quantiles, weighted_quantiles, values)




def plot_yu16_comparison(ax, line, max_phase=100):
    if line == 5876:
        lbl = "VHeI5"
    elif line == 6678:
        lbl = "VHeI6"
    elif line == 7065:
        lbl = "VHeI7"
    else:
        return
    
    dat = table.Table.read(os.path.join(DATA_DIR, "comparison/J_ApJ_827_90_table3.dat.fits"))

    idx = np.logical_and(dat["e_%s" % lbl] > 0, dat["Type"] == "IB ")
    idx = np.logical_and(idx, dat["Phase"] < max_phase)

    phase_grid = np.linspace(-20, max_phase)
    v_min = np.empty_like(phase_grid)
    v_max = np.empty_like(phase_grid)
    dphase = np.diff(phase_grid).mean()

    for i in range(len(phase_grid)):
        idx2 = np.logical_and(idx, np.abs(dat["Phase"] - phase_grid[i]) < 2*dphase)
        if np.count_nonzero(idx2) < 3:
            v_min[i] = np.nan
            v_max[i] = np.nan
        else:
            y = -dat[lbl][idx2]
            yerr = dat["e_%s" % lbl][idx2]
            yweight = 1/yerr
            v_min[i], v_max[i] = weighted_quantile(y, [0.05, 0.95], yweight)

    # TODO: interpolate the inner missing values

    ax.fill_between(phase_grid, v_min, v_max, color="k", alpha=0.2)


@click.command()
@click.option('--debug', is_flag=True)
@click.option('--ew', is_flag=True)
@click.option('-o', '--output')
@click.option('--dataset', default='SN2019odp')
@click.option('--yu16-comparison', is_flag=True)
@click.argument('lines', nargs=-1)
def main(debug, output, dataset, lines, ew, yu16_comparison):
    if debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    
    if output:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    if output:
        plt.rcParams.update({'font.size': 20})

    num_lines = len(lines)

    fig, axs = plt.subplots(nrows=num_lines, sharex=True, figsize=(11, 11))
    if num_lines == 0:
        axs = []
    elif num_lines == 1:
        axs = [axs]
    
    #fig = plt.figure(figsize=(15,11))
    #ax = fig.add_subplot(111)

    if yu16_comparison:
        for ax, line in zip(axs, lines):
            plot_yu16_comparison(ax, int(line))

    # load the dataset
    for ds_name in dataset.split(','):
        ds_name, rev = ds_name.split(':')
        sds = load_dataset(ds_name)
        #print(sds.names)
        for ax,line in zip(axs, lines):
            print(f'\n==> {ds_name} {line}')
            #line, rev = line.split(':')
            line = int(line)
            rev = int(rev)
            rows = []
            
            for entry in sds.names:
                measurements,velocities = load_measurements(sds, entry, rev, line)
                if measurements is None:
                    continue
                measurements['phase'] = sds[entry].meta['phase']
                #print(sds[entry].meta['phase'])
                rows.append(measurements)

                #plt.violinplot(velocities, positions=[measurements['phase']], showmedians=True, showextrema=False, quantiles=[0.025, 0.975])
                #plt.boxplot(velocities, positions=[measurements['phase']], showfliers=False)

            # skip if we didn't load any data...
            if len(rows) < 1:
                continue
            
            # create a table from the rows
            t = table.Table(rows=rows)
            t.sort('phase')
            #print(t)

            #idx = t['amplitude_q15.9'] > 3
            #idx = np.logical_and(idx, t['phase'] < 150)
            idx = t['phase'] < 100
            if not ew:
                idx = np.logical_and(idx, t['EW_q15.9'] > 5)
            #print(t[idx])
            print(t['velocity_q50', 'velocity_q2.5', 'velocity_q97.5', 'fname'][idx])
            t = t[idx]

            
            if ew:
                ax.errorbar(t['phase'], t['EW_q50'], yerr=(t['EW_q50']-t['EW_q15.9'], t['EW_q84.1']-t['EW_q50']), capsize=3.0, label=ds_name, ls='solid', marker='.')
            else:
                ax.errorbar(t['phase'], t['velocity_q50'], yerr=(t['velocity_q50']-t['velocity_q2.5'], t['velocity_q97.5']-t['velocity_q50']), capsize=3.0, label=ds_name, ls='solid', marker='.')
            #

        #idx = t['amplitude_em_q15.9'] > 1
        #t = t[idx]
        
        #plt.errorbar(t['phase'], t['velocity_em_q50'], yerr=(t['velocity_em_q50']-t['velocity_em_q15.9'], t['velocity_em_q84.1']-t['velocity_em_q50']), capsize=3.0, label='%.0f Emission' % line, ls='', marker='.')
        #plt.errorbar(t['phase'], t['amplitude_q50'], yerr=(t['amplitude_q50']-t['amplitude_q15.9'], t[)/2, label=line)
        
    for ax, line in zip(axs, lines):
        if ew:
            ax.set_ylabel('Equivalent Width [A]')
        else:
            ax.set_ylabel(r'He I $\lambda %s$ Line Velocity [km/s]' % line)
            ax.set_ylim((5000, 18e3))
        ax.legend()
        #ax.grid()
    axs[-1].set_xlabel('Phase[d]')
    
    if output:
        fig.tight_layout()
        fig.savefig(output)
    else:
        plt.show()
    


if __name__ == '__main__':
    main()
