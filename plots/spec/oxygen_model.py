import click, glob, re, sys, os, collections, time, logging
import pandas as pd
import numpy as np
import astropy.table as table
import matplotlib
from dynesty import utils as dyfunc
from dynesty import plotting as dyplot

try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../../lib'))
import snelib19odp.const as const
import snelib19odp.analysis.oxygen_lum as oxygen_lum



class MyResults(object):

    def __init__(self, trace):
        print(trace)
        print(trace.colnames)
        def convert_data(name, data):
            if name == 'Dist':
                return np.log10(data)
            else:
                return data
        self.samples = np.column_stack(tuple([convert_data(x, trace[x].data) for x in oxygen_lum.LABELS]))
        if 'weights' not in trace:
            # TODO: print warning
            self.weights = np.ones(len(trace))/len(trace)
        else:
            self.weights = trace['weights']
        #self.labels = oxygen_lum.LABELS
        self.labels = ['M(O I)', 'T', 'log10 D', 'Tau', 'NLTE']

    def __getitem__(self, name):
        if name in ['samples', 'weights', 'labels']:
            return getattr(self, name)
        else:
            raise KeyError(name)

    def importance_weights(self):
        return self.weights

def plot_corner(fit_file, output):
    if os.path.isdir(fit_file):
        files = glob.glob(os.path.join(fit_file, '*.fits'))
        fit_file = sorted(files)[-1]
    trace = table.Table.read(fit_file)

    #results = dyfunc.Results([(cn, trace[cn].data) for cn in trace.colnames])
    results = MyResults(trace)

    #labels = list(filter(lambda a: a != 'weights', trace.colnames))
    labels = results.labels

    import matplotlib.pyplot as plt
    fig, axs = plt.subplots(5, 5, figsize=(20,20))
    
    (fig, axs) = dyplot.cornerplot(results, show_titles=True, labels=labels, fig=(fig, axs))

    if output:
        #fig.tight_layout()
        fig.savefig(output)
    else:
        
        plt.show()

def plot_nlte_diag(diag_dir, output):
    # get the files
    fit_files = glob.glob(os.path.join(diag_dir, "*.fits"))
    nlte = np.array(list(map(lambda a: float(os.path.basename(a).split(".fits")[0]), fit_files)))

    # table containing all the final things
    x = table.Table({'nlte': nlte, 'fname': fit_files})
    #print(x)
    for qlabel in ['q16', 'q50', 'q84']:
        x[qlabel] = np.empty_like(nlte)
    
    for i, fname in enumerate(fit_files):
        trace = table.Table.read(fname)
        p = np.percentile(trace['MOI'], [15.9, 50, 84.1])
        x['q16'][i] = p[0]
        x['q50'][i] = p[1]
        x['q84'][i] = p[2]
    
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(1, figsize=(12,9))
    ax.scatter(nlte, x['q16'], label='16%')
    ax.scatter(nlte, x['q50'], label='50%')
    ax.scatter(nlte, x['q84'], label='84%')
    ax.legend(title='Percentile')
    ax.set_xscale('log')
    ax.set_xlabel('NLTE Deviation Factor')
    ax.set_ylabel('Oxygen Mass [$M_\odot$]')
    #ax.grid()

    if output:
        #fig.tight_layout()
        fig.savefig(output)
    else:
        
        plt.show()
        

@click.command()
@click.option('--debug', is_flag=True)
@click.option('-o', '--output')
@click.option('--corner', is_flag=True)
@click.option('--nlte-diag', is_flag=True)
@click.option('--d2-min', default=1e-4)
@click.option('--d2-max', default=1.0)
@click.option('--temp-min', default=300.0)
@click.option('--temp-max', default=1e4)
@click.option('--mark-d2-range', default=None)
@click.argument('fit_files', nargs=-1)
def main(debug, output, fit_files, corner, nlte_diag, d2_min, d2_max, temp_min, temp_max, mark_d2_range):
    if debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    if output:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    if output:
        plt.rcParams.update({'font.size': 20})

    if corner:
        assert len(fit_files) == 1
        plot_corner(fit_files[0], output)
        return
    elif nlte_diag:
        assert len(fit_files) == 1
        assert os.path.isdir(fit_files[0])
        plot_nlte_diag(fit_files[0], output)
        return

    fig, ax = plt.subplots(1, figsize=(15, 11))
    a = None
    norm = plt.Normalize(np.log10(0.1), np.log10(1.0))
    #norm = plt.Normalize(2500, 8000)
    for fit_file in fit_files:
        if os.path.isdir(fit_file):
            files = glob.glob(os.path.join(fit_file, '*.fits'))
            fit_file = sorted(files)[-1]
        trace = table.Table.read(fit_file)

        # TODO: read more info from meta
        label = os.path.basename(fit_file)

        temp_range = np.linspace(trace['Temp'].min(), trace['Temp'].max())
        temp_diff = np.mean(np.diff(temp_range))
        min_mass = np.empty_like(temp_range)
        for i in range(len(temp_range)):
            idx = np.abs(trace['Temp']-temp_range[i]) < temp_diff
            if np.count_nonzero(idx) > 0:
                min_mass[i] = trace['MOI'][idx].min()
            else:
                min_mass[i] = np.nan

        idx1 = np.logical_and(trace['NLTE'] > d2_min, trace['NLTE'] < d2_max)
        idx2 = np.logical_and(trace['Temp'] > temp_min, trace['Temp'] < temp_max)
        idx = np.logical_and(idx1, idx2)
        #ax.plot(temp_range, min_mass, label=label)
        if 'not_late' in fit_file:
            a=ax.scatter(trace['Temp'][idx]+500, trace['MOI'][idx], c=np.log10(trace['NLTE'][idx]), s=0.1, cmap='gist_rainbow', norm=norm)
        else:
            a=ax.scatter(trace['Temp'][idx], trace['MOI'][idx], c=np.log10(trace['NLTE'][idx]), s=0.1, cmap='gist_rainbow', norm=norm)
        #a=ax.scatter(trace['NLTE'], trace['MOI'], c=trace['Temp'], s=0.1, cmap='gist_rainbow', norm=norm)

        print(f"==> {fit_file})")
        m = trace['MOI'][idx]
        print(f"\tPercentiles: {repr(np.percentile(m, [1, 50, 99]))}")
        

    #ax.legend()
    ax.set_xlabel('Temperature [K]')
    #ax.set_xlabel('NLTE Deviation')
    ax.set_ylabel('Oxygen Mass [Msol]')
    #ax.grid(ls='--')
    #ax.set_xscale('log')
    cbar = fig.colorbar(a, ax=ax)
    cbar.set_label('log10 NLTE Deviation')
    if mark_d2_range is not None:
        p = mark_d2_range.split(',')
        upper, lower = float(p[0]), float(p[1])
        mean_v = np.log10((upper+lower)/2)
        top_err = np.log10(upper) - mean_v
        bot_err = mean_v - np.log10(lower)
        print('colorbar X:', mean_v)
        print('range: ', cbar.ax.get_xlim())
        cbar.ax.errorbar(-0.5, mean_v, yerr=([top_err], [bot_err]), color='white', capsize=5)

    ax.axvline(3888, ls='--', color='black')

    if output:
        fig.tight_layout()
        fig.savefig(output)
    else:
        plt.show()



if __name__ == '__main__':
    main()
