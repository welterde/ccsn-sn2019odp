import click, glob, re, sys, os, collections, time, logging
import pandas as pd
import numpy as np
import matplotlib
import astropy.table as table
from dynesty import utils as dyfunc
from dynesty import plotting as dyplot

try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../../lib'))
import snelib19odp.const as const

CONFIG_DIR = os.path.join(os.path.dirname(__file__), '../../config')
PRODUCTS_DIR = os.path.join(os.path.dirname(__file__), '../../products')
sys.path.append(CONFIG_DIR)

import models




class MyResults(object):

    def __init__(self, trace, labels):
        self.samples = np.column_stack(tuple([trace[x].data for x in labels]))
        if 'weights' not in trace:
            # TODO: print warning
            self.weights = np.ones(len(trace))/len(trace)
        else:
            self.weights = trace['weights']
        #self.labels = oxygen_lum.LABELS
        self.labels = labels

    def __getitem__(self, name):
        if name in ['samples', 'weights', 'labels']:
            return getattr(self, name)
        else:
            raise KeyError(name)

    def importance_weights(self):
        return self.weights


@click.command()
@click.option('--debug', is_flag=True)
@click.option('--output', '-o')
@click.option('--sample-num', default=1000)
@click.option('--corner', is_flag=True)
@click.option('--show-titles', is_flag=True)
@click.argument('spec_name')
@click.argument('model_name')
@click.argument('trace_file')
def main(debug, output, spec_name, model_name, trace_file, sample_num, corner, show_titles):
    if debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    if output:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    if output:
        plt.rcParams.update({'font.size': 20})

    
    model = models.make_model(model_name, spec_name)
    plot_func = model['plot_func']
    plot_spec_func = model['plot_spec_func']
    #corner_plot_func = model['plot_corner']

    # load the trace file
    trace = table.Table.read(trace_file, format='fits')

    if corner:
        results = MyResults(trace, model['labels'])
        ndim = len(model['labels'])
        #fig = plot_corner_func(results)
        import matplotlib.pyplot as plt
        fig, axs = plt.subplots(ndim, ndim, figsize=(3*ndim+2, 3*ndim+2))

        #if rename_labels:
        #labels = remap_label_names(model_name, model['plot_labels'])
        #else:
        labels = model['plot_labels']
    
        (fig, axs) = dyplot.cornerplot(results, show_titles=show_titles, labels=labels, fig=(fig, axs))
    else:

        # resample the trace
        #samples = np.column_stack((lum_trace['log_F6300'].data, lum_trace['R6364'].data, lum_trace['R5577'].data))
        samples = np.column_stack(tuple([trace[x].data for x in model['labels']]))
        weights = trace['weights']
        new_samples = dyfunc.resample_equal(samples, weights)
        
        fig, (ax_5577, ax_63xx) = plt.subplots(2, figsize=(15, 11))

        plot_spec_func(ax_63xx=ax_63xx, ax_5577=ax_5577)
    
        for i in range(sample_num):
            p = new_samples[np.random.randint(0, new_samples.shape[0])]
            
            plot_func(p, ax_63xx=ax_63xx, ax_5577=ax_5577)
            
        ax_5577.set_xlabel(r'Rest Wavelength [$\AA$]')
        ax_63xx.set_xlabel(r'Rest Wavelength [$\AA$]')
        ax_5577.set_ylabel('Flux')
        ax_63xx.set_ylabel('Flux')

    if output:
        if not corner:
            fig.tight_layout()
        fig.savefig(output)
    else:
        plt.show()


if __name__ == '__main__':
    main()
