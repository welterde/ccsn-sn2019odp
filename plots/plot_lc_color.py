#!/usr/bin/env python
import sys, os
import pandas as pd
import click
import matplotlib

import h5py
import astropy.time as time
import numpy as np

try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../lib'))
import snelib19odp.datasets.sn1993j as sn1993j
import snelib19odp.datasets.ptf12os as ptf12os
import snelib19odp.datasets.sn2008d as sn2008d
import snelib19odp.datasets.ptf13bvn as ptf13bvn
import snelib19odp.const as const

# TODO: read that from somewhere else
PHASE_CENTER_MYSN = const.sne_peak_mjd['g']

MJD_CUTOFF_MYSN = 1e12


def detect_peak(df, col):
    x = df[col].idxmin()
    print(x)
    return x
    

EXTINCTION_COEFF = {
    'g': 3.303,
    'r': 2.285,
    'i': 1.698,
    'z': 1.263,
    # from https://irsa.ipac.caltech.edu/workspace/TMP_3mtiHn_9142/DUST/SN2004aw.v0001/extinction.html
    'U': 4.107,
    'B': 3.641,
    'V': 2.682,
    'R': 2.119,
    'I': 1.516,
    'J': 0.723,
    'H': 0.460,
    'K': 0.310
}


@click.command()
@click.option('--show', is_flag=True)
#@click.argument('raw_lc')
@click.option('--destfile')
def main(destfile, show):
    if not show:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    plt.rcParams.update({'font.size': 20})
    fig = plt.figure(figsize=(15,9))
    ax = fig.add_subplot(111)

    ### plot SN1993J lc
    df93j = sn1993j.sloan_lc
    phase_center = detect_peak(df93j, 'r_mag')
    interp_idx = df93j.index.get_level_values('mjd') < phase_center + 130
    df93j = sn1993j.sloan_lc.iloc[interp_idx]
    interp_key = 'g'
    mag_g_max = df93j['%s_mag' % interp_key] - 1*df93j['%s_mag_err' % interp_key]
    mag_g_min = df93j['%s_mag' % interp_key] + 1*df93j['%s_mag_err' % interp_key]
    interp_key = 'r'
    mag_r_max = df93j['%s_mag' % interp_key] - 1*df93j['%s_mag_err' % interp_key]
    mag_r_min = df93j['%s_mag' % interp_key] + 1*df93j['%s_mag_err' % interp_key]

    color_max = mag_g_max - mag_r_min
    color_min = mag_g_min - mag_r_max
    
    #ax.fill_between(df93j.index - phase_center, color_max, color_min, alpha=0.3, label='SN1993J g-r')

    ### plot SN2008D lc
    df08d = sn2008d.interpolated_lc2
    phase_center = sn2008d.phase_center #detect_peak(df08d, 'R_mag')
    interp_idx = df08d.index.get_level_values('mjd') < phase_center + 130
    df08d = sn2008d.interpolated_lc2.iloc[interp_idx]
    interp_key = 'V'
    mag_g_max = df08d['%s_mag' % interp_key] - 1*df08d['%s_mag_err' % interp_key]
    mag_g_min = df08d['%s_mag' % interp_key] + 1*df08d['%s_mag_err' % interp_key]
    interp_key = "r'"
    mag_r_max = df08d['%s_mag' % interp_key] - 1*df08d['%s_mag_err' % interp_key]
    mag_r_min = df08d['%s_mag' % interp_key] + 1*df08d['%s_mag_err' % interp_key]

    extinction = (EXTINCTION_COEFF['V']-EXTINCTION_COEFF['r'])*0.6
    color_max = mag_g_max - mag_r_min - extinction
    color_min = mag_g_min - mag_r_max - extinction
    
    ax.fill_between(df08d.index - phase_center, color_max, color_min, alpha=0.3, label='SN2008D V-R')

    ### PTF12os
    df12os = ptf12os.lc_interp
    phase_center = detect_peak(df12os, 'r_mag')
    
    interp_key = 'g'
    mag_g_max = df12os['%s_mag' % interp_key] - 1*df12os['%s_mag_err' % interp_key]
    mag_g_min = df12os['%s_mag' % interp_key] + 1*df12os['%s_mag_err' % interp_key]
    interp_key = 'r'
    mag_r_max = df12os['%s_mag' % interp_key] - 1*df12os['%s_mag_err' % interp_key]
    mag_r_min = df12os['%s_mag' % interp_key] + 1*df12os['%s_mag_err' % interp_key]

    color_max = mag_g_max - mag_r_min
    color_min = mag_g_min - mag_r_max

    #ax.fill_between(df12os.index - phase_center, color_max, color_min, alpha=0.3, label='PTF12os g-r')

    ### PTF13bvn
    df13bvn = ptf13bvn.lc_interp
    phase_center = detect_peak(df13bvn, 'r_mag')
    
    interp_key = 'g'
    mag_g_max = df13bvn['%s_mag' % interp_key] - 1*df13bvn['%s_mag_err' % interp_key]
    mag_g_min = df13bvn['%s_mag' % interp_key] + 1*df13bvn['%s_mag_err' % interp_key]
    interp_key = 'r'
    mag_r_max = df13bvn['%s_mag' % interp_key] - 1*df13bvn['%s_mag_err' % interp_key]
    mag_r_min = df13bvn['%s_mag' % interp_key] + 1*df13bvn['%s_mag_err' % interp_key]

    color_max = mag_g_max - mag_r_min
    color_min = mag_g_min - mag_r_max
    ax.fill_between(df13bvn.index - phase_center, color_max, color_min, alpha=0.3, label='PTF13bvn g-r')
    
    ### plot combined g-r color
    # load combined interpolated lightcurve
    df_interpolated = pd.read_hdf('products/lc_interpolated_combined.h5', '/lc')
    phase_center = detect_peak(df_interpolated, 'mag_"r"_global')

    interp_idx = df_interpolated.index.get_level_values('mjd') < MJD_CUTOFF_MYSN
    df_interp_sel = df_interpolated.iloc[interp_idx]
    interp_key = '"g"_global'
    mag_g_max = df_interp_sel['mag_%s' % interp_key] - 1*df_interp_sel['magerr_%s' % interp_key]
    mag_g_min = df_interp_sel['mag_%s' % interp_key] + 1*df_interp_sel['magerr_%s' % interp_key]
    interp_key = '"r"_global'
    mag_r_max = df_interp_sel['mag_%s' % interp_key] - 1*df_interp_sel['magerr_%s' % interp_key]
    mag_r_min = df_interp_sel['mag_%s' % interp_key] + 1*df_interp_sel['magerr_%s' % interp_key]

    color_max = mag_g_max - mag_r_min
    color_min = mag_g_min - mag_r_max

    ax.fill_between(df_interp_sel.index - phase_center, color_max, color_min, alpha=0.4, label='SN2019odp g-r')

    
    
    ax.legend()
    #ax.invert_yaxis()
    ax.set_xlabel('Phase [d]')
    ax.set_ylabel('Color Index [mag]')

    ax.set_xlim([-30, 80])

    if not show:
        fig.savefig(destfile)
    else:
        plt.show()




if __name__ == '__main__':
    main()
