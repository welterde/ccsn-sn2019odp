import json
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import astropy.table as table
import numpy as np
import astropy.time as time
import spectres
import collections
import click
import scipy.optimize as optimize


DATASETS = {
    '10vgv': {
        'filename': 'data/comparison/PTF10vgv.json',
        'name': 'PTF10vgv',
        'type': 'Ic-BL'
    },
    '2008d': {
        'filename': 'data/comparison/SN2008D.json',
        'name': 'SN2008D',
        'type': 'Ib',
    },
    '2008ax': {
        'filename': 'data/comparison/SN2008ax.json',
        'name': 'SN2008ax',
        'type': 'IIb'
    }
}

for dataset_name in DATASETS.keys():
    entry = DATASETS[dataset_name]
    with open(entry['filename'], 'r') as f:
        data = json.load(f)
        sne_name = list(data.keys())[0]
        entry['data'] = data[sne_name]
    
    # extract peak time
    peak_time = time.Time(entry['data']['maxvisualdate'][0]['value'].replace('/','-')).mjd
    entry['peak_mjd'] = peak_time

Entry = collections.namedtuple('Entry', 'dataset index label specbins fit_range')
Spec = collections.namedtuple('Spec', 'wave flux phase_t')

COMPARISONS = {
    'Ib-early': [
        Entry('19odp', 'data/specs/preproc_marshall/ZTF19abqwtfu_20190823_NTT_v1.ascii', 'NTT', 300, None),
        Entry('2008d', 1, '2008d (Ib @ %d)', 300, None),
        Entry('2008d', 6, '2008d (Ib @ %d)', 300, None)
    ],
    'Icbl-early': [
        Entry('19odp', 'data/specs/preproc_marshall/ZTF19abqwtfu_20190823_NTT_v1.ascii', 'NTT', 300, None),
        Entry('10vgv', 0, 'PTF10vgv (Ic-BL @ %d)', 300, None),
        #Entry('10vgv', 1, 'PTF10vgv (Ic-BL @ %d)', 300),
    ],
    'IIb-early': [
        Entry('19odp', 'data/specs/preproc_marshall/ZTF19abqwtfu_20190823_NTT_v1.ascii', 'NTT', 300, None),
        Entry('2008ax', 0, 'SN2008ax (IIb @ %d)', 300, (3600, 4500)),
        Entry('2008ax', 1, 'SN2008ax (IIb @ %d)', 300, (3600, 4500)),
        Entry('2008ax', 2, 'SN2008ax (IIb @ %d)', 300, (3600, 4500)),
    ]
}

DEFAULT_FIT_RANGE = (3800, 6000)


def load_spec(entry):
    if entry.dataset == '19odp':
        data = table.Table.read(entry.index, format='ascii')
        wave = data['col1']/(1+0.0143)
        flux = data['col2']
        phase_t = None
    else:
        data = DATASETS[entry.dataset]['data']['spectra'][entry.index]
        wave = np.array([float(y[0]) for y in data['data']])
        flux = np.array([float(y[1]) for y in data['data']])
        phase_t = float(data['time']) - DATASETS[entry.dataset]['peak_mjd']
    wave_grid = np.linspace(wave.min(), wave.max(), 300)
    new_flux = spectres.spectres(wave_grid, wave, flux)
    #if new_flux.max() < 1e-5:
    max_val = np.quantile(new_flux[~np.isnan(new_flux)], 0.99)
    #print(new_flux)
    #print('Max Val: ', max_val)
    new_flux /= max_val
    
    return Spec(wave_grid, new_flux, phase_t)

# def normalize(spec):
#     idx = np.logical_and(spec.wave > 4000, spec.wave < 5000)
#     mean_flux = np.nanmean(spec.flux[idx])
#     new_flux = spec.flux/mean_flux
#     return Spec(spec.wave, new_flux, spec.phase_t)


@click.command()
@click.argument('comparison')
@click.argument('destfile')
def main(comparison, destfile):
    entries = COMPARISONS[comparison]

    plt.figure(figsize=(11,11))

    ref_spec = None
    
    for entry in entries:
        spec = load_spec(entry)
        #spec = normalize(spec)
        if entry.fit_range is None:
            fit_range = DEFAULT_FIT_RANGE
        else:
            fit_range = entry.fit_range
        
        if ref_spec is None:
            ref_spec = spec
            flux = spec.flux
        else:
            # resample this spectrum to the same grid as the reference spectrum
            grid_min = max(ref_spec.wave.min(), spec.wave.min())
            grid_max = min(ref_spec.wave.max(), spec.wave.max())
            grid = np.linspace(grid_min, grid_max, 80)
            new_flux = spectres.spectres(grid, spec.wave, spec.flux)
            ref_flux = spectres.spectres(grid, ref_spec.wave, ref_spec.flux)
            print(new_flux)
            print(ref_flux)
            grid_idx = np.logical_and(grid > fit_range[0], grid < fit_range[1])
            
            def func(p):
                continuum, log_amplitude_scaling = p
                amplitude_scaling = np.exp(log_amplitude_scaling)
                adj_flux = (new_flux+continuum)*amplitude_scaling
                diff_flux = (adj_flux - ref_flux)[grid_idx]
                
                return np.sum(diff_flux**2)

            bounds = [
                (-5, 5),
                (-5, 5)
            ]
            result = optimize.differential_evolution(func, bounds)
            print(result)

            flux = (spec.flux+result.x[0])*np.exp(result.x[1])

        if '%d' in entry.label and spec.phase_t is not None:
            label = entry.label % spec.phase_t
        else:
            label = entry.label

        plt.plot(spec.wave, flux, label=label)

    plt.legend()
    plt.xlabel('Rest Wavelength [A]')
    plt.ylabel('Relative Flux')
    plt.savefig(destfile)


if __name__ == '__main__':
    main()

    
