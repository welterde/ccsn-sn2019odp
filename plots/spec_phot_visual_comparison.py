#!/usr/bin/env python
import pandas as pd
import click
import matplotlib
import numpy as np

import astropy.table as table
import astropy.units as u
import astropy.constants as const


# ZTF filters
def load_filter(inst, band):
    data = table.Table.read('const/filters/%s.%s.dat' % (inst, band), format='ascii')
    wave = []
    transmission = []
    for entry in data:
        if len(wave) > 0 and wave[-1] == entry['col1']:
            continue
        wave.append(entry['col1'])
        if entry['col2'] < 0.01 or entry['col1'] < 3790:
            transmission.append(0.0)
        else:
            transmission.append(entry['col2'])
    wave = np.array(wave)*u.Angstrom
    transmission = np.array(transmission)

    return (wave, transmission)


FILTERS = {
    'ZTF_g': ('blue', load_filter('Palomar_ZTF', 'g')),
    'ZTF_r': ('green', load_filter('Palomar_ZTF', 'r')),
    'GROND_g': ('blue', load_filter('LaSilla_GROND', 'g')),
    'GROND_r': ('green', load_filter('LaSilla_GROND', 'r')),
    'GROND_i': ('red', load_filter('LaSilla_GROND', 'i')),
    'GROND_z': ('cyan', load_filter('LaSilla_GROND', 'z'))
    
}


@click.command()
@click.option('--output')
@click.option('--figsize', nargs=2, default=(15,9))
@click.option('--scale-filter', type=float, default=1.0)
@click.option('--spec', nargs=2, multiple=True)
@click.argument('filter', nargs=-1)
def main(spec, filter, scale_filter, figsize, output):
    if output:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    if output:
        plt.rcParams.update({'font.size': 20})
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111)
    
    for spec_fname, spec_label in spec:
        spec = table.Table.read(spec_fname, format='ascii')
        if 'col1' in spec.colnames:
            wave = spec['col1']
            flux = spec['col2']
        else:
            wave = spec['wave']
            flux = spec['flux']

        idx = np.logical_and(wave > 4000, wave < 9000)
        plt.plot(wave, flux/np.quantile(flux[idx], 0.90)/scale_filter, label=spec_label)

    colors = ['green', 'red', 'blue', 'violet', 'cyan']
        
    for (filter_name, color) in zip(filter, colors):
        (color, (wave, transmission)) = FILTERS[filter_name]
        plt.plot(wave, transmission, label=filter_name, color=color)
        plt.fill_between(wave, 0, transmission, alpha=0.1, color=color)

    plt.legend()
    if output:
        fig.savefig(output)
    else:
        plt.show()




if __name__ == '__main__':
    main()
