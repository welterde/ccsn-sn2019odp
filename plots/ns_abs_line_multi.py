#!/usr/bin/env python
import click, glob, os, re, json, sys
import numpy as np
import matplotlib
import astropy.time as time
from datetime import datetime
import astropy.table as table
import george
from george import kernels
import scipy.optimize as optimize
try:
    import snelib19odp
except ImportError:
    sys.path.append('lib')
import snelib19odp.const as const
import snelib19odp.datasets.ptf13bvn as ptf13bvn


FILE_RE = re.compile(r'^.*_(\d+)_.*$')

MJD_FILE_RE = re.compile(r'^.*_mjd(\d+)\..*$')

def extract_time(fname):
    m = MJD_FILE_RE.match(fname)
    if m:
        return float(m.groups(1)[0])
    m = FILE_RE.match(fname)
    dat = m.groups(1)[0]
    #print(dat)
    d2 = datetime.strptime(dat, '%Y%m%d')
    #print(d2)
    #print(Time(d2).mjd)
    return time.Time(d2).mjd

def extract_jd(fname):
    m = MJD_FILE_RE.match(fname)
    if m:
        return float(m.groups(1)[0]) + 2400000.5
    m = FILE_RE.match(fname)
    dat = m.groups(1)[0]
    #print(dat)
    d2 = datetime.strptime(dat, '%Y%m%d')
    #print(d2)
    #print(Time(d2).mjd)
    return time.Time(d2).jd


LABELS = ['Continuum', 'Slope', 'Amplitude', 'Sigma', 'Center', 'ln f']

# TODO: move to common module
# TODO: add err
ZERO_JD = {
    'ZTF19abqwtfu': const.sne_peak_mjd['g'] + 2400000.5,
    'iPTF13bvn': ptf13bvn.sne_peak_mjd['g'] + 2400000.5
}

EXPLOSION_JD = {
    'ZTF19abqwtfu': (const.explosion_epoch_mjd + 2400000.5, const.explosion_epoch_mjd_err),
    'iPTF13bvn': (ptf13bvn.sne_explosion_mjd + 2400000.5, ptf13bvn.sne_explosion_err)
}

@click.command()
@click.option('-i', '--interactive', is_flag=True)
@click.option('-d', '--dest-file')
@click.option('--max-phase', default=300)
@click.option('--peak-phase', is_flag=True)
@click.argument('datadirs', nargs=-1)
def main(datadirs, interactive, dest_file, max_phase, peak_phase):
    if peak_phase:
        raise ValueError('peak phase not implemented')
    
    if not interactive:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt

    fig, (ax1, ax2) = plt.subplots(2, sharex=True, figsize=(9, 9))
    ax1.set_xlabel('Phase since Explosion [d]')
    ax1.set_ylabel('Velocity [km/s]')

    ax2.set_xlabel('Phase since Explosion [d]')
    ax2.set_ylabel('log Amplitude')
    
    for spec in datadirs:
        datadir, center_s = spec.split(':')
        center = int(center_s)
        
        files = list(glob.glob(os.path.join(datadir, '*.json')))
        sne_name = os.path.basename(files[0]).split('_')[0]
    
        # extract mjds
        mjds = [extract_time(os.path.basename(x)) for x in files]
        phases = [extract_jd(os.path.basename(x)) - EXPLOSION_JD[sne_name][0] for x in files]
        phase_err = EXPLOSION_JD[sne_name][1]
        
        data = []
        for fname in files:
            with open(fname, 'r') as f:
                d = json.load(f)
                data.append(d)

        rows = []
        for mjd, phase, d in zip(mjds, phases, data):
            row = [mjd, phase]
            for i, label in enumerate(LABELS):
                row.append(d['mean'][i])
                row.append(d['quantiles'][label][0])
                row.append(d['quantiles'][label][1])
            rows.append(row)

        labels = ['MJD', 'Phase']
        for i, label in enumerate(LABELS):
            labels.append('%s_mean' % label)
            labels.append('%s_q2.5' % label)
            labels.append('%s_q97.5' % label)
        t = table.Table(rows=rows, names=labels)
        t.sort('MJD')
        #print(t)
    
        mjd = t['MJD']
        phase = t['Phase']

        idx = phase < max_phase
        t = t[idx]
        mjd = t['MJD']
        phase = t['Phase']
        
        velocity_mean = (t['Center_mean']-center)/center*300e3*(-1)
        velocity_q2 = (t['Center_q2.5']-center)/center*300e3*(-1)
        velocity_q97 = (t['Center_q97.5']-center)/center*300e3*(-1)
        
        sigma_mean = (t['Sigma_mean'])/center*300e3
        sigma_q2 = (t['Sigma_q2.5'])/center*300e3
        sigma_q97 = (t['Sigma_q97.5'])/center*300e3
            
    

        ax1.errorbar(phase, velocity_mean, xerr=phase_err, yerr=(velocity_mean-velocity_q2, velocity_q97-velocity_mean), fmt='o', label='%s Line Center' % sne_name, capsize=3)
        #ax1.fill_between(phase_grid, velocity_gp - np.sqrt(velocity_gp_var), velocity_gp + np.sqrt(velocity_gp_var), alpha=0.4)
        #ax1.set_xlabel('MJD [d]')
    

        #ax1.errorbar(phase, sigma_mean, yerr=(sigma_mean-sigma_q2, sigma_q97-sigma_mean), fmt='o', label='Sigma')

        amplitude_mean = t['Amplitude_mean']
        amplitude_q2 = t['Amplitude_q2.5']
        amplitude_q97 = t['Amplitude_q97.5']
        ax2.errorbar(phase, amplitude_mean, yerr=(amplitude_mean-amplitude_q2, amplitude_q97-amplitude_mean), fmt='o', label=sne_name)
    

    ax1.legend()
    ax2.legend()
    
    if interactive:
        plt.show()
    else:
        plt.savefig(dest_file)
    
    
if __name__ == '__main__':
    main()
