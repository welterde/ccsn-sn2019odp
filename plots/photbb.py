import os, sys, logging, click
import numpy as np
import matplotlib
import astropy.table as table
import astropy.constants as aconst
import astropy.units as u
from dynesty import plotting as dyplot
from dynesty import utils as dyfunc
from scipy import interpolate
from scipy.signal import savgol_filter

try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../lib'))
import snelib19odp.dataset as dataset
import snelib19odp.blackbody as blackbody
import snelib19odp.analysis.blackbody as bbfit






CM_TO_RSUN = u.cm.to(u.Rsun)
RSUN_TO_KM = u.Rsun.to(u.km)
D_TO_S = u.d.to(u.s)


    
def plot_ds(ds, ax1, ax2, ax3, bands, fine=False, band_label=None, no_cache=False):
    mjd_peak = ds.transient.prior_t0
    if band_label is None:
        band_label = bands
    label = f"{ds.transient.name} {band_label}"
    data = bbfit.run_observation_grid(ds, bands, progressbar=False, fine_pre_peak_grid=fine, no_cache=no_cache, max_phase=50)
    #mjd_grid, data = data
    mjd_grid = np.array([x.mjd for x in data])
    temps = np.empty_like(mjd_grid)
    temp_errs = np.empty_like(mjd_grid)
    rads = np.empty_like(mjd_grid)
    rad_errs = np.empty_like(mjd_grid)
    for i, mjd in enumerate(mjd_grid):
        #obs_mags, obs_mags_errs, sampler, results = data[i]
        results = data[i].results
        samples, weights = results.samples, np.exp(results.logwt - results.logz[-1])
        trace = dyfunc.resample_equal(samples, weights)
        temps[i], temp_errs[i] = np.mean(trace[:,0]), np.std(trace[:,0])
        t_rads = np.log10(np.exp(trace[:,1])*CM_TO_RSUN)
        rads[i], rad_errs[i] = np.mean(t_rads), np.std(t_rads)

    #mjd_peak = 58735
    ax1.errorbar(mjd_grid-mjd_peak, temps, yerr=temp_errs, label=label)
    ax2.errorbar(mjd_grid-mjd_peak, rads, yerr=rad_errs, label=label)

    if ax3 is not None:
        x = mjd_grid-mjd_peak
        x_grid = np.linspace(x.min(), x.max(), len(x))
        x_grid_s = x_grid * D_TO_S
        y_interp = interpolate.interp1d(x, 10**rads*RSUN_TO_KM)(x_grid)
        y_flt = savgol_filter(y_interp, 15, 3)
        #tck = interpolate.splrep(x, 10**rads*RSUN_TO_KM)
        #y_flt = interpolate.splev(x_grid, tck, der=1)
        der = np.diff(y_flt) / np.diff(x_grid_s)
        x_grid2 = (x_grid[:-1] + x_grid[1:]) / 2
        ax3.plot(x_grid2, der, label=label)
        #ax3.plot(x_grid, y_flt, label=label)
        x2 = (x[:-1] + x[1:]) / 2
        #ax3.plot(x2, np.diff(10**rads*RSUN_TO_KM)/np.diff(x*D_TO_S), label=label)
        der = np.diff(10**rads*RSUN_TO_KM)/np.diff(x*D_TO_S)
        x2_grid = np.linspace(x2.min(), x2.max())
        der_interp = interpolate.interp1d(x2, der)(x2_grid)
        der_flt = savgol_filter(der_interp, 11, 3)
        #ax3.plot(x2_grid, der_flt, label=label)
        ax3.axhline(10912, ls='--', c='b')
        ax3.axhline(8000, ls='--', c='r')

@click.command()
@click.option('--debug', is_flag=True)
@click.option('--no-cache', is_flag=True)
@click.option('--show-velocity', is_flag=True)
@click.option('-o', '--output')
def main(debug, output, no_cache, show_velocity):
    if debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    
    if output:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    if output:
        plt.rcParams.update({'font.size': 20})

    if show_velocity:
        fig, (ax1, ax2, ax3) = plt.subplots(nrows=3, sharex=True, figsize=(11, 11))
    else:
        fig, (ax1, ax2) = plt.subplots(nrows=2, sharex=True, figsize=(11, 8))
        ax3 = None

    plot_ds(dataset.load_dataset('SN2019odp_phot'), ax1, ax2, ax3, 'gri', fine=True, no_cache=no_cache)
    plot_ds(dataset.load_dataset('SN1998bw'), ax1, ax2, ax3, ['V', 'Rc', 'Ic'], band_label='VRI', no_cache=no_cache)
    plot_ds(dataset.load_dataset('SN2008D'), ax1, ax2, ax3, 'Vri', no_cache=no_cache)
    plot_ds(dataset.load_dataset('iPTF13bvn'), ax1, ax2, ax3, 'gri', no_cache=no_cache)
    plot_ds(dataset.load_dataset('SN2002ap'), ax1, ax2, ax3, 'VRI', no_cache=no_cache)
    
    #plot_ds(data_13bvn_ri, ax1, ax2, 'iPTF13bvn ri', 56477)
    #plot_ds(data_08d_ri, ax1, ax2, 'SN2008D ri', 54493)

    ax1.set_ylabel('Temperature [K]')
    ax1.legend()
    #ax1.grid(ls='--')
    ax1.set_ylim((4000, 17000))

    ax2.set_ylabel('log10 Radius [log Rsun]')
    #ax2.grid(ls='--')

    if show_velocity:
        ax3.set_xlabel('Phase [d]')
        #ax3.grid(ls='--')
        ax3.set_ylabel('Velocity [km/s]')
    else:
        ax2.set_xlabel('Phase [d]')

    #ax2.legend()
    plt.tight_layout()


    if output:
        fig.tight_layout()
        fig.savefig(output)
    else:
        plt.show()



if __name__ == '__main__':
    main()
