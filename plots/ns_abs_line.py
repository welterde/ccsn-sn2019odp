#!/usr/bin/env python
import click, glob, os, re, json
import numpy as np
import matplotlib
import astropy.time as time
from datetime import datetime
import astropy.table as table
import george
from george import kernels
import scipy.optimize as optimize

FILE_RE = re.compile(r'^.*_(\d+)_.*$')

MJD_FILE_RE = re.compile(r'^.*_mjd(\d+)\..*$')

def extract_time(fname):
    m = MJD_FILE_RE.match(fname)
    if m:
        return float(m.groups(1)[0])
    m = FILE_RE.match(fname)
    dat = m.groups(1)[0]
    #print(dat)
    d2 = datetime.strptime(dat, '%Y%m%d')
    #print(d2)
    #print(Time(d2).mjd)
    return time.Time(d2).mjd

def extract_jd(fname):
    m = MJD_FILE_RE.match(fname)
    if m:
        return float(m.groups(1)[0]) + 2400000.5
    m = FILE_RE.match(fname)
    dat = m.groups(1)[0]
    #print(dat)
    d2 = datetime.strptime(dat, '%Y%m%d')
    #print(d2)
    #print(Time(d2).mjd)
    return time.Time(d2).jd


LABELS = ['Continuum', 'Slope', 'Amplitude', 'Sigma', 'Center', 'ln f']

ZERO_JD = {
    'ZTF19abqwtfu': 2458735.8720,
    'iPTF13bvn': 12345
}

@click.command()
@click.option('-i', '--interactive', is_flag=True)
@click.option('-d', '--dest-file')
@click.option('--center', default=5876)
@click.argument('datadir')
def main(datadir, interactive, center, dest_file):
    files = list(glob.glob(os.path.join(datadir, '*.json')))

    if not interactive:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt

    sne_name = os.path.basename(files[0]).split('_')[0]
    
    # extract mjds
    mjds = [extract_time(os.path.basename(x)) for x in files]
    phases = [extract_jd(os.path.basename(x)) - ZERO_JD[sne_name] for x in files]

    data = []
    for fname in files:
        with open(fname, 'r') as f:
            d = json.load(f)
            data.append(d)

    rows = []
    for mjd, phase, d in zip(mjds, phases, data):
        row = [mjd, phase]
        for i, label in enumerate(LABELS):
            row.append(d['mean'][i])
            row.append(d['quantiles'][label][0])
            row.append(d['quantiles'][label][1])
        rows.append(row)

    labels = ['MJD', 'Phase']
    for i, label in enumerate(LABELS):
        labels.append('%s_mean' % label)
        labels.append('%s_q2.5' % label)
        labels.append('%s_q97.5' % label)
    t = table.Table(rows=rows, names=labels)
    t.sort('MJD')
    print(t)
    
    fig, (ax1, ax2) = plt.subplots(2, sharex=True, figsize=(9, 9))

    mjd = t['MJD']
    phase = t['Phase']
    
    velocity_mean = (t['Center_mean']-center)/center*300e3*(-1)
    velocity_q2 = (t['Center_q2.5']-center)/center*300e3*(-1)
    velocity_q97 = (t['Center_q97.5']-center)/center*300e3*(-1)

    sigma_mean = (t['Sigma_mean'])/center*300e3
    sigma_q2 = (t['Sigma_q2.5'])/center*300e3
    sigma_q97 = (t['Sigma_q97.5'])/center*300e3

    # do GP
    #kernel = np.var(velocity_mean) * kernels.ExpSquaredKernel(20)
    kernel = np.exp(16) * kernels.ExpSquaredKernel(np.exp(10))
    #kernel =  * kernels.ExpSquaredKernel(20)
    gp = george.GP(kernel, mean=np.mean(velocity_mean), fit_mean=True)
    gp.compute(phase, velocity_q97-velocity_q2)
    
    def nll(p):
        #if p[1] < 0:
        #    return 1e25
        gp.set_parameter_vector(p)
        ll = gp.log_likelihood(velocity_mean, quiet=True)
        return -ll if np.isfinite(ll) else 1e25

    # And the gradient of the objective function.
    def grad_nll(p):
        gp.set_parameter_vector(p)
        return -gp.grad_log_likelihood(velocity_mean, quiet=True)
        
    p0 = gp.get_parameter_vector()
    #results = optimize.minimize(nll, p0, jac=grad_nll, method="L-BFGS-B")
    #results = optimize.differential_evolution(nll, [(5000, 20e3), (15, 35), (4, 15)])
    #gp.set_parameter_vector(results.x)
    print(gp.get_parameter_names())
    print(gp.get_parameter_vector())
    
    

    phase_grid = np.linspace(np.min(phase), np.max(phase), 100)
    velocity_gp, velocity_gp_var = gp.predict(velocity_mean, phase_grid, return_var=True)

    print(velocity_gp)
    ax1.errorbar(phase, velocity_mean, yerr=(velocity_mean-velocity_q2, velocity_q97-velocity_mean), fmt='o', label='Line Center')
    #ax1.fill_between(phase_grid, velocity_gp - np.sqrt(velocity_gp_var), velocity_gp + np.sqrt(velocity_gp_var), alpha=0.4)
    #ax1.set_xlabel('MJD [d]')
    ax1.set_xlabel('Phase [d]')
    ax1.set_ylabel('Velocity [km/s]')

    ax1.errorbar(phase, sigma_mean, yerr=(sigma_mean-sigma_q2, sigma_q97-sigma_mean), fmt='o', label='Sigma')

    amplitude_mean = t['Amplitude_mean']
    amplitude_q2 = t['Amplitude_q2.5']
    amplitude_q97 = t['Amplitude_q97.5']
    ax2.errorbar(phase, amplitude_mean, yerr=(amplitude_mean-amplitude_q2, amplitude_q97-amplitude_mean), fmt='o')
    ax2.set_xlabel('Phase [d]')
    ax2.set_ylabel('log Amplitude')

    ax1.legend()
    
    if interactive:
        plt.show()
    else:
        plt.savefig(dest_file)
    
    
if __name__ == '__main__':
    main()
