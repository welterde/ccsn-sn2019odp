#!/usr/bin/env python
import pandas as pd
import click
import sys
import matplotlib
import numpy as np

import astropy.table as table
import astropy.units as u
import astropy.constants as const
from astropy.cosmology import Planck15 as cosmology

try:
    import snelib19odp
except ImportError:
    sys.path.append('lib')

print('loading PTF12os..')
import snelib19odp.datasets.ptf12os as ptf12os
print('loading PTF13bvn..')
import snelib19odp.datasets.ptf13bvn as ptf13bvn
print('loading SN1993J..')
import snelib19odp.datasets.sn1993j as sn1993j
print('loading SN2008D..')
import snelib19odp.datasets.sn2008d as sn2008d

import snelib19odp.utils as utils
import snelib19odp.extinction as extinction
import snelib19odp.bolometric.lyman as lyman


import snelib19odp.peakmag as peakmag


# TODO: move the 19odp parameters somewhere global
ODP_EBV = ('uniform', 0.1361, 0.1989)
ODP_REDSHIFT = ('gaussian', 0.014353, 0.000013)
ODP_HOST_EBV = ('uniform', 0, 0.2)


def load_sn19odp(band):
    print('loading SN19odp..')
    # load the raw interpolated dataset
    df = pd.read_hdf('products/lc_interpolated_combined.h5')
    # FIXME: load a completed bolometric LC

    return df.rename(columns={
        'mag_"%s"_global' % band: 'mag',
        'magerr_"%s"_global' % band: 'mag_err'
    })


# table of Ic-BL from Taddia(2019)
def load_taddia2019(samples=20):
    t = table.Table.read('data/comparison/2020-07-13-Taddia2019-tab2.html')
    def parse_mag(x):
        mag, mag_err = x.replace('(', ' ').replace(')', '').replace('−', '-').split(' ')
        return (float(mag), float(mag_err))
    data = []
    for entry in t:
        mag, mag_err = parse_mag(entry['M_max r'])
        data.extend(np.random.normal(mag, mag_err, size=samples))
    return data

def load_cristina2020(extinction_corrected, samples=200):
    if extinction_corrected:
        mean = -18.03
        std = 0.79
    else:
        mean = -17.71
        std = 0.85
    return np.random.normal(mean, std, size=samples)


@click.command()
@click.option('-b', '--band', default='g')
@click.option('-o', '--output')
@click.option('--taddia2019-sample', is_flag=True)
@click.option('--richardson-2006', is_flag=True)
@click.option('--cristina2020-sample', is_flag=True)
@click.argument('datasets', nargs=-1)
def main(output, datasets, band, taddia2019_sample, richardson_2006, cristina2020_sample):
    if output:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    if output:
        plt.rcParams.update({'font.size': 20})
    fig = plt.figure(figsize=(15,9))
    ax = fig.add_subplot(111)

    data = []
    
    for dataset in datasets:
        print(dataset)
        dist_lum = None
        if dataset == 'PTF12os':
            df = ptf12os.lc_interp
            df.rename(inplace=True, columns={
                '%s_mag' % band: 'mag',
                '%s_mag_err' % band: 'mag_err'
                })
            ebv_mw = ptf12os.ebv_mw_dist
            ebv_host = ptf12os.ebv_host_dist
            redshift = ptf12os.redshift_dist
        elif dataset == 'PTF13bvn':
            df = ptf13bvn.lc_interp
            df.rename(inplace=True, columns={
                '%s_mag' % band: 'mag',
                '%s_mag_err' % band: 'mag_err'
                })
            ebv_mw = ptf13bvn.ebv_mw_dist
            ebv_host = ptf13bvn.ebv_host_dist
            redshift = ptf13bvn.redshift_dist
        elif dataset in ['SN2019odp', '19odp']:
            df = load_sn19odp(band)
            redshift = ODP_REDSHIFT
            ebv_mw = ODP_EBV
            ebv_host = ODP_HOST_EBV
        elif dataset == 'SN1993J':
            df = sn1993j.r_lc.rename(columns={
                'r_mag': 'mag',
                'r_mag_err': 'mag_err'
            })
            ebv_mw = sn1993j.ebv_mw_dist
            ebv_host = sn1993j.ebv_host_dist
            dist_lum = sn1993j.distance_modulus_dist
            redshift = None
        elif dataset == 'SN2008D':
            df = sn2008d.r_lc.rename(columns={
                'r_mag': 'mag',
                'r_mag_err': 'mag_err'
            })
            ebv_mw = sn2008d.ebv_mw_dist
            ebv_host = sn2008d.ebv_host_dist
            redshift = sn2008d.redshift_dist
        else:
            raise ValueError('Unknwon dataset %s' % dataset)
        
        peak_mag_samples = peakmag.sample_abs_peak(df, 'SDSS %s' % band,
                                                   redshift=redshift, dist_modulus=dist_lum,
                                                   mw_extinction=ebv_mw,
                                                   host_extinction=ebv_host)
        print('Mean: ', np.mean(peak_mag_samples))
        print('Std: ', np.std(peak_mag_samples))
        data.extend(list(map(lambda a: [dataset, a], peak_mag_samples)))

    if taddia2019_sample:
        if band != 'r':
            print(' * WARN: using wrong band for Taddia+2019.. needs to be r-band!')
        label = 'Ic-BL Sample\n(Taddia+2019)'
        data.extend(list(map(lambda a: [label, a], load_taddia2019())))

    if cristina2020_sample:
        if band != 'r':
            print(' * WARN: using wrong band for Cristina+2020.. needs to be r-band!')
        label = 'Ic Sample\n(Cristina+2020)'
        data.extend(list(map(lambda a: [label, a], load_cristina2020(extinction_corrected=True))))

    if richardson_2006:
        print(' * WARN: not a compatible band.. its V-band!')
        label = 'Ib Sample\n(Richardson+2006)'
        samples = np.random.normal(-17.98, 0.13, size=200)
        data.extend(list(map(lambda a: [label, a], samples)))
            
        
    df = pd.DataFrame(data, columns=['supernova', 'peak_mag'])
    import seaborn
    #seaborn.boxplot(x='peak_mag', y='supernova', data=df, ax=ax)
    #seaborn.swarmplot(x='peak_mag', y='supernova', data=df, ax=ax, size=1)
    seaborn.violinplot(x='peak_mag', y='supernova', data=df, ax=ax)

    ax.set(ylabel="")
    seaborn.despine(trim=True, left=True)
    ax.xaxis.grid(True)

    fig.tight_layout()
    
    if output:
        fig.savefig(output)
    else:
        plt.show()


if __name__ == '__main__':
    main()

