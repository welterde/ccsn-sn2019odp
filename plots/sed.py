#!/usr/bin/env python
import pandas as pd
import click
import matplotlib
import numpy as np

import astropy.table as table
import astropy.units as u
import astropy.constants as const


# TODO: refactor that into a common location
EXTINCTION_COEFF = {
    'g': 3.303,
    'r': 2.285,
    'i': 1.698,
    'z': 1.263,
    'J': 0.723,
    'H': 0.460,
    'K': 0.310
}
mw_ebv = 0.165


# (band, wavelength, AB zeropoint flux)
# taken from http://svo2.cab.inta-csic.es/svo/theory//fps3/index.php?id=LaSilla/GROND.J&&mode=browse&gname=LaSilla&gname2=GROND#filter
GROND_INS = [
    ('g', 4586.85, 5.36482e-9, 1491.42),
    ('r', 6220.09, 2.92735e-9, 1692.11),
    ('i', 7640.69, 1.88225e-9, 1001.74),
    ('z', 8989.58, 1.36524e-9, 1274.41),
    ('J', 12399.17, 7.25809e-10, 2365.58),
    ('H', 16468.80, 4.08192e-10, 2701.52),
    ('K', 21705.48, 2.34388e-10, 3031.62)
]
GROND_BANDS = [x[0] for x in GROND_INS]
GROND_CENT_WAVELENGTHS = [x[1] for x in GROND_INS]
GROND_ZERO_FLUXES = [x[2] for x in GROND_INS]
GROND_FWHM = [x[3] for x in GROND_INS]



# FIXME: make it somehow better the choice of epoch we use..
# we use idx=3 since that's the first one that doesn't have super-large errorbars
def load_grond(idx=3, ebv=0):
    t = table.Table.read('data/grond_lc/current.fits')
    t.pprint()
    for band in 'grizJHK':
        t['mag_%s' % band] -= EXTINCTION_COEFF[band] * ebv
    
    entry = t[idx]
    #ret = []
    #ret_err = []
    mags = []
    for band in GROND_BANDS:
        #ret.append(entry['mag_%s' % band])
        #ret_err.append(entry['magerr_%s' % band])
        #mag = entry['mag_%s' % band]
        mags.append(entry['mag_%s' % band])
    flux = GROND_ZERO_FLUXES*10**(-0.4*np.array(mags))
    #return GROND_CENT_WAVELENGTHS, GROND_FWHM, ret, ret_err
    return GROND_CENT_WAVELENGTHS, GROND_FWHM, flux, None


def load_2004aw():
    obs = [
        # UBVRI from NOT
        ('U', 3617.41, 595.75, 3.97645e-9, 20.91, 0.06),
        ('B', 4346.66, 1013.59, 6.30566e-9, 19.96, 0.06),
        ('V', 5417.18, 891.04, 3.78699e-9, 18.35, 0.03),
        ('R', 6464.12, 1230.80, 2.26888e-9, 17.61, 0.03),
        ('I', 8682.26, 2955.82, 9.66006e-10, 17.00, 0.03),
        # JHK from TNG+NICS
        ('J', 12789.08, 2965.63, 2.87698e-10, 16.25, 0.09),
        ('H', 16288.24, 2963.05, 1.20622e-10, 15.99, 0.14),
        ('K', 22038.01, 3378.31, 4.0487e-11, 15.95, 0.13)
    ]
    cent_wave = [x[1] for x in obs]
    fwhm = [x[2] for x in obs]
    zero_flux = [x[3] for x in obs]
    mags = [x[4] for x in obs]
    flux = zero_flux*10**(-0.4*np.array(mags))
    return cent_wave, fwhm, flux, None


@click.command()
@click.option('-o', '--output')
@click.argument('datasets', nargs=-1)
def main(output, datasets):
    if output:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    if output:
        plt.rcParams.update({'font.size': 20})
    fig = plt.figure(figsize=(15,9))
    ax = fig.add_subplot(111)

    for ds in datasets:
        if ds == 'SN2019odp':
            central_wavelength, filter_fwhm, flux, flux_err = load_grond(ebv=mw_ebv)
        elif ds == 'SN2019odp+0.1mag':
            central_wavelength, filter_fwhm, flux, flux_err = load_grond(ebv=mw_ebv+0.1)
        elif ds == 'SN2004aw':
            central_wavelength, filter_fwhm, flux, flux_err = load_2004aw()
        else:
            print(' * Unknown dataset %s' % ds)
            continue

        ax.errorbar(central_wavelength, flux, xerr=np.array(filter_fwhm)/2, label=ds)
    ax.legend()
    ax.set_xlabel('Wavelength [A]')
    ax.set_ylabel('Flux')
    
    if output:
        fig.savefig(output)
    else:
        plt.show()

if __name__ == '__main__':
    main()
