#!/usr/bin/env python3
import click, sys, os, functools, dynesty, numba, collections,json
from dynesty import plotting as dyplot
from dynesty import utils as dyfunc
import numpy as np
import astropy.table as table
import astropy.constants as aconst
import astropy.units as u
try:
    import snelib19odp
except ImportError:
    sys.path.append('lib')
import snelib19odp.const as const
import snelib19odp.dataloader as dataloader
import snelib19odp.specsmooth as specsmooth
import snelib19odp.gaussian as gaussian

import matplotlib
import matplotlib.gridspec as gridspec


C_KM_S = aconst.c.to(u.km/u.s).value

LINES = {
    'OI:63xx': (r'O I$\lambda6300,6364$', 6300),
    'OI:777x': (r'O I$\lambda7772,7774,7775$', 7772),
    'CaII:7xxx': (r'$Ca II\lambda7292,7324$', 7292),
    'CaII:8xxx': (r'Ca II$\lambda8498,8542,8662$', 8498),
    'MgI:4571': (r'Mg I$\lambda4571$', 4571),
    'HeI:5876': (r'He I$\lambda5876$', 5876)
}


@click.command()
@click.option('--line', 'lines', default=['OI:63xx'], multiple=True)
@click.option('--velocity-min', default=-13e3)
@click.option('--velocity-max', default=13e3)
@click.option('--offset', default=0)
@click.option('-o', '--output')
@click.option('--show-residual', is_flag=True)
@click.argument('specfiles', nargs=-1)
def main(lines, output, specfiles, velocity_min, velocity_max, offset, show_residual):
    if output:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    if output:
        plt.rcParams.update({'font.size': 20})
    
    fig = plt.figure(figsize=(14,20))
    if show_residual:
        gs = gridspec.GridSpec(2, 1,height_ratios=[2,1])
        ax = fig.add_subplot(gs[0])
        ax_res = fig.add_subplot(gs[1], sharex=ax)
        ax_res.set_ylabel('Residual')
    else:
        ax = fig.add_subplot(111)
    
    for spec_fname in specfiles:
        spec = dataloader.load_obs_spec(spec_fname, fluxcal=False)
        phase = dataloader.get_obs_phase(spec_fname)

        spec = specsmooth.downsample(spec, factor=3)

        label = '+%.1fd' % phase

        # now plot the lines
        for line in lines:
            print(line)
            line_label, center_wave = LINES[line]

            if len(lines) == 1:
                plot_label = label
            else:
                plot_label = '%s %s' % (label, line_label)

            velocity = (spec['restwave'] - center_wave)/center_wave * C_KM_S
            idx = np.logical_and(velocity > velocity_min, velocity < velocity_max)
            idx = np.logical_and(idx, ~np.isnan(spec['flux']))
            #peak_idx = np.logical_and(idx, velocity < 1000)
            peak_idx = idx
            peak_flux = np.percentile(spec['flux'][peak_idx], 95)
            print('\tPeak Flux: %e' % peak_flux)
            ax.plot(velocity[idx]+offset, spec['flux'][idx]/peak_flux, label=plot_label, alpha=0.6)

            if show_residual:
                gauss = gaussian.gaussian(velocity[idx], -offset, 1, 2000)
                gauss_norm = 0.2+0.8*gauss/np.nanmax(gauss)
                ax_res.plot(velocity[idx]+offset, spec['flux'][idx]/peak_flux - gauss_norm, label=plot_label)

    gauss_v = np.linspace(-8000, 8000)
    gauss = gaussian.gaussian(gauss_v, 0, 1, 1700)
    ax.plot(gauss_v, 0.2+0.75*gauss/np.nanmax(gauss), label='Gaussian Profile (FWHM 1700)', alpha=0.4)
            
    ax.set_ylabel('Normalized Flux')
    if show_residual:
        ax_res.axhline(0, ls='--', alpha=0.5, c='black')
        ax_res.set_xlabel('Velocity [km/s]')
        ax_res.legend()
    else:
        ax.set_xlabel('Velocity [km/s]')
    ax.legend()
    ax.axvline(0, alpha=0.5)
            
            
    
    
    

    if output:
        fig.savefig(output)
    else:
        plt.show()
    



if __name__ == '__main__':
    main()
