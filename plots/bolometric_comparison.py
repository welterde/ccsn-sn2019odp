#!/usr/bin/env python
import pandas as pd
import click
import matplotlib
import numpy as np

import astropy.table as table
import astropy.units as u
import astropy.constants as const
from astropy.cosmology import Planck15 as cosmology

print('loading PTF12os..')
import snelib19odp.datasets.ptf12os as ptf12os
print('loading PTF13bvn..')
import snelib19odp.datasets.ptf13bvn as ptf13bvn
import snelib19odp.utils as utils
import snelib19odp.extinction as extinction
import snelib19odp.bolometric.lyman as lyman


def load_sn19odp(method):
    if method == 'spectral':
        data = table.Table.read("products/lum_lc_spectral_integration.fits").to_pandas().set_index('mjd')
        return utils.unconvert_luminosity(data['luminosity'])
    print('loading SN19odp..')
    # load the raw interpolated dataset
    df = pd.read_hdf('products/lc_interpolated_combined.h5')
    # FIXME: load a completed bolometric LC

    # params..
    ebv = 0.165
    redshift = 0.014353

    dist = cosmology.luminosity_distance(redshift).to(u.pc).value
    dist_mod = 5*np.log10(dist) - 5
    
    extinction.correct_extinction_columns(df, ebv, 3.1, column_band_mapping={
        'mag_"g"_global': 'SDSS g',
        'mag_"r"_global': 'SDSS r'
    })

    if method == 'lyman_pbc':
        lc = lyman.compute_bol_mag(df['mag_"g"_global'], df['mag_"r"_global'], 'pbc')
    else:
        raise ValueError('Unsupport method "%s"' % method)

    return lc - dist_mod

    
    
def calc_phase(df, phase_method, luminosity):
    if phase_method == 'auto':
        if luminosity:
            idx_max = df.argmax()
        else:
            idx_max = df.argmin()
        t_max = df.index[idx_max]
        new_df = df.copy(deep=True)
        new_df.index = new_df.index - t_max
        return new_df
    else:
        raise ValueError('Unknown phase method %s' % phase_method)


@click.command()
@click.option('--luminosity', is_flag=True)
@click.option('--log-luminosity', is_flag=True)
@click.option('--use-phase')
@click.option('-o', '--output')
@click.argument('datasets', nargs=-1)
def main(output, luminosity, datasets, use_phase, log_luminosity):
    if output:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    if output:
        plt.rcParams.update({'font.size': 20})
    fig = plt.figure(figsize=(15,9))
    ax = fig.add_subplot(111)

    for dataset in datasets:
        if ':' in dataset:
            dataset, method = dataset.split(':')
        else:
            method = 'lyman_pbc'
        if dataset == 'PTF12os':
            df = ptf12os.abs_mag_lcs[method]
        elif dataset == 'PTF13bvn':
            df = ptf13bvn.abs_mag_lcs[method]
        elif dataset in ['SN2019odp', '19odp']:
            df = load_sn19odp(method)
        else:
            raise ValueError('Unknwon dataset %s' % dataset)
        
        if luminosity:
            lc = utils.convert_luminosity(df)
            if log_luminosity:
                lc = np.log10(lc)
        else:
            lc = df
        if not use_phase:
            ax.plot(lc.index, lc, label='%s (%s)' % (dataset, method))
        else:
            new_lc = calc_phase(lc, use_phase, luminosity)
            ax.plot(new_lc.index, lc, label='%s (%s)' % (dataset, method))
    
    ax.legend()
    if not use_phase:
        ax.set_xlabel('MJD [d]')
    else:
        # TODO: might depend on use-phase setting..
        ax.set_xlabel('Phase [d]')
    if luminosity:
        if log_luminosity:
            ax.set_ylabel('log10 Luminosity [log erg/s/cm2]')
        else:
            ax.set_ylabel('Luminosity [erg/s/cm2]')
    else:
        ax.set_ylabel('Absolute Magnitude [mag]')
        ax.invert_yaxis()
    if output:
        fig.savefig(output)
    else:
        plt.show()



if __name__ == '__main__':
    main()
