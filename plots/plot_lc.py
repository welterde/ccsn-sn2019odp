#!/usr/bin/env python
import os, sys, glob
import pandas as pd
import click
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import h5py
import astropy.time as time
import numpy as np
import matplotlib.ticker as ticker
import tqdm
import astropy.table as table

try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../lib'))
import snelib19odp.const as const
import snelib19odp.dataloader as dataloader


PHASE_CENTER = const.sne_peak_mjd['g']

PLOT_CFG = [
    ('g', '"g"_global', '"g"', 1e12, 'blue', 'Blues'),
    ('r', '"r"_global', '"r"', 1e12, 'green', 'Greens'),
    ('i', '"i"_global', '"i"', 58792, 'red', 'Reds')
]

products_dir = os.path.join(os.path.dirname(__file__), '../products')
data_dir = os.path.join(os.path.dirname(__file__), '../data')


def plot_main(ax, mark_peak_bands):
    # load combined interpolated lightcurve
    df_interpolated = pd.read_hdf('products/lc_interpolated_combined.h5', '/lc')

    # load raw lightcurve
    df_raw = pd.read_hdf('products/marshal_raw_lc.h5', '/lc')

    max_per_band = {}

    for band, interp_key, raw_key, mjd_cutoff, color, color_cmap in PLOT_CFG:
    
        # plot the interpolated lightcurve
        interp_idx = df_interpolated.index.get_level_values('mjd') < mjd_cutoff
        df_interp_sel = df_interpolated.iloc[interp_idx]

        # generate realizations of that curve
        mjd_grid = np.linspace(df_interp_sel.index.min(), df_interp_sel.index.max(), 2000)
        mag_interp = np.interp(mjd_grid, df_interp_sel.index, df_interp_sel['mag_%s' % interp_key])
        magerr_interp = np.interp(mjd_grid, df_interp_sel.index, df_interp_sel['magerr_%s' % interp_key])
        alpha = 0.2 / magerr_interp * np.min(magerr_interp)
        dmjd = np.mean(np.diff(mjd_grid))
        #for i in tqdm.trange(len(mjd_grid)):
        #    ax.fill_between([mjd_grid[i], mjd_grid[i]+dmjd], mag_interp[i] - 3*magerr_interp[i], mag_interp[i] + 3*magerr_interp[i], alpha=alpha[i], color=color)
        
        #for i in tqdm.trange(600):
        #    flux_rand= np.random.normal(mag_interp, magerr_interp)
        #    ax.scatter(mjd_grid, flux_rand, color=color, alpha=0.004, marker='.', linewidths=0.01)
        #ax.fill_between(df_interp_sel.index, df_interp_sel['mag_%s' % interp_key] - 3*df_interp_sel['magerr_%s' % interp_key], df_interp_sel['mag_%s' % interp_key] + 3*df_interp_sel['magerr_%s' % interp_key], alpha=0.3, color=color)

        # plot the raw datapoints
        df_raw_sel = df_raw.xs(raw_key, level='filter').dropna(axis='rows', subset=['mag', 'magerr'])
        ax.errorbar(df_raw_sel.index.get_level_values('mjd'), df_raw_sel['mag'], yerr=df_raw_sel['magerr'], label='%s-band' % band, fmt='o', color=color)

        max_per_band[band] = df_raw_sel['mag'].min()
        

    ax.set_ylabel('Apparent Magnitude [mag]')
    ax.invert_yaxis()

    yticks = []
    for band in mark_peak_bands:
        yticks.append(max_per_band[band])
    for x in np.arange(16.5, 20, 0.5):
        collision = False
        for y in yticks:
            if abs(x-y) < 0.25:
                collision = True
        if not collision:
            yticks.append(x)
    ax.set_yticks(yticks)

def plot_early(ax, end_time=58721):
    # TODO: refactor this loading stuff to some utility functions?
    # load the ztflc lc
    ztflc = table.Table.read(os.path.join(data_dir, 'ztflc_forcefit.h5'))
    ztflc['mag'] = -2.5*np.log10(ztflc['ampl']) + ztflc['magzp']

    # load the swift-uvot observations
    uvot = table.Table.read(os.path.join(data_dir, 'uvot_phot.txt'), format='ascii')
    uvot['mjd'] = uvot['JD'] - 2400000.5
    idx = uvot['AB_MAG'] < uvot['AB_MAG_LIM']-3*uvot['AB_MAG_ERR']

    # plot ztflc
    scalezp = 0
    for band in 'gri':
        idx = np.logical_and(ztflc['filter'] == ('ZTF_%s' % band), np.logical_and(ztflc['obsmjd'] > 58710, ztflc['obsmjd'] < end_time))
        f0coef = 3631e6 * 10 ** (-(ztflc["magzp"][idx] - scalezp) / 2.5)
        ax.errorbar(ztflc['obsmjd'][idx], f0coef*ztflc['ampl'][idx], yerr=f0coef*ztflc['ampl.err'][idx], fmt='o', label='ZTF %s' % band, capsize=2, alpha=0.7)

    # plot UVOT detections
    idx = np.logical_and(uvot['AB_MAG'] < uvot['AB_MAG_LIM']-uvot['AB_MAG_ERR'], uvot['mjd'] < end_time)
    for i, band in enumerate(['U', 'B', 'V']):
        idx2 = np.logical_and(idx, uvot['FILTER'] == band)
        if np.count_nonzero(idx2) == 0:
            continue
        flux = 3631e6 * 10 ** (-uvot['AB_MAG']/2.5)
        flux_err = uvot['AB_MAG_ERR'] * flux / 1.087
        #ax.errorbar(uvot['mjd'][idx2]+(i-1)*0.02, flux[idx2], yerr=flux_err[idx2], fmt='o', label='UVOT %s' % band, alpha=0.7, capsize=2)

    # plot UVOT non-detections
    idx = np.logical_and(uvot['AB_MAG'] >= uvot['AB_MAG_LIM']-uvot['AB_MAG_ERR'], uvot['mjd'] < end_time)
    for i, band in enumerate(np.unique(uvot['FILTER'])):
        print(repr(band))
        if band == 'B':
            continue
        idx2 = np.logical_and(idx, uvot['FILTER'] == band)
        if np.count_nonzero(idx2) == 0:
            continue
        flux = 3631e6 * 10 ** (-uvot['AB_MAG_LIM']/2.5)
        #flux_err = uvot['AB_MAG_ERR'] * flux / 1.087
        #ax.errorbar(uvot['mjd'][idx2]+(i-3)*0.02, flux[idx2], fmt='v', label='UVOT %s' % band)

    ax.axhline(0, ls='--', alpha=0.5, color='black')

    ax.set_ylabel('Flux [uJy]')
    
    



@click.command()
@click.option('--early', is_flag=True)
@click.option('--mark-peak-bands', default='gr')
@click.argument('destfile')
def main(destfile, early, mark_peak_bands):
    if early:
        plt.rcParams.update({'font.size': 15})
        fig = plt.figure(figsize=(9,6))
    else:
        plt.rcParams.update({'font.size': 20})
        fig = plt.figure(figsize=(15,9))
    ax = fig.add_subplot(111)
    
    spec_mjds = dataloader.load_spec_mjds(fluxcal=False)

    if not early:
        plot_main(ax, mark_peak_bands)
    else:
        plot_early(ax)
        
    ax.legend()
    ax.set_xlabel('MJD [d]')
    

    def abs2rel(x):
        return x - PHASE_CENTER

    def rel2abs(x):
        return x + PHASE_CENTER

    ax.grid(linestyle = '--', linewidth = 0.5)
        
    sec_ax = ax.secondary_xaxis('top', functions=(abs2rel, rel2abs))
    sec_ax.set_xlabel('Phase [d]')

    spec_ax = ax.secondary_xaxis(1.2)
    spec_ax.set_xlabel('Spectroscopy')
    #spec_ax.plot(spec_mjds, np.ones(len(spec_mjds)), 'v')
    spec_ax.xaxis.set_major_locator(ticker.FixedLocator(spec_mjds))
    spec_ax.xaxis.set_ticklabels([])
    spec_ax.tick_params(width=3, length=12, direction='inout')
    # from https://matplotlib.org/examples/axes_grid/demo_parasite_axes2.html
    #par2 = ax.twinx()
    #offset = 60
    #print(repr(par2.axis))
    #par2.axis['top'] = par2.get_grid_helper().new_fixed_axis(loc='top',
    #                                                         axes=par2,
    #                                                         offset=(0, offset))
    #par2.axis["top"].toggle(all=True)
    
    
    plt.tight_layout()
    fig.savefig(destfile)




if __name__ == '__main__':
    main()
