#!/usr/bin/env python
import pandas as pd
import json
import click
import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import h5py
import astropy.time as time
import numpy as np
import george
from george import kernels
import astropy.table as table
import scipy.optimize as optimize
import pandas as pd



PHASE_CENTER_MYSN = 58740

def interpolate_lc(mjd, mag, mag_err, mjd_grid):
    #mjd_grid = np.arange(grid_start, grid_end, grid_step)

    kernel = 30 * kernels.ExpSquaredKernel(140)
    gp = george.GP(kernel, mean=0.5, fit_mean=True)
    gp.compute(mjd, mag_err)
    
    # Define the objective function (negative log-likelihood in this case).
    def nll(p):
        gp.set_parameter_vector(p)
        ll = gp.log_likelihood(mag, quiet=True)
        return -ll if np.isfinite(ll) else 1e25
    
    # And the gradient of the objective function.
    def grad_nll(p):
        gp.set_parameter_vector(p)
        return -gp.grad_log_likelihood(mag, quiet=True)
        
    #p0 = gp.get_parameter_vector()
    #results = optimize.minimize(nll, p0, jac=grad_nll, method="L-BFGS-B")
    
    # Update the kernel and print the final log-likelihood.
    #gp.set_parameter_vector(results.x)
    pred, pred_var = gp.predict(mag, mjd_grid, return_var=True)
    pred_var[pred_var < 0.05] = 0.05

    return pred, pred_var

def load_json_phot(fname):
    with open(fname, 'r') as f:
        data = json.load(f)
    sne = data[list(data.keys())[0]]
    phot = sne['photometry']
    def check(x):
        if 'band' not in x:
            return False
        if 'e_magnitude' not in x:
            return False
        if 'time' not in x:
            return False
        return True
    times = [float(x['time']) for x in phot if check(x)]
    mag = [float(x['magnitude']) for x in phot if check(x)]
    mag_err = [float(x['e_magnitude']) for x in phot if check(x)]
    band = [x['band'] for x in phot if check(x)]
    t = table.Table({'time': times, 'mag': mag, 'band': band, 'mag_err': mag_err})
    return t

def to_comparison_color(phot, nogrid=False, extra_err=0.1):
    # calculate time grid
    t_min = 0
    t_max = 1e9
    for band in 'gr':
        idx1 = phot['band'] == band
        idx2 = phot['band'] == ("%s'" % band)
        idx = np.logical_or(idx1, idx2)
        min_t, max_t = np.min(phot['time'][idx]), np.max(phot['time'][idx])
        if min_t > t_min:
            t_min = min_t
        if max_t < t_max:
            t_max = max_t
        #if band == 'g':
        #    i_peak = phot['mag'][idx].argmin()
        #    t_peak = phot['time'][idx][i_peak]
    if nogrid:
        mjd_grid = np.unique(phot['time'])
    else:
        mjd_grid = np.linspace(t_min, t_max, 300)
    t = table.Table({'mjd': mjd_grid})
    for band in 'gr':
        idx1 = phot['band'] == band
        idx2 = phot['band'] == ("%s'" % band)
        idx = np.logical_or(idx1, idx2)
        mag = phot['mag'][idx]
        mag_err = phot['mag_err'][idx]+extra_err
        mjd = phot['time'][idx]

        mag_interp, magerr_interp = interpolate_lc(mjd, mag, mag_err, mjd_grid)
        t['mag_%s' % band] = mag_interp
        t['magerr_%s' % band] = magerr_interp
    # use the interpolated data for peak
    idx = t['magerr_g'] < 0.09
    i_peak = t['mag_g'][idx].argmin()
    t_peak = t['mjd'][idx][i_peak]
    t['color'] = t['mag_g'] - t['mag_r']
    return t,t_peak

def plot_object(ax, name, df=None, alpha=0.3, havedf=False, max_date=150, onlygood=False, color=None):
    if not havedf:
        if name == 'SN2019odp':
            df = pd.read_hdf('products/lc_interpolated_combined.h5', '/lc')
            df['mjd'] = df.index.get_level_values('mjd')
            for band in 'gr':
                interp_key = '"%s"_global' % band
                df['mag_%s' % band] = df['mag_%s' % interp_key]
                df['magerr_%s' % band] = df['magerr_%s' % interp_key]
            t_peak = PHASE_CENTER_MYSN
        else:
            # load photometry
            phot = load_json_phot('data/comparison/%s.json' % name)
            df, t_peak = to_comparison_color(phot)
    else:
        t_peak = None
    
    idx =  np.logical_and((df['mjd'] - df['mjd'].min()) < 120, df['magerr_g'] < 0.1)
    #print(sn2016coi)
    #if name == 'SN2019odp':
    #    t_peak = PHASE_CENTER_MYSN
    #else:
    #    t_peak = df['mjd'][idx][np.argmin(df['mag_g'][idx])]
    if 'phase' not in df.columns:
        df['phase'] = df['mjd'] - t_peak
    idx = df['phase'] < 100
    #ax.plot(phase[idx], df['color'][idx], label=name)
    mag_g_max = df['mag_g'] - df['magerr_g']
    mag_g_min = df['mag_g'] + df['magerr_g']

    mag_r_max = df['mag_r'] - df['magerr_r']
    mag_r_min = df['mag_r'] + df['magerr_r']

    color_max = mag_g_max - mag_r_min
    color_min = mag_g_min - mag_r_max

    color_range = color_max - color_min
    if onlygood:
        #idx = np.logical_and(idx, df['magerr_g'] < 0.15)
        #idx = np.logical_and(idx, df['magerr_r'] < 0.15)
        idx2 = np.logical_or(df['magerr_g'] > 0.1, df['magerr_r'] > 0.1)
        if np.count_nonzero(idx2) > 0.3*np.count_nonzero(idx):
            return
        color_max[idx2] = np.nan
        color_min[idx2] = np.nan
    err_range = df['magerr_g'] + df['magerr_r']
    max_alpha = 1.0
    min_range = min(err_range.min(), 0.02)
    alpha2 = max_alpha*(min_range/err_range)**1.0
    #print(alpha)
    #print(min_range)
    colors = plt.cm.Reds(np.ones(len(df['phase'][idx])))
    colors[:,-1] = alpha2[idx]
    print(colors)
    if color is None:
        #ax.fill_between(df['phase'][idx], color_max[idx], color_min[idx], alpha=alpha, label=name)
        ax.fill_between(df['phase'][idx], color_max[idx], color_min[idx], label=name, color=colors, linewidth=0.0)
    else:
        #ax.fill_between(df['phase'][idx], color_max[idx], color_min[idx], alpha=alpha, label=name, color=color)
        ax.fill_between(df['phase'][idx], color_max[idx], color_min[idx], label=name, color=colors, linewidth=0.0)

def plot_bts(ax, fname):
    # load object list
    objects = table.Table.read(fname, path='objects')

    # load each object, determine peak, create phase and append to datapoints
    phase = np.array([], dtype=np.float64)
    color = np.array([], dtype=np.float64)
    color_err = np.array([], dtype=np.float64)
    
    for row in objects:
        objname = row['Object']
        t_peak_cat = row['Peak_MJD']
        if row['Peak_Filter'] != 'r':
            continue
        
        phot = table.Table.read(fname, path=objname)
        #print(phot)
        phot['time'] = phot['mjd']
        phot['band'] = np.array([x[-1] for x in phot['filter']])
        phot['mag_err'] = phot['magerr']
        idx = ~np.isnan(phot['mag'])
        if np.count_nonzero(idx) < 20:
            continue
        newdf, t_peak = to_comparison_color(phot[idx], nogrid=False, extra_err=0)
        newdf['phase'] = newdf['mjd'] - t_peak_cat
        #print(newdf)
        #if objname == 'ZTF19aalouag':
        if False:
            plot_object(ax, objname, df=newdf, alpha=0.1, havedf=True, color='red', onlygood=True)
        else:
            plot_object(ax, None, df=newdf, alpha=0.1, havedf=True, color='green', onlygood=True)
        #plot_object(ax, objname, df=newdf, alpha=0.1, havedf=True, onlygood=True)
        #ax.plot(newdf['phase'], newdf['mag_r'], alpha=0.1)
        #print(color)

    
@click.command()
#@click.option('--title')
@click.option('--ic-bl-comparison', is_flag=True)
@click.option('--ic-comparison', is_flag=True)
@click.option('--ib-comparison', is_flag=True)
@click.option('--use-bts-sample', is_flag=True)
def main(ic_bl_comparison, ic_comparison, ib_comparison, use_bts_sample):
    plt.rcParams.update({'font.size': 20})
    fig = plt.figure(figsize=(15,9))
    ax = fig.add_subplot(111)

    ax.legend()
    #ax.invert_yaxis()
    ax.set_xlabel('Phase [d]')
    ax.set_ylabel('Sloan g-r Color Index [mag]')

    plot_object(ax, 'SN2019odp')
    
    #ax.set_xlim([-30, 80])
    if ic_bl_comparison:
        if use_bts_sample:
            plot_bts(ax, 'data/comparison/ztf_bts_icbl_lcs.h5')
        else:
            plot_object(ax, 'SN2016coi')
            plot_object(ax, 'SDSS_SN_14475')
        ax.set_title('Ic-BL Comparison')
        ax.set_ylim((-0.2, 1.5))
    elif ic_comparison:
        if use_bts_sample:
            plot_bts(ax, 'data/comparison/ztf_bts_ic_lcs.h5')
        else:
            plot_object(ax, 'SN2017ein')
            plot_object(ax, 'PTF15dtg')
        ax.set_title('Ic Comparison')
        ax.set_ylim((-0.2, 1.5))
    elif ib_comparison:
        if use_bts_sample:
            plot_bts(ax, 'data/comparison/ztf_bts_ib_lcs.h5')
        else:
            plot_object(ax, 'iPTF13bvn')
            #plot_object(ax, 'SN2007C')
            #plot_object(ax, 'SN2008D')
            plot_object(ax, 'SN2004gq')
            plot_object(ax, 'SN2005bf')
            #plot_object(ax, 'SN2009jf')
        ax.set_title('Ib Comparison')
        ax.set_ylim((-0.2, 1.5))
    #plot_object(ax, 'SN2015bn')
    
    
    #ax.plot(phase, sn2016coi['mag_g'])
    ax.legend()
    #ax.invert_yaxis()
    #ax.axvline(0)
    
    # TODO: plot 19odp
    #if title:
    #    ax.set_title(title)

    plt.show()


if __name__ == '__main__':
    main()
