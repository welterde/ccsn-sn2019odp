import pandas as pd
import click
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


@click.command()
#@click.argument('raw_lc')
@click.argument('destfile')
@click.argument('interp_key')
@click.argument('raw_key')
def main(destfile, interp_key, raw_key):
    plt.rcParams.update({'font.size': 20})
    fig = plt.figure(figsize=(15,9))
    ax = fig.add_subplot(111)
    
    # load combined interpolated lightcurve
    df_interpolated = pd.read_hdf('products/lc_interpolated_combined.h5', '/lc')

    # load raw lightcurve
    df_raw = pd.read_hdf('products/marshal_raw_lc.h5', '/lc')

    # plot the interpolated lightcurve
    ax.fill_between(df_interpolated.index, df_interpolated['mag_%s' % interp_key] - 3*df_interpolated['magerr_%s' % interp_key], df_interpolated['mag_%s' % interp_key] + 3*df_interpolated['magerr_%s' % interp_key], alpha=0.4, label='Interpolated')

    # plot the raw datapoints
    df_raw_sel = df_raw.xs(raw_key, level='filter').dropna(axis='rows', subset=['mag', 'magerr'])
    ax.errorbar(df_raw_sel.index.get_level_values('mjd'), df_raw_sel['mag'], yerr=df_raw_sel['magerr'], label='Raw', fmt='o')
    
    ax.legend()
    ax.invert_yaxis()
    ax.set_xlabel('MJD [d]')
    ax.set_ylabel('Apparent Magnitude [mag]')
    fig.savefig(destfile)




if __name__ == '__main__':
    main()
