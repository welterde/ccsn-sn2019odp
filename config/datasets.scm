(("SN2019odp_phot"
  (rev 23)
  (transient "SN2019odp")
  (load:table "data/ztflc_forcefit.h5"
	      (tag "ZTF")
	      (ztflc)
	      )
  (load:table "data/grond_lc/current.fits"
	      (tag "GROND")
	      (transpose
	       (bands "g" "r" "i" "z" "J" "H" "K")
	       (columns
					; stupid non-jd or mjd format..
		(mjd "mjd")
		(mag "mag_%s")
		(mag_err "magerr_%s")
		)
	       (defaults
		 (instrument "GROND")
		 (telescope "2p2")
		 )
	       ))
  (load:marshalcsv "data/marshal_raw_lc_v2.csv"
		   (tag "P60")
		   (filter:key "telescope" "P60")
		   )
  (load:table "data/uvot_phot.txt"
	      (format "ascii")
	      (tag "UVOT")
	      (reformat
	       (columns
		(mjd "JD" (offset -2400000.5))
		(band "FILTER")
		(mag "AB_MAG")
		(mag_err "AB_MAG_ERR")
		(maglim "AB_MAG_LIM")
		)
	       (telescope "Swift")
	       (instrument "UVOT")
	       ))
  (load:table "products/imagesub/NOT.h5"
	      (format "hdf5")
	      (path "/lc")
	      (tag "NOT")
	      (reformat
	       (columns
		(mjd "mjd")
		(band "band")
		(mag "mag")
		(mag_err "mag_err")
		)
	       (telescope "NOT")
	       (instrument "ALFOSC")
	       )
	      )

  (process
   ((match:tag "ZTF")
    (set:system_prefix "SDSS "))
   ((match:tag "NOT")
    (set:system_prefix "SDSS "))
   ((match:tag "P60")
    (set:system_prefix "SDSS "))
   ((match:tag "GROND")
    (match:key "band" "J" "H" "K")
    (set:system_prefix "2MASS "))
   ((match:tag "GROND")
    (match:key "band" "g" "r" "i" "z")
    (set:system_prefix "SDSS "))
   ((match:tag "GROND")
    (match:mjd < 58775)
    (delete)
    )

   ((match:key "instrument" "UVOT")
    (match:key "band" "U")
					; the data given in the UVOT table is already in AB format
    (set:system_prefix "SDSS ")	       
    (set:zeropoint-system "AB")
    (set:band "u")
    )

   ((match:unmatched)
    (match:key "instrument" "UVOT")
    (delete)
    )
   
   )

  (interpolate
   (prior:param "prior_t0_range" 16.0)
   (only (band? "u")
	 (ds:no-offset "UVOT")
	 (ds:no-offset "P60")
	 (model "plateau-contardo")
	 )
   (only (band? "z" "J" "H" "K")
	 (model "linear")
	 )
   (ds:no-offset "NOT")
   )

  (bolometric "lyman"
	      (method "lyman" "pbc" "g" "r")
	      (sample-lyman-scatter)
	      (sample-correlated)
	      )
  (bolometric "lyman-gi"
	      (method "lyman" "pbc" "g" "i")
	      (sample-lyman-scatter)
	      (sample-correlated)
	      )
  (bolometric "int"
	      (method "direct-int" "g" "r" "i")
	      )

  ; what configuration parameters do we need here..?
  ;(combine)

  ; milkyway extinction
  (extinction
	      (ebv:uniform 0.14 0.2)
	      )

  (distance
   (Mpc:gaussian 64 5)
   )
  )

 ("SN2019odp_phot_short"
  (rev 18)
  (transient "SN2019odp")
  ;(load:table "data/ztflc_forcefit.h5"
	;      (tag "ZTF")
	 ;     (ztflc)
					;    )
  (load:table "data/nora_force_phot/ZTF19abqwtfu_nocuts.escv"
	      (format "ascii.ecsv")
	      (tag "ZTF")
	      (reformat
	       (columns
		(mjd "jdobs" (offset -2400000.5))
		(band "filter")
		(mag "mag")
		(mag_err "mag_unc")
		(maglim "limmag")
		)
	       (telescope "P48")
	       (instrument "ZTF")
	       )
					;(noralc)
	      (mark-upper-limit)
	      )
  (load:table "data/grond_lc/current.fits"
	      (tag "GROND")
	      (transpose
	       (bands "g" "r" "i" "z" "J" "H" "K")
	       (columns
					; stupid non-jd or mjd format..
		(mjd "mjd")
		(mag "mag_%s")
		(mag_err "magerr_%s")
		)
	       ))
  (load:marshalcsv "data/marshal_raw_lc_v2.csv"
		   (tag "P60")
		   (filter:key "telescope" "P60")
		   )
  (load:table "data/uvot_phot.txt"
	      (format "ascii")
	      (tag "UVOT")
	      (reformat
	       (columns
		(mjd "JD" (offset -2400000.5))
		(band "FILTER")
		(mag "AB_MAG")
		(mag_err "AB_MAG_ERR")
		(maglim "AB_MAG_LIM")
		)
	       (telescope "Swift")
	       (instrument "UVOT")
	       ))

  (process
   ((match:tag "ZTF")
    (set:system_prefix "SDSS "))
   ((match:tag "P60")
    (set:system_prefix "SDSS "))
   ((match:tag "GROND")
    (match:key "band" "J" "H" "K")
    (set:system_prefix "2MASS "))
   ((match:tag "GROND")
    (match:key "band" "g" "r" "i" "z")
    (set:system_prefix "SDSS "))
   ((match:tag "GROND")
    (match:mjd < 58775)
    (delete)
    )
					; de-apply the extinction correction from Nora LC
					; TODO: XXX: Ask for Nora without extinction correction and remove this!!
   ((match:tag "ZTF")
    (match:key "band" "g")
    (modify "mag" + 0.54945)
    )
   ((match:tag "ZTF")
    (match:key "band" "r")
    (modify "mag" + 0.3804525)
    )
   ((match:tag "ZTF")
    (match:key "band" "i")
    (modify "mag" + 0.282717)
    )
   
					; ignore all the non-detections early on
   ((match:mjd < 58716.4)
    (delete))
					; cut off everything after the sun gap
   ((match:mjd > 58900)
    (delete))

   ((match:key "instrument" "UVOT")
    (match:key "band" "U")
					; the data given in the UVOT table is already in AB format
    (set:system_prefix "SDSS ")	       
    (set:zeropoint-system "AB")
    (set:band "u")
    )
   ((match:unmatched)
    (delete)
    )
   )

  (interpolate
   (prior:param "prior_t0_range" 16.0)
   (model "prebump-contardo")
   (only (band? "u")
	 (ds:no-offset "UVOT")
	 (ds:no-offset "P60")
	 ;(model "modified-contardo")
	 )
   (only (band? "z" "J" "H" "K")
	 (model "linear")
	 )
   )

  (bolometric "lyman"
	      (method "lyman" "bc" "g" "r")
	      (sample-lyman-scatter)
	      (sample-correlated)
   )

  ; what configuration parameters do we need here..?
  ;(combine)

  ; milkyway extinction
  (extinction
	      (ebv:uniform 0.14 0.2)
	      )

  (distance
   (Mpc:gaussian 64 5)
   )
  )

 ("SN2019odp_synphot_uncal"
  (rev 2)
  (transient "SN2019odp")
  (load:table "data/ztflc_forcefit.h5"
	      (tag "ZTF")
	      (ztflc)
	      )
  (load:table "data/grond_lc/current.fits"
	      (tag "GROND")
	      (transpose
	       (bands "g" "r" "i" "z" "J" "H" "K")
	       (columns
					; stupid non-jd or mjd format..
		(mjd "mjd")
		(mag "mag_%s")
		(mag_err "magerr_%s")
		)
	       ))
  (synphot
   (specs:glob "products/specs/preproc/*.spec")
   
   )

  (process
   ((match:tag "ZTF")
    (set:system_prefix "SDSS "))
   ((match:tag "GROND")
    (match:key "band" "J" "H" "K")
    (set:system_prefix "2MASS "))
   ((match:tag "GROND")
    (match:key "band" "g" "r" "i" "z")
    (set:system_prefix "SDSS "))
   ((match:tag "GROND")
    (match:mjd < 58775)
    (delete)
    )
   
					; ignore all the non-detections early on
   ((match:mjd < 58716.4)
    (delete))
   )

  (interpolate
   (prior:param "prior_t0_range" 16.0)
   )

  ; what configuration parameters do we need here..?
  ;(combine)

  ; milkyway extinction
  (extinction
	      (ebv:uniform 0.14 0.2)
	      )

  (bolometric "lyman"
	      (method "lyman" "pbc" "g" "r")
	      (sample-lyman-scatter)
	      
   )

  (distance
   (Mpc:gaussian 64 5)
   )
  )


 ("iPTF13bvn"
  (rev 14)
  (transient "iPTF13bvn")
  (load:json "data/comparison/iPTF13bvn.json"
	     (filter:source "2016A&A...593A..68F")
	     (filter:key "telescope" "P60")
	     (tag "F16_P60")
	     )
  (load:json "data/comparison/iPTF13bvn.json"
	     (filter:source "2016A&A...593A..68F")
	     (filter:key "telescope" "P48")
	     (tag "F16_P48")
	     )
  (load:json "data/comparison/iPTF13bvn.json"
	     (filter:source "2016A&A...593A..68F")
	     (filter:key "telescope" "LCOGT")
	     (tag "F16_LCOGT")
	     )
  (load:json "data/comparison/iPTF13bvn.json"
	     (filter:source "2016A&A...593A..68F")
	     (filter:key "telescope" "NOT")
	     (tag "F16_NOT")
	     )
  (process
   ((match:key "telescope" "P48")
    (set:system_prefix "SDSS ")
					; photometric errors seem underestimated
    (modify "mag_err" setmin 0.03)
    )
   ((match:key "telescope" "P60")
    (set:system_prefix "SDSS ")
					; photometric errors seem underestimated
    (modify "mag_err" setmin 0.03)
    )
   ((match:key "telescope" "LCOGT")
    (match:key "band" "g" "r" "i" "z")
    (set:system_prefix "SDSS ")
					; photometric errors seem underestimated
    (modify "mag_err" setmin 0.05)
    )
   ((match:key "telescope" "NOT")
    (match:key "band" "g" "r" "i" "z")
    (set:system_prefix "SDSS ")
					; photometric errors seem underestimated
    (modify "mag_err" setmin 0.05)
    )
   ((match:key "instrument" "UVOT")
    (match:key "band" "U")
    (set:system_prefix "Landolt ")
					;(set:zeropoint-system "Vega")
					; use the U-band AB-Vega correction from Breeveld+2011
    (set:zeropoint-system "AB")
    (modify "mag" + 1.02)
    )
   ((match:unmatched)
    (delete)
    )
   )
					; MW extinction from IRSA DUST
  (extinction
   (ebv:uniform 0.0421 0.0448)
   )
					; host extinction from Fremling+2016
  (extinction
   (ebv:uniform 0.04 0.15)
   )

  (interpolate
   (model "plateau-contardo")
   ;(prior:param "kernel_win_min" -1)
   (prior:param "kernel_win_max" 4.0)
					; we dont have overlapping data for NOT obs
   (ds:no-offset "F16_NOT")
   )
	       
					; Distance taken from NED
  (distance
   (Mpc:gaussian 26.8 2.6)
   )

  (bolometric "lyman"
	      (method "lyman" "bc" "g" "r")
	      (sample-lyman-scatter)
	      (sample-correlated)
   )
  )

 ("SN2008D"
  (rev 12)
  (transient "SN2008D")
  (load:json "data/comparison/SN2008D.json")
  (process
   ((match:key "telescope" "CfA3_KEP")
    (match:key "band" "r'" "i'")
    (set:system_prefix "SDSS ")
    )
   ((match:key "telescope" "CfA3_KEP")
    (match:key "band" "U" "B" "V")
    (set:system_prefix "Landolt ")
    )
   ((match:key "instrument" "UVOT")
    (match:key "band" "U")
    (set:system_prefix "Landolt ")
					;(set:zeropoint-system "Vega")
					; use the U-band AB-Vega correction from Breeveld+2011
    (set:zeropoint-system "AB")
    (modify "mag" + 1.02)
    )
   ((match:unmatched)
    (delete)
    )
   ((re:sub "band" "(\w+)'" "$1"))
   )

					; MW extinction (taken from IRSA Dust)
  (extinction
   (ebv:gaussian 0.0193 0.0002)
   )
					; host extinction
  (extinction
					; was from Modjaz paper
					;(ebv:gaussian 0.6 0.1)
					; from original Soderberg nature paper supplemental material
   (ebv:uniform 0.4 0.8)
   )

  (bolometric "lyman"
	      (method "lyman" "bc" "B" "V")
	      (sample-lyman-scatter)
	      (sample-correlated)
	      )

  (interpolate
   (model "prebump-contardo")
   ;(only (band? "U")
;	 (model "linear")
;	 )
   )

					; taken from NED
  (distance
   (Mpc:gaussian 33.69 2.36)
   )
  )
  
 ("SN1998bw"
  (rev 12)
  (transient "SN1998bw")
  ; from 2011AJ....141..163C
  (load:table "data/comparison/SN1998bw-Clocchiatti-2011-table2.h5"
	      (transpose
	       (bands "U" "B" "V" "Rc" "Ic")
	       (columns
					; stupid non-jd or mjd format..
		(mjd "JD" (offset 50000.5))
		(mag "%smag")
		(mag_err "e_%smag")
		)
	       )
	      )
   
   
  (process
   ((match:key "band" "U" "B" "V" "Rc" "Ic")
    (set:system_prefix "CTIO ")
    (set:zeropoint-system "Vega")
    (modify "mag_err" + 0.05)
    )
					; we currently dont want to deal with the host galaxy contamination
   ((match:mjd > 51340)
    (delete))
   ((match:unmatched)
    (delete)
    )
   )

  ;; MW extinction from IRSA dust service
  (extinction
   (ebv:uniform 0.047 0.06)
   )

  (distance
					;(Mpc:gaussian 38 3)
					; taken from NED
   (Mpc:gaussian 40.84 2.86)
   )

  (interpolate
   ;(prior:param "prior_t0_range" 16.0)
   (model "plateau-contardo")
   )

  (bolometric "lyman"
					;(method "lyman" "bc" "B" "V")
	      (method "lyman" "bc" "V" "Rc")
	      (sample-lyman-scatter)
	      (sample-correlated)
	      )
  (bolometric "lyman-vr"
	      (method "lyman" "bc" "V" "Rc")
	      (sample-lyman-scatter)
	      (sample-correlated)
   )

  )

 ("SN2002ap"
  (rev 8)
  (transient "SN2002ap")
  (load:json "data/comparison/SN2002ap.json"
	     (filter:source "2003PASP..115.1220F")
	     (tag "Foley")
	     )
  (process
   ((match:key "band" "U" "B" "V" "R" "I")
    (set:system_prefix "CTIO ")
    )
   ((match:nan "mag_err")
    (delete))
   ((match:unmatched)
    (delete)
    )
   )

					; MW extinction from DUST
  (extinction
   (ebv:uniform 0.0585 0.0661)
   )

					; Host extinction from Takada-Hidai, Masahide+2002
  (extinction
   (ebv:uniform 0.01 0.02)
   )

  

  (distance
					; Distance from NED for M74
   (Mpc:gaussian 10.69 0.75)
   )

  (interpolate
   ;(prior:param "prior_t0_range" 16.0)
   (model "plateau-contardo")
   )

  (bolometric "lyman"
	      (method "lyman" "pbc" "B" "V")
	      (sample-lyman-scatter)
	      (sample-correlated)
   )
  )
   
	     

 
 )
