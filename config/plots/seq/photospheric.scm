((specs
  (dataset "SN2019odp")
  (select:phase > -11)
  (select:phase < 42)
  ; cut-off spike at the end that leads to label in wrong position
  (if (specname "ZTF19abqwtfu_20190827_P200_v1") (wavelength:max 9970))
  )
 (atom "He I"
       (color "green")
       (line 5876)
;       (shade 5600 6000)
       (line 4472)
;       (shade 4200 4596)
       (line 6678)
;       (shade 6400 6800)
       (line 7065 (offset:text -120))
;       (shade 6850 7100)
       ;(line 4713)
       ;(line 5015)
       ;(line 5047)
       (line 4921)
       )
 (atom "He II"
       (color "green")
       (line 4686)
       )
 ;(atom "O I"
 ;      (color "orange")
 ;      (line 7774)
       ;(shade 7650 7900)
					;      )
 ;; (atom "[O III]"
 ;;       (color "orange")
 ;;       ;(line 4353)
 ;;       (line 4959 (offset:text -60))
 ;;       (line 5007)
 ;;       (line 4363)
 ;;       )
 ;; (atom "H"
 ;;       (color "blue")
 ;;       (line 6563)
 ;;       (line 4861)
 ;;       (line 4340)
 ;;       )

 (atom "[Ca II]"
       (color "blue")
       (line 7291 (offset:text -120))
       (line 7324 (offset:text +20))
;       (shade 7000 7500)
       )
 (atom "Ca II"
       (color "blue")
       (line 8498 (offset:text -120))
       (line 8542)
       (line 8662)
       ; misc test lines
       ;(line 5890)
       ;(line 5896)
       
       ;(line 5876)
       ;(line 9931)
;       (shade 8200 9000)
       )
 (atom "Mg I]"
       (color "cyan")
       (line 4571)
       (line 5167)
;       (shade 4500 4700)
;       (shade 5100 5250)
       )

 (atom "[O I]"
       (color "orange")
       (line 6300 (offset:text -100))
       (line 6364)
       (line 5577)
;       (shade 6100 6500)
;       (shade 5400 5600)
       )
 
 (telluric "O2-A")
 (plot:wave-range 3500 10050)
 )
