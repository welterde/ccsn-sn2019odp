((specs
  (dataset "SN2019odp")
  (select:phase < 0)
  ; cut-off spike at the end that leads to label in wrong position
  (if (specname "ZTF19abqwtfu_20190827_P200_v1") (wavelength:max 9970))
  )
 (atom "He I"
       (color "green")
       (line 5876)
       (line 4472)
       (line 6678)
       (line 7065)
       )
 (atom "He II"
       (color "green")
       (line 4686)
       )
 ;(atom "O I"
 ;      (color "orange")
 ;      (line 7774)
       ;(shade 7650 7900)
 ;      )
 (atom "H"
       (color "blue")
       (line 6563)
       (line 4861)
       )
 (telluric "O2-A")
 (plot:wave-range 3500 10400)
 )

 
