((sim "19odp_short"
      (dataset "SN2019odp_phot_short")
      (bands "g" "r" "i")
      (fitter-arg "iterations" 5000)
      (fitter-arg "method" "nester")
      ; after the end of the plateau
      (min-mjd 58719)
					;(distance-param)
      (fitter-arg "num_walkers" 80)
      (model-param "texplosion" "min_value" -10)
					; lets use the vej = velocity at peak assumption and take that from the He line velocity
      (model-param "vejecta" "min_value" 12000)
      (model-param "vejecta" "max_value" 13000)
					; lets assume very little host extinction
      (model-param "nhhost" "max_value" 1e19)
      (fitter-arg:del "nhost")
      
      (butcher-ds "g" "instrument" "ZTF")
      (butcher-ds "g" "telescope" "P48")
      (butcher-ds "r" "instrument" "ZTF")
      (butcher-ds "r" "telescope" "P48")
      (butcher-ds "i" "instrument" "ZTF")
      (butcher-ds "i" "telescope" "P48")
      )
 (sim "19odp_allbands"
      (dataset "SN2019odp_phot")
      (bands "u" "g" "r" "i" "z" "J" "H")
      (fitter-arg "iterations" 10000)
      (fitter-arg "method" "nester")
      ; after the end of the plateau
      (min-mjd 58719)
      (max-mjd 58900)
					;(distance-param)
      (fitter-arg "num_walkers" 120)
      (model-param:del "nhhost")
      (model-param "texplosion" "min_value" -8)
      (model-param "vejecta" "min_value" 9000)
      (model-param "vejecta" "max_value" 13000)
      (model-param "kappa" "min_value" 0.1)
      (model-param "kappa" "max_value" 0.3)
					; lets assume very little host extinction
      ;(model-param "nhhost" "max_value" 1e19)
      )
 (sim "19odp_allbands_ic"
      (dataset "SN2019odp_phot")
      (bands "u" "g" "r" "i" "z" "J" "H")

      (model "ic")
      
      (fitter-arg "iterations" 10000)
      (fitter-arg "method" "nester")
      ; after the end of the plateau
      (min-mjd 58719)
      (max-mjd 58900)
					;(distance-param)
      (fitter-arg "num_walkers" 120)
      (model-param:del "nhhost")
      (model-param "texplosion" "min_value" -8)
      (model-param "vejecta" "min_value" 9000)
      (model-param "vejecta" "max_value" 13000)
      ;(model-param "kappa" "min_value" 0.1)
					;(model-param "kappa" "max_value" 0.3)
      (model-param "radiussource" "min_value" 1e13)
      (model-param "radiussource" "max_value" 1e16)
      
					; lets assume very little host extinction
      ;(model-param "nhhost" "max_value" 1e19)
      )

 (sim "19odp_allbands_ib"
      (dataset "SN2019odp_phot")
      (bands "u" "g" "r" "i" "z")

      (model "ib")
      
      (fitter-arg "iterations" 5000)
      ;(fitter-arg "method" "nester")
      ; after the end of the plateau
      (min-mjd 58721)
      (max-mjd 58900)
					;(distance-param)
      (fitter-arg "num_walkers" 80)
      (model-param:del "nhhost")
      (model-param "texplosion" "min_value" -8)
      (model-param "vejecta" "min_value" 9000)
      (model-param "vejecta" "max_value" 13000)
      ;(model-param "kappa" "min_value" 0.1)
					;(model-param "kappa" "max_value" 0.3)
      ;(model-param "radiussource" "min_value" 1e13)
					;(model-param "radiussource" "max_value" 1e16)

      (model-param "cutoff_wavelength" "min_value" 2000)
      (model-param "cutoff_wavelength" "max_value" 4000)
      
					; lets assume very little host extinction
      ;(model-param "nhhost" "max_value" 1e19)
      )
 (sim "19odp_allbands_ib_peak"
      (dataset "SN2019odp_phot")
      (bands "u" "g" "r" "i" "z")

      (model "ib")
      
      (fitter-arg "iterations" 5000)
      ;(fitter-arg "method" "nester")
      ; after the end of the plateau
      (min-mjd 58719)
      (max-mjd 58770)
					;(distance-param)
      (fitter-arg "num_walkers" 80)
      ;(model-param:del "nhhost")
      (model-param "texplosion" "min_value" -8)
      ;(model-param "vejecta" "min_value" 9000)
      ;(model-param "vejecta" "max_value" 13000)
      ;(model-param "kappa" "min_value" 0.1)
					;(model-param "kappa" "max_value" 0.3)
      ;(model-param "radiussource" "min_value" 1e13)
					;(model-param "radiussource" "max_value" 1e16)

      ;(model-param "cutoff_wavelength" "min_value" 2000)
      ;(model-param "cutoff_wavelength" "max_value" 5000)
      
					; lets assume very little host extinction
      ;(model-param "nhhost" "max_value" 1e19)
      )
 (sim "19odp_allbands_slsn"
      (dataset "SN2019odp_phot")
      (bands "u" "g" "r" "i" "z")

      (model "slsn")
      
      (fitter-arg "iterations" 10000)
      (fitter-arg "method" "nester")
      ; after the end of the plateau
      (min-mjd 58719)
      (max-mjd 58900)
					;(distance-param)
      (fitter-arg "num_walkers" 180)
      (model-param:del "nhhost")
      (model-param "texplosion" "min_value" -8)
      (model-param "vejecta" "min_value" 9000)
      (model-param "vejecta" "max_value" 13000)
      ;(model-param "kappa" "min_value" 0.1)
					;(model-param "kappa" "max_value" 0.3)
      ;(model-param "radiussource" "min_value" 1e13)
					;(model-param "radiussource" "max_value" 1e16)

      (model-param "cutoff_wavelength" "min_value" 2000)
      (model-param "cutoff_wavelength" "max_value" 4500)
      
					; lets assume very little host extinction
      ;(model-param "nhhost" "max_value" 1e19)
      )
 (sim "19odp_allbands_fit_host_nh"
      (dataset "SN2019odp_phot")
      (bands "u" "g" "r" "i" "z" "J" "H")
      (fitter-arg "iterations" 20000)
      (fitter-arg "method" "nester")
      ; after the end of the plateau
      (min-mjd 58719)
      (max-mjd 58900)
					;(distance-param)
      (fitter-arg "num_walkers" 180)
      ;(model-param:del "nhhost")
      (model-param "texplosion" "min_value" -8)
      (model-param "vejecta" "min_value" 9000)
      (model-param "vejecta" "max_value" 13000)
      ;(model-param "kappa" "min_value" 0.1)
      ;(model-param "kappa" "max_value" 0.3)
					; lets assume very little host extinction
      ;(model-param "nhhost" "max_value" 1e19)
      )
 (sim "19odp_allbands_red_only"
      (dataset "SN2019odp_phot")
      (bands "r" "i" "z" "J" "H")
      (fitter-arg "iterations" 10000)
      (fitter-arg "method" "nester")
      ; after the end of the plateau
      (min-mjd 58719)
      (max-mjd 58900)
					;(distance-param)
      (fitter-arg "num_walkers" 120)
      ;(model-param:del "nhhost")
      (model-param "texplosion" "min_value" -8)
      (model-param "vejecta" "min_value" 6000)
      (model-param "vejecta" "max_value" 15000)
      (model-param "kappa" "min_value" 0.1)
      (model-param "kappa" "max_value" 0.3)
					; lets assume very little host extinction
      ;(model-param "nhhost" "max_value" 1e19)
      )
 (sim "13bvn"
      (dataset "iPTF13bvn")
      (bands "g" "r" "i")
      (fitter-arg "iterations" 8000)
      (fitter-arg "method" "nester")
      (fitter-arg "num_walkers" 80)

      (model-param "texplosion" "min_value" -10)
      (model-param "vejecta" "min_value" 6000)
      (model-param "vejecta" "max_value" 9000)

      (butcher-ds "g" "instrument" "ZTF")
      (butcher-ds "g" "telescope" "P48")
      (butcher-ds "r" "instrument" "ZTF")
      (butcher-ds "r" "telescope" "P48")
      (butcher-ds "i" "instrument" "ZTF")
      (butcher-ds "i" "telescope" "P48")
      )

 (sim "13bvn_ib"
      (dataset "iPTF13bvn")
      (bands "g" "r" "i")

      (model "ib")
      
      (fitter-arg "iterations" 8000)
      (fitter-arg "method" "nester")
      (fitter-arg "num_walkers" 80)

      (model-param "texplosion" "min_value" -10)
      (model-param "vejecta" "min_value" 6000)
      (model-param "vejecta" "max_value" 9000)

      (butcher-ds "g" "instrument" "ZTF")
      (butcher-ds "g" "telescope" "P48")
      (butcher-ds "r" "instrument" "ZTF")
      (butcher-ds "r" "telescope" "P48")
      (butcher-ds "i" "instrument" "ZTF")
      (butcher-ds "i" "telescope" "P48")
      )

 (sim "08d"
      (dataset "SN2008D")
      (bands "r" "i" "U" "B" "V")
      (fitter-arg "iterations" 8000)
      (fitter-arg "method" "nester")

					; TODO: configure a fixed texpl of 0
      (model-param "texplosion" "min_value" -5)
      (model-param "kappa" "min_value" 0.05)
      (model-param "kappa" "max_value" 0.3)
      
      (fitter-arg "num_walkers" 80)
      (model-param "vejecta" "min_value" 5000)
      (model-param "vejecta" "max_value" 10000)

      (min-mjd 54480)
      )

 (sim "08d_ib"
      (dataset "SN2008D")
      (bands "r" "i" "U" "B" "V")

      (model "ib")
      
      (fitter-arg "iterations" 80000)
      (fitter-arg "method" "nester")

					; TODO: configure a fixed texpl of 0
      (model-param "texplosion" "min_value" -2)
      (model-param "kappa" "min_value" 0.05)
      (model-param "kappa" "max_value" 0.3)
      (model-param "vejecta" "min_value" 5000)
      (model-param "vejecta" "max_value" 10000)
      ;(model-param:del "texplosion")
      (fitter-arg "num_walkers" 80)

      (min-mjd 54480)
      )

 (sim "98bw"
      (dataset "SN1998bw")
      (bands "U" "B" "V" "Rc" "Ic")
      (fitter-arg "iterations" 30000)
      (fitter-arg "method" "nester")

      ;(model "ic")

					; TODO: configure a fixed texpl of 0
      (model-param "texplosion" "min_value" -15)
      ;(model-param "kappa" "min_value" 0.05)
					;(model-param "kappa" "max_value" 0.3)
      ;(model-param "vejecta" "min_value" 29000)
      ;(model-param "vejecta" "max_value" 31000)
      ;(model-param:del "texplosion")
      ;(model-param:del "texplosion")
      (fitter-arg "num_walkers" 40)

      (max-mjd 51000)
      )
      
 )
