((rule
  (match:instrument "ZTF")
					; values taken from SVO2 service
					; ref-wlen is mean wavelength on that site
  (entry "SDSS g"
	 (ref-wlen 4804.79)
	 (zeropoint "Vega" 5.32798e-9)
	 (zeropoint "AB" 4.88043e-9)
	 )
  (entry "SDSS r"
	 (ref-wlen 6436.92)
	 (zeropoint "AB" 2.70846e-9)
	 (zeropoint "Vega" 2.2691e-9)
	 )
  (entry "SDSS i"
	 (ref-wlen 7968.22)
	 (zeropoint "Vega" 1.1695e-9)
	 (zeropoint "AB" 1.75033e-9)
	 )
  )
 )
