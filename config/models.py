import functools
import numpy as np

import snelib19odp.specds as specds
import snelib19odp.analysis.oxygen7774 as oxygen7774
import snelib19odp.analysis.oxygen_lineprofiles as oxygen_gauss






def make_7774_model(spec, start_63xx, stop_63xx, start_5577, stop_5577, start_7774, stop_7774, **kwargs):
    regions = {
        '7774_min': start_7774,
        '7774_max': stop_7774,
        '63xx_min': start_63xx,
        '63xx_max': stop_63xx,
        '5577_min': start_5577,
        '5577_max': stop_5577
    }
    # extract the wave grids the same way as they are used - need pixel-matching, so cant just use linspace
    wave_63xx, _ = oxygen7774.extract_region(spec, regions['63xx_min'], regions['63xx_max'])
    wave_5577, _ = oxygen7774.extract_region(spec, regions['5577_min'], regions['5577_max'])

    wave_7774, flux_7774 = oxygen7774.premangle_7774_flux(spec, wave7774_min=regions['7774_min'], wave7774_max=regions['7774_max'], wave63xx_min=regions['63xx_min'], wave63xx_max=regions['63xx_max'])

    def plot_func(p, ax_63xx, ax_5577):
        flux_63xx, flux_5577 = oxygen7774.render_func(p, wave_7774, flux_7774, wave_63xx, wave_5577)
        ax_63xx.plot(wave_63xx, flux_63xx, alpha=0.03, lw=1, color='grey')
        ax_5577.plot(wave_5577, flux_5577, alpha=0.03, lw=1, color='grey')
    
    return {
        'fit_func': functools.partial(oxygen7774.run_fit, spec, regions=regions, **kwargs),
        'labels': oxygen7774.LABELS,
        'plot_labels': oxygen7774.PLOT_LABELS,
        'plot_func': plot_func
    }


def make_gauss_model(spec, start_63xx, stop_63xx, start_5577, stop_5577, **kwargs):
    regions = {
        '63xx_min': start_63xx,
        '63xx_max': stop_63xx,
        '5577_min': start_5577,
        '5577_max': stop_5577
    }
    
    # create grids to render the functions on
    wave_63xx = np.linspace(start_63xx, stop_63xx)
    wave_5577 = np.linspace(start_5577, stop_5577)
    
    def plot_func(p, ax_63xx, ax_5577):
        flux_63xx, flux_5577 = oxygen_gauss.render_func(p, wave_63xx, wave_5577)
        ax_63xx.plot(wave_63xx, flux_63xx, alpha=0.03, lw=1, color='grey')
        ax_5577.plot(wave_5577, flux_5577, alpha=0.03, lw=1, color='grey')
        
    
    return {
        'fit_func': functools.partial(oxygen_gauss.run_fit, spec, regions=regions, **kwargs),
        'labels': oxygen_gauss.LABELS,
        'plot_labels': oxygen_gauss.PLOT_LABELS,
        'plot_func': plot_func
    }


SPEC_MODELS = {
    '7774': make_7774_model,
    'gaussian': make_gauss_model
}



SDS_19ODP = specds.load_dataset('SN2019odp')


SPEC_SETUPS = {
    'not_late': {
        'spec': SDS_19ODP['ZTF19abqwtfu_20200113_NOT_v1'],
        'start_5577': 5400,
        'stop_5577': 5650,
        'start_63xx': 6100,
        'stop_63xx': 6540,
        'start_7774': 7550,
        'stop_7774': 8000,
        'nlive_init': 9000,
        'dynesty_method': 'rwalk'
    },
    'keck_early': {
        'spec': SDS_19ODP['ZTF19abqwtfu_20200124_Keck1_v1'],
        'start_5577': 5400,
        'stop_5577': 5650,
        'start_63xx': 6100,
        'stop_63xx': 6540,
        'start_7774': 7550,
        'stop_7774': 8000
    },
    'keck_late': {
        'spec': SDS_19ODP['ZTF19abqwtfu_20200821_Keck1_v1'],
        'start_5577': 5520,
        'stop_5577': 5669,
        'start_63xx': 6150,
        'stop_63xx': 6540,
    }
}


def make_model(line_profile_model, spec_name):
    spec_data = SPEC_SETUPS[spec_name].copy()
    spec = spec_data['spec']
    del spec_data['spec']
    spec_kw = spec_data

    setup_func = SPEC_MODELS[line_profile_model]

    funcs = setup_func(spec, **spec_kw)

    def plot_spec_func(ax_5577, ax_63xx, overscan=0):
        idx_5577 = np.logical_and(spec['restwave'] > spec_kw['start_5577']-overscan, spec['restwave'] < spec_kw['stop_5577']+overscan)
        idx_63xx = np.logical_and(spec['restwave'] > spec_kw['start_63xx']-overscan, spec['restwave'] < spec_kw['stop_63xx']+overscan)

        ax_5577.plot(spec['restwave'][idx_5577], spec['flux'][idx_5577])
        ax_63xx.plot(spec['restwave'][idx_63xx], spec['flux'][idx_63xx])
    
    funcs['plot_spec_func'] = plot_spec_func
    return funcs
