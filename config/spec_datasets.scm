(("SN2019odp"
  (transient "SN2019odp")
  ; load all the spectra
  (load "ZTF19abqwtfu_20190821_P60_v1.ascii")
  (load "ZTF19abqwtfu_20190823_NTT_v1.ascii")
  ;(load "ZTF19abqwtfu_20190823_NTT_v1.fits")
  (load "ZTF19abqwtfu_20190823_P60_v1.ascii")
  (load "ZTF19abqwtfu_20190824_P60_v1.ascii")
  (load "ZTF19abqwtfu_20190827_P200_v1.ascii")
  ;(load "ZTF19abqwtfu_20190827_P200_v1.fits")
  (load "ZTF19abqwtfu_20190827_P60_v1.ascii")
  (load "ZTF19abqwtfu_20190830_NOT_v1.ascii")
  (load "ZTF19abqwtfu_20190831_P60_v1.ascii")
  (load "ZTF19abqwtfu_20190910_P60_v1.ascii")
  (load "ZTF19abqwtfu_20190917_P60_v1.ascii")
  (load "ZTF19abqwtfu_20190922_P60_v1.ascii")
  (load "ZTF19abqwtfu_20190927_P60_v1.ascii")
  (load "ZTF19abqwtfu_20191003_NOT_v1.ascii")
  (load "ZTF19abqwtfu_20191006_P60_v1.ascii")
  ;(load "ZTF19abqwtfu_20191013_P60_v1.ascii")
  (load "ZTF19abqwtfu_20191013_P60_v2.ascii")
  (load "ZTF19abqwtfu_20191019_P60_v1.ascii")
  (load "ZTF19abqwtfu_20191021_NOT_v1.ascii")
  (load "ZTF19abqwtfu_20191026_P60_v1.ascii")
  (load "ZTF19abqwtfu_20191103_P60_v1.ascii")
  (load "ZTF19abqwtfu_20191110_P60_v1.ascii")
  ;(load "ZTF19abqwtfu_20191122_NOT_v1.ascii")
  (load "ZTF19abqwtfu_20191122_NOT_v2.ascii")
  ;(load "ZTF19abqwtfu_20191122_NOT_v2.fits")
  (load "ZTF19abqwtfu_20191123_P60_v1.ascii")
  (load "ZTF19abqwtfu_20191218_P60_v1.ascii")
  (load "ZTF19abqwtfu_20191221_P60_v1.ascii")
  (load "ZTF19abqwtfu_20200103_P60_v1.ascii")
  (load "ZTF19abqwtfu_20200113_NOT_v1.ascii")
  ;(load "ZTF19abqwtfu_20200113_NOT_v1.fits")
  (load "ZTF19abqwtfu_20200124_Keck1_v1.ascii")
  (load "ZTF19abqwtfu_20200821_Keck1_v1.ascii")

  (fluxcal
   (dataset "SN2019odp_phot")
   (band "r")
   (filter:system "Palomar_ZTF")
   )

  ; mean value from https://irsa.ipac.caltech.edu/applications/DUST/
  (extinction:mw 0.1651)
  )
 ("iPTF13bvn"
  (transient "iPTF13bvn")
  (rev 1)
					; load all the spectra
  (load:comparison "iPTF13bvn.json")
  (fluxcal
   (dataset "iPTF13bvn")
   (band "r")
   (filter:system "Palomar_ZTF")
   )
				
					; MW extinction from Fremling+2016
  (extinction:mw 0.0437)
					; host extinction from Fremling+2016
  (extinction:host 0.08)
					
  
  )
 ("SN2008D"
  (transient "SN2008D")
  (rev 5)

  (load:comparison "SN2008D.json" (redshift 0.00649))
  (fluxcal
   (dataset "SN2008D")
   (band "r")
   (filter:system "FLWO_KeplerCam")
   )

					; MW extinction from ?
  (extinction:mw 0.0193)
					; host extinction from ?
  (extinction:host 0.2)
  
  )
 ("SN2002ap"
  (transient "SN2002ap")
  (rev 1)

  (load:comparison "SN2002ap.json")
  (fluxcal
   (dataset "SN2002ap")
   (band "R")
   (filter:system "Generic_Cousins")
   (photometric:system "CTIO ")
   )
  )

 ("SN1998bw"
  (transient "SN1998bw")
  (rev 1)

  (load:comparison "SN1998bw.json")
  (fluxcal
   (dataset "SN1998bw")
   (band "Rc")
   (filter:system "Generic_Cousins")
   (photometric:system "CTIO ")
   )
  )
 )
