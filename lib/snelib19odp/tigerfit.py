import numpy as np
import numba
import collections
from scipy.integrate import quad
import astropy.table as table
from scipy.optimize import curve_fit, differential_evolution
import tqdm
import dynesty

from . import utils

# Taken from https://github.com/manolis07gr/TigerFit/blob/5439d107198c74a17166f7d1021209246660eec9/LCmods.py
# Mostly the equations..

#Global model constants, all units are CGI
day=86400.     #seconds in a day
year=3.15436e7 #seconds in a year
Msun=1.99e33   #solar mass in grams
c=2.99792458e10#speed of light
sb=5.67051e-5  #Stefan-Boltzmann constant
kms2cms=1.e5   #km/s in cm/s
r15=1.e15      #radii in units of 10^15cm
tni=8.8        #Ni-56 decay time-scale
tco=111.3      #Co-56 decay time-scale
eni=3.9e10     #Ni-56 decay specific energy generation rate
eco=6.8e9      #Co-56 decay specific energy generation rate
TH=5500.       #Hydrogen ionization temperature in K
A0=1.e12       #Radioactive decay model gamma-ray leakage parameter in 10^14 units (sec^2)
E51=1.e51      #Energy in units of 1 F.O.E. (10^51 erg)
L45=1.e45      #Luminosity in units of 10^45 erg/s
nmax=1000000   #Grid resolution for fallback accretion model integration



#####################################
#SUPERNOVA LIGHT CURVE MODELS FOLLOW
#####################################

#1.RADIOACTIVE DECAY DIFFUSION MODEL (Arnett 1980,1982)

# Factor function that appears in front of exponents in radioactive decay model
# inputs are time (t) and diffusion time-scale (td) both in days, progenitor radius
# (r0) in units of 10^15cm and SN ejecta velocity (vej) in units of km/s
@numba.njit(inline='always')
def rad_decay_dep(t,td,r0,vej):
    return ((r0*r15/(vej*td*day*kms2cms))+t/td)*np.exp((t/td)**2+(2.*r0*r15*t/(vej*kms2cms*(td**2)*day)))

# Integrants for Ni-56 and Co-56 decay energy depositions
#@numba.njit(inline='always')
@utils.jit_integrand_function_3param
def rad_decay_int1(t,td,r0,vej):
    return rad_decay_dep(t,td,r0,vej)*np.exp(-t/tni)

#@numba.njit(inline='always')
@utils.jit_integrand_function_3param
def rad_decay_int2(t,td,r0,vej):
    return rad_decay_dep(t,td,r0,vej)*np.exp(-t/tco)

# Final radioactive decay luminosity integral function

# A is the gamma-ray trapping coefficient
#@numba.njit(inline='always')
def Lum_rad(x,Mni,td,r0,vej,A):
    res = (2.*Mni*Msun/td)*np.exp(-((x/td)**2+(2.*r0*r15*x/(vej*kms2cms*(td**2)*day))))* \
    ((eni-eco)*quad(rad_decay_int1,0,x,args=(td,r0,vej))[0] + eco*quad(rad_decay_int2,0,x,args=(td,r0,vej))[0])* \
    (1.-np.exp(-A*A0/((x+0.01)*day)**2))
    return np.log(res)

def Lum_rad_eth(x,Mni,td,r0,vej,Eth, A):
    u = 2*(r0*r15)/(vej*kms2cms)*(x/td)**2 + (x/td)**2
    factor1 = (Eth*E51)*2*(r0*r15)/(td*day)**2/(vej*kms2cms) * np.exp(-u)
    res = (2.*Mni*Msun/td)*np.exp(-((x/td)**2+(2.*r0*r15*x/(vej*kms2cms*(td**2)*day))))* \
    ((eni-eco)*quad(rad_decay_int1,0,x,args=(td,r0,vej))[0] + eco*quad(rad_decay_int2,0,x,args=(td,r0,vej))[0])* \
    (1.-np.exp(-A*A0/((x+0.01)*day)**2))
    return np.log(factor1+res)

FitResult = collections.namedtuple('FitResult', 'mej dmej e de chi_red params pconv fitcurve texpl t residual')


def tigerfit(lc, texpl_mjd, kappa=0.2, param_bounds={}):
    assert 'mjd' in lc.colnames
    assert 'luminosity' in lc.colnames

    t = lc['mjd'] - texpl_mjd

    obs = np.log(lc['luminosity'])

    model = np.vectorize(Lum_rad)
    #guess = (0.1,10.,0.1,10000.,30.)
    #bound = (((0.01,5.,0.0001,5000.,7.),(20.0,100.,1.0,20000.,100.)))
    guess = [0.1,10.,0.1,12000.,30.]
    bound = ([0.01,5.,0.0001,5000.0,7.],[20.0,100.,1.0,20000.,100.])
    bounds = [
        # Mni
        (0.01, 20.0),
        # td
        (5, 100),
        # r0
        (0.0001, 1.0),
        # vej
        (11500, 12500),
        # A
        (7, 100)
    ]

    for k in param_bounds.keys():
        assert k in ['mni', 'td', 'r0', 'vej', 'A']
        v = param_bounds[k]
        if k == 'mni':
            i = 0
        elif k == 'td':
            i = 1
        elif k == 'r0':
            i = 2
        elif k == 'vej':
            i = 3
        elif k == 'A':
            i = 4
        
        if v[0] == 'const':
            guess[i] = v[1]
            bound[0][i] = v[1]-1e-3
            bound[1][i] = v[1]+1e-3
        elif v[0] == 'range':
            guess[i] = (v[2]+v[1])/2
            bound[0][i] = v[1]
            bound[1][i] = v[2]

    def func(x):
        m = model(t, *x)/1e40
        return -0.5 * np.sum( (m-obs)**2 )
    
    p, pconv = curve_fit(model, t, obs, p0=guess, bounds=bound)
    #res = differential_evolution(func, bounds)
    #p = res.x
    
    chi_red = (1./(len(t)-len(guess))) * sum((obs-model(t,p[0],p[1],p[2],p[3],p[4]))**2)
    
    Mej = ((3./10.)*(13.8*c/kappa)*p[3]*kms2cms*(p[1]*day)**2)/Msun
    DMej = (Mej*np.sqrt(4.*((pconv.diagonal()[1]/p[1])**2)+(p[3]/pconv.diagonal()[3])**2))

    E = (3./10.)*Mej*Msun*(p[3]*kms2cms)**2
    DE = E*np.sqrt((DMej/Mej)**2+4.*(p[3]/pconv.diagonal()[3])**2)

    return FitResult(
        mej=Mej,
        dmej=DMej,
        e=E,
        de=DE,
        chi_red=chi_red,
        params=p,
        pconv=pconv,
        fitcurve=model(t,*p),
        texpl=texpl_mjd,
        t=t,
        residual=(obs-model(t, *p)),
    )


class Sampler(object):

    def __init__(self, ds, params={}, sample_num=300, time_mode='random', time_range=(0, 120), time_samples=40, bolometric_method='lyman'):
        self.ds = ds
        self.params = params
        self.sample_num = sample_num
        self.time_mode = time_mode
        self.time_range = time_range
        self.time_samples = time_samples
        self.bolometric_method = bolometric_method

    def sample_func(self, _i):
        attempts = 0
        while attempts < 5:
            t_grid = np.sort(np.random.uniform(min_t+time_range[0], min_t+time_range[1], time_samples))
            if np.count_nonzero(t_grid < prior_t0) < 4 or np.count_nonzero(t_grid > prior_t0) < 4:
                attempts += 1
                continue
            lc = ds.sample_abs_lc(method=bolometric_method, t_grid=t_grid)
            lums = utils.convert_luminosity(lc)
            lc = table.Table({'mjd': t_grid, 'luminosity': lums})
            texpl = min_t
            return lc, texpl

    def sample(self, sample_func=None):
        r = {}
        for k in ['mejecta', 'vejecta', 'energy', 'mni', 'td', 'r0', 'chi2red', 'texpl']:
            r[k] = np.empty(sample_num)

        min_t = ds.first_detection_mjd
        prior_t0 = ds.transient.prior_t0
    
        if sample_func is None:
            sample_func = self.sample_func
        

        for i in range(sample_num):
            lc = None
            fit_results = None
            while fit_results is None:
                try:
                    lc, texpl = sample_func(i)
                    bounds = {}
                    for k in params.keys():
                        v = params[k]
                        if k == 'texpl':
                            if v[0] == 'samples':
                                texpl = v[1][i]
                            elif v[0] == 'const':
                                texpl = v[1]
                        elif k in ['mni', 'td', 'r0', 'vej', 'A']:
                            if v[0] == 'samples':
                                bounds[k] = ('const', v[1][i])
                            elif v[0] == 'const':
                                bounds[k] = ('const', v[1])
                            elif v[0] == 'range':
                                bounds[k] = v
                            elif v[0] == 'gauss':
                                bounds[k] = ('const', np.random.normal(v[1], v[2]))
                            elif v[0] == 'uniform':
                                bounds[k] = ('const', np.random.uniform(v[1], v[2]))
                    fit_results = tigerfit(lc, texpl_mjd=texpl, param_bounds=bounds)
                except RuntimeError:
                    continue
            yield (lc, fit_results)

            # extract the fit results
            r['mejecta'][i] = fit_results.mej
            r['vejecta'][i] = fit_results.params[3]
            r['energy'][i] = fit_results.e
            r['mni'][i] = fit_results.params[0]
            r['td'][i] = fit_results.params[1]
            r['r0'][i] = fit_results.params[2]
            r['chi2red'][i] = fit_results.chi_red
            r['texpl'][i] = fit_results.texpl
            
        self.results = table.Table(r)
    


def sample(ds, params={}, sample_num=300, time_mode='random', time_range=(0, 120), time_samples=40, bolometric_method='lyman', return_results=False, sample_func=None, time_format='relative_min_t', quiet=False):
    r = {}
    for k in ['mejecta', 'vejecta', 'energy', 'mni', 'td', 'r0', 'chi2red', 'texpl']:
        r[k] = np.empty(sample_num)

    if time_format == 'relative_min_t':
        min_t = ds.first_detection_mjd
    elif time_format == 'peak':
        min_t = ds.transient.prior_t0
    else:
        raise ArgumentError('Unknown time format %s' % time_format)
    prior_t0 = ds.transient.prior_t0

    results = []

    if sample_func is None:
        def sample_func(_i):
            attempts = 0
            while attempts < 5:
                t_grid = np.sort(np.random.uniform(min_t+time_range[0], min_t+time_range[1], time_samples))
                if np.count_nonzero(t_grid < prior_t0) < 4 or np.count_nonzero(t_grid > prior_t0) < 4:
                    attempts += 1
                    continue
                lc = ds.sample_abs_lc(method=bolometric_method, t_grid=t_grid)
                lums = utils.convert_luminosity(lc)
                lc = table.Table({'mjd': t_grid, 'luminosity': lums})
                texpl = min_t
                return lc, texpl

    for i in tqdm.trange(sample_num, disable=quiet):
        #print(lc)
    
        fit_results = None
        while fit_results is None:
            try:
                lc, texpl = sample_func(i)
                bounds = {}
                kwargs = {}
                for k in params.keys():
                    v = params[k]
                    if k == 'texpl':
                        if v[0] == 'samples':
                            texpl = v[1][i]
                        elif v[0] == 'const':
                            texpl = v[1]
                    elif k in ['mni', 'td', 'r0', 'vej', 'A']:
                        if v[0] == 'samples':
                            bounds[k] = ('const', v[1][i])
                        elif v[0] == 'const':
                            bounds[k] = ('const', v[1])
                        elif v[0] == 'range':
                            bounds[k] = v
                        elif v[0] == 'gauss':
                            bounds[k] = ('const', np.random.normal(v[1], v[2]))
                        elif v[0] == 'uniform':
                            bounds[k] = ('const', np.random.uniform(v[1], v[2]))
                    elif k in ['kappa']:
                        kwargs['kappa'] = v
                fit_results = tigerfit(lc, texpl_mjd=texpl, param_bounds=bounds, **kwargs)
            except RuntimeError:
                continue
            except ValueError:
                continue
            except TypeError:
                continue

        if return_results:
            results.append(fit_results)

        # extract the fit results
        r['mejecta'][i] = fit_results.mej
        r['vejecta'][i] = fit_results.params[3]
        r['energy'][i] = fit_results.e
        r['mni'][i] = fit_results.params[0]
        r['td'][i] = fit_results.params[1]
        r['r0'][i] = fit_results.params[2]
        r['chi2red'][i] = fit_results.chi_red
        r['texpl'][i] = fit_results.texpl

    if return_results:
        return table.Table(r), results
    else:
        return table.Table(r)


@numba.njit
def gen_times(num, valid_mask, t_min, t_max):
    """
    Utility function to generate times that fall into discrete validity windows
    """
    ret = np.empty(num, dtype=np.float64)
    for i in range(num):
        valid = False
        t = 0.0
        while not valid:
            t = np.random.uniform(t_min, t_max)
            for row in valid_mask:
                if t > row[0] and t < row[1]:
                    valid = True
                    break
        ret[i] = t
    return ret
    

@numba.njit
def prior(u, vej_min, vej_max, gamma_trapped):
    x = np.empty_like(u)
    # M_ni [Msol] - max is M_ch
    x[0] = 1.4*u[0] + 0.01
    # t_d
    x[1] = 60*u[1] + 5
    # r_0 [1e15 cm] - max is 100 Rsun min is 0.005 Rsun
    x[2] = np.exp(-10*u[2] - 5)
    # v_ej
    x[3] = (vej_max-vej_min)*u[3] + vej_min
    # A
    if not gamma_trapped:
        x[4] = 300*u[4] + 40
    
    return x

@numba.njit
def prior_eth(u, vej_min, vej_max, gamma_trapped):
    x = np.empty_like(u)
    # M_ni
    x[0] = 10*u[0] + 0.01
    # t_d
    x[1] = 95*u[1] + 5
    # r_0 [1e15 cm] - max is 5000 Rsun
    x[2] = np.exp(-10*u[2] - 1)
    # v_ej
    x[3] = (vej_max-vej_min)*u[3] + vej_min
    # Eth
    x[4] = np.exp(-40*u[4])
    # A
    if not gamma_trapped:
        x[5] = 300*u[5] + 10
    
    
    return x

# TODO: refactor that stuff to common module (maybe noisy_likelihood_sampling ?)
#@numba.njit
def likelihood(x, modfunc, pregen_times, pregen_lums, texpl_min, texpl_max, scalefactor, extra_args={}):
    texpl_mjd = np.random.uniform(texpl_min, texpl_max)
    
    # select random entry from pre-generated LCs
    idx = np.random.randint(0, pregen_times.shape[0])
    t_grid = pregen_times[idx]
    t_expl = t_grid - texpl_mjd
    lums = pregen_lums[idx]
    #model = np.exp(modfunc(t_expl, *x))/1e42
    model = scalefactor*modfunc(t_expl, *x, **extra_args)

    x = -0.5 * np.sum((lums - model)**2)
    if np.isnan(x):
        #return -np.inf
        x = -100 * np.nansum((lums - model)**2)
    if np.isnan(x):
        return -np.inf
    return x

def nested_fitting(ds, time_range, prior_kwargs={}, num_pregen=12000, num_times=50, bolometric_method='lyman', time_valid=np.array([[1, 1e12]]), quiet=False, texpl_range=None, include_e_thermal=False, nlive_init=1000, scalefactor=2.5, gamma_trapped=False):
    t_min, t_max = time_range

    # TODO: implement that once we prior_texpl
    #if texpl_range is None:
    #    texpl_range = (ds.transient.prior_t0-0.1, ds.transient.prior_t0+0.1)
    texpl_min, texpl_max = texpl_range

    # TODO: outsource this into utility function
    # pre-generate bolometric lightcurves
    # TODO: cache this somehow
    pregen_t = np.empty((num_pregen, num_times), dtype=np.float)
    pregen_lums = np.empty((num_pregen, num_times), dtype=np.float)
    for i in tqdm.trange(num_pregen, disable=quiet):
        #t_grid = np.sort(np.random.uniform(t_min, t_max, M))
        t_grid = gen_times(num_times, time_valid, t_min, t_max)
        lc = ds.sample_abs_lc(method=bolometric_method, t_grid=t_grid, tweaks=['correlated-error'])
        lums = scalefactor*np.log(utils.convert_luminosity(lc))
        #lums = utils.convert_luminosity(lc)/1e42
        pregen_lums[i] = lums
        pregen_t[i] = t_grid

    logl_kwargs = {
        'pregen_times': pregen_t,
        'pregen_lums': pregen_lums,
        'texpl_min': texpl_min,
        'texpl_max': texpl_max,
        'scalefactor': scalefactor
    }

    if include_e_thermal:
        prior_func = prior_eth
        logl_kwargs['modfunc'] = np.vectorize(Lum_rad_eth)
        ndim = 5
    else:
        prior_func = prior
        logl_kwargs['modfunc'] = np.vectorize(Lum_rad)
        ndim = 4

    prior_kwargs['gamma_trapped'] = gamma_trapped
    if not gamma_trapped:
        ndim += 1
    else:
        logl_kwargs['extra_args'] = {'A': 10000}

    sf_kwargs = {
        'evid_thresh': 0.3,
        'post_thresh': 0.1
    }
        
    sampler = dynesty.NestedSampler(likelihood, prior_func, ndim, ptform_kwargs=prior_kwargs, logl_kwargs=logl_kwargs, method='unif', nlive=1000)
    #sampler = dynesty.DynamicNestedSampler(likelihood, prior_func, ndim, ptform_kwargs=prior_kwargs, logl_kwargs=logl_kwargs, method='unif', nlive_init=nlive_init, stop_kwargs=sf_kwargs)
    return sampler
