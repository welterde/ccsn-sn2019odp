import numpy as np
import numba
from scipy.special import gamma

@numba.njit(inline='always')
def gaussian(eval_wave, center, amplitude, sigma):
    return amplitude/sigma/np.sqrt(2*np.pi)*np.exp(-(eval_wave - center)**2/2/sigma**2)


@numba.njit(inline='always')
def gaussian_continuum(eval_wave, center, amplitude, sigma, continuum_level, continuum_slope):
    dwlen = eval_wave - center
    return continuum_level + continuum_slope*dwlen + amplitude/sigma/np.sqrt(2*np.pi)*np.exp(-(eval_wave - center)**2/2/sigma**2)

DIV_2 = gamma(1/2)
DIV_3 = gamma(1/3)
DIV_4 = gamma(1/4)
DIV_6 = gamma(1/6)
DIV_8 = gamma(1/8)

# https://stats.stackexchange.com/questions/203629/is-there-a-plateau-shaped-distribution
@numba.njit(inline='always')
def flat_top_gaussian(eval_wave, center, amplitude, sigma, order=3):
    if order == 2:
        div = DIV_2
    if order == 3:
        div = DIV_3
    elif order == 4:
        div = DIV_4
    elif order == 6:
        div = DIV_6
    elif order == 8:
        div = DIV_8
    else:
        div = 1
    return amplitude*order/2/sigma/div*np.exp(-np.abs(eval_wave - center)**order/sigma**order)


# https://stackoverflow.com/a/457805
@numba.njit(inline='always')
def erf(x):
    # save the sign of x
    if x < 0:
        sign = -1
    else:
        sign = 1
    
    x = np.abs(x)

    # constants
    a1 =  0.254829592
    a2 = -0.284496736
    a3 =  1.421413741
    a4 = -1.453152027
    a5 =  1.061405429
    p  =  0.3275911

    # A&S formula 7.1.26
    t = 1.0/(1.0 + p*x)
    y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*np.exp(-x*x)
    return sign*y # erf(-x) = -erf(x)


# https://en.wikipedia.org/wiki/Skew_normal_distribution
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.skewnorm.html
@numba.njit(inline='always')
def gaussian_emg(eval_wave, center, amplitude, sigma, rate):
    erf_c = 1-erf((center+rate*sigma**2 - eval_wave)/np.sqrt(2)/sigma)
    
    return rate/2 * np.exp(rate/2 * (2*center+rate*sigma**2 - 2*eval_wave)) * erf_c

@numba.njit(inline='always')
def cdf(eval_wave, center, sigma):
    c = (eval_wave - center)/sigma/np.sqrt(2)
    return 0.5*(1+erf(c))

@numba.njit(inline='always')
def cdf_skew(eval_wave, center, sigma, skew):
    c = skew*(eval_wave - center)/sigma/np.sqrt(2)
    return 0.5*(1+erf(c))


@numba.njit(inline='always')
def skewed(eval_wave, center, amplitude, sigma, skew):
    return 2*gaussian(eval_wave, center, amplitude, sigma)*cdf_skew(eval_wave, center, sigma, skew)



@numba.njit(inline='always')
def elongated_gaussian(eval_wave, center, amplitude, sigma, deadzone):
    """
    Gaussian is split into two halfs with deadzone at peak amplitude of gaussian in between
    """
    # peak amplitude of the gaussian
    peak_amplitude = 1/sigma/np.sqrt(2*np.pi)
    box_amplitude = deadzone*peak_amplitude
    # gaussian is normalized to 1 already
    total_amplitude = 1+box_amplitude
    new_amplitude = amplitude/total_amplitude #??

    dwlen = eval_wave - center
    if dwlen < -deadzone/2:
        dw = eval_wave - center + deadzone/2
        return new_amplitude/sigma/np.sqrt(2*np.pi)*np.exp(-dw**2/2/sigma**2)
    elif dwlen < deadzone/2:
        return new_amplitude*peak_amplitude
    else:
        dw = eval_wave - center - deadzone/2
        return new_amplitude/sigma/np.sqrt(2*np.pi)*np.exp(-dw**2/2/sigma**2)
