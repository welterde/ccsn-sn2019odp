import numpy as np
import numba




@numba.njit(inline='always')
def cauchy(eval_wave, center, amplitude, sigma):
    return amplitude/np.pi/sigma/(1+((eval_wave-center)/sigma)**2)

@numba.njit(inline='always')
def cauchy_continuum(eval_wave, center, amplitude, sigma, continuum_level, continuum_slope):
    dwlen = eval_wave - center
    return continuum_level + continuum_slope*dwlen + amplitude/np.pi/sigma/(1+((eval_wave-center)/sigma)**2)

