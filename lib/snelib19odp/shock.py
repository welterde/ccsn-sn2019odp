import numpy as np
import numba



# based on Waxman (2007) paper
# https://ui.adsabs.harvard.edu/abs/2007ApJ...667..351W/abstract



@numba.njit
def r_phot_waxman07(t, f, e_ej, m_ej, r_shell12):
    """ Calculate photosphere radius in Waxman (2007) uv-optical model
    
    Parameters:
    t (float): Time since explosion in days
    f (float): Ratio of densities (unitless)
    e_ej (float): Energy of ejecta divided by 10^51 erg
    m_ej (float): Mass of ejecta in solar-masses
    r_shell12 (float): radius of shell divided by 10^12 cm
    
    Returns:
    Radius of photosphere in cm
    """
    return 3.2e14 * f**(-0.04) * e_ej**0.4 / m_ej**0.3 * t**0.8
    
    
@numba.njit
def t_phot_waxman07(t, f, e_ej, m_ej, r_shell12):
    """ Calculate photospheric temperature in Waxman (2007) uv-optical model
    
    Parameters:
    t (float): Time since explosion in days
    f (float): Ratio of densities (unitless)
    e_ej (float): Energy of ejecta divided by 10^51 erg
    m_ej (float): Mass of ejecta in solar-masses
    r_shell12 (float): radius of shell divided by 10^12 cm
    
    Returns:
    Photosphere temperature in eV
    """
    return 2.2 * f**(-0.02) * e_ej**0.02 / m_ej**0.03 * r_shell12**0.25 * t**(-0.5)
