import os, json
import sexpdata
import numba
from scipy import LowLevelCallable
from numba import cfunc, jit, carray
from numba.types import intc, CPointer, float64
import numpy as np
import astropy.table as table
import astropy.units as u
import astropy.constants as const
from astropy.cosmology import Planck15 as cosmology

def load_raw_dataset(group, dataset_name, format=None, out_fmt='pandas+defaultidx'):
    fname = os.path.join('data', group, dataset_name)
    t = table.Table.read(fname, format=format)

    # transform JD to MJD if found in table
    if 'JD' in t.colnames:
        t['mjd'] = t['JD'] - 2400000.5

    if out_fmt == 'pandas+defaultidx':
        return t.to_pandas().set_index(['mjd', 'band'])
    else:
        raise ValueError('Unknown output format: %s' % out_fmt)


def convert_luminosity(abs_mag):
    luminosity = (const.L_sun.cgs.value)*10 ** ((4.74 - abs_mag)/2.5)
    return luminosity

def unconvert_luminosity(lum):
    abs_mag = 4.74 - 2.5*np.log10(lum/(const.L_sun.cgs.value))
    return abs_mag

@numba.njit(inline='always')
def luminosity2flux(luminosity, distance):
    # all in cgs units
    return luminosity / 4 / np.pi / distance**2

@numba.njit(inline='always')
def flux2luminosity(flux, distance):
    # all in cgs units
    return flux * 4 * np.pi * distance**2

def flux2lum(flux, dist):
    pass

def convert_dict(spec):
    ret = {}
    for entry in spec:
        assert len(entry) > 0
        assert isinstance(entry[0], sexpdata.Symbol)
        
        key = entry[0].value()
        if len(entry) == 1:
            val = None
        else:
            val = entry[1:]
        ret[key] = val
    return ret

def redshift2distance(redshift_min, redshift_max):
    dist_cm_min = cosmology.luminosity_distance(redshift_min).to(u.cm).value
    dist_cm_max = cosmology.luminosity_distance(redshift_max).to(u.cm).value
    return dist_cm_min, dist_cm_max

def jit_integrand_function_1d(integrand_function):
    jitted_function = jit(integrand_function, nopython=True)

    @cfunc(float64(intc, CPointer(float64)))
    def wrapped(n, xx):
        ar = carray(xx, n)
        return jitted_function(ar[0], ar[1:])
    return LowLevelCallable(wrapped.ctypes)

def jit_integrand_function_3param(integrand_function):
    jitted_function = jit(integrand_function, nopython=True)

    @cfunc(float64(intc, CPointer(float64)))
    def wrapped(n, xx):
        ar = carray(xx, n)
        return jitted_function(ar[0], ar[1], ar[2], ar[3])
    return LowLevelCallable(wrapped.ctypes)
