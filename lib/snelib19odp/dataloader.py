import os, json, re, glob
import numpy as np
import astropy.table as table
import astropy.io.fits as fits
import astropy.time as time
import pandas as pd

from . import const
from . import spec as spectools
from . import extinction


raw_spec_dir = os.path.join(os.path.dirname(__file__), '../../data/specs/preproc_marshall')
fluxcal_spec_dir = os.path.join(os.path.dirname(__file__), '../../products/specs/fluxcal')


def load_obs_spec(fname, fluxcal=True, mask_telluric=True, mask_extra=True):
    if fluxcal:
        full_fname = os.path.join(fluxcal_spec_dir, fname)
    else:
        full_fname = os.path.join(raw_spec_dir, fname)

    data = table.Table.read(full_fname, format='ascii')
    if not fluxcal:
        data['obswave'] = data['col1']
        data['flux'] = data['col2']
    elif 'obswave' not in data.colnames:
        data['obswave'] = data['wavelength']
    if 'fluxerr' not in data.colnames:
        data['fluxerr'] = np.nan*np.empty(len(data))
    if 'mask' not in data.colnames:
        data['mask'] = np.ones(len(data), dtype=np.bool)

    data.meta['file_name'] = os.path.basename(fname)
    
    spectools.add_restframe(data, inplace=True)

    if mask_telluric:
        spectools.mask_telluric(data, inplace=True)

    if mask_extra:
        spectools.mask_extra_regions(data, fname)

    return data
    

REQUIRED_HEADERS = ['DATE-OBS', 'EXPTIME']
SEMI_REQUIRED_HEADERS = ['OBSERVER', 'REDUCER']

def check_headers(meta, semi_required=False):
    if 'DATE-OBS' not in meta:
        if 'UTC' in meta:
            meta['DATE-OBS'] = meta['UTC']
        elif 'UTSHUT' in meta:
            meta['DATE-OBS'] = meta['UTSHUT']
    for key in REQUIRED_HEADERS:
        if key not in meta:
            return False
    if semi_required:
        for key in SEMI_REQUIRED_HEADERS:
            if key not in meta:
                return False
    return True


raw_spec_dir = os.path.join(os.path.dirname(__file__), '../../data/specs/preproc_marshall')
ASCII_META_RE = re.compile(r'\# (\S+)\: (\S+)')

DATA_SPECS_EXTRA_FILE = os.path.join(os.path.dirname(__file__), '../../data/specs_extra_metadata.txt')
DATA_SPECS_EXTRA_PEOPLE_FILE = os.path.join(os.path.dirname(__file__), '../../data/specs_extra_people.txt')


def get_meta(fname):
    # load extra metadata
    # TODO: cache this somehow
    extra_meta = table.Table.read(DATA_SPECS_EXTRA_FILE, format='ascii')
    extra_ppl = table.Table.read(DATA_SPECS_EXTRA_PEOPLE_FILE, format='ascii')
    print(extra_ppl)

    # try to load directly from the header of the unprocessed file
    meta = {}
    with open(os.path.join(raw_spec_dir, fname), 'r') as f:
        lines = f.readlines()
        for line in lines:
            if line[0] != '#':
                break
            m = ASCII_META_RE.match(line)
            if m:
                k,v = m.groups()
                meta[k] = v

    if not check_headers(meta):
        # try to read them from the fits file if it exists
        fits_fname = os.path.join(raw_spec_dir, fname.replace('ascii', 'fits'))
        if os.path.exists(fits_fname):
            fits_hdr = fits.getheader(fits_fname)
            for k in fits_hdr.keys():
                v = fits_hdr[k]
                meta[k] = v

    if not check_headers(meta):
        # try to read them from the extra metadata file
        if fname in extra_meta['filename']:
            for k in extra_meta.colnames:
                if k == 'filename':
                    continue
                idx = extra_meta['filename'] == fname
                meta[k] = extra_meta[k][idx][0]

    if not check_headers(meta, semi_required=True):
        # most likely still missing observer and/or reducer
        if fname in extra_ppl['filename']:
            for k in extra_ppl.colnames:
                if k == 'filename':
                    continue
                idx = extra_ppl['filename'] == fname
                if k not in meta:
                    print(meta)
                    print(extra_ppl)
                    meta[k] = extra_ppl[k][idx][0]

    if not check_headers(meta):
        # TODO: use logging instead of this!
        sys.stderr.write('Still missing headers for %s\n' % fname)

    return meta

def get_spec_obs_time(fname):
    meta = get_meta(fname)
    return time.Time(meta['DATE-OBS'])

def get_spec_obs_mjd(fname):
    meta = get_meta(fname)
    obs_mjd = time.Time(meta['DATE-OBS']).mjd
    return obs_mjd

def get_obs_phase(fname):
    meta = get_meta(fname)
    obs_mjd = time.Time(meta['DATE-OBS']).mjd
    peak_mjd = const.sne_peak_mjd['g']
    return obs_mjd - peak_mjd

def guess_setup(fname, meta):
    # parse filename
    fname_parts = fname.split('_')
    
    if fname_parts[2] == 'P60':
        return 'P60/SEDM', 'IFU'
    elif 'INSTRUMENT_SETUP' in meta:
        return meta['TELESCOPE_INSTRUMENT'], meta['INSTRUMENT_SETUP']
    tel_ins, setup = 'TBD', 'TBD'
    if 'TELESCOP' in meta and 'INSTRUME' in meta:
        tel_ins = ('%s/%s' % (meta['TELESCOP'], meta['INSTRUME'])).replace('_', ' ')
        if 'FASU' in tel_ins:
            tel_ins = tel_ins.replace('FASU', '')
    if fname_parts[2] == 'P200':
        tel_ins = 'P200/DBSP'
        setup = meta['GRATING']
    if 'HIERARCH ESO INS GRIS1 NAME' in meta:
        setup = '%s %s' % (meta['HIERARCH ESO INS GRIS1 NAME'], meta['HIERARCH ESO INS SLIT1 NAME'])
    return tel_ins, setup


comparison_dir = os.path.join(os.path.dirname(__file__), '../../data/comparison')

const_dir = os.path.join(os.path.dirname(__file__), '../../const')

def load_comparison_info(fname):
    dat = table.Table.read(os.path.join(const_dir, 'comparison_info.txt'), format='ascii')

    idx = dat['fname'] == os.path.basename(fname)
    assert np.count_nonzero(idx) == 1
    return dat[idx]

def get_comparison_phase0(fname):
    row = load_comparison_info(fname)
    return row['phase0'][0]

def get_comparison_ebv(fname, what):
    assert what in ['mw', 'host']
    row = load_comparison_info(fname)
    wrapper = extinction.EbvSpec(row['ebv_%s' % what])
    return wrapper


def load_comparison_json_specs(fname, phase0=None, mask_telluric=True, mask_extra=True, redshift=None):
    if phase0 is None:
        phase0 = get_comparison_phase0(fname)
    
    full_fname = os.path.join(comparison_dir, fname)
    with open(full_fname, 'r') as f:
        data = json.load(f)
    sne = data[list(data.keys())[0]]
    specs = sne['spectra']
    if redshift is None:
        redshift = float(sne['redshift'][0]['value'])
    #print(specs[1])
    ret_specs = []
    for spec in specs:
        if not 'time' in spec:
            continue
        t = spec['time']
        wave = np.array([float(x[0]) for x in spec['data']])
        flux = np.array([float(x[1]) for x in spec['data']])
        # TODO: add fluxerr column
        # TODO: process masks from the input dataset? I think there were some keywords for that..
        new_spec = table.Table({'obswave': wave, 'restwave': wave/(1+redshift), 'flux': flux, 'mask': np.ones(len(spec['data']), dtype=np.bool)})
        new_spec.meta['time'] = float(t)
        new_spec.meta['phase'] = float(t)-phase0
        new_spec.meta['filename'] = spec['filename']
        new_spec.meta['file_name'] = spec['filename']
        new_spec.meta['obs_mjd'] = float(t)
        new_spec.meta['name'] = spec['filename'].split('.')[0]

        if mask_telluric:
            spectools.mask_telluric(new_spec, inplace=True)
        
        if mask_extra:
            spectools.mask_extra_regions(new_spec, spec['filename'])
        
        ret_specs.append(new_spec)
    return sorted(ret_specs, key=lambda x: x.meta['time'])


def load_comparison_json_phot(fname, phase0=None):
    if phase0 is None:
        phase0 = get_comparison_phase0(fname)
    
    full_fname = os.path.join(comparison_dir, fname)
    with open(full_fname, 'r') as f:
        data = json.load(f)
    sne = data[list(data.keys())[0]]

    photometry = sne['photometry']

    lc_f = []
    for lc_p in photometry:
        if lc_p.get('upperlimit'):
            continue
        if 'source' not in lc_p:
            continue
        if 'e_magnitude' not in lc_p:
            lc_p['e_magnitude'] = np.nan
        if 'instrument' not in lc_p:
            lc_p['instrument'] = None
        if 'telescope' not in lc_p:
            lc_p['telescope'] = None
        lc_f.append(lc_p)
    lc = pd.DataFrame(data={
        'mjd': [float(x['time']) for x in lc_f],
        'band': [x['band'] for x in lc_f],
        'instrument': [x['instrument'] for x in lc_f],
        'telescope': [x['telescope'] for x in lc_f],
        'mag': [float(x['magnitude']) for x in lc_f],
        'mag_err': [float(x['e_magnitude']) for x in lc_f],
        'source': [x['source'] for x in lc_f],
    })
    lc['phase'] = lc['mjd'] - phase0
    return lc

# TODO: rewrite the two functions.. they are almost identical

def load_comparison_json_phot_tbl(fname, phase0=None, relative_fname=True):
    if phase0 is None:
        phase0 = get_comparison_phase0(fname)

    if relative_fname:
        full_fname = os.path.join(comparison_dir, fname)
    else:
        full_fname = fname
    with open(full_fname, 'r') as f:
        data = json.load(f)
    sne = data[list(data.keys())[0]]

    photometry = sne['photometry']
    sources = sne['sources']

    lc_f = []
    for lc_p in photometry:
        if lc_p.get('upperlimit'):
            continue
        if 'e_magnitude' not in lc_p:
            lc_p['e_magnitude'] = np.nan
        if 'instrument' not in lc_p:
            lc_p['instrument'] = None
        if 'telescope' not in lc_p:
            lc_p['telescope'] = None
        lc_f.append(lc_p)
    lc = table.Table(data={
        'mjd': [float(x['time']) for x in lc_f],
        'band': [x['band'] for x in lc_f],
        'instrument': [x['instrument'] for x in lc_f],
        'telescope': [x['telescope'] for x in lc_f],
        'mag': [float(x['magnitude']) for x in lc_f],
        'mag_err': [float(x['e_magnitude']) for x in lc_f],
        'source': [x['source'] for x in lc_f],
    })
    sources_aliases = []
    for source in sources:
        src_id = source['alias'].strip()
        assert ',' not in src_id
        name = source['name']
        sources_aliases.append(src_id)
        lc.meta['source-%s-name' % src_id] = name
    lc.meta['source-aliases'] = ','.join(sources_aliases)
    
    lc['phase'] = lc['mjd'] - phase0
    return lc


def load_spec_mjds(fluxcal=True):
    if fluxcal:
        dirname = fluxcal_spec_dir
    else:
        dirname = raw_spec_dir

    spec_files = glob.glob(os.path.join(dirname, '*.ascii'))
    mjds = [get_spec_obs_mjd(os.path.basename(x)) for x in spec_files]
    return mjds




def load_aa_table(fname):
    ds = table.Table.read(fname, format='ascii')

    def parse_row(x):
        if not isinstance(x, str):
            return x, np.nan
        if '..' in x:
            return np.nan, np.nan
        if '(' not in x:
            try:
                return float(x), np.nan
            except ValueError:
                return x, np.nan
        val,err = x.replace('(', ' ').replace(')', '').split(' ')
        return float(val), float(err)

    # parse the columns
    new_tab = {}
    for col_name in ds.colnames:
        entries = [parse_row(x) for x in ds[col_name]]
        vals = np.array([x[0] for x in entries])
        errs = np.array([x[1] for x in entries])
        if np.count_nonzero(np.isnan(errs)) < len(errs):
            new_tab[col_name + '_err'] = errs
        new_tab[col_name] = vals

    return table.Table(new_tab)
