import collections
import numpy as np

import astropy.table as table

import scipy.optimize as optimize
from scipy.interpolate import interp1d
from scipy.interpolate import splev, splrep
from numpy.lib import recfunctions as rfn


SPLINE_MEAN_KNOTNUM = 17

# TODO: move that datastruct to common module?
Spec = collections.namedtuple('Spec', 'wave flux mask')



def meanzero(flux):
    xknot = np.empty(len(flux))
    yknot = np.empty(len(flux))

    nw = len(flux)

    #print(flux[0])
    idx_nonzero = flux != 0.0
    assert np.count_nonzero(idx_nonzero) > 0
    l1 = np.min(np.arange(nw)[idx_nonzero])
    l2 = np.max(np.arange(nw)[idx_nonzero])
    assert (l2-l1) >= 3*SPLINE_MEAN_KNOTNUM

    # choose knots for spline
    nknot = 0
    kwidth = int(nw / SPLINE_MEAN_KNOTNUM)
    nave = 0
    wave = 0
    fave = 0
    istart = 0

    #print('Range: %d %d' % (l1, l2))
    #print('kwidth: %s' % repr(kwidth))
    #print('nw: %d' % nw)

    for i in range(nw):
        if i > l1 and i < l2:
            nave = nave + 1
            wave = wave + i-0.5
            fave = fave + flux[i]
        if (i-istart) % kwidth == 0:
            #print('nave: %s fave: %s' % (repr(nave), repr(fave)))
            if nave > 0 and fave > 0:
                #print(' -> knot')
                xknot[nknot] = wave / nave
                yknot[nknot] = np.log10(fave/nave)
                nknot = nknot + 1
            nave = 0
            wave = 0
            fave = 0

    assert nknot != 0

    xknot = xknot[0:nknot]
    yknot = yknot[0:nknot]
    #print('nknot: ', nknot)
    #print(xknot)
    #print(len(xknot))

    spline = splrep(xknot, yknot)

    flux_norm = np.zeros(nw)
    #if nknot % 2 == 0:
    #    interpolator = interp1d(xknot, yknot, kind=nknot-1)
    #else:
    #    interpolator = interp1d(xknot, yknot, kind=nknot)
    for i in range(l1, l2):
        #logspl = interpolator(i-0.5)
        logspl = splev(i-0.5, spline)
        flux_norm[i] = flux[i] / 10**logspl - 1.0
    #plt.plot(flux_norm[l1:l2])
    #plt.show()
    return flux_norm, (l1,l2)



#@numba.njit
def snidbin(wlen, flux, nw, wlen0, dwlen_log, fnu=False):
    #wlen_bin = wlen0 * np.exp(np.arange(nw+1)*dwlen_log)
    #dwlen_bin = np.diff(wlen_bin)
    # mangle centers
    #for i in range(nw):
    #    wlen_bin[i] = 0.5*(wlen_bin[i]+wlen_bin[i+1])
    #wlen_bin = wlen_bin[0:(nw-2)]

    flux_bin = np.zeros(nw)

    for wlen_i in range(len(wlen)):
        if wlen_i == 0:
            s0 = 0.5*(3*wlen[wlen_i]-wlen[wlen_i+1])
            s1 = 0.5*(wlen[wlen_i]+wlen[wlen_i+1])
        elif wlen_i == len(wlen)-1:
            s0 = 0.5*(wlen[wlen_i-1]+wlen[wlen_i])
            s1 = 0.5*(3*wlen[wlen_i]-wlen[wlen_i-1])
        else:
            s0 = 0.5*(wlen[wlen_i-1]+wlen[wlen_i])
            s1 = 0.5*(wlen[wlen_i]+wlen[wlen_i+1])

        s0log = np.log(s0/wlen0)/dwlen_log
        s1log = np.log(s1/wlen0)/dwlen_log

        #print('log s0 = %f / log s1 = %f' % (s0log, s1log))

        if fnu:
            dw = (s1-s0)*2.99793e10/(wlen[wlen_i]*wlen[wlen_i]*1e-8)
        else:
            dw = s1-s0

        #print('dw = %e' % dw)
        
        for bin_i in range(int(s0log), int(s1log)+1):
            if bin_i >= 0 and bin_i < nw:
                alen = min([s1log, bin_i+1]) - max([s0log, bin_i])
                #print('alen = ', alen)
                #print('input flux = ', flux[wlen_i])
                flux_bin[bin_i] += flux[wlen_i] * alen/(s1log-s0log) * dw
                #print('%d -> %e' % (bin_i, flux_bin[bin_i]))

    #print(flux_bin*1e20)
    #print('Binned flux array size: %d' % len(flux_bin))

    #if True:
    #    wlen_bin = wlen0 * np.exp(np.arange(nw)*dwlen_log)
    #    plt.plot(wlen_bin, flux_bin)
    #    plt.show()
    return flux_bin



def snidflat(spec, redshift, wlen_min, wlen_max):
    # wavelength bins (num, start, stop)
    nw = 1024
    w0 = 2500
    w1 = 10000

    # dwlog = np.log(w1/w0)/nw
    # wlen_log = w0*np.exp(np.arange(nw)*dwlog)
    # for i in range(nw-1):
    #     # half-wavelength-bins? snidbins??
    #     wlen_log[i] = 0.5*(wlen_log[i]+wlen_log[i+1])
    # wlen_log = wlen_log[:nw-1]
    # binsize = wlen_log[-1]-wlen_log[-2]
    # #wlog = np.array([0.5*(wlog[i]+wlog[i+1]) for i in range(nw-1)])

    # # de-redshift input spectrum
    wlen_z = spec.wave/(1+redshift)
    
    # # rebin onto log wavelength scale
    # 
    # log_flux_bin = snidbin(wlen_z[idx], flux[idx], wlen_log.min(), wlen_log.max(), binsize)

    dwlog = np.log(w1/w0)/nw
    wlen_log = w0*np.exp(np.arange(nw+1)*dwlog)
    
    idx = np.logical_and(spec.wave > wlen_min, spec.wave < wlen_max)
    flux_bin = snidbin(wlen_z[idx], spec.flux[idx], nw, w0, dwlog, fnu=True)
    
    
    for i in range(nw):
        # half-wavelength-bins? snidbins??
        wlen_log[i] = 0.5*(wlen_log[i]+wlen_log[i+1])
    wlen_log = wlen_log[:nw]

    #print('Len flux_bin = %d' % len(flux_bin))
    flux_bin_norm, (l1,l2) = meanzero(flux_bin)
    #print('Len flux_bin_norm = %d' % len(flux_bin_norm))

    #if do_filter:
    #    flux_bin_norm[l1:l2] = snidfilt(flux_bin_norm, k1=1, k2=4, k3=nw/12, k4=nw/10)[l1:l2]

    #print(flux_bin_norm)
        
    #print('wlen_flat: ', len(wlen_flat), ' flux_bin_norm ', len(flux_bin_norm))
    #flux_flat = np.interp(wlen_z, wlen_log, flux_bin_norm)

    #print(l1, l2)
    
    # this apodize thing from snid
    # if True:
    #     ftmp = np.array(flux_flat, copy=True)
    #     nsquash = min([nw*0.05, float((l2-l1)/2)])
    #     if nsquash >= 1:
    #         for i in range(int(nsquash)):
    #             arg = np.pi * i / (nsquash - 1)
    #             factor = 0.5 * (1 - np.cos(arg))
    #             ftmp[l1+i] = factor * flux_flat[l1+i]
    #             ftmp[l2-i] = factor * flux_flat[l2-i]
    #     flux_flat = ftmp
    
    # construct valid mask
    tmp_idx = np.arange(len(flux_bin_norm))
    idx = np.logical_and(np.logical_and(tmp_idx > l1, tmp_idx < l2), ~np.isnan(flux_bin_norm))
    
    return Spec(wlen_log, flux_bin_norm, idx)



def correlate_snid_highpass(spec1, spec2):

    mask = np.logical_and(spec1.mask, spec2.mask)

    assert spec1.wave.shape == spec2.wave.shape

    #@numba.njit
    def likelihood(x, flux1, flux2, flux_mask):
        amplitude = np.exp(x)

        model = amplitude*flux2
        diff = (flux1-model)**2
        return -0.5*np.sum(diff[flux_mask])
        # ret = 0.0
        # for i in range(len(flux1)):
        #     if flux_mask[i]:
        #         ret += (flux1[i]-amplitude*flux2[i])**2

        #return ret

    #result = optimize.least_squares(likelihood, 1.0, bounds=(-30, 30), args=(spec1.flux, spec2.flux, mask))

    #scale_factor = result.x
    scale_factor = 0.0
    #print(scale_factor)
    if False:
        import matplotlib.pyplot as plt
        plt.plot(spec1.wave, spec1.flux)
        plt.plot(spec2.wave, np.exp(scale_factor)*spec2.flux)
        plt.show()
    
    return likelihood(scale_factor, spec1.flux, spec2.flux, mask), np.count_nonzero(mask)

def preprocess_spectrum(spec, clip_telluric, redshift=0.0):
    spec = snidflat(spec, wlen_min=4000, wlen_max=9000, redshift=redshift)#redshift=0.014353)

    if clip_telluric:
        old_mask = spec.mask
        # clip from 7464 to 7582 (in shifted spectrum..)
        clip_mask = np.logical_and(spec.wave > 7464/(1+redshift), spec.wave < 7582/(1+redshift))
        new_mask = np.logical_and(old_mask, ~clip_mask)
        return Spec(spec.wave, spec.flux, new_mask)
    else:
        return spec
