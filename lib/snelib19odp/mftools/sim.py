import os, logging, time, json
import sexpdata

from schwimmbad import MPIPool

import mosfit.fitter as mf_fitter

from . import dump
from .. import dataset

ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../..'))

DEFAULT_PRODUCT_DIR = os.path.join(ROOT_DIR, 'products/mosfit_runs')



class Simulator(object):

    def __init__(self, name, cfg, products_dir=None, mpi=False):
        self.name = name
        self.cfg = cfg
        self.log = logging.getLogger('sim')

        if mpi:
            self.pool = MPIPool()
            self.master = self.pool.is_master()
            self.log.info(f"Running with MPI (pool size={self.pool.size}; master={self.master})")
        else:
            self.master = True
            self.pool = None

        # default config
        self.env_name = 'default'
        self.model_name = 'default'
        self.fitter_kwargs = {}
        self.butcher_ds = {}
        self.tweaks = set()
        self.model_params = []
        self.model_params_del = []
        self.products_dir = products_dir

        self.parse_config()
            
        if self.master:
            self.run_id = int(time.time())

            if products_dir is None:
                self.products_dir = os.path.abspath(os.path.join(DEFAULT_PRODUCT_DIR, name, str(self.run_id)))
            

            if not os.path.isdir(self.products_dir):
                os.makedirs(self.products_dir)

            if mpi:
                self.log.debug('Sending run-id to slaves')
                for rank in range(1, self.pool.size + 1):
                    self.pool.comm.send(str(self.run_id), dest=rank, tag=3)

            

        else:
            self.log.debug('Waiting for run-id from master')
            self.run_id = int(self.pool.comm.recv(source=0, tag=3))
            if products_dir is None:
                self.products_dir = os.path.abspath(os.path.join(DEFAULT_PRODUCT_DIR, name, str(self.run_id)))

    def parse_config(self):
        for rule in self.cfg:
            assert len(rule) > 0
            assert isinstance(rule[0], sexpdata.Symbol)
            cmd = rule[0].value()

            if cmd == 'dataset' and self.master:
                assert not hasattr(self, 'ds')
                self.log.info('Loading dataset %s', rule[1])
                self.ds = dataset.load_dataset(rule[1])
            elif cmd == 'bands':
                self.bands = rule[1:]
            elif cmd == 'model':
                self.model_name = rule[1]
            elif cmd == 'env':
                self.env_name = rule[1]
            elif cmd == 'fitter-arg':
                self.fitter_kwargs[rule[1]] = rule[2]
            elif cmd == 'butcher-ds':
                band = rule[1]
                if band not in self.butcher_ds:
                    self.butcher_ds[band] = []
                self.butcher_ds[band].append(rule[2:])
            elif cmd == 'distance-param':
                self.tweaks.add('lumdist-param')
            elif cmd == 'min-mjd':
                self.tweaks.add('min-mjd')
                self.min_mjd = rule[1]
            elif cmd == 'max-mjd':
                self.tweaks.add('max-mjd')
                self.max_mjd = rule[1]
            elif cmd == 'model-param':
                pname = rule[1]
                k = rule[2]
                v = rule[3]
                self.model_params.append((pname, k, v))
            elif cmd == 'model-param:del':
                pname = rule[1]
                self.model_params_del.append(pname)


    @property
    def results_available(self):
        # TODO: implemen this (check if we have some simulation results)
        return False

    @property
    def event_file_path(self):
        return os.path.abspath(os.path.join(self.products_dir, self.name + '.json'))

    @property
    def parameters_file_path(self):
        return os.path.abspath(os.path.join(self.products_dir, self.name + '_params.json'))
    

    @property
    def env_dir(self):
        return os.path.abspath(os.path.join(ROOT_DIR, "mosfitenv", self.env_name))

    def gen_event_file(self):
        event_file_fname = self.event_file_path

        gen = dump.CatGen(self.ds.transient.name)

        for band in self.bands:
            lc = self.ds.get_corrected_dataset(band)
            
            self.log.info('Dumping %s-band dataset with %d entries (includes sub-datasets = %s)', band, len(lc), lc.meta['combined-tag'])
            #lc['instrument', 'telescope'].pprint(max_lines=-1, max_width=-1)

            if band in self.butcher_ds:
                for k,v in self.butcher_ds[band]:
                    lc[k] = v

            if 'min-mjd' in self.tweaks:
                lc = lc[lc['mjd'] > self.min_mjd]
            if 'max-mjd' in self.tweaks:
                lc = lc[lc['mjd'] < self.max_mjd]
            
            for row in lc:
                gen.add_photometry_point(
                    mjd=row['mjd'],
                    telescope=row['telescope'],
                    instrument=row['instrument'],
                    band=band,
                    mag=row['mag'],
                    mag_err=row['mag_err'],
                    mag_system=row['phot_zeropoint_system']
                )

        # find the distance
        dist = None
        dist_err = None
        for trans in self.ds.transformations:
            if isinstance(trans, dataset.DistanceTransform):
                dist = trans.lumdist_mean
                dist_err = trans.lumdist_std
        if dist is not None:
            gen.add_lumdist(dist, dist_err)
        

        gen.dump_json(self.event_file_path)

    def gen_params_file(self):
        # load the template file
        # TODO: make that overridable
        src_fname = os.path.join(self.env_dir, 'models', self.model_name, 'parameters.json')

        with open(src_fname, 'r') as f:
            params = json.load(f)

        # override them
        for pname, k, v in self.model_params:
            if pname not in params:
                params[pname] = {}
            params[pname][k] = v

        for pname in self.model_params_del:
            del params[pname]

        if 'lumdist-param' in self.tweaks:
            # find the distance
            dist = None
            dist_err = None
            for trans in self.ds.transformations:
                if isinstance(trans, dataset.DistanceTransform):
                    dist = trans.lumdist_mean
                    dist_err = trans.lumdist_std
            if dist is None:
                raise ValueError('Could not find distance for dataset %s' % self.ds.name)
            params['lumdist'] = {
                'class': 'gaussian',
                'mu': dist,
                'sigma': dist_err,
                'min_value': dist - 5*dist_err,
                'max_value': dist + 5*dist_err,
            }

        with open(self.parameters_file_path, 'w') as f:
            json.dump(params, f)
        


    def run_mosfit(self):
        # setup the mosfit machinery
        mf = mf_fitter.Fitter(
            # not really looking for interaction here..
            exit_on_prompt=True,
            # dont try to fetch anything from the interwebz
            offline=True,
            # dont spam us with that progress thingy?
            #quiet=True
            pool=self.pool
        )

        # now run mosfit
        mosfit_products_dir = os.path.abspath(os.path.join(self.products_dir, 'mosfit'))

        os.chdir(self.env_dir)
        
        mf.fit_events(
            # we are using our own generated event file
            events=[self.event_file_path],
            # we are just running the default model for now
            models=[self.model_name],
            # the parameters source
            parameter_paths=[self.parameters_file_path],
            # where mosfit will dump it's outputs
            output_path=mosfit_products_dir,
            # use nested sampling
            #method='nester',
            # dont download random values from the interwebz
            local_data_only=True,
            # actually write the output files....
            write=True,
            # we want a bolometric LC
            extra_outputs=['dense_times', 'dense_luminosities'],
            # save the full monty not just here and there
            #save_full_chain=True,
            # evaluate the lc at 100 additional points
            smooth_times=100,
            # extra arguments from the config file
            **self.fitter_kwargs
        )
        
        

    def run(self):
        if self.master:
            self.gen_event_file()
            self.gen_params_file()
            if self.pool is not None:
                for rank in range(1, self.pool.size + 1):
                    self.pool.comm.send(self.run_id, dest=rank, tag=0)
                self.pool.close()
        else:
            self.pool.comm.recv(source=0, tag=0)
            self.pool.wait()
        self.run_mosfit()





def get_sim(sim_name, products_dir=None, config_file=os.path.join(ROOT_DIR, 'config/mosfit.scm'), mpi=False):
    with open(config_file, 'r') as f:
        cfg = sexpdata.load(f)

    # find the relevant section of the config file
    cfg_sim = None
    for entry in cfg:
        assert len(entry) > 0
        if entry[0] == sexpdata.Symbol("sim") and entry[1] == sim_name:
            cfg_sim = entry[2:]
    if cfg_sim is None:
        raise ValueError('Simulation %s not found' % repr(sim_name))

    return Simulator(sim_name, cfg=cfg_sim, products_dir=products_dir, mpi=mpi)
