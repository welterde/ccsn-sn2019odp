import json
import numpy as np
import astropy.table as table



def load_bol_lc(fname):
    with open(fname, 'r', encoding = 'utf-8') as f:
        data = json.loads(f.read())

    dense_times = extra['dense_times']
    dense_lums = extra['dense_luminosities']

    assert len(dense_times) == len(dense_lums)

    ret = []
    for t, lums in zip(dense_times, dense_lums):
        ret.append(table.Table({'time': t, 'lums': lums}))
    return ret
        
    
def load_params(fname, model_idx=0):
    with open(fname, 'r', encoding = 'utf-8') as f:
        data = json.loads(f.read())
        if 'name' not in data:
            data = data[list(data.keys())[0]]

    photo = data['photometry']
    model = data['models'][model_idx]

    pars = [x for x in model['setup'] if model['setup'][x].get('kind') == 'parameter' and
            'min_value' in model['setup'][x] and 'max_value' in model['setup'][x]]

    t = {}
    for pname in pars:
        t[pname] = np.empty(len(model['realizations']))

    if 'weights' in model['realizations'][0]:
        t['weight'] = np.empty(len(model['realizations']))

    for i,realization in enumerate(model['realizations']):
        for p in pars:
            t[p][i] = realization['parameters'][p]['value']
        if 'weight' in t:
            t['weight'][i] = realization['weight']


    return table.Table(t)
