import astropy.units as u


# values from https://arxiv.org/pdf/2004.07244.pdf eqn 4 and eqn 5
NI56_LIFETIME = 8.76 * u.d
CO56_LIFETIME = 111.4 * u.d

# values from Arnett+2015
NI56_LIFETIME = (6.075/0.693) * u.d
CO56_LIFETIME = (77.236/0.693) * u.d
