import numpy as np
import astropy.units as u
import astropy.constants as const
import astropy.table as table
import logging

from .dataset import compute_extinction_correction


DEFAULT_LOG = logging.getLogger('speccal')



def fluxcal_spectrum(spec, ds, band, filter_wave, filter_response, phot_system='SDSS ', interpolation_samples=300, log=DEFAULT_LOG):
    # get the interpolator for that band
    interpolator = ds.get_interpolator(band)

    # we have to de-apply the extinction correction
    extinction_correction = compute_extinction_correction(ds, phot_system + band)
    log.debug('Correcting for %.2f of extinction', extinction_correction)

    # observation date of the spectrum
    obsmjd = spec.meta['obs_mjd']

    # sample the mean value at that time using the interpolator
    mag_phot_samples = np.empty(interpolation_samples)
    for i in range(interpolation_samples):
        mag_phot_samples[i] = interpolator.sample_lc(obsmjd, sample='random')[0]
    mag_phot = np.nanmean(mag_phot_samples) + extinction_correction

    log.debug('Estimated photometry magnitude: %.2f pm %.2f', mag_phot, np.nanstd(mag_phot_samples))

    # interpolate masked pixels in the observed spectrum
    # TODO: check if there is some kind of mask and include that
    spec_valid = ~np.isnan(spec['flux'])
    flux_fixed = np.interp(spec['obswave'], spec['obswave'][spec_valid], spec['flux'][spec_valid])

    # interpolate the filter response onto the observed spec grid
    filter_response_interp = np.interp(spec['obswave'], filter_wave, filter_response)

    # calculate the synthetic photometry
    # TODO: maybe extract all that into synphot module
    pivot_factor2 = np.trapz(spec['obswave']*filter_response_interp, spec['obswave'])/np.trapz(filter_response_interp/spec['obswave'], spec['obswave'])
        
    spec_flux = np.trapz(spec['obswave']*flux_fixed*filter_response_interp, spec['obswave'])/np.trapz(spec['obswave']*filter_response_interp, spec['obswave'])
        
    mag_spec = -2.5*np.log10(spec_flux) - 2.5*np.log10(pivot_factor2/(const.c.to(u.Angstrom/u.s).value)) - 48.6

    log.debug('Synthetic Photometry Magnitude: %.2f', mag_spec)

    # Correction magnitude
    dmag = (mag_spec - mag_phot)

    log.debug('Estimated correction factor: %f', dmag)

    # Flux Correction ratio
    flux_ratio = 10 ** (dmag/2.5)

    new_spec = table.Table(spec, copy=True)
    new_spec['flux'] *= flux_ratio

    return new_spec


    
