import numba
import numpy as np
import astropy.units as u
import astropy.constants as const


A_TO_CM = u.Angstrom.to(u.cm)

# in units of cm^2 * erg / s
# pre-factor is 2-pi for full surface
B_LAM_PRE_FACTOR = 2*np.pi * const.h.cgs.value * const.c.cgs.value ** 2
# exponent factor is in units K*cm
B_LAM_X_FACTOR = const.h.cgs.value / const.k_B.cgs.value * const.c.cgs.value

@numba.njit
def bb(temp_k, radius_cm, wavs_a):
    """
    Compute blackbody SED

    Arguments:
      temp_k -- temperature in Kelvin
      radius_cm -- radius in cm
      wavs_a -- wavelength grid in A
    Returns:
      erg/s/A
    """
    wavs_cm = wavs_a*A_TO_CM
    
    # cm^2 * erg / s / cm^5 = erg / s / cm^2 / cm
    B_lambda = 1 / wavs_cm**5 / (np.exp(B_LAM_X_FACTOR / wavs_cm / temp_k) - 1.0)
    
    surface_area = 4*np.pi*radius_cm**2

    # we want erg / s / A, so convert 1/cm to 1/A -> multiply by 1e-8
    # (1/u.cm).to(1/u.Angstrom) = 1e-8
    return B_LAM_PRE_FACTOR * surface_area * B_lambda * 1e-8

