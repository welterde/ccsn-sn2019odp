import pandas as pd
import numpy as np

import astropy.units as u
import astropy.constants as const
from astropy.cosmology import Planck15 as cosmology

import snelib19odp.utils as utils
import snelib19odp.interpolate as interpolate
import snelib19odp.extinction as extinction

import snelib19odp.bolometric.lyman as lyman


##################################################
# Parameters from paper Fremling (2016)

# redshift
# from sec. 3.0 p5
#distance_modulus = 32.14
#err_distance_modulus = 0.20

# my distance modulus
redshift = 0.00449
dist = cosmology.luminosity_distance(redshift).to(u.pc).value
distance_modulus = 5*np.log10(dist) - 5

redshift_dist = ('gaussian', 0.00450, 0.00007)

# extinction (sec 4)
rv = 3.1

# milkyway (S&F 2011 given as ref)
ebv_mw = 0.0437

ebv_mw_dist = ('uniform', 0.0421, 0.0448)

# host estimate
ebv_host = 0.08

ebv_host_dist = ('gaussian', 0.08, 0.07)

##################################################

raw_lc = utils.load_raw_dataset('comparison/PTF13bvn', 'raw_lc.dat', format='ascii', out_fmt='pandas+defaultidx')

# apply milkyway extinction
extinction.correct_extinction_rows(raw_lc, ebv_mw, rv=rv, filter_prefix='SDSS ', ignore_unknown=True)

# apply host extinction
extinction.correct_extinction_rows(raw_lc, ebv_host, rv=rv, filter_prefix='SDSS ', ignore_unknown=True)

# interpolate to daily cadence
lc_interp = interpolate.linear_interpolation(raw_lc, 'gr', target_cadence=0.3)

# compute the bolometric magnitude using Lyman method
lyman_pbc = lyman.compute_bol_mag(lc_interp['g_mag'], lc_interp['r_mag'], 'pbc')


sne_peak_mjd = {
    'g':  2456477.5 - 2400000.5
}

sne_explosion_mjd = 2456459.07 - 2400000.5
sne_explosion_err = 1.0


##################################################
# Outputs

bolometric_lcs = {
    'lyman_pbc': lyman_pbc
}

abs_mag_lcs = {}
for lc_name, lc in bolometric_lcs.items():
    abs_mag_lcs[lc_name] = lc - distance_modulus
