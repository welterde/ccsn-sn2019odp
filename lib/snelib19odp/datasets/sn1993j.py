import pandas as pd
import numpy as np
import json

import astropy.units as u
import astropy.constants as const
from astropy.cosmology import Planck15 as cosmology

import snelib19odp.utils as utils
import snelib19odp.interpolate as interpolate
import snelib19odp.extinction as extinction

import snelib19odp.bolometric.lyman as lyman




def load_lc():
    with open('data/comparison/SN1993J.json', 'r') as f:
        data = json.load(f)
    sne_data = data[list(data.keys())[0]]

    photometry = sne_data['photometry']
    lc_f = []
    for lc_p in photometry:
        if lc_p.get('upperlimit'):
            continue
        if 'e_magnitude' not in lc_p:
            lc_p['e_magnitude'] = 0.05
        if 'instrument' not in lc_p:
            lc_p['instrument'] = None
        lc_f.append(lc_p)
    lc = pd.DataFrame(data={
        'mjd': [float(x['time']) for x in lc_f],
        'band': [x['band'] for x in lc_f],
        'instrument': [x['instrument'] for x in lc_f],
        'mag': [x['magnitude'] for x in lc_f],
        'mag_err': [x['e_magnitude'] for x in lc_f]
    })
    return lc.set_index(['mjd', 'band'])

raw_lc = load_lc()
print(raw_lc)
r_lc = interpolate.interpolate_and_convert_johnson(raw_lc, 'V', 'B', 'mjd')

sloan_lc = interpolate.interpolate_and_convert_johnson(raw_lc, 'V', 'B', 'mjd')

ebv_mw_dist = ('uniform', 0.0687, 0.0691)
ebv_host_dist = ('uniform', 0, 0.2)

# from http://adsabs.harvard.edu/full/1988ApJ...332L..63F
distance_modulus_dist = ('gaussian', 27.59, 0.31)

phase_center = 49076.0
