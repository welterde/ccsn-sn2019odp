import numpy as np



# lyman parameters from paper

lyman_pbc_params = np.array([
    0.168, -0.407, -0.608, # c0 c1 c2
    0.074 # rms
])

lyman_bc_params = np.array([
    0.054, -0.195, -0.719, # c0 c1 c2
    0.076 # rms
])

lyman_bc_cooling_params = np.array([
    -0.146, 0.479, 2.257, # c0 c1 c2
    0.078 # rms
])

COLOR_RANGES = {
    'pbc': (-0.3, 1.0),
    'bc': (-0.3, 1.0),
    'cooling': (-0.3, 0.3)
}


# TODO: add support for other band configurations than just (g,r)
# TODO: add error estimation

def compute_bol_mag(mag_g, mag_r, method='pbc'):
    if method == 'pbc':
        c0, c1, c2, rms = lyman_pbc_params
    elif method == 'bc':
        c0, c1, c2, rms = lyman_bc_params
    elif method == 'cooling':
        c0, c1, c2, rms = lyman_bc_cooling_params
    else:
        raise ValueError('Unknown method: %s' % method)

    dmag = mag_g - mag_r

    # validate color range
    color_min = dmag.min()
    color_max = dmag.max()

    if color_min < COLOR_RANGES[method][0]:
        print('* WARN: color in lc exceeds lyman (min lc = %f < %f)' % (color_min, COLOR_RANGES[method][0]))
    if color_max > COLOR_RANGES[method][1]:
        print('* WARN: color in lc exceeds lyman (max lc = %f > %f)' % (color_max, COLOR_RANGES[method][1]))
    
    bol_correction = c0 + c1*dmag + c2*dmag ** 2
    
    return mag_g + bol_correction

