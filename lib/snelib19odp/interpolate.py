import numpy as np
import pandas as pd

import scipy.interpolate as interpolate

import astropy.table as table

def linear_interpolation(df, filters=[], target_cadence=1.0, time_key='mjd'):
    # compute start and end times of the grid
    # based on the overlap
    start_mjd = 0
    end_mjd = 1e9
    for band in filters:
        df_sel = df.xs(band, level='band')
        start_mjd = max(start_mjd, np.min(df_sel.index.get_level_values(time_key)))
        end_mjd = min(end_mjd, np.max(df_sel.index.get_level_values(time_key)))

    mjd_grid = np.linspace(start_mjd, end_mjd, int((end_mjd - start_mjd)/target_cadence)+1)

    data = [mjd_grid]
    names = [time_key]

    for band in filters:
        df_sel = df.xs(band, level='band')
        mjd = df_sel.index.get_level_values(time_key)
        mags = df_sel['mag']
        magserr = df_sel['mag_err']
        
        interpolator = interpolate.interp1d(mjd, mags)
        interpolator_err = interpolate.interp1d(mjd, magserr)

        mags_interp = interpolator(mjd_grid)
        mags_err_interp = interpolator_err(mjd_grid)+0.05

        data.append(mags_interp)
        data.append(mags_err_interp)
        names.append('%s_mag' % band)
        names.append('%s_mag_err' % band)

    # create a new data table
    t = table.Table(data, names=names)
    return t.to_pandas().set_index(time_key)


# just use this equation from SDSS:
# r      =    V - 0.19*(B-V) - 0.02
# RMS = 0.08  for quasars..
# lets just assume extra error of 0.15 RMS for now..

def interpolate_and_convert_johnson(df, v_label, b_label, time_key):
    # generate interpolated LC
    df_interp = linear_interpolation(df, [v_label, b_label], time_key=time_key)

    v_mag = df_interp['%s_mag' % v_label]
    b_mag = df_interp['%s_mag' % b_label]
    v_magerr = df_interp['%s_mag_err' % v_label]
    b_magerr = df_interp['%s_mag_err' % b_label]

    r_mag = v_mag - 0.19*(b_mag - v_mag) - 0.02
    r_magerr = np.sqrt(v_magerr**2 + (0.2*b_magerr)**2) + 0.15

    g_mag = v_mag + 0.74*(b_mag - v_mag) - 0.07
    g_magerr = np.sqrt((0.4*v_magerr)**2 + (0.8*b_magerr)**2) + 0.15

    return pd.DataFrame(data={'r_mag': r_mag, 'r_mag_err': r_magerr, 'g_mag': g_mag, 'g_mag_err': g_magerr})

