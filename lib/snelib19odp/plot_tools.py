import numpy as np

from . import extinction



def plot_spec(ax, spec, label, offset=0, blue_cut=3500, red_cut=9000, ebv_mw=0, scale=1.0, alpha=0.8, restframe=True):
    if 'filename' in spec.meta:
        print('Plotting %s (filename: %s)' % (label, spec.meta['filename']))
    if restframe:
        wavelength = spec['restwave']
    else:
        wavelength = spec['obswave']
    flux = spec['flux']
    
    # de-apply extinction
    dered_flux = extinction.spec_extinction_correct(wavelength, flux, ebv_mw)

    # TODO: do the same at host redshift if there is any further reddening there

    
    # normalize the flux
    norm_idx = np.logical_and(wavelength > 5000, wavelength < 8000)
    flux_peak = np.percentile(dered_flux[norm_idx], 95)

    flux_norm = scale*dered_flux/flux_peak

    plot_idx = np.logical_and(wavelength > blue_cut, wavelength < red_cut)
    if 'mask' in spec.colnames:
        flux_norm[~spec['mask']] = np.nan
    ax.plot(wavelength[plot_idx], flux_norm[plot_idx]+offset, label=label, alpha=alpha)
