


# Mapping of band to color if we only show a single supernova
BAND_COLOR_MAP = {
    'u': 'tab:blue',
    'g': 'tab:green',
    'r': 'tab:red',
    'i': 'tab:olive',
    'z': 'tab:pink',
    'J': 'tab:cyan',
    'H': 'tab:purple'
}
