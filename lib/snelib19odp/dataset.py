import os
import re
import numpy as np
import astropy.table as table
import sexpdata
import logging

from . import transient
from . import dataloader
from . import extinction
from .lc import interpolate
from .lc import bolometric
from . import utils
convert_dict = utils.convert_dict


BASE_DIR = os.path.join(os.path.dirname(__file__), '../..')
TRACE_BASE_DIR = os.path.join(os.path.dirname(__file__), '../../products/lc')
CFG_FILE = os.path.join(os.path.dirname(__file__), '../../config/datasets.scm')





# XXX: shouldn't those functions go in the dataloader module?

def load_ds_json(cfg):
    fname = cfg[0]
    tab = dataloader.load_comparison_json_phot_tbl(os.path.join(BASE_DIR, fname), relative_fname=False, phase0=0)
    del tab['phase']
                
    tab['phot_system_prefix'] = np.zeros(len(tab), dtype='S10')
    tab['phot_filter'] = np.zeros(len(tab), dtype='S15')
    tab['mag_uncor'] = tab['mag']
    return tab, cfg[1:]

def load_ds_table(cfg):
    fname = cfg[0]
    cfg_dict = convert_dict(cfg[1:])
    kwargs = {}
    if 'format' in cfg_dict:
        kwargs['format'] = cfg_dict['format'][0]
        del cfg_dict['format']
    if 'path' in cfg_dict:
        kwargs['path'] = cfg_dict['path'][0]
        del cfg_dict['path']
    tab = table.Table.read(os.path.join(BASE_DIR, fname), **kwargs)
    
    return tab, list(filter(lambda a: a[0].value() not in ['format', 'path'], cfg[1:]))

def load_ds_marshalcsv(cfg):
    fname = cfg[0]
    df = table.Table.read(os.path.join(BASE_DIR, fname), format='ascii')

    df['mjd'] = df['jdobs'] - 2400000.5
    del df['jdobs']

    instrument = np.empty(len(df), dtype='S8')
    telescope = np.empty(len(df), dtype='S8')
    for i in range(len(df)):
        row = df['instrument'][i].strip()
        if row == 'P48+ZTF':
            instrument[i] = 'ZTFCam'
            telescope[i] = 'P48'
        elif row == 'P60+SEDM':
            instrument[i] = 'SEDM'
            telescope[i] = 'P60'
        elif '+' in row:
            tel, ins = row.split('+', 1)
            instrument[i] = ins
            telescope[i] = tel
        else:
            telescope[i] = ''
            instrument[i] = row
    df['instrument'] = instrument
    df['telescope'] = telescope
            

    # TODO: reimplement those:
    #df['telescope'] = df['instrument'].map(GROWTH_TEL_MAP)
    #df['instrument'] = df['instrument'].map(GROWTH_INS_MAP)

    # mask upper limits for now
    idx = df['magpsf'] > 30.0
    idx = np.logical_or(idx, df['absmag'] > 30.0)
    #idx = np.logical_or(idx, df['limmag'] > 30.0)
    df['limmag'][df['limmag'] > 30.0] = np.nan

    df.rename_column('magpsf', 'mag')
    df.rename_column('sigmamagpsf', 'mag_err')
    del df['absmag']
    df.rename_column('limmag', 'mag_limiting')
    df.rename_column('filter', 'band')

    return df[~idx], cfg[1:]
    


def lookup_source_id(ds, name):
    if 'source-aliases' not in ds.meta:
        return None
    for src_id in ds.meta['source-aliases'].split(','):
        if ds.meta['source-%s-name' % src_id] == name:
            return src_id
    return None

class DSTransform(object):
    pass

class ExtinctionTransform(DSTransform):

    def __init__(self, cfg):
        self.spec = convert_dict(cfg)
        assert len(list(filter(lambda a: a.startswith('ebv:'), self.spec.keys()))) == 1

    def process(self, ds, ds_mask, mag_cor, **kwargs):
        m = np.ones(mag_cor.shape)

        if 'ebv:uniform' in self.spec:
            ebv_min = self.spec['ebv:uniform'][0]
            ebv_max = self.spec['ebv:uniform'][1]
            ebv_mean = (ebv_max+ebv_min)/2
            
            ebv_samples = np.random.uniform(ebv_min*m, ebv_max*m)
        elif 'ebv:gaussian' in self.spec:
            ebv_mean = self.spec['ebv:gaussian'][0]
            ebv_std = self.spec['ebv:gaussian'][1]
            ebv_samples = np.random.normal(ebv_mean*m, ebv_std*m)
        else:
            raise ValueError('Missing ebv spec for extinction correction')

        # extract extinction coefficients
        extinct_coeff = np.empty(mag_cor.shape)
        extinct_coeff_mean = np.empty(len(ds))
        for i in range(len(ds)):
            if not ds_mask[i]:
                continue
            extinct_coeff[i] = extinction.EXTINCTION_COEFF[ds['phot_filter'][i]]
            extinct_coeff_mean[i] = extinction.EXTINCTION_COEFF[ds['phot_filter'][i]]

        abs_extinction = extinct_coeff * ebv_samples
        mag_cor -= abs_extinction

        abs_extinction_mean = extinct_coeff_mean * ebv_mean
        if 'dmag_extinct' not in ds:
            ds['dmag_extinct'] = np.zeros(len(ds), dtype=np.float)
        ds['dmag_extinct'] -= abs_extinction_mean
        
    def get_mean_extinction(self, band):
        if 'ebv:uniform' in self.spec:
            ebv_min = self.spec['ebv:uniform'][0]
            ebv_max = self.spec['ebv:uniform'][1]
            ebv_mean = (ebv_max+ebv_min)/2
        elif 'ebv:gaussian' in self.spec:
            ebv_mean = self.spec['ebv:gaussian'][0]
        else:
            raise ValueError('Missing ebv spec for extinction correction')

        extinction_coeff =  extinction.EXTINCTION_COEFF[band]
        return extinction_coeff * ebv_mean

class DistanceTransform(DSTransform):

    def __init__(self, cfg):
        self.spec = convert_dict(cfg)
        assert len(list(filter(lambda a: a.startswith('Mpc:'), self.spec.keys()))) == 1

    def process(self, ds, mag_cor, **kwargs):
        m = np.ones(mag_cor.shape)
        
        if 'Mpc:gaussian' in self.spec:
            assert len(self.spec['Mpc:gaussian']) == 2
            mean, std = self.spec['Mpc:gaussian']
            dist_samples_pc = np.random.normal(1e6*mean*m, 1e6*std*m)
            # clamp min distance at close to 0
            dist_samples_pc[dist_samples_pc <= 0] = 1e-6
            dist_samples_mod = 5*np.log10(dist_samples_pc) - 5
            dist_mod_mean = 5*np.log10(1e6*mean) - 5
        else:
            raise ValueError('Unsupported distance spec: %s' % repr(self.spec))

        mag_cor -= dist_samples_mod

        dmag_extinct = 0
        if 'dmag_extinct' in ds:
            dmag_extinct = dmag_extinct

        assert 'absmag' not in ds
        ds['absmag'] = ds['mag'] + dmag_extinct - dist_mod_mean

    @property
    def lumdist_mean(self):
        if 'Mpc:gaussian' in self.spec:
            assert len(self.spec['Mpc:gaussian']) == 2
            mean, std = self.spec['Mpc:gaussian']
            return float(mean)
        else:
            raise ValueError('Unsupported distance spec: %s' % repr(self.spec))

    @property
    def lumdist_std(self):
        if 'Mpc:gaussian' in self.spec:
            assert len(self.spec['Mpc:gaussian']) == 2
            mean, std = self.spec['Mpc:gaussian']
            return float(std)
        else:
            raise ValueError('Unsupported distance spec: %s' % repr(self.spec))
        

class ProcessTransform(DSTransform):

    def __init__(self, cfg):
        self.rules = cfg
        for rule in self.rules:
            assert len(rule) > 0
            for subrule in rule:
                assert len(subrule) > 0
                assert isinstance(subrule[0], sexpdata.Symbol)
    
    def process(self, ds, ds_mask, **kwargs):
        matched = np.zeros(len(ds), dtype=np.bool)
        masked = np.zeros(len(ds), dtype=np.bool)
        for rule in self.rules:
            idx = np.ones(len(ds), dtype=np.bool)
            for subrule in rule:
                cmd = subrule[0].value()
                if cmd == 'match:key':
                    key = subrule[1]
                    values = subrule[2:]
                    idx_m = np.zeros(len(ds), dtype=np.bool)
                    for value in values:
                        idx_m = np.logical_or(idx_m, ds[key] == value)
                    assert key in ds.colnames
                    idx = np.logical_and(idx, idx_m)
                elif cmd == 'match:tag':
                    values = subrule[1:]
                    if ds.meta['tag'] in values:
                        idx_m = np.ones(len(ds), dtype=np.bool)
                    else:
                        idx_m = np.zeros(len(ds), dtype=np.bool)
                    idx = np.logical_and(idx, idx_m)
                elif cmd == 'match:unmatched':
                    idx = np.logical_and(idx, ~matched)
                elif cmd == 'match:mjd':
                    assert len(subrule) == 3
                    assert isinstance(subrule[1], sexpdata.Symbol)
                    if subrule[1].value() == '<':
                        idx = np.logical_and(idx, ds['mjd'] < subrule[2])
                    elif subrule[1].value() == '>':
                        idx = np.logical_and(idx, ds['mjd'] > subrule[2])
                    else:
                        raise ValueError('Unknown sub-cmd')
                elif cmd == 'match:nan':
                    assert len(subrule) > 1
                    sub_idx = np.zeros(len(ds), dtype=np.bool)
                    for key in subrule[1:]:
                        sub_idx = np.logical_or(sub_idx, np.isnan(ds[key]))
                    idx = np.logical_and(idx, sub_idx)
                elif cmd == 'match:val':
                    assert len(subrule) == 4
                    key = subrule[1]
                    op = subrule[2].value()
                    val = subrule[3]
                    if op == '<':
                        idx = np.logical_and(idx, ds[key] < val)
                    elif op == '>':
                        idx = np.logical_and(idx, ds[key] > val)
                    else:
                        raise ValueError('Unknown operator %s' % op)
                elif cmd == 'set:system_prefix':
                    ds['phot_system_prefix'][idx] = subrule[1]
                elif cmd == 'set:zeropoint-system':
                    ds['phot_zeropoint_system'][idx] = subrule[1]
                elif cmd == 'set:band':
                    ds['band'][idx] = subrule[1]
                elif cmd == 'delete':
                    masked = np.logical_or(masked, idx)
                elif cmd == 're:sub':
                    assert len(subrule) == 4
                    key = subrule[1]
                    regex = re.compile(subrule[2])
                    replacement_str = subrule[3]
                    cur_entry = None
                    
                    def replace_fun(m):
                        x = replacement_str.replace('$0', cur_entry)
                        grps = m.groups()
                        num_groups = len(grps)
                        for i in range(num_groups):
                            x = x.replace('$%s' % (i+1), grps[i])
                        return x
                    for i,entry in enumerate(ds[key]):
                        cur_entry = entry
                        new_entry = regex.sub(replace_fun, cur_entry)
                        ds[key][i] = new_entry
                elif cmd == 'modify':
                    assert len(subrule) == 4
                    key = subrule[1]
                    op = subrule[2].value()
                    val = subrule[3]
                    if op == '+':
                        ds[key][idx] += val
                    elif op == '-':
                        ds[key][idx] -= val
                    elif op == 'setmin':
                        idx2 = np.logical_and(idx, ds[key] < val)
                        ds[key][idx2] = val
                    else:
                        raise ValueError('Unknown operator %s' % op)
                    
                else:
                    raise ValueError('Unknown filter subrule command: "%s"' % cmd)
            matched = np.logical_or(matched, idx)

        # add missing photometric filters
        # TODO: vectorize this somehow
        for i in range(len(ds)):
            if masked[i]:
                continue
            if ds['phot_filter'][i] == '':
                ds['phot_filter'][i] = ds['phot_system_prefix'][i] + ds['band'][i]
        ds_mask[masked] = False
    

def transpose_table(tab, cfg):
    # new table columns
    new_tab = {
        "mjd": [],
        "band": [],
        "instrument": [],
        "telescope": [],
        "mag": [],
        "mag_err": []
    }
            
    # extract configuration
    cfg_dict = convert_dict(cfg)
    #print(cfg_dict)
    cfg_bands = cfg_dict['bands']
    cfg_columns = {}
    for x in ['mjd', 'instrument', 'telescope', 'mag', 'mag_err']:
        cfg_columns[x] = {
            'offset': None,
            'src_column': None,
            'default_value': None
        }
    cfg_columns['instrument']['default_value'] = ''
    cfg_columns['telescope']['default_value'] = ''
    for x in cfg_dict['columns']:
        cfg_columns[x[0].value()]['src_column'] = x[1]
        for y in x[2:]:
            if y[0].value() == 'offset':
                cfg_columns[x[0].value()]['offset'] = y[1]
                    
    for x in cfg_dict.get('defaults', []):
        cfg_columns[x[0].value()]['default_value'] = x[1]

    for i_band in cfg_bands:
        idx = np.ones(len(tab))
        empty_columns = []
        nonempty_columns = []
        for colname, col in cfg_columns.items():
            if col['src_column'] is not None:
                if '%s' in col['src_column']:
                    src_column = col['src_column'] % i_band
                else:
                    src_column = col['src_column']
                    
                idx = np.logical_and(idx, ~np.isnan(tab[src_column]))
                mask_column = src_column + '.mask'
                if mask_column in tab.colnames:
                    idx = np.logical_and(idx, ~tab[mask_column])
                nonempty_columns.append((colname, src_column))
            else:
                empty_columns.append(colname)

        assert len(nonempty_columns) > 0

        count = np.count_nonzero(idx)
        new_tab['band'].extend([i_band] * count)
        
        for colname in empty_columns:
            default_val = cfg_columns[colname]['default_value']
            new_tab[colname].extend([default_val]*count)

        for colname, src_column in nonempty_columns:
            offset = cfg_columns[colname]['offset']
            if offset is not None:
                new_tab[colname].extend([x + offset for x in tab[src_column][idx]])
            else:
                new_tab[colname].extend([x for x in tab[src_column][idx]])

    ret_tab = table.Table(new_tab)
    #ret_tab['phot_system_prefix'] = np.zeros(len(ret_tab), dtype='S10')
    #ret_tab['phot_filter'] = np.zeros(len(ret_tab), dtype='S15')
    #ret_tab['mag_uncor'] = ret_tab['mag']
                
    return ret_tab



def reformat_table(tab, cfg):
    # new table columns
    new_tab = {
    }

    # extract configuration
    cfg_dict = convert_dict(cfg)
    #print(cfg_dict)
    columns = convert_dict(cfg_dict['columns'])
    del cfg_dict['columns']
    # TODO: check that there is no collision between columns and the static config
    for k in cfg_dict.keys():
        assert k in ['telescope', 'instrument', 'band']

    for col_name in columns.keys():
        # parse the configuration
        col_cfg = columns[col_name]
        old_col_name = col_cfg[0]
        col_cfg = convert_dict(col_cfg[1:])

        if 'offset' in col_cfg.keys():
            offset = col_cfg.get('offset', [0.0])[0]
            new_tab[col_name] = tab[old_col_name] + offset
        else:
            new_tab[col_name] = tab[old_col_name]

    ret_tab = table.Table(new_tab)

    if not 'telescope' in ret_tab.colnames:
        ret_tab['telescope'] = np.zeros(len(ret_tab), dtype='S8')
    if not 'instrument' in ret_tab.colnames:
        ret_tab['instrument'] = np.zeros(len(ret_tab), dtype='S8')
    if not 'band' in ret_tab.colnames:
        ret_tab['band'] = np.zeros(len(ret_tab), dtype='S7')
    
    for key, val in cfg_dict.items():
        assert key not in columns.keys()
        ret_tab[key][:] = val
    #print(ret_tab)
    return ret_tab
    
        
    

class PropertyProxy(object):

    def __init__(self, ds, lc_type):
        self.ds = ds
        self.lc_type = lc_type

    
class Dataset(object):

    def __init__(self, name, cfg, sample_num=1000):
        self.log = logging.getLogger('dataset.%s' % name)

        self.name = name
        self.rev = 1
        self.sample_num = sample_num
        self.sub_datasets = {}
        self.transformations = []
        self.combined_lcs = {}
        self.processed_sub_datasets = {}
        self.primary_sub_dataset = None
        self.interpolators = {}
        self.interpolator_cfg = None
        self.bolometrics_cfg = {}
        self.bolometrics = {}
        self.phot_models = {}
        
        
        # process config
        for rule in cfg:
            assert len(rule) > 0
            assert isinstance(rule[0], sexpdata.Symbol)
            cmd = rule[0].value()

            if cmd == 'transient':
                # sets the transient we are talking about
                assert len(rule) == 2
                self.log.debug('Loading transient info for %s', rule[1])
                self.transient = transient.load_transient(rule[1])
            elif cmd.startswith('load:'):
                assert len(rule) >= 2
                load_cmd = cmd.split(':')[1]
                
                if load_cmd == 'table':
                    ds, unproc_load_rules = load_ds_table(rule[1:])
                elif load_cmd == 'json':
                    ds, unproc_load_rules = load_ds_json(rule[1:])
                elif load_cmd == 'marshalcsv':
                    ds, unproc_load_rules = load_ds_marshalcsv(rule[1:])
                else:
                    raise ValueError('Unknown loader "%s"' % load_cmd)
                
                tag = 'main'
                for subrule in unproc_load_rules:
                    assert len(subrule) > 0
                    assert isinstance(subrule[0], sexpdata.Symbol)
                    subcmd = subrule[0].value()
                    if subcmd == 'tag':
                        assert len(subrule) == 2
                        tag = subrule[1]
                    elif subcmd == 'transpose':
                        assert len(subrule) > 1
                        ds = transpose_table(ds, subrule[1:])

                    elif subcmd == 'reformat':
                        assert len(subrule) > 1
                        ds = reformat_table(ds, subrule[1:])
                        
                    elif subcmd == 'ztflc':
                        ds['mag'] = np.nan*np.empty(len(ds), dtype=np.float)
                        idx = ds['ampl'] > 0
                        ds['mag'][idx] = -2.5*np.log10(ds['ampl'][idx]) + ds['magzp'][idx]
                        ds['mag_err'] = ds['ampl.err']/ds['ampl']*1.087 + ds['magzprms']
                        ds['band'] = np.array([x[-1] for x in ds['filter']])
                        ds['mjd'] = ds['obsmjd']
                        ds['telescope'] = np.empty(len(ds), dtype='S8')
                        ds['instrument'] = np.empty(len(ds), dtype='S8')
                        ds['telescope'] = 'P48'
                        ds['instrument'] = 'ZTF'
                        ds['upper_limit'] = False
                        
                        idx = np.isnan(ds['mag'])
                        idx = np.logical_or(idx, np.isinf(ds['mag']))
                        idx = np.logical_or(idx, ds['ampl'] - 2*ds['ampl.err'] < 0)
                        # HACK: specific to SN2019odp
                        idx = np.logical_or(idx, ds['obsmjd'] < 58716)
                        #ds = ds[~idx]
                        ds['upper_limit'][idx] = True

                    elif subcmd == 'filter:source':
                        assert len(subrule) == 2
                        
                        src_id = lookup_source_id(ds, subrule[1])
                        idx = np.zeros(len(ds), dtype=np.bool)
                        for i,srces in enumerate(ds['source']):
                            if src_id in [x.strip() for x in srces.strip().split(',')]:
                                idx[i] = True
                        ds = ds[idx]
                    elif subcmd == 'filter:key':
                        assert len(subrule) >= 3
                        key = subrule[1]
                        vals = subrule[2:]
                        assert key in ds.colnames
                        
                        idx = np.zeros(len(ds), dtype=np.bool)
                        for val in vals:
                            idx = np.logical_or(idx, ds[key] == val)
                        ds = ds[idx]
                    elif subcmd == 'mark-upper-limit':
                        assert 'maglim' in ds.colnames
                        ds['upper_limit'] = ds['mag'] > ds['maglim']
                assert tag not in self.sub_datasets
                self.sub_datasets[tag] = ds
                ds.meta['tag'] = tag
                if self.primary_sub_dataset is None:
                    self.primary_sub_dataset = tag

                # Add some required columns if they are absent
                if 'phot_system_prefix' not in ds.colnames:
                    ds['phot_system_prefix'] = np.zeros(len(ds), dtype='S10')
                if 'phot_filter' not in ds.colnames:
                    ds['phot_filter'] = np.zeros(len(ds), dtype='S15')
                if 'mag_uncor' not in ds.colnames:
                    ds['mag_uncor'] = ds['mag']
                if 'phot_zeropoint_system' not in ds.colnames:
                    ds['phot_zeropoint_system'] = np.zeros(len(ds), dtype='S10')
                    ds['phot_zeropoint_system'][:] = 'AB'
                if 'upper_limit' not in ds.colnames:
                    ds['upper_limit'] = np.zeros(len(ds), dtype=np.bool)
                    
            elif cmd == 'interpolate':
                assert len(rule) > 1
                self.interpolator_cfg = rule[1:]
            elif cmd == 'extinction':
                assert len(rule) > 1
                self.transformations.append(ExtinctionTransform(rule[1:]))
            elif cmd == 'distance':
                assert len(rule) > 1
                t = DistanceTransform(rule[1:])
                self.transformations.append(t)
            elif cmd == 'process':
                assert len(rule) > 1
                self.transformations.append(ProcessTransform(rule[1:]))
            elif cmd == 'rev':
                assert len(rule) == 2
                self.rev = rule[1]
            elif cmd == 'bolometric':
                assert len(rule) > 2
                name = rule[1]
                bol_cfg = rule[2:]
                assert name not in self.bolometrics_cfg
                self.bolometrics_cfg[name] = convert_dict(bol_cfg)
                assert 'method' in self.bolometrics_cfg[name]
                
    
    @property
    def lightcurve(self):
        return PropertyProxy(self, 'combined')

    @property
    def interpolated(self):
        return PropertyProxy(self, 'interpolated')

    def prepare_interpolator_sub_dss(self, band):
        self.process_sub_datasets()

        datasets = list([self.processed_sub_datasets[self.primary_sub_dataset]]+list(map(lambda tag: self.processed_sub_datasets[tag], filter(lambda tag: tag != self.primary_sub_dataset, self.processed_sub_datasets.keys()))))
        datasets_filtered = []
        for i,ds in enumerate(datasets):
            idx = ds['band'] == band

            # for now there is no support for upper limits in the interpolation engine
            # so filter them out for now if there any in the sub dataset
            if 'upper_limit' in ds.colnames:
                idx = np.logical_and(idx, ~ds['upper_limit'])
                
            if i == 0 and np.count_nonzero(idx) == 0:
                # primary dataset should have it!
                self.log.warn('Primary dataset does not contain band %s and is thus not present or primary dataset in the interpolation', band)
                continue
                
            if np.count_nonzero(idx) > 0:
                datasets_filtered.append(ds[idx])

        return datasets_filtered

    def get_interpolator(self, band, run_fit=True):
        if band not in self.interpolators:
            datasets_filtered = self.prepare_interpolator_sub_dss(band)
            
            self.interpolators[band] = interpolate.Interpolator(self, datasets_filtered, band=band, cfg=self.interpolator_cfg)
        interpolator = self.interpolators[band]

        if not interpolator.fit_complete and interpolator.saved_fit_available:
            interpolator.load_fit_data()
        elif not interpolator.fit_complete and not run_fit:
            self.log.warn('Interpolator not fitted yet but instructed not to run fit, returning None')
            return None
        elif not interpolator.fit_complete:
            self.log.info('Interpolator not fitted yet.. Running fit!')
            interpolator.setup()
            interpolator.run_fit()
            interpolator.save_fit_data()

        return self.interpolators[band]

    def get_phot_model(self, model_name, band, run_fit=True):
        # FIXME: Heavy duplication with the get_interpolator function!!
        key = (model_name, band)
        if key not in self.phot_models:
            datasets_filtered = self.prepare_interpolator_sub_dss(band)

            self.phot_models[key] = interpolate.Interpolator(self, datasets_filtered, band=band, model_name=model_name, cfg=[])

        model = self.phot_models[key]

        if not model.fit_complete and model.saved_fit_available:
            model.load_fit_data()
        elif not model.fit_complete and not run_fit:
            self.log.warn('Photometric model not fitted yet but instructed not to run fit, returning None')
            return None
        elif not model.fit_complete:
            self.log.info('Photometric model not fitted yet.. Running fit!')
            model.setup()
            model.run_fit()
            model.save_fit_data()

        return model
    
    def get_interpolated(self, band, t_grid=None, run_fit=True, parametric=True, sample=None):
        interpolator = self.get_interpolator(band, run_fit=run_fit)

        # compute time grid if not specified
        if t_grid is None:
            # compute the completely combined dataset for random band - we are not using only that band
            self.get_combined_lc(band)
            # and use the t_min and t_max for any band (and not just $band we are looking at here)
            t_min = self.combined_ds['mjd'].min()
            t_max = self.combined_ds['mjd'].max()
            dt = t_max - t_min
            t_grid = np.linspace(t_min, t_max, int(dt*4))
        
        if parametric:
            return interpolator.get_parametric(t_grid, sample=sample)
        else:
            return interpolator.sample_lc(t_grid, sample=sample)


    def process_sub_datasets(self):
        for tag in self.sub_datasets.keys():
            if tag in self.processed_sub_datasets:
                continue
            
            # copy the input table
            ds = table.Table(self.sub_datasets[tag], copy=True)

            # setup the needed temp arrays
            mag_cor = np.zeros((len(ds), self.sample_num))
            mask = np.ones(len(ds), dtype=np.bool)

            for transform in self.transformations:
                transform.process(ds=ds, ds_mask=mask, mag_cor=mag_cor)

            ds['mag_err_correlated'] = np.nanstd(mag_cor, axis=1)

            self.processed_sub_datasets[tag] = ds[mask]

        if not hasattr(self, 'combined_ds'):
            self.combined_ds = table.vstack(list(self.processed_sub_datasets.values()), metadata_conflicts='silent')
            if len(list(self.processed_sub_datasets.values())) > 1:
                self.combined_ds.meta['tag'] = 'COMBINED'
            for band in np.unique(self.combined_ds['band']):
                idx = self.combined_ds['band'] == band
                self.combined_lcs[band] = self.combined_ds[idx]
            
    def get_sub_dataset(self, tag):
        self.process_sub_datasets()
        if tag not in self.sub_datasets.keys():
            raise ValueError("Subdataset %s not found" % tag)
        return self.processed_sub_datasets[tag]

    def get_combined_lc(self, band, include_upperlim=True):
        self.process_sub_datasets()

        idx = np.ones(len(self.combined_lcs[band]), dtype=np.bool)
        if not include_upperlim:
            idx = ~self.combined_lcs[band]['upper_limit']
        
        return self.combined_lcs[band][idx]

    def get_bolometric(self, method):
        if method not in self.bolometrics_cfg:
            raise ValueError('Non configured bolometrics method %s' % method)
        
        if method not in self.bolometrics:
            self.bolometrics[method] = bolometric.setup_bolometric(self, self.bolometrics_cfg[method], name=method)

        return self.bolometrics[method]

    def sample_abs_lc(self, method, t_grid, tweaks=set(), return_logl=False):
        # TODO: support the auto-method for the t_grid

        bolometric = self.get_bolometric(method)
        
        return bolometric.sample(t_grid, tweaks=tweaks, return_logl=return_logl)

    @property
    def first_detection_mjd(self):
        self.process_sub_datasets()
        return self.combined_ds['mjd'].min()

    def first_detection_mjd_band(self, band):
        self.process_sub_datasets()
        idx = self.combined_ds['band'] == band
        idx = np.logical_and(idx, ~self.combined_ds['upper_limit'])
        return self.combined_ds['mjd'][idx].min()

    def get_corrected_dataset(self, band, extinct_correct=True, offset_correct=True):
        self.process_sub_datasets()
        self.log.debug('Generating corrected dataset for band %s (extinct_correct=%s, offset_correct=%s)', band, extinct_correct, offset_correct)
        # generate the seperate color datasets
        # TODO: move this to a common function (but I modded it somewhat here..)
        datasets = self.processed_sub_datasets.values()
        datasets_filtered = []
        for ds in datasets:
            idx = ds['band'] == band
            self.log.debug('Dataset %s matched %d entries', ds.meta['tag'], np.count_nonzero(idx))
            if np.count_nonzero(idx) > 0:
                datasets_filtered.append(ds[idx])

        def update_ds(ds):
            new_ds = table.Table(ds, copy=True)
            if offset_correct and len(datasets_filtered) > 1:
                interp = self.get_interpolator(band)
                offset = interp.get_obs_dataset_offset(ds.meta['tag'])
                new_ds['mag'] += offset
            if 'dmag_extinct' in ds.colnames and extinct_correct:
                new_ds['mag'] += new_ds['dmag_extinct']
            return new_ds

        self.log.debug('Combining %d datasets', len(datasets_filtered))
        
        ret_ds = table.vstack(list(map(update_ds, datasets_filtered)), metadata_conflicts='silent')
        ret_ds.meta['tag'] = 'COMBINED'
        ret_ds.meta['combined-tag'] = ','.join(list(map(lambda ds: ds.meta['tag'], datasets_filtered)))
        return ret_ds
        
        


def load_dataset(name):
    # 1) load config file
    with open(CFG_FILE, 'r') as f:
        cfg = sexpdata.load(f)
    
    # 2) extract config for $name
    cfg_ds = None
    for entry in cfg:
        assert len(entry) > 0
        if entry[0] == name:
            cfg_ds = entry[1:]
    if cfg_ds is None:
        raise ValueError('Dataset %s not found' % repr(name))
    
    # 3) create and return dataset object with that config
    return Dataset(name, cfg_ds)




# utility functions

def compute_extinction_correction(ds, band):
    """ 
    Calculate the total extinction correction factor in a certain band.
    Note that this does not take into account any neccessary k-correction or anything.
    However currently does the main extinction correction module, so eh..
    """
    correction_factor = 0.0

    for transform in ds.transformations:
        if not isinstance(transform, ExtinctionTransform):
            continue
        correction_factor += transform.get_mean_extinction(band)

    return correction_factor
