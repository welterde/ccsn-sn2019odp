import numpy as np
from scipy import integrate
import pandas as pd




# GROND central wavelengths
# taken from http://svo2.cab.inta-csic.es/svo/theory//fps3/index.php?id=LaSilla/GROND.K&&mode=browse&gname=LaSilla&gname2=GROND#filter
CENTER_WAVELENGTH = {
    'g': 4586.85,
    'r': 6220.09,
    'i': 7640.69,
    'z': 8989.58,
    'J': 12399.17,
    'H': 16468.80,
    'K': 21705.48
}



def mc_flux_from_phot(phot_df, bands='', mag_key='mag_%s', magerr_key='magerr_%s', samples=200):
    center_wave = np.empty(len(bands))
    for i,band in enumerate(bands):
        center_wave[i] = CENTER_WAVELENGTH[band]
    
    def perform(row):
        # extract params from input DF
        mags_obs = np.empty(len(bands))
        mags_err = np.empty(len(bands))
        for i,band in enumerate(bands):
            mags_obs[i] = row[mag_key % band]
            mags_err[i] = row[magerr_key % band]

        mags = np.random.normal(mags_obs, mags_err, size=(samples, len(bands)))
        #flux = 10**((mags+48.60-30)/(-2.5))
        flux = 10**((mags+23.9-30)/(-2.5))
        print(flux.shape)
        
        total_flux = np.empty(samples)
        for i in range(samples):
            total_flux[i] = integrate.simps(flux[i], center_wave)

        flux = np.mean(total_flux)
        flux_err = np.std(total_flux)
        return pd.Series([flux, flux_err], index=['flux', 'flux_err'])

    phot = phot_df.apply(perform, axis=1)
    print(phot)
