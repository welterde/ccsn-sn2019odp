import astropy.table as table
import numpy as np








class Observables(object):

    def __init__(self, ds, mode='gp', samples=1000):
        self.dataset = ds
        self.mode = mode
        self.samples = samples


    def basic(self, band):
        peak_time = np.empty(self.samples, dtype=np.float)
        peak_mag = np.empty(self.samples, dtype=np.float)
        peak_lum = np.empty(self.samples, dtype=np.float)
        delta_p15 = np.empty(self.samples, dtype=np.float)
        delta_n10 = np.empty(self.samples, dtype=np.float)
        linear_slope = np.empty(self.samples, dtype=np.float)
        plateau_level = np.empty(self.samples, dtype=np.float)
        peak_width = np.empty(self.samples, dtype=np.float)
        rise_timescale = np.empty(self.samples, dtype=np.float)

        interpolator = self.dataset.get_interpolator(band)

        t_grid_peak = np.linspace(self.dataset.transient.prior_t0 - 6, self.dataset.transient.prior_t0 + 10, 1000)

        for i,s in enumerate(np.random.randint(interpolator.trace_len, size=self.samples)):
            # estimate the peak epoch
            mu = interpolator.get_parametric(t_grid_peak, sample=s)
            i_peak = np.argmin(mu)
            t_peak = t_grid_peak[i_peak]
            peak_time[i] = t_peak

            # now measure the other parameters
            if self.mode == 'gp':
                func = interpolator.sample_lc
            else:
                func = interpolator.get_parametric
            peak_mag[i] = func(t_peak, sample=s)
            # L = Lbol * 10^(4.74-Mbol)/2.5
            peak_lum[i] = 3.828e33 * 10 ** ((4.74-peak_mag[i])/2.5)
            delta_p15[i] = func(t_peak+15, sample=s) - peak_mag[i]
            delta_n10[i] = func(t_peak-10, sample=s) - peak_mag[i]
            linear_slope[i] = interpolator.get_parameter('linear_slope', sample=s)
            if 'plateau_level' in interpolator.trace_parameter_names:
                plateau_level[i] = interpolator.get_parameter('plateau_level', sample=s)
            if 'gaussian_sigma' in interpolator.trace_parameter_names:
                peak_width[i] = interpolator.get_parameter('gaussian_sigma', sample=s)
            if 'rise_timescale' in interpolator.trace_parameter_names:
                rise_timescale[i] = interpolator.get_parameter('rise_timescale', sample=s)

        
        t = table.Table((peak_time, peak_mag, peak_lum, delta_p15, delta_n10, linear_slope, plateau_level, peak_width, rise_timescale), names=('peak_time', 'peak_mag', 'peak_lum', 'delta_m15', 'delta_m-10', 'linear_slope', 'plateau_level', 'peak_width', 'rise_timescale'))
        if 'plateau_level' not in interpolator.trace_parameter_names:
            del t['plateau_level']
        if 'gaussian_sigma' not in interpolator.trace_parameter_names:
            del t['peak_width']
        if 'rise_timescale' not in interpolator.trace_parameter_names:
            del t['rise_timescale']
        return t

    def __getitem__(self, key):
        ret = np.empty(self.samples, dtype=np.float)

        
        
        assert isinstance(key, tuple)
        cmd = key[0]
        if cmd == 'Delta m15':
            pass
        elif cmd == 'Delta m-10':
            pass
        elif cmd == 'peak_mag':
            pass
        elif cmd == 'peak_time':
            pass
        
        return ret

    
    
