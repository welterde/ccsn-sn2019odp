import sexpdata
from . import interpolate


MODEL_CONFIG = [
    [sexpdata.Symbol('ds:limit-phase'), -15, 15]
]

def peak_observables(ds, band, peak_model='gaussian'):
    # get the prepared sub-dss for the interpolator
    subdss = ds.prepare_interpolator_sub_dss(band)
    
    # setup the model ("interpolator")
    model = interpolate.Interpolator(ds, subdss, band=band, model_name='gaussian', cfg=MODEL_CONFIG)
    
    # if data is available load it
    if model.saved_fit_available:
        model.load_fit_data()
    else:
        # its not.. run it!
        #self.log.info('Interpolator not fitted yet.. Running fit!')
        model.setup()
        model.run_fit()
        model.save_fit_data()

    return model

# TODO: these functions are kinda the same for all observables
def percentiles(t, param, digits):
    x = np.percentile(t['param'], [32, 50, 68])
    def f(x):
        return ('%%.%df' % digits) % x
    return {'p32': f(x[0]), 'p50': f(x[1]), 'p68': f(x[2]), 'dneg': f(x[1]-x[0]), 'dpos': f(x[2]-x[1])}
