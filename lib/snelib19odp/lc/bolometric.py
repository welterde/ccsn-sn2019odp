import numpy as np
import sexpdata


# c0, c1, c2, rms scatter
LYMAN_COEFFS_PBC = {
    ('g', 'r'): (0.168, -0.407, -0.608, 0.074),
    ('g', 'i'): (0.051, -0.511, -0.195, 0.055),
    ('B', 'V'): (0.076, -0.347, -0.620, 0.112),
    ('V', 'R'): (0.299, -0.372, -0.358, 0.087),
}

# c0, c1, c2, rms scatter
LYMAN_COEFFS_BC = {
    ('g', 'r'): (0.054, -0.195, -0.719, 0.076),
    ('B', 'V'): (-0.083, -0.139, -0.691, 0.109),
    ('V', 'R'): (-0.029, -0.302, -0.224, 0.069),
    ('V', 'Rc'): (-0.029, -0.302, -0.224, 0.069),
}




class Bolometric(object):

    def __init__(self, ds, name, cfg):
        self.ds = ds
        self.name = name
        self.cfg = cfg

        # parse config
        self.method = cfg['method'][0]
        if self.method == 'lyman':
            self.setup_lyman(cfg['method'])
        elif self.method == 'phot-integration':
            self.setup_phot_int(cfg['method'])
        else:
            raise ValueError('Unknown method %s' % method)
        

    def setup_lyman(self, cfg):
        assert len(cfg) == 4
        self.lyman_a_band = cfg[2]
        self.lyman_b_band = cfg[3]

        if cfg[1] == 'pbc':
            coeffs = LYMAN_COEFFS_PBC[(self.lyman_a_band, self.lyman_b_band)]
        elif cfg[1] == 'bc':
            coeffs = LYMAN_COEFFS_BC[(self.lyman_a_band, self.lyman_b_band)]
        else:
            raise ValueError('Unknown sub-group')

        self.lyman_c0 = coeffs[0]
        self.lyman_c1 = coeffs[1]
        self.lyman_c2 = coeffs[2]
        self.lyman_rms = coeffs[3]

    def setup_phot_int(self, cfg, return_logl):
        self.phot_int_bands = cfg[1:]
        

    def sample(self, t_grid, tweaks, return_logl=False):
        if self.method == 'lyman':
            return self.sample_lyman(t_grid, tweaks, return_logl=return_logl)
        elif self.method == 'phot-integration':
            return self.sample_phot_int(t_grid, tweaks, return_logl=return_logl)

    def sample_lyman(self, t_grid, tweaks, sample='random', return_logl=False):
        a_mag, logl_a = self.ds.get_interpolator(self.lyman_a_band).sample_lc(t_grid, sample=sample, return_logl=True)
        b_mag, logl_b = self.ds.get_interpolator(self.lyman_b_band).sample_lc(t_grid, sample=sample, return_logl=True)

        logl = logl_a + logl_b

        assert len(a_mag) == len(b_mag)

        # convert them to absolute magnitude
        a_combined = self.ds.get_combined_lc(self.lyman_a_band)
        b_combined = self.ds.get_combined_lc(self.lyman_b_band)

        a_distmod = np.nanmean(a_combined['absmag'] - a_combined['mag'])
        b_distmod = np.nanmean(b_combined['absmag'] - b_combined['mag'])

        a_absmag = a_mag + a_distmod
        b_absmag = b_mag + b_distmod

        diff_mag = a_absmag - b_absmag

        bol_cor = self.lyman_c0 + self.lyman_c1 * diff_mag + self.lyman_c2 * diff_mag * diff_mag

        ret = a_absmag + bol_cor
        #print(tweaks)
        if 'sample-lyman-scatter' in self.cfg and 'no-correlated-error' not in tweaks:
            #print('Sampling Lyman scatter')
            mag_cor_a = np.random.normal(0, self.lyman_rms)
            ret += mag_cor_a

        if 'sample-correlated' in self.cfg and 'no-correlated-error' not in tweaks:
            #print('Sampling correlated error')
            mag_cor_a = np.random.normal(0, np.nanmax(a_combined['mag_err_correlated']))
            ret += mag_cor_a

        if return_logl:
            return ret, logl
        else:
            return ret

    @property
    def validity_range(self):
        # return (t_min, t_max)
        # first and last measurement that contribute
        if self.method == 'lyman':
            a_mag_min = self.ds.get_combined_lc(self.lyman_a_band, include_upperlim=False)['mjd'].min()
            b_mag_min = self.ds.get_combined_lc(self.lyman_b_band, include_upperlim=False)['mjd'].min()
            min_mjd = max(a_mag_min, b_mag_min)

            a_mag_max = self.ds.get_combined_lc(self.lyman_a_band, include_upperlim=False)['mjd'].max()
            b_mag_max = self.ds.get_combined_lc(self.lyman_b_band, include_upperlim=False)['mjd'].max()
            max_mjd = min(a_mag_max, b_mag_max)

            return (min_mjd, max_mjd)
        else:
            raise ValueError('Not implemented for this scheme yet')
            
    
        
        



def setup_bolometric(ds, cfg, name):
    return Bolometric(ds, name=name, cfg=cfg)

