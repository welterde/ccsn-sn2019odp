import os
import time
import json
import numpy as np
import numba
import george
import h5py
import sexpdata
import logging
from george import kernels
import george.modeling as gmodel
from george.utils import multivariate_gaussian_samples
import dynesty
from dynesty import plotting as dyplot
from dynesty import utils as dyfunc
from scipy.special import erfinv
import astropy.table as table
import matplotlib
from scipy.stats import multivariate_normal
from scipy.linalg import cholesky, cho_solve


DIAG_PLOT_DIR = os.path.join(os.path.dirname(__file__), '../../../products/lc/fit_diag')

RUN_LOG_FILE = os.path.join(os.path.dirname(__file__), '../../../products/lc/fit_run.log')


def log_run(**kwargs):
    with open(RUN_LOG_FILE, 'a') as f:
        f.write(json.dumps(kwargs))
        f.write('\n')


class ModifiedContardo(gmodel.Model):
    parameter_names = ('linear_intercept', 'linear_slope', 'gaussian_amplitude', 'gaussian_t0','gaussian_sigma', 'rise_t0', 'rise_timescale', 'plateau_level')

    def get_value(self, x):
        phase = x
        
        ret = (self.linear_intercept + self.linear_slope / 1e3 * phase + self.gaussian_amplitude * np.exp(-(phase+self.gaussian_t0)**2 / 2 / self.gaussian_sigma**2))/(1-np.exp((self.rise_t0-x)/self.rise_timescale))
        idx = np.logical_and(phase < -5, ret > self.plateau_level)
        ret[idx] = self.plateau_level
        #ret[~idx] = 0
        return ret

class PlateauContardo(gmodel.Model):
    parameter_names = ('linear_intercept', 'linear_slope', 'gaussian_amplitude', 'gaussian_t0','gaussian_sigma', 'rise_t0', 'rise_timescale', 'plateau_level', 'plateau_t0', 'plateau_tau')

    def get_value(self, x):
        phase = x
        # calculate the saturation time
        #time_sat = self.rise_timescale * np.log(1-self.linear_intercept/self.plateau_level) + self.rise_t0
        #time_sat = self.rise_t0 - self.rise_timescale * np.log(1-self.linear_intercept/self.plateau_level)
        # now calculate the saturated time (fix tau = 8 for now)
        #phase_sat = time_at + (phase - time_sat) * (1 + np.arctan((phase-time_sat)/8)/np.pi - 0.5)
        plateau_func = self.plateau_level
        final_func = (self.linear_intercept + self.linear_slope / 1e3 * phase + self.gaussian_amplitude * np.exp(-(phase+self.gaussian_t0)**2 / 2 / self.gaussian_sigma**2))/(1-np.exp((self.rise_t0-phase)/self.rise_timescale))
        # fixed value for now
        #smooth_tau = 1.0
        smooth_tau = self.plateau_tau
        #plateau_t0 = -14
        plateau_t0 = self.plateau_t0
        smooth_func = np.arctan((phase-plateau_t0)/smooth_tau)/np.pi + 0.5
        ret = plateau_func + (final_func-plateau_func)*smooth_func**2
        
        #ret = (self.linear_intercept + self.linear_slope / 1e3 * phase + self.gaussian_amplitude * np.exp(-(phase+self.gaussian_t0)**2 / 2 / self.gaussian_sigma**2))/(1-np.exp((self.rise_t0-phase_sat)/self.rise_timescale))
        #ret[~idx] = 0
        return ret
    
class PreBumpContardo(gmodel.Model):
    parameter_names = ('linear_intercept', 'linear_slope', 'gaussian_amplitude', 'gaussian_t0','gaussian_sigma', 'rise_t0', 'rise_timescale', 'prebump_amplitude', 'prebump_t0', 'prebump_sigma')

    def get_value(self, x):
        phase = x
        
        ret = (self.linear_intercept + self.linear_slope / 1e3 * phase + self.gaussian_amplitude * np.exp(-(phase-self.gaussian_t0)**2 / 2 / self.gaussian_sigma**2) + self.prebump_amplitude * np.exp(-(phase-self.prebump_t0)**2 / 2 / self.prebump_sigma**2) )/(1-np.exp((self.rise_t0-x)/self.rise_timescale))
        
        return ret

class Linear(gmodel.Model):
    parameter_names = ('linear_intercept', 'linear_slope')

    def get_value(self, x):
        phase = x
        
        ret = self.linear_intercept + self.linear_slope / 1e3 * phase
        return ret


class Gaussian(gmodel.Model):
    parameter_names = ('peak_mag', 'gaussian_amplitude', 'gaussian_t0', 'gaussian_sigma')

    def get_value(self, x):
        phase = x

        return self.peak_mag + self.gaussian_amplitude * np.exp(-(phase+self.gaussian_t0)**2 / 2 / self.gaussian_sigma**2)
    
    

# rough initial value that have pretty much no effect on anything
ModContardoPInit = {
    'linear_intercept': 18,
    'linear_slope': 10,
    'gaussian_amplitude': -3,
    'gaussian_t0': 0,
    'gaussian_sigma': 20,
    'rise_t0': 0,
    'rise_timescale': 10,
    'plateau_level': 18.5,
}
PlateauContardoPInit = {
    'linear_intercept': 18,
    'linear_slope': 10,
    'gaussian_amplitude': -3,
    'gaussian_t0': 0,
    'gaussian_sigma': 20,
    'rise_t0': 0,
    'rise_timescale': 10,
    'plateau_level': 18.5,
    'plateau_t0': -20,
    'plateau_tau': 1,
}

# for both modified-contardo and plateau-contardo
ModContardoPriorDefaults = {
    'kernel_win_min': 2.0,
    'kernel_win_max': 3.0,
    'max_offset': 0.5,
    'prior_t0_range': 12,
}

PreBumpContardoPInit = {
    'linear_intercept': 18,
    'linear_slope': 10,
    'gaussian_amplitude': -3,
    'gaussian_t0': 0,
    'gaussian_sigma': 20,
    'rise_t0': 0,
    'rise_timescale': 10,
    'prebump_amplitude': 1,
    'prebump_t0': 0,
    'prebump_sigma': 10
}

PreBumpContardoPriorDefaults = {
    'kernel_win_min': 2.0,
    'kernel_win_max': 3.0,
    'max_offset': 0.5,
    'prior_t0_range': 12,
    'prebump_t0_range': 5,
    'prebump_t0_center': 'texpl'
}

LinearPInit = {
    'linear_intercept': 18,
    'linear_slope': 10,
}

LinearPriorDefaults = {
    'kernel_win_min': 2.0,
    'kernel_win_max': 3.0,
    'max_offset': 0.5
    
}

GaussianPInit = {
    'gaussian_amplitude': -3,
    'gaussian_t0': 0,
    'gaussian_sigma': 20,
    'peak_mag': 10
}

GaussianPriorDefaults = {
    'kernel_win_min': 2.0,
    'kernel_win_max': 3.0,
    'max_offset': 0.5,
    'prior_t0_range': 5
}

# prior function for modified-contardo
@numba.njit
def mc_prior_transform(u, prior_t_min, num_ds_offset, max_offset, kernel_win_min, kernel_win_max, prior_t0_range):
    p = np.empty(len(u))
    # linear_intercept
    p[0] = 20*u[0]+10
    # linear_slope
    p[1] = 100*u[1]
    # gaussian_amplitude
    p[2] = 0 - 10*u[2]
    # gaussian_t0
    p[3] = prior_t0_range*u[3] - prior_t0_range/2
    # gaussian_sigma
    p[4] = 45*u[4] + 5
    # rise_t0
    p[5] = prior_t_min - 40*u[5]
    # rise_timescale
    p[6] = 20*u[6]+1
    # plateau level
    #p[7] = 6*u[7]+17
    # reparameterized based on linear-intercept.. has to be fainter!!
    # Otherwise they might overlap and if intercept is fainter the plateau-contardo model will produce lots of RuntimeWarnings during
    # the run since log(1- m_intercept/m_plateau) is invalid
    p[7] = p[0] + 0.01 + 6*u[7]
    
    # kernel params.. always last!
    # kernel amplitude
    p[8] = 9*u[8]-10
    # kernel scale
    #p[9] = 1.0*u[9]+2.4
    p[9] = (kernel_win_max-kernel_win_min)*u[9]+kernel_win_min

    for i in range(num_ds_offset):
        p[10+i] = 2*max_offset*u[10+i]-max_offset
        # gaussian
        #p[9+i] = np.sqrt(2)*max_offset*erfinv(2*u[9+i]-1)
    
    
    return p

# prior function for plateau-contardo
@numba.njit
def pc_prior_transform(u, prior_t_min, num_ds_offset, max_offset, kernel_win_min, kernel_win_max, prior_t0_range):
    p = np.empty(len(u))
    # linear_intercept
    p[0] = 20*u[0]+10
    # linear_slope
    p[1] = 100*u[1]
    # gaussian_amplitude
    p[2] = 0 - 10*u[2]
    # gaussian_t0
    p[3] = prior_t0_range*u[3] - prior_t0_range/2
    # gaussian_sigma
    p[4] = 45*u[4] + 5
    # rise_t0
    p[5] = prior_t_min - 40*u[5]
    # rise_timescale
    p[6] = 20*u[6]+1
    # plateau level
    #p[7] = 6*u[7]+17
    # reparameterized based on linear-intercept.. has to be fainter!!
    # Otherwise they might overlap and if intercept is fainter the plateau-contardo model will produce lots of RuntimeWarnings during
    # the run since log(1- m_intercept/m_plateau) is invalid
    p[7] = p[0] + 0.01 + 6*u[7]

    # plateau t0
    p[8] = -30*u[8]

    # plateau tau
    p[9] = 32*u[9]
    
    # kernel params.. always last!
    # kernel amplitude
    p[10] = 9*u[10]-10
    # kernel scale
    #p[9] = 1.0*u[9]+2.4
    p[11] = (kernel_win_max-kernel_win_min)*u[11]+kernel_win_min

    for i in range(num_ds_offset):
        p[12+i] = 2*max_offset*u[12+i]-max_offset
        # gaussian
        #p[9+i] = np.sqrt(2)*max_offset*erfinv(2*u[9+i]-1)
    
    
    return p


# prior function for pre-bump-contardo
@numba.njit
def pbc_prior_transform(u, prior_t_min, num_ds_offset, max_offset, kernel_win_min, kernel_win_max, prior_t0_range, prebump_t0_range, prebump_t0_center):
    p = np.empty(len(u))
    # linear_intercept
    p[0] = 20*u[0]+10
    # linear_slope
    p[1] = 100*u[1]
    # gaussian_amplitude
    p[2] = 0 - 10*u[2]
    # gaussian_t0
    p[3] = prior_t0_range*u[3] - prior_t0_range/2
    # gaussian_sigma
    p[4] = 45*u[4] + 5
    # rise_t0
    p[5] = prior_t_min - 40*u[5]
    # rise_timescale
    p[6] = 20*u[6]+1
    # prebump amplitude
    p[7] = 0 - 10*u[7]
    # prebump t0
    p[8] = prebump_t0_center + prebump_t0_range*u[8] - prebump_t0_range/2
    # prebump sigma
    p[9] = 10*u[9]+1

    
    # kernel params.. always last!
    # kernel amplitude
    p[10] = 9*u[10]-10
    # kernel scale
    #p[9] = 1.0*u[9]+2.4
    p[11] = (kernel_win_max-kernel_win_min)*u[11]+kernel_win_min

    for i in range(num_ds_offset):
        p[12+i] = 2*max_offset*u[12+i]-max_offset
        # gaussian
        #p[9+i] = np.sqrt(2)*max_offset*erfinv(2*u[9+i]-1)
    
    
    return p


@numba.njit
def linear_prior_transform(u, prior_t0_range, prior_t_min, num_ds_offset, max_offset, kernel_win_min, kernel_win_max):
    p = np.empty(len(u))
    # linear_intercept
    p[0] = 20*u[0]+10
    # linear_slope
    p[1] = 100*u[1]

    # kernel params.. always last!
    # kernel amplitude
    p[2] = 9*u[2]-10
    # kernel scale
    #p[9] = 1.0*u[9]+2.4
    p[3] = (kernel_win_max-kernel_win_min)*u[3]+kernel_win_min

    for i in range(num_ds_offset):
        p[4+i] = 2*max_offset*u[4+i]-max_offset
        # gaussian
        #p[9+i] = np.sqrt(2)*max_offset*erfinv(2*u[9+i]-1)
    
    
    return p

@numba.njit
def gaussian_prior_transform(u, prior_t0_range, prior_t_min, num_ds_offset, max_offset, kernel_win_min, kernel_win_max):
    p = np.empty(len(u))
    # peak mag
    p[0] = 10*u[0] + 10
    # gaussian_amplitude
    p[1] = 0 - 10*u[1]
    # gaussian_t0
    p[2] = prior_t0_range*u[2] - prior_t0_range/2
    # gaussian_sigma
    p[3] = 45*u[3] + 5

    # kernel params.. always last!
    # kernel amplitude
    p[4] = 9*u[4]-10
    # kernel scale
    p[5] = (kernel_win_max-kernel_win_min)*u[5]+kernel_win_min

    for i in range(num_ds_offset):
        p[6+i] = 2*max_offset*u[6+i]-max_offset
        # gaussian
        #p[9+i] = np.sqrt(2)*max_offset*erfinv(2*u[9+i]-1)
    
    
    return p


def sample_model(ds, prior_t0=0.0, mag_column='mag', magerr_column='mag_err', time_column='mjd'):
    t = ds[time_column]
    y = ds[mag_column]
    yerr = ds[magerr_column]

    gp = george.GP(np.var(y) * kernels.Matern32Kernel(30.0), mean=ModifiedContardo(**ModContardoPInit))
    gp.compute(t, yerr)

    def lnprob2(p):
        gp.set_parameter_vector(p)
        return gp.log_likelihood(y, quiet=True)

    ndim = len(gp.get_parameter_vector())
    sampler = dynesty.DynamicNestedSampler(lnprob2, mc_prior_transform, ndim, ptform_args=(prior_t0, t.min(), 1, 0.0))
    sampler.run_nested()

    return gp, sampler.results


def sample_model_multi_ds(dss, prior_t0=0.0, mag_column='mag', magerr_column='mag_err', time_column='mjd', max_ds_offset=1.0):
    
    ts = []
    ys = []
    yerrs = []
    srcdims = []
    for i,ds in enumerate(dss):
        t = ds[time_column]
        ts.append(t)
        y = ds[mag_column]
        ys.append(y)
        yerr = ds[magerr_column]
        yerrs.append(yerr)
        srcdims.extend([i-1]*len(t))

        assert np.count_nonzero(np.logical_or(np.isnan(y), np.isinf(y))) == 0
        assert np.count_nonzero(np.logical_or(np.isnan(yerr), np.isinf(yerr))) == 0

    # create combined arrays
    t = np.concatenate(ts, axis=None)
    y = np.concatenate(ys, axis=None)
    yerr = np.concatenate(yerrs, axis=None)
        
    # construct the offset matrix
    offset_matrix = np.zeros((len(t), len(dss)-1))
    for i in range(len(t)):
        # skip offsets for the master dataset (the offset is relative to that dataset..)
        if srcdims[i] != -1:
            offset_matrix[i,srcdims[i]] = 1

    
    

    gp = george.GP(np.var(y) * kernels.Matern32Kernel(30.0), mean=ModifiedContardo(**ModContardoPInit))
    gp.compute(t, yerr)
    model_dims = len(gp.get_parameter_vector())
            
    def lnprob2(p):
        master_offsets = p[model_dims:]
        offsets = offset_matrix.dot(master_offsets)
        gp.set_parameter_vector(p[0:model_dims])
        return gp.log_likelihood(y+offsets, quiet=True)
        
    num_ds = len(dss)

    ndim = len(gp.get_parameter_vector())+num_ds-1
    assert ndim == (num_ds+model_dims-1)
    sampler = dynesty.DynamicNestedSampler(lnprob2, mc_prior_transform, ndim, ptform_args=(prior_t0, t.min(), num_ds, max_ds_offset))
    sampler.run_nested()

    return gp, sampler.results, (t, y, yerr), offset_matrix
    


class Interpolator(object):

    def __init__(self, ds, sub_dss, band, cfg, model_name='modified-contardo'):
        self.ds = ds
        self.sub_datasets = sub_dss
        self.sub_dataset_tags = list(map(lambda ds: ds.meta['tag'], self.sub_datasets))
        self.band = band
        self.cfg = cfg
        self.loaded = False
        # default config
        self.extinction_correct = True
        self.ds_without_offset = []
        self.model_name = model_name

        self.log = logging.getLogger('lc.interpolator')
        
        #self.max_ds_offset = 0.5
        self.run_id = int(time.time())
        

        # get the prior for the phase t0
        self.prior_t0 = self.ds.transient.prior_t0

        # limit the fitting to the specified phase
        self.phase_limit = (-9999, 9999)

        # setup the prior kwargs
        self.prior_kwargs = {}

        self.process_config()

        self.log.info(f"Setup for DS={self.ds.name} (transient={self.ds.transient.name} band={band}) model={self.model_name} run-id={self.run_id} prior-t0={self.prior_t0:.0f} num-datasets={len(sub_dss)}")

    def process_config(self):
        if self.cfg is None:
            return
        
        for rule in self.cfg:
            assert isinstance(rule[0], sexpdata.Symbol)
            cmd = rule[0].value()
            if cmd == 'only':
                assert len(rule) > 2
                test_expr = rule[1]
                assert len(test_expr) > 0
                assert isinstance(test_expr[0], sexpdata.Symbol)
                test_cmd = test_expr[0].value()
                sub_rules = rule[2:]
                matched = False
                if test_cmd == 'band?':
                    if self.band in test_expr[1:]:
                        matched = True
                else:
                    raise ValueError('Unknown test expression %s' % test_cmd)
                if matched:
                    for sub_rule in sub_rules:
                        self.process_config_rule(sub_rule)
            else:
                self.process_config_rule(rule)

    def process_config_rule(self, rule):
        assert isinstance(rule[0], sexpdata.Symbol)
        cmd = rule[0].value()
        if cmd == 'prior:param':
            assert len(rule) == 3
            key = rule[1]
            val = rule[2]
            self.prior_kwargs[key] = val
        elif cmd == 'extinction-correction':
            assert len(rule) == 2
            if isinstance(rule[1], bool):
                self.extinction_correct = rule[1]
            elif isinstance(rule[1], sexpdata.Symbol):
                v = rule[1].value
                if v == 'yes' or v == 'true':
                    self.extinction_correct = True
                elif v == 'no' or v == 'false':
                    self.extinction_correct = False
                else:
                    raise ValueError('Unknown bool value %s (use true/false,yes/no,t/f)' % v)
            else:
                raise ValueError('Unknown bool value %s (use true/false,yes/no,t/f)' % v)
        elif cmd == 'ds:no-offset':
            assert len(rule) == 2
            assert isinstance(rule[1], str)
            self.ds_without_offset.append(rule[1])
        elif cmd == 'model':
            assert len(rule) == 2
            assert rule[1] in ['modified-contardo', 'linear', 'prebump-contardo', 'plateau-contardo']
            self.model_name = rule[1]
        elif cmd == 'ds:limit-phase':
            assert len(rule) == 3
            assert (isinstance(rule[1], int) or isinstance(rule[1], float))
            assert (isinstance(rule[2], int) or isinstance(rule[2], float))
            self.phase_limit = (rule[1], rule[2])
        else:
            raise ValueError('Unknown rule command %s' % cmd)
        

    def setup(self):
        assert not self.loaded
        assert not self.fit_complete

        ts = []
        ys = []
        yerrs = []
        srcdims = []
        src_dss = []
        num_ds_offset = 0
        self.tag2idx = {}
        self.active_sub_ds = set()
        idx_ctr = 0
        for i,ds in enumerate(self.sub_datasets):
            # transform the time to phase relative to the adopted t0
            t = ds['mjd'] - self.prior_t0
            t_idx = np.logical_and(t > self.phase_limit[0], t < self.phase_limit[1])
            if i == 0:
                # primary dataset needs to be present after cuts!
                assert np.count_nonzero(t_idx) != 0
            if np.count_nonzero(t_idx) == 0:
                self.log.debug('Skipping dataset %s because it contains no values in the phase-limit', ds.meta['tag'])
                continue
            ts.append(t[t_idx])
            if 'dmag_extinct' in ds.colnames and self.extinction_correct:
                y = ds['mag'] + ds['dmag_extinct']
            else:
                y = ds['mag']
            ys.append(y[t_idx])
            yerr = ds['mag_err']
            yerrs.append(yerr[t_idx])
            if ds.meta['tag'] not in self.ds_without_offset and i != 0:
                num_ds_offset += 1
                srcdims.extend([idx_ctr]*len(t[t_idx]))
                self.tag2idx[ds.meta['tag']] = idx_ctr
                idx_ctr += 1
            else:
                srcdims.extend([-1]*len(t[t_idx]))
            src_dss.append([ds.meta['tag']] * len(t[t_idx]))
            self.active_sub_ds.add(ds.meta['tag'])

            assert np.count_nonzero(np.logical_or(np.isnan(y[t_idx]), np.isinf(y[t_idx]))) == 0
            assert np.count_nonzero(np.logical_or(np.isnan(yerr[t_idx]), np.isinf(yerr[t_idx]))) == 0

        # make sure we have at least one dataset that contains observations here
        assert len(ts) > 0

        # create combined arrays
        self.t = np.concatenate(ts, axis=None)
        self.y = np.concatenate(ys, axis=None)
        self.yerr = np.concatenate(yerrs, axis=None)
        self.src_ds = np.concatenate(src_dss, axis=None)
        self.src_ds_idx = np.concatenate(srcdims, axis=None)
        self.num_ds_offset = num_ds_offset
        
        # construct the offset matrix
        self.offset_matrix = np.zeros((len(self.t), num_ds_offset))
        for i in range(len(self.t)):
            # skip offsets for the master dataset (the offset is relative to that dataset..)
            if srcdims[i] != -1:
                self.offset_matrix[i,srcdims[i]] = 1

        self.setup_gp()

    def setup_gp(self):
        if self.model_name == 'modified-contardo':
            self.gp = george.GP(np.var(self.y) * kernels.Matern32Kernel(30.0), mean=ModifiedContardo(**ModContardoPInit))
            self.gp_class = ModifiedContardo
            self.prior_kwargs_default = ModContardoPriorDefaults
            self.prior_transform = mc_prior_transform
        elif self.model_name == 'plateau-contardo':
            self.gp = george.GP(np.var(self.y) * kernels.Matern32Kernel(30.0), mean=PlateauContardo(**PlateauContardoPInit))
            self.gp_class = PlateauContardo
            self.prior_kwargs_default = ModContardoPriorDefaults
            self.prior_transform = pc_prior_transform
        elif self.model_name == 'linear':
            self.gp = george.GP(np.var(self.y) * kernels.Matern32Kernel(30.0), mean=Linear(**LinearPInit))
            self.gp_class = Linear
            self.prior_kwargs_default = LinearPriorDefaults
            self.prior_transform = linear_prior_transform
        elif self.model_name == 'prebump-contardo':
            self.gp = george.GP(np.var(self.y) * kernels.Matern32Kernel(30.0), mean=PreBumpContardo(**PreBumpContardoPInit))
            self.gp_class = PreBumpContardo
            self.prior_kwargs_default = PreBumpContardoPriorDefaults
            self.prior_transform = pbc_prior_transform
        elif self.model_name == 'gaussian':
            self.gp = george.GP(np.var(self.y) * kernels.Matern32Kernel(30.0), mean=Gaussian(**GaussianPInit))
            self.gp_class = Gaussian
            self.prior_kwargs_default = GaussianPriorDefaults
            self.prior_transform = gaussian_prior_transform
        else:
            raise ValueError('Unknown interpolation model %s' % self.model_name)
        self.gp.compute(self.t, self.yerr)

    def run_fit(self):
        t0 = time.time()
        model_dims = len(self.gp.get_parameter_vector())
            
        def lnprob2(p):
            master_offsets = p[model_dims:]
            offsets = self.offset_matrix.dot(master_offsets)
            self.gp.set_parameter_vector(p[0:model_dims])
            return self.gp.log_likelihood(self.y+offsets, quiet=True)
        
        num_ds = len(self.sub_datasets)
        
        ndim = len(self.gp.get_parameter_vector())+self.num_ds_offset

        # setup prior kwargs
        prior_kwargs = self.prior_kwargs_default.copy()
        prior_kwargs.update(self.prior_kwargs)
        prior_kwargs['num_ds_offset'] = self.num_ds_offset
        prior_kwargs['prior_t_min'] = self.t.min()
        # prior_args (old) : (self.prior_t0, self.t.min(), num_ds, self.max_ds_offset)
        
        # special handling for prebump-contardo:prebump_t0_center
        # if the value is set to texpl (default) then replace it with the texpl prior (as phase)
        if prior_kwargs.get('prebump_t0_center', None) == 'texpl':
            prior_kwargs['prebump_t0_center'] = self.ds.transient.prior_texpl - self.ds.transient.prior_t0
        
        self.sampler = dynesty.DynamicNestedSampler(lnprob2, self.prior_transform, ndim, ptform_kwargs=prior_kwargs, method='unif')
        self.sampler.run_nested()
        results = self.sampler.results

        # resample the trace to be equal weight
        samples, weights = results.samples, np.exp(results.logwt - results.logz[-1])
        self.trace = dyfunc.resample_equal(samples, weights)

        self.mean_trace, self.cov_matrix = dyfunc.mean_and_cov(samples, weights)

        t1 = time.time()
        self.log.debug('Fit complete. Took %.0f seconds' % (t1-t0))
        log_run(ds_name=self.ds.name, ds_rev=self.ds.rev, band=self.band, num_sub_ds=len(self.sub_datasets), run_id=self.run_id, duration=(t1-t0), logz=results.logz[-1], model_name=self.model_name, run_completed=t1)

        self.make_fit_diagnostics()

    def make_fit_diagnostics(self):
        if not hasattr(self, 'sampler'):
            self.log.warning('Not actually generating fit diagnostics since we just loaded the fit!')
            return
        results = self.sampler.results

        matplotlib.use('Agg')
        import matplotlib.pyplot as plt

        plot_dir = os.path.join(DIAG_PLOT_DIR, f'{self.ds.name}_{self.ds.rev}_{self.model_name}')
        if not os.path.isdir(plot_dir):
            os.makedirs(plot_dir)

        # corner plot
        labels = self.trace_parameter_names
        fig, ax = dyplot.cornerplot(results, show_titles=True, labels=labels)
        corner_fname = os.path.join(plot_dir, f'{self.ds.name}_{self.ds.rev}_{self.band}_N{len(self.sub_datasets)}_{self.run_id}_corner.png')
        fig.savefig(corner_fname)

        # lightcurve overlay
        fig, ax = plt.subplots(figsize=(11,9))
        lc_cor = self.get_corrected_obs_dataset(sample=None)
        for subds in self.sub_datasets:
            label = 'Dataset = %s' % subds.meta['tag']
            idx = self.src_ds == subds.meta['tag']
            y = lc_cor['mag'][idx]
            yerr = lc_cor['mag_err'][idx]
            t = lc_cor['mjd'][idx] - self.prior_t0
            ax.errorbar(t, y, yerr=yerr, ls='', fmt='.', label=label)

        t_grid = np.linspace(self.t.min(), self.t.max(), 750)

        lc = self.get_parametric(t_grid + self.prior_t0)
        ax.plot(t_grid, lc, color='red', alpha=0.3, label='Parametric Model Mean')
        
        ax.legend()
        for sidx in np.random.randint(len(self.trace), size=200):
            lc = self.sample_lc(t_grid+self.prior_t0, sample=sidx)
            ax.plot(t_grid, lc, color="#4682b4", alpha=0.05)
            lc = self.get_parametric(t_grid+self.prior_t0, sample=sidx)
            ax.plot(t_grid, lc, color="green", alpha=0.02)
        
        lc_fname = os.path.join(plot_dir, f'{self.ds.name}_{self.ds.rev}_{self.band}_N{len(self.sub_datasets)}_{self.run_id}_lc.png')
        ax.invert_yaxis()
        ax.set_xlabel('Phase [d]')
        ax.set_ylabel('Apparent Magnitude')
        fig.savefig(lc_fname)

        # residual plot
        fig, ax = plt.subplots(figsize=(11,9))
        ax.axhline(0, ls='--', color='black', alpha=0.5)

        for j,sidx in enumerate(np.random.randint(len(self.trace), size=30)):
            ycor = self.get_corrected_obs_dataset(sample=sidx)
            for i, (subds, color) in enumerate(zip(self.sub_datasets, ['tab:blue', 'tab:red', 'tab:pink', 'tab:purple', 'tab:orange'])):
                idx = self.src_ds == subds.meta['tag']
                if np.count_nonzero(idx) == 0:
                    # TODO: spit warning
                    continue
                
                label = 'Dataset = %s' % subds.meta['tag']
                
                lc = self.sample_lc(self.t[idx]+self.prior_t0, sample=sidx)
                phase = self.t[idx]
                diff = ycor['mag'][idx] - lc
                yerr = ycor['mag_err'][idx]
                if j == 0:
                    ax.errorbar(phase, diff, yerr=yerr, color=color, alpha=0.1, ls='', fmt='.', capsize=5, label=label)
                else:
                    ax.errorbar(phase, diff, yerr=yerr, color=color, alpha=0.1, ls='', fmt='.', capsize=5)

        residual_fname = os.path.join(plot_dir, f'{self.ds.name}_{self.ds.rev}_{self.band}_N{len(self.sub_datasets)}_{self.run_id}_res.png')
        ax.legend()
        #ax.invert_yaxis()
        ax.set_xlabel('Phase [d]')
        ax.set_ylabel('Residual Magnitude')
        fig.savefig(residual_fname)
        

    def load_fit_data(self, fname=None):
        if fname is None:
            fname = self.get_file_name()

        f = h5py.File(fname, 'r')
        
        # load the _c_ombined _d_ata_s_et
        self.combined_dataset = f['/input/combined']
        self.t = self.combined_dataset['t']
        self.y = self.combined_dataset['y']
        self.yerr = self.combined_dataset['yerr']
        self.src_ds = self.combined_dataset['src_ds']

        # load the offset matrix
        self.offset_matrix = np.array(f['/input/offset_matrix'], copy=True)

        # load the offset index mapping
        self.tag2idx = {}
        for row in f['/input/tag2offset_idx']:
            self.tag2idx[row['tag']] = row['idx']

        # load the trace object
        self.trace = f['/fit/trace']

        self.mean_trace = f['/fit/mean']
        self.cov_matrix = f['/fit/cov_matrix']

        self.fit_parameters_loaded = list(map(lambda a: a.decode('utf-8'), f['/fit/parameters']))
        #print(self.fit_parameters_loaded)

        #f.close()
        self.loaded = True
        
        self.setup_gp()

    def save_fit_data(self, fname=None, overwrite=False):
        if fname is None:
            fname = self.get_file_name()

        if not self.fit_complete:
            raise ValueError('Cannot fit data that does not exist. Run fit first!')

        if os.path.exists(fname) and not overwrite:
            raise ValueError('Trace file already exists')

        # truncate if it exists
        f = h5py.File(fname, 'w')

        # write the combined dataset
        cds_dt = np.dtype({'names': ['t', 'y', 'yerr', 'src_ds'], 'formats': ((['f8']*3)+['S20'])})
        self.combined_dataset = np.rec.fromarrays([self.t, self.y, self.yerr, self.src_ds], dtype=cds_dt)
        f.create_dataset('/input/combined', data=self.combined_dataset)

        # write the offset matrix
        f.create_dataset('/input/offset_matrix', data=self.offset_matrix)

        # write mapping of ds-tag to offset matrix index if any
        offset_idx_dt = np.dtype({'names': ['tag', 'idx'], 'formats': ['S20', 'i4']})
        offset_idx_tags = list(self.tag2idx.keys())
        offset_idx_idx = list(map(lambda tag: self.tag2idx[tag], offset_idx_tags))
        offset_idx = np.rec.fromarrays([offset_idx_tags, offset_idx_idx], dtype=offset_idx_dt)
        f.create_dataset('/input/tag2offset_idx', data=offset_idx)

        # write the trace object
        f.create_dataset('/fit/trace', data=self.trace)

        # write the parameter names from the fit (even though they are not loaded?)
        f.create_dataset('/fit/parameters', data=np.array(list(map(lambda a: a.encode('utf-8'), self.trace_parameter_names))))

        f.create_dataset('/fit/mean', data=self.mean_trace)
        f.create_dataset('/fit/cov_matrix', data=self.cov_matrix)

        

        f.close()
        

    def verify(self):
        """ Verify that the cached combined dataset matches the provided one """
        if not self.loaded:
            raise ValueError('No fit result has been loaded')

        # TODO: check that the order of parameters matches the loaded one
        

    def get_file_name(self):
        fname = f'{self.ds.name}_{self.ds.rev}_{self.band}_{self.model_name}_N{len(self.sub_datasets)}_trace.h5'
        dirname = os.path.join(os.path.dirname(__file__), '../../../products/lc/fit')
        if not os.path.isdir(dirname):
            os.makedirs(dirname)
        return os.path.join(dirname, fname)

    def get_offset_trace(self, ds_tag):
        if self.sub_dataset_tags.index(ds_tag) == 0:
            return 0
        # TODO: get trace somehow
        
    @property
    def fit_complete(self):
        return hasattr(self, 'trace')

    @property
    def saved_fit_available(self):
        fname = self.get_file_name()
        return os.path.exists(fname)

    @property
    def trace_parameter_names(self):
        # if we have loaded it from a file use the parameters from that trace!
        if hasattr(self, 'fit_parameters_loaded'):
            return self.fit_parameters_loaded
        return list(self.gp_class.parameter_names)+['K Amp', 'K Width'] + list(map(lambda a: 'Offset %s' % a.meta['tag'], filter(lambda a: (a.meta['tag'] not in self.ds_without_offset) and (a.meta['tag'] in self.active_sub_ds), self.sub_datasets[1:])))

    @property
    def trace_len(self):
        return len(self.trace)

    def get_parametric(self, t_grid, sample=None):
        if sample is None:
            s = self.mean_trace
        else:
            s = self.trace[sample]
        self.gp.set_parameter_vector(s[0:len(self.gp.get_parameter_vector())])
        return self.gp.mean.get_value(t_grid-self.prior_t0)

    def sample_lc(self, t_grid, sample=None, return_logl=False):
        if sample is None:
            s = self.mean_trace
        elif sample == 'random':
            s = self.trace[np.random.randint(len(self.trace))]
        else:
            s = self.trace[sample]
        self.gp.set_parameter_vector(s[0:len(self.gp.get_parameter_vector())])

        master_offsets = s[len(self.gp.get_parameter_vector()):]
        offsets = self.offset_matrix.dot(master_offsets)

        # we are doing what sample_conditional used to do, but we do it ourself so we can get some log likelihood in here!
        #sampled_lc = self.gp.sample_conditional(self.y+offsets, t_grid-self.prior_t0)
        mu, cov = self.gp.predict(self.y+offsets, t_grid-self.prior_t0, return_cov=True)
        sampled_lc = multivariate_gaussian_samples(cov, N=1, mean=mu)
        
        if return_logl:
            #return sampled_lc, self.gp.log_likelihood(sampled_lc, quiet=True)
            #return sampled_lc, self.gp.log_likelihood(self.y+offsets, quiet=False)
            y = sampled_lc - mu
            chov = (cholesky(cov, overwrite_a=True, lower=False), False)
            cov_cho = cho_solve(chov, y)
            logl = np.dot(y.T, cov_cho)
            return sampled_lc, -0.5*logl#multivariate_normal.logpdf(x=sampled_lc, mean=mu, cov=cov, allow_singular=True)
        else:
            return sampled_lc
            
    def get_parameter(self, name, sample=None):
        if sample is None:
            s = self.mean_trace
        else:
            s = self.trace[sample]
        d = dict(zip(self.trace_parameter_names, s))
        return d[name]

    @property
    def parameter_trace_table(self):
        # TODO: check if fit is run and error if not
        data = {}
        for i, param_name in enumerate(self.trace_parameter_names):
            data[param_name] = self.trace[:,i]

        return table.Table(data)

    def get_corrected_obs_dataset(self, sample=None):
        if sample is None:
            s = self.mean_trace
        elif sample == 'random':
            s = self.trace[np.random.randint(len(self.trace))]
        else:
            s = self.trace[sample]
        self.gp.set_parameter_vector(s[0:len(self.gp.get_parameter_vector())])

        master_offsets = s[len(self.gp.get_parameter_vector()):]
        offsets = self.offset_matrix.dot(master_offsets)

        return table.Table({'mjd': self.t+self.prior_t0, 'mag': (self.y+offsets), 'mag_err': self.yerr, 'phase': self.t})

    def get_obs_dataset_offset(self, tag, sample=None):
        if sample is None:
            s = self.mean_trace
        elif sample == 'random':
            s = self.trace[np.random.randint(len(self.trace))]
        else:
            s = self.trace[sample]
        self.gp.set_parameter_vector(s[0:len(self.gp.get_parameter_vector())])

        master_offsets = s[len(self.gp.get_parameter_vector()):]

        if self.tag2idx.get(tag, None) is None:
            return 0
        return master_offsets[self.tag2idx[tag]]
