import numpy as np
import numba
import astropy.units as u



MSOL_TO_GRAM = u.solMass.to(u.g)













@numba.njit
def pior_transform(u, prior_t0, prior_t0_range):
    x = np.empty(len(u))
    # Mni [Msol]
    x[0] = u[0]
    # T0
    x[1] = 100*u[1]
    # t0
    x[2] = prior_t0 + prior_t0_range*(2*u[2] - 1)
    # sample selection
    x[3] = u[3]

    return x


@numba.njit(inline='always')
def modelfunc(p):
    m_ni = p[0] * MSOL_TO_GRAM
    
