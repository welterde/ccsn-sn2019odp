import collections
import numpy as np
import numba
import collections
from scipy.integrate import quad
import astropy.table as table
from scipy.optimize import curve_fit, differential_evolution
import tqdm
import dynesty

from . import utils



SamplerWrapper = collections.namedtuple('SamplerWrapper', 'sampler pregen_times pregen_lums pregen_logls')



@numba.njit
def gen_times(num, valid_mask, t_min, t_max):
    """
    Utility function to generate times that fall into discrete validity windows
    """
    ret = np.empty(num, dtype=np.float64)
    for i in range(num):
        valid = False
        t = 0.0
        while not valid:
            t = np.random.uniform(t_min, t_max)
            for row in valid_mask:
                if t > row[0] and t < row[1]:
                    valid = True
                    break
        ret[i] = t
    return ret



# t_mjd = use t in MJD format
def likelihood_t_mjd(x, modfunc, pregen_times, pregen_lums, pregen_logs):
    # select random entry from pre-generated LCs
    idx = np.random.randint(0, pregen_times.shape[0])
    t_grid = pregen_times[idx]
    lums = 2.5*pregen_lums[idx]
    logls = pregen_logs[idx]
    
    model = 2.5*modfunc(t_grid, *x)

    x = logls - 0.5 * np.sum((lums - model)**2)
    if np.isnan(x):
        #return -np.inf
        x = -100 * np.nansum((lums - model)**2)
    if np.isnan(x):
        return -np.inf
    return x


def nested_fitting(ds, model_func, likelihood_func, prior_func, ndim, time_range, prior_kwargs={}, likelihood_kwargs={}, num_pregen=12000, num_times=50, bolometric_method='lyman', time_valid=np.array([[1, 1e12]]), quiet=False, disable_lc_logl=True, sampler='nested', pool=None):
    print(f"Time Range: {time_range}")
    t_min, t_max = time_range

    # TODO: outsource this into utility function
    # pre-generate bolometric lightcurves
    # TODO: cache this somehow
    pregen_t = np.empty((num_pregen, num_times), dtype=np.float)
    pregen_lums = np.empty((num_pregen, num_times), dtype=np.float)
    pregen_logs = np.empty(num_pregen, dtype=np.float)
    for i in tqdm.trange(num_pregen, disable=quiet):
        success = False
        while not success:
            try:
                #t_grid = np.sort(np.random.uniform(t_min, t_max, M))
                t_grid = gen_times(num_times, time_valid, t_min, t_max)
                lc, lc_logl = ds.sample_abs_lc(method=bolometric_method, t_grid=t_grid, tweaks=['correlated-error'], return_logl=True)
            except np.linalg.LinAlgError:
                continue
            lums = np.log(utils.convert_luminosity(lc))
            #lums = utils.convert_luminosity(lc)/1e42
            pregen_lums[i] = lums
            pregen_t[i] = t_grid
            pregen_logs[i] = lc_logl
            if not np.isnan(lc_logl):
                success = True

    # subtract the peak log likelihood
    #print('Max logl: %e / 50%% percentile: %e' % (np.nanmax(pregen_logs), np.percentile(pregen_logs, 50)))
    #pregen_logs -= np.percentile(pregen_logs, 95)
    if disable_lc_logl:
        pregen_logs[:] = 0
    

    logl_kwargs = {
        'pregen_times': pregen_t,
        'pregen_lums': pregen_lums,
        'pregen_logs': pregen_logs,
        'modfunc': np.vectorize(model_func)
    }
    logl_kwargs.update(likelihood_kwargs)

    queue_size = 1
    if pool is not None:
        queue_size = 64
    #
    if sampler == 'nested':
        sampler = dynesty.NestedSampler(likelihood_func, prior_func, ndim, ptform_kwargs=prior_kwargs, logl_kwargs=logl_kwargs, method='rwalk', nlive=2000, bound='multi', pool=pool, use_pool={'prior_transform': False}, queue_size=queue_size, walks=50, enlarge=1.75)
    elif sampler == 'dynamic_nested':
        sampler = dynesty.DynamicNestedSampler(likelihood_func, prior_func, ndim, ptform_kwargs=prior_kwargs, logl_kwargs=logl_kwargs, method='rwalk', nlive_init=4000, pool=pool, use_pool={'prior_transform': False}, queue_size=queue_size, update_interval=3.0, walks=50)
    else:
        raise ValueError('Unknown sampler %s' % repr(sampler))
    return SamplerWrapper(sampler=sampler, pregen_times=pregen_t, pregen_lums=pregen_lums, pregen_logls=pregen_logs)
