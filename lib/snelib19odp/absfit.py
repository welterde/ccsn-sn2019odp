import dynesty, os, collections, numba, logging, functools, time
from dynesty import plotting as dyplot
from dynesty import utils as dyfunc
from dynesty.dynamicsampler import stopping_function
import numpy as np
import astropy.table as table
from multiprocessing import Pool
import h5py




class AbsFitResult(object):

    def __init__(self, trace, mean, parameter_names, setup, meta=None):
        self.trace = trace
        self.mean = mean
        self.parameter_names = parameter_names
        self.setup = setup
        if meta is None:
            meta = {}
        self.meta = meta

    @staticmethod
    def load(fname):
        f = h5py.File(fname, 'r')

        parameters = [x.decode('utf-8') for x in f['/fit/parameters']]
        trace = f['/fit/trace']
        setup = AbsFitSetup.load(f['/fit/setup'])
        mean = f['/fit/mean']

        meta = {}
        for k in f['/fit'].attrs:
            meta[k] = f['/fit'].attrs[k]

        return AbsFitResult(trace=trace, mean=mean, parameter_names=parameters, setup=setup, meta=meta)

    def save(self, fname, overwrite=False):
        if os.path.exists(fname) and not overwrite:
            raise ValueError('Fitresult file already exists')

        # truncate if it exists
        f = h5py.File(fname, 'w')

        # save all the meta attributes
        grp = f.create_group('/fit')
        for k in self.meta.keys():
            grp.attrs[k] = self.meta[k]

        # write the parameter names from the fit (even though they are not loaded?)
        f.create_dataset('/fit/parameters', data=np.array(list(map(lambda a: a.encode('utf-8'), self.parameter_names))))
        
        f.create_dataset('/fit/mean', data=self.mean)

        # write the used setup
        f.create_dataset('/fit/setup', data=self.setup.dump())

        # write the trace
        f.create_dataset('/fit/trace', data=self.trace, compression='gzip', shuffle=True)

        f.close()
        
        



class AbsFitSetup(collections.namedtuple('AbsFitSetup', 'wlen_min wlen_max fit_wlen_min fit_wlen_max line_center scalefactor pcyg')):

    @staticmethod
    def load(arr):
        return AbsFitSetup(arr['wlen_min'][0], arr['wlen_max'][0], arr['fit_wlen_min'][0], arr['fit_wlen_max'][0], arr['line_center'][0], arr['scalefactor'][0], arr['pcyg'][0] > 0)
    
    def dump(self):
        if self.pcyg:
            pcyg = 1
        else:
            pcyg = 0
        vals = (self.wlen_min, self.wlen_max, self.fit_wlen_min, self.fit_wlen_max, self.line_center, self.scalefactor, pcyg)
        names = ['wlen_min', 'wlen_max', 'fit_wlen_min',  'fit_wlen_max', 'line_center', 'scalefactor', 'pcyg']
        dt = ['f4', 'f4', 'f4', 'f4', 'f4', 'f8', 'i4']
        rec_dt = np.dtype({'names': names, 'formats': dt})
        return np.rec.array([vals], dtype=rec_dt)

    def __str__(self):
        return f"AbsFitSetup<wlen_min={self.wlen_min},wlen_max={self.wlen_max},fit_wlen_min={self.fit_wlen_min},fit_wlen_max={self.fit_wlen_max},line_center={self.line_center},scalefactor={self.scalefactor:.2E},pcyg={self.pcyg}>"



@numba.njit
def prior_transform(u, wlen_min, wlen_max, pcyg, min_continuum, max_continuum):
    wlen_range = wlen_max - wlen_min
    max_slope = (max_continuum-min_continuum)/wlen_range
    max_amplitude = (max_continuum-min_continuum)*wlen_range/2
    
    x = np.empty_like(u)
    #x[0] = 250*u[0] + 1 # continuum
    x[0] = (max_continuum-min_continuum)*u[0] + min_continuum
    x[1] = max_slope*(1*u[1]-0.5) # slope
    #x[2] = 30e3*u[2] # amplitude
    x[2] = np.log(max_amplitude) - 17*u[2] # log amplitude
    x[3] = 40*u[3] + 20 # sigma
    x[4] = (wlen_max-wlen_min)*u[4]+wlen_min # center position
    x[5] = 20*u[5]-10 # ln f

    if pcyg:
        # log amplitude
        x[6] = np.log(max_amplitude) - 17*u[6]
        # center position relative to absorption minimum
        x[7] = 200*u[7]+60
        # sigma
        x[8] = 40*u[8]+20
    
    return x

@numba.njit(inline='always')
def modelfunc(p, wave, wave_center):
    continuum, slope, log_amplitude, sigma, center = p
    amplitude = np.exp(log_amplitude)
    dwlen = wave - wave_center
    model = continuum + slope*dwlen - amplitude/sigma/np.sqrt(2*np.pi)*np.exp(-(wave - center)**2/2/sigma**2)
    return model

@numba.njit(inline='always')
def modelfunc_pcyg(p, wave, wave_center):
    continuum, slope, log_amplitude, sigma, center, _ln_f, log_amplitude_em, center_em_offset, sigma_em = p
    amplitude = np.exp(log_amplitude)
    amplitude_em = np.exp(log_amplitude_em)
    dwlen = wave - wave_center
    model = continuum + slope*dwlen - amplitude/sigma/np.sqrt(2*np.pi)*np.exp(-(wave - center)**2/2/sigma**2) + amplitude_em/sigma_em/np.sqrt(2*np.pi)*np.exp(-(wave - (center+center_em_offset))**2/2/sigma_em**2) 
    return model

@numba.njit
def likelihood(p, wave, obs_flux, wave_center, pcyg, scale_factor):
    lnf = p[5]

    if pcyg:
        model = modelfunc_pcyg(p, wave, wave_center)
    else:
        model = modelfunc(p[0:5], wave, wave_center)
    sigma2 = (scale_factor*model)**2 * np.exp(2*lnf)
    
    #return -0.5 * np.nansum((sed - obs_spec)**2)
    ##return -0.5 * np.nansum(np.abs(model - obs_spec))
    return -0.5 * np.sum((obs_flux*scale_factor - model*scale_factor) ** 2 / sigma2 + np.log(sigma2))

PARAMETER_NAMES = ['continuum', 'slope', 'amplitude', 'sigma', 'center', 'ln f']
PARAMETER_NAMES_PCYG = ['continuum', 'slope', 'amplitude', 'sigma', 'center', 'ln f', 'amplitude_em', 'center_offset_em', 'sigma_em']

def do_nothing(**kwargs):
    pass

def stop_function(results, args=None, rstate=None, M=None, return_vals=False):
    ret = stopping_function(results, args, rstate, M, return_vals)
    
    if time.time() > args['timelimit']:
        if return_vals:
            return True, ret[1]
        else:
            return True
    else:
        return ret

def nested_fit(spec, setup, plot_fig=do_nothing, smp=1, timelimit=None, nlive=8000, dlogz_init=0.1, enlarge=3, method='unif', walks=50, update_interval=None):
    log = logging.getLogger('absfit')
    
    wave = spec['restwave']
    flux = spec['flux']#*setup.scalefactor#1e17
    
    #print(flux)
    idx = np.logical_and(wave > setup.wlen_min, wave < setup.wlen_max)
    # XXX: why are we still seeing nan values at this point?
    idx = np.logical_and(idx, ~np.isnan(flux))

    min_continuum = np.nanpercentile(flux[idx], 1)
    max_continuum = np.nanpercentile(flux[idx], 99)
    
    wlen_center = np.mean(wave[idx])
    logl_args = (wave[idx], flux[idx], wlen_center, setup.pcyg, setup.scalefactor)
    prior_args = (setup.fit_wlen_min, setup.fit_wlen_max, setup.pcyg, min_continuum, max_continuum)
    print(prior_args)

    log.info('Running absfit with smp=%d, setup=%s, selected_spex=%d, nlive=%d dlogz-init=%.2f enlarge=%d method=%s walks=%d update-interval=%s', smp, setup, np.count_nonzero(idx), nlive, dlogz_init, enlarge, method, walks, repr(update_interval))
    log.debug('Flux: %s', flux[idx])


    meta = {}
    
    pool = None
    queue_size = None
    if smp > 1:
        pool = Pool(smp)
        queue_size = smp
        meta['run_smp'] = smp

    if setup.pcyg:
        ndim = 9
        parameter_names = PARAMETER_NAMES_PCYG
    else:
        ndim = 6
        parameter_names = PARAMETER_NAMES
    
    sampler = dynesty.DynamicNestedSampler(likelihood, prior_transform, ndim, logl_args=logl_args, ptform_args=prior_args, nlive=nlive, pool=pool, queue_size=queue_size, method=method, enlarge=enlarge, walks=walks, update_interval=update_interval)
    meta['ndim'] = ndim
    meta['nlive'] = nlive

    # setup the stopping function
    n_effective = max(sampler.npdim * sampler.npdim, 10000)
    if timelimit is None:
        # default to 1 week
        end_time = time.time() + 604800
    else:
        end_time = time.time() + timelimit
    stop_kwargs = {
        'target_neff': n_effective,
        'timelimit': end_time
    }
    

    sampler.run_nested(stop_function=stop_function, maxcall_init=999000, maxcall_batch=999000, stop_kwargs=stop_kwargs, dlogz_init=dlogz_init)
    results = sampler.results

    meta['logz'] = results.logz[-1]
    samples, weights = results.samples, np.exp(results.logwt - results.logz[-1])
    mean, cov = dyfunc.mean_and_cov(samples, weights)
    log.debug('Mean: %s', repr(mean))
    log.debug('Cov: %s', repr(cov))

    new_samples = dyfunc.resample_equal(samples, weights)
    
    quantiles = {}
    for i, label in enumerate(parameter_names):
        q = np.quantile(new_samples[:,i], [0.025, 0.5, 0.975])
        quantiles[label] = q.tolist()
    
    plot_fig(functools.partial(dyplot.cornerplot, results, labels=parameter_names, show_titles=True), 'corner', autocreates_fig=True)
    #plot_fig(functools.partial(dyplot.runplot, results), 'run')
    plot_fig(functools.partial(dyplot.traceplot, results, labels=parameter_names, show_titles=True), 'trace', autocreates_fig=True)

    def plot_spec(fig):
        ax = fig.subplots(1)
        ax.plot(wave[idx], flux[idx], label='Observed')
        wave_grid = np.linspace(wave[idx].min(), wave[idx].max(), 1000)
        if setup.pcyg:
            sed_mean = modelfunc_pcyg(mean, wave_grid, wlen_center)
        else:
            sed_mean = modelfunc(mean[0:5], wave_grid, wlen_center)
        ax.plot(wave_grid, sed_mean, label='Fit (mean)')

        # mark the center bounds
        ax.axvline(setup.fit_wlen_min, color='green')
        ax.axvline(setup.line_center, color='orange')
        ax.axvline(setup.fit_wlen_max, color='green')
        
        for i in range(50):
            i = np.random.randint(10, new_samples.shape[0])
            s = new_samples[i]

            if setup.pcyg:
                sed = modelfunc_pcyg(s, wave_grid, wlen_center)
            else:
                sed = modelfunc(s[0:5], wave_grid, wlen_center)
            ax.plot(wave_grid, sed, alpha=0.1, color='grey')
        ax.legend()
        ax.set_xlabel('Wavelength [A]')
        ax.set_ylabel('Flux')
    plot_fig(plot_spec, 'comparison')

    return AbsFitResult(trace=new_samples, mean=mean, parameter_names=parameter_names, setup=setup, meta=meta)
