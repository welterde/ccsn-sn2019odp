import os
import numpy as np
import sexpdata
import astropy.table as table

from . import dataloader
from . import extinction



def convert_dict(spec):
    ret = {}
    for entry in spec:
        key = entry[0].value()
        if len(entry) == 1:
            val = None
        else:
            val = entry[1:]
        ret[key] = val
    return ret


class Processor(object):

    def __init__(self, rule_file, base_dir, samples=1000):
        self.rules = {}

        self.sample_num = samples
        self.base_dir = base_dir
        
        with open(rule_file, 'r') as f:
            rules = sexpdata.load(f)
            for entry in rules:
                dataset_name = entry[0]
                dataset_rules = entry[1:]
                assert dataset_name not in self.rules
                self.rules[dataset_name] = dataset_rules

    def process_filter_rules(self, tab, rules):
        matched = np.zeros(len(tab), dtype=np.bool)
        masked = np.zeros(len(tab), dtype=np.bool)
        for rule in rules:
            idx = np.ones(len(tab), dtype=np.bool)
            for subrule in rule:
                cmd = subrule[0].value()
                if cmd == 'match:key':
                    key = subrule[1]
                    values = subrule[2:]
                    idx_m = np.zeros(len(tab), dtype=np.bool)
                    for value in values:
                        idx_m = np.logical_or(idx_m, tab[key] == value)
                    assert key in tab.colnames
                    idx = np.logical_and(idx, idx_m)
                elif cmd == 'match:unmatched':
                    idx = np.logical_and(idx, ~matched)
                elif cmd == 'set:system_prefix':
                    tab['phot_system_prefix'][idx] = subrule[1]
                elif cmd == 'delete':
                    masked = np.logical_or(masked, idx)
                else:
                    raise ValueError('Unknown filter subrule command: "%s"' % cmd)
            matched = np.logical_or(matched, idx)

        # add missing photometric filters
        # TODO: vectorize this somehow
        for i in range(len(tab)):
            if tab['phot_filter'][i] == '':
                tab['phot_filter'][i] = tab['phot_system_prefix'][i] + tab['band'][i]
        #print(tab[~masked])
        return tab[~masked]

    def process_extinction_correction(self, tab, mag_cor, spec):
        spec = convert_dict(spec)
        assert len(list(filter(lambda a: a.startswith('ebv:'), spec.keys()))) == 1
        m = np.ones(mag_cor.shape)
        
        if 'ebv:uniform' in spec:
            ebv_min = spec['ebv:uniform'][0]
            ebv_max = spec['ebv:uniform'][1]
            ebv_mean = (ebv_max+ebv_min)/2
            
            ebv_samples = np.random.uniform(ebv_min*m, ebv_max*m)
        elif 'ebv:gaussian' in spec:
            ebv_mean = spec['ebv:gaussian'][0]
            ebv_std = spec['ebv:gaussian'][1]
            ebv_samples = np.random.normal(ebv_mean*m, ebv_std*m)
        else:
            raise ValueError('Missing ebv spec for extinction correction')

        # extract extinction coefficients
        extinct_coeff = np.empty(mag_cor.shape)
        extinct_coeff_mean = np.empty(len(tab))
        for i in range(len(tab)):
            extinct_coeff[i] = extinction.EXTINCTION_COEFF[tab['phot_filter'][i]]
            extinct_coeff_mean[i] = extinction.EXTINCTION_COEFF[tab['phot_filter'][i]]

        abs_extinction = extinct_coeff * ebv_samples
        mag_cor -= abs_extinction

        abs_extinction_mean = extinct_coeff_mean * ebv_mean
        tab['mag'] -= abs_extinction_mean


    def process_distance(self, tab, mag_cor, subrules):
        assert len(subrules) == 1
        cmd = subrules[0][0]
        m = np.ones(mag_cor.shape)
        if cmd == sexpdata.Symbol('Mpc:gaussian'):
            assert len(subrules[0]) == 3
            dist_samples_pc = np.random.normal(1e6*subrules[0][1]*m, 1e6*subrules[0][2]*m)
            dist_samples_mod = 5*np.log10(dist_samples_pc) - 5
            dist_mod_mean = 5*np.log10(1e6*subrules[0][1]) - 5
        else:
            raise ValueError('Unsupported distance spec: %s' % repr(subrules))

        mag_cor -= dist_samples_mod

        tab['mag'] -= dist_mod_mean


    def process_table_transpose(self, tab, cfg):
        # new table columns
        new_tab = {
            "mjd": [],
            "band": [],
            "instrument": [],
            "telescope": [],
            "mag": [],
            "mag_err": []
        }
            
        # extract configuration
        cfg_dict = dict([(x[0].value(), x[1:]) for x in cfg])
        #print(cfg_dict)
        cfg_bands = cfg_dict['bands']
        cfg_columns = {}
        for x in ['mjd', 'instrument', 'telescope', 'mag', 'mag_err']:
            cfg_columns[x] = {
                'offset': None,
                'src_column': None,
                'default_value': None
            }
        cfg_columns['instrument']['default_value'] = ''
        cfg_columns['telescope']['default_value'] = ''
        for x in cfg_dict['columns']:
            cfg_columns[x[0].value()]['src_column'] = x[1]
            for y in x[2:]:
                if y[0].value() == 'offset':
                    cfg_columns[x[0].value()]['offset'] = y[1]
                    
                

        for i_band in cfg_bands:
            idx = np.ones(len(tab))
            empty_columns = []
            nonempty_columns = []
            for colname, col in cfg_columns.items():
                if col['src_column'] is not None:
                    if '%s' in col['src_column']:
                        src_column = col['src_column'] % i_band
                    else:
                        src_column = col['src_column']
                    
                    idx = np.logical_and(idx, ~np.isnan(tab[src_column]))
                    mask_column = src_column + '.mask'
                    if mask_column in tab.colnames:
                        idx = np.logical_and(idx, ~tab[mask_column])
                    nonempty_columns.append((colname, src_column))
                else:
                    empty_columns.append(colname)

            assert len(nonempty_columns) > 0

            count = np.count_nonzero(idx)
            new_tab['band'].extend([i_band] * count)

            for colname in empty_columns:
                default_val = cfg_columns[colname]['default_value']
                new_tab[colname].extend([default_val]*count)

            for colname, src_column in nonempty_columns:
                offset = cfg_columns[colname]['offset']
                if offset is not None:
                    new_tab[colname].extend([x + offset for x in tab[src_column][idx]])
                else:
                    new_tab[colname].extend([x for x in tab[src_column][idx]])

        return table.Table(new_tab)
            
    


    def finalize_table(self, tab, mag_cor):
        tab['mag_err_correlated'] = np.std(mag_cor, axis=1)
        # TODO: add percentiles
        return tab
                
    def load(self, name):
        tab = None
        mag_cor = None
        phase0 = None
        for rule in self.rules[name]:
            assert len(rule) > 0
            cmd = rule[0]
            if cmd == sexpdata.Symbol('load:json'):
                fname = rule[1]
                tab = dataloader.load_comparison_json_phot_tbl(os.path.join(self.base_dir, fname))
                
                tab['phot_system_prefix'] = np.zeros(len(tab), dtype='S10')
                tab['phot_filter'] = np.zeros(len(tab), dtype='S15')
                tab['mag_uncor'] = tab['mag']
            elif cmd == sexpdata.Symbol('load:table'):
                fname = rule[1]
                if len(rule) == 2:
                    tab = table.Table.read(fname)
                elif len(rule) == 3:
                    tab = table.Table.read(fname, format=rule[2])
                else:
                    raise ValueError('Usage: (load:table "fname" ["format"])')
                phase0 = dataloader.get_comparison_phase0(os.path.basename(fname))
                
            elif cmd == sexpdata.Symbol('filters'):
                # filter setup
                tab = self.process_filter_rules(tab, rule[1:])
                mag_cor = np.zeros((len(tab), self.sample_num))
                
            elif cmd == sexpdata.Symbol('extinction'):
                # perform extinction correction
                self.process_extinction_correction(tab, mag_cor, rule[1:])
            elif cmd == sexpdata.Symbol('distance'):
                # distance setting
                self.process_distance(tab, mag_cor, rule[1:])
            elif cmd == sexpdata.Symbol('table:transpose'):
                tab = self.process_table_transpose(tab, rule[1:])
                tab['phot_system_prefix'] = np.zeros(len(tab), dtype='S10')
                tab['phot_filter'] = np.zeros(len(tab), dtype='S15')
                tab['mag_uncor'] = tab['mag']
                if phase0 is not None and 'phase' not in tab:
                    tab['phase'] = tab['mjd'] - phase0
                
        return self.finalize_table(tab, mag_cor)


BASE_DIR = os.path.join(os.path.dirname(__file__), '../..')
RULE_FILE = os.path.join(os.path.dirname(__file__), '../../const/comparison_process.scm')
    
COMPARISON_PROCESSOR = Processor(rule_file=RULE_FILE, base_dir=BASE_DIR)
