import numpy as np
import pandas as pd

import astropy.units as u
from astropy.cosmology import Planck15 as cosmology

from snelib19odp.extinction import EXTINCTION_COEFF




def sample(spec, samples):
    if spec[0] == 'gaussian':
        return np.random.normal(spec[1], spec[2], size=samples)
    elif spec[0] == 'pos_gaussian':
        return np.abs(np.random.normal(spec[1], spec[2], size=samples))
    elif spec[0] == 'uniform':
        return np.random.uniform(spec[1], spec[2], size=samples)
    else:
        raise ValueError('Unknown distribution spec %s' % spec[0])

    


# TODO: make Rv configurable for Rv
def sample_abs_peak(lc, lc_band, redshift, mw_extinction, host_extinction, samples=1000, dist_modulus=None):
    # find the peak of the lightcurve
    idx_min = lc['mag'].idxmin()

    # sample apparent peak mag from LC
    peak_mag = sample(('gaussian', lc.loc[idx_min, 'mag'], lc.loc[idx_min, 'mag_err']), samples)

    # calc distance modulus
    if redshift:
        redshift_dist = sample(redshift, samples)
        dist = cosmology.luminosity_distance(redshift_dist).to(u.pc).value
        dist_mod = 5*np.log10(dist) - 5
    elif dist_modulus:
        dist_mod = sample(dist_modulus, samples)
    else:
        raise ValueError('Need either redshift of distance modulus')

    peak_mag -= dist_mod

    if mw_extinction is not None:
        extinction = sample(mw_extinction, samples)*EXTINCTION_COEFF[lc_band]
        peak_mag -= extinction

    # TODO: investigate effect of redshift on this
    if host_extinction is not None:
        extinction = sample(host_extinction, samples)*EXTINCTION_COEFF[lc_band]
        peak_mag -= extinction

    return peak_mag

