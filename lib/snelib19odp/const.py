import shlex, os
import astropy.table as table

const_dir = os.path.join(os.path.dirname(__file__), '../../const')

# load explosion epoch constant
with open(os.path.join(const_dir, 'explosion_epoch/current'), 'r') as f:
    content = f.read()
    parts = shlex.split(content)
    explosion_epoch_mjd, explosion_epoch_mjd_err = float(parts[0]), float(parts[1])

sne_peak_mjd = {}
sne_peak_mjd_err = {}
with open(os.path.join(const_dir, 'peak_epoch/current'), 'r') as f:
    for line in f.readlines():
        parts = shlex.split(line)
        if len(parts) == 3:
            band, mjd, mjd_err = parts
            sne_peak_mjd[band] = float(mjd)
            sne_peak_mjd_err[band] = float(mjd_err)

redshift_tbl = table.Table.read(os.path.join(const_dir, 'distance/current'), format='ascii')
redshift = redshift_tbl['redshift'][0]
redshift_err = redshift_tbl['err_redshift'][0]
