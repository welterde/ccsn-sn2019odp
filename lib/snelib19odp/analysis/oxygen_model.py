from . import specmodel
from . import oxygen



class OxygenLineModel(specmodel.MultiRegionSpectralModel):
    """
    Fit the 5577 and 63xx lines, but without physical model
    """

    def __init__(self, spec, distance_prior, max_line_width, max_amplitude, max_continuum, **kwargs):
        

        # prior params
        self.distance_prior = distance_prior
        self.max_line_width = max_line_width
        self.max_amplitude = max_amplitude
        self.max_continuum = max_continuum
        
        # setup the rest
        super().__init__(spec=spec, **kwargs)
        self.add_param('distance')
        self.add_param('line_width')
        self.add_param('lum5577')
        self.add_param('continuum5577')
        self.add_param('lum6300')
        self.add_param('lum6364')
        self.add_param('continuum63xx')

    def get_spectral_regions(self):
        func5577, func63xx = self.make_model_functions()
        
        region_5577 = specmodel.SpectralRegion(min_wave=self.min_5577, max_wave=self.max_5577, func=func5577)
        region_63xx = specmodel.SpectralRegion(min_wave=self.min_63xx, max_wave=self.max_63xx, func=func63xx)

        return [region_5577, region_63xx]

    def get_transform_func(self):
        @numba.njit
        def trans_func(p):
            return p

        return trans_func

    def make_prior_func(self):
        distance_min, distance_max = self.distance_prior
        max_line_width = self.max_line_width
        max_amplitude = self.max_amplitude
        max_continuum = self.max_continuum
        
        prev_prior_func, ndim = super().make_prior_func()

        distance_idx = self.params['distance']
        line_width_idx = self.params['line_width']
        lum5577_idx = self.params['lum5577']
        continuum5577_idx = self.params['continuum5577']
        lum6300_idx = self.params['lum6300']
        lum6364_idx = self.params['lum6364']
        continuum63xx_idx = self.params['continuum63xx']
        
        @numba.njit(inline='always')
        def prior_func(u):
            # populate the likelihood specific params
            p = prev_prior_func(u)

            # distance [cm]
            p[distance_idx] = (distance_max-distance_min)*u[distance_idx] + distance_min

            # line width [A]
            p[line_width_idx] = np.exp(np.log(max_line_width)*u[line_width_idx])

            # luminosities
            p[lum5577_idx] = np.exp(np.log(max_amplitude)*u[lum5577_idx])
            p[lum6300_idx] = np.exp(np.log(max_amplitude)*u[lum6300_idx])
            p[lum6364_idx] = np.exp(np.log(max_amplitude)*u[lum6364_idx])

            # continuum fluxes
            p[continuum5577_idx] = 2*max_continuum*u[continuum5577_idx]-max_continuum
            p[continuum63xx_idx] = 2*max_continuum*u[continuum63xx_idx]-max_continuum
            
            return p

        return prior_func, ndim+7
            

    def make_model_functions(self):
        spec_wave = self.fit_spec_wave

        distance_idx = self.params['distance']
        line_width_idx = self.params['line_width']
        lum5577_idx = self.params['lum5577']
        continuum5577_idx = self.params['continuum5577']
        lum6300_idx = self.params['lum6300']
        lum6364_idx = self.params['lum6364']
        continuum63xx_idx = self.params['continuum63xx']
        
        @numba.njit
        def func5577(p, spec_idx):
            wlen = spec_wave[spec_idx]
            
            distance = p[distance_idx]
            line_width = p[line_width_idx]
            
            lum5577 = p[lum5577_idx]
            amplitude5577 = oxygen.luminosity2flux(lum5577, distance)

            continuum5577 = p[continuum5577_idx]

            return oxygen.generation_func_5577(wlen, continuum5577, 0.0, amplitude5577, line_width,
                                               np.array([]), np.array([]))

        @numba.njit
        def func63xx(p, spec_idx):
            wlen = spec_wave[spec_idx]
            
            distance = p[distance_idx]
            line_width = p[line_width_idx]
            
            lum6300 = p[lum6300_idx]
            amplitude6300 = oxygen.luminosity2flux(lum6300, distance)
            lum6364 = p[lum6364_idx]
            amplitude6364 = oxygen.luminosity2flux(lum6364, distance)

            continuum63xx = p[continuum63xx_idx]

            return oxygen.generation_func_6300_6364(wlen, continuum63xx, 0.0, amplitude6300, amplitude6364, line_width,
                                               np.array([]), np.array([]))

        return func5577, func63xx


class MultizoneOxygenLineModel(OxygenLineModel):

    def __init__(self, spec, num_zones, zone_setup, zone_fixed_width=False, **kwargs):
        """
        zone_setup: array of length num_zones; (min_velocity, max_velocity)
        """
        self.num_zones = num_zones
        # only 1-2 zones support right now
        assert num_zones < 3
        assert num_zones == len(zone_setup)
        
        self.zone_setup = zone_setup
        # TODO: validate zone setup more
        self.zone_fixed_width = zone_fixed_width
        super().__init__(spec=spec, **kwargs)

        for zone_id in range(num_zones):
            self.add_param('zone%d_velocity' % zone_id)
            self.add_param('zone%d_lum5577' % zone_id)
            self.add_param('zone%d_lum6300' % zone_id)
            # we alias the ratio to the 6364 since we will transform the ratio in the prior into
            # the luminosity right away
            self.add_param('zone%d_ratio' % zone_id, alias=['zone%d_lum6364' % zone_id])
            if not self.zone_fixed_width:
                self.add_param('zone%d_width' % zone_id)
            

            
    def make_prior_func(self):
        max_amplitude = self.max_amplitude
        max_line_width = self.max_line_width
        
        num_zones = self.num_zones
        fixed_width = self.zone_fixed_width
        if num_zones >= 1:
            # first zone
            zone0_min_velocity, zone0_max_velocity = self.zone_setup[0]
            zone0_velocity_idx = self.params['zone0_velocity']
            zone0_lum5577_idx = self.params['zone0_lum5577']
            zone0_lum6300_idx = self.params['zone0_lum6300']
            zone0_lum6364_idx = self.params['zone0_lum6364']
            zone0_ratio_idx = self.params['zone0_ratio']
            zone0_width_idx = self.params.get('zone0_width', None)
            
        if num_zones >= 2:
            zone1_min_velocity, zone1_max_velocity = self.zone_setup[1]
            zone1_velocity_idx = self.params['zone1_velocity']
            zone1_lum5577_idx = self.params['zone1_lum5577']
            zone1_lum6300_idx = self.params['zone1_lum6300']
            zone1_lum6364_idx = self.params['zone1_lum6364']
            zone1_ratio_idx = self.params['zone1_ratio']
            zone1_width_idx = self.params.get('zone1_width', None)

        
        prev_prior_func, ndim = super().make_prior_func()
        
        @numba.njit(inline='always')
        def prior_func(u):
            # populate the likelihood specific params
            p = prev_prior_func(u)
            
            if num_zones >= 1:
                ratio0 = 2*u[zone0_ratio_idx]+1
                p[zone0_lum5577_idx] = np.exp(np.log(max_amplitude)*u[zone0_lum5577_idx])
                p[zone0_lum6300_idx] = np.exp(np.log(max_amplitude)*u[zone0_lum6300_idx])
                p[zone0_lum6364_idx] = np.exp(np.log(max_amplitude)*u[zone0_lum6300_idx])/ratio0
                p[zone0_velocity_idx] = (zone0_max_velocity-zone0_min_velocity)*u[zone0_velocity_idx] + zone0_min_velocity
                if not fixed_width:
                    p[zone0_width_idx] = np.exp(np.log(max_line_width)*u[zone0_width_idx])
            if num_zones >= 1:
                ratio1 = 2*u[zone1_ratio_idx]+1
                p[zone1_lum5577_idx] = np.exp(np.log(max_amplitude)*u[zone1_lum5577_idx])
                p[zone1_lum6300_idx] = np.exp(np.log(max_amplitude)*u[zone1_lum6300_idx])
                p[zone1_lum6364_idx] = np.exp(np.log(max_amplitude)*u[zone1_lum6300_idx])/ratio1
                p[zone1_velocity_idx] = (zone1_max_velocity-zone1_min_velocity)*u[zone1_velocity_idx] + zone1_min_velocity
                if not fixed_width:
                    p[zone1_width_idx] = np.exp(np.log(max_line_width)*u[zone1_width_idx])
            
            return p

        # add the core 4 params per zone
        ndim += 4*self.num_zones
        # if the zone line widths are not fixed add another param for each
        if not fixed_width:
            ndim += self.num_zones
        
        return prior_func, ndim

    def make_model_functions(self):
        spec_wave = self.fit_spec_wave

        num_zones = self.num_zones
        fixed_width = self.zone_fixed_width
        if num_zones >= 1:
            # first zone
            zone0_min_velocity, zone0_max_velocity = self.zone_setup[0]
            zone0_velocity_idx = self.params['zone0_velocity']
            zone0_lum5577_idx = self.params['zone0_lum5577']
            zone0_lum6300_idx = self.params['zone0_lum6300']
            zone0_lum6364_idx = self.params['zone0_lum6364']
            zone0_ratio_idx = self.params['zone0_ratio']
            zone0_width_idx = self.params.get('zone0_width', None)
            
        if num_zones >= 2:
            zone1_min_velocity, zone1_max_velocity = self.zone_setup[1]
            zone1_velocity_idx = self.params['zone1_velocity']
            zone1_lum5577_idx = self.params['zone1_lum5577']
            zone1_lum6300_idx = self.params['zone1_lum6300']
            zone1_lum6364_idx = self.params['zone1_lum6364']
            zone1_ratio_idx = self.params['zone1_ratio']
            zone1_width_idx = self.params.get('zone1_width', None)

        distance_idx = self.params['distance']
        line_width_idx = self.params['line_width']
        lum5577_idx = self.params['lum5577']
        continuum5577_idx = self.params['continuum5577']
        lum6300_idx = self.params['lum6300']
        lum6364_idx = self.params['lum6364']
        continuum63xx_idx = self.params['continuum63xx']
        
        @numba.njit(inline='always')
        def func5577(p, spec_idx):
            wlen = spec_wave[spec_idx]
            
            distance = p[distance_idx]
            line_width = p[line_width_idx]
            
            lum5577 = p[lum5577_idx]
            amplitude5577 = oxygen.luminosity2flux(lum5577, distance)

            continuum5577 = p[continuum5577_idx]

            extra_lines_wlen = oxygen.velocity2line(zone_velocity, 5577)

            return oxygen.generation_func_5577(wlen, continuum5577, 0.0, amplitude5577, line_width,
                                               extra_lines=extra_lines_wlen,
                                               extra_lines_amps=extra_lines_amps,
                                               mz_line_widths=mz_line_widths)

        @numba.njit(inline='always')
        def func63xx(p, spec_idx):
            wlen = spec_wave[spec_idx]
            
            distance = p[distance_idx]
            line_width = p[line_width_idx]
            
            lum6300 = p[lum6300_idx]
            amplitude6300 = oxygen.luminosity2flux(lum6300, distance)
            lum6364 = p[lum6364_idx]
            amplitude6364 = oxygen.luminosity2flux(lum6364, distance)

            continuum63xx = p[continuum63xx_idx]

            return oxygen.generation_func_6300_6364(wlen, continuum63xx, 0.0, amplitude6300, amplitude6364, line_width,
                                               np.array([]), np.array([]))

        return func5577, func63xx


class OxygenModel(specmodel.MultiRegionSpectralModel):

    def __init__(self, spec):
        # TODO: implement this
        pass

    def get_spectral_regions(self):
        func5577, func63xx = self.make_model_functions()
        
        region_5577 = specmodel.SpectralRegion(min_wave=self.min_5577, max_wave=self.max_5577, func=func5577)
        region_63xx = specmodel.SpectralRegion(min_wave=self.min_63xx, max_wave=self.max_63xx, func=func63xx)

        return [region_5577, region_63xx]

    def get_transform_func(self):
        @numba.njit
        def trans_func(p):
            fluxes = oxygen.create_fluxes(p)

            return fluxes

        return trans_func
    

    def make_model_functions(self):
        spec_wave = self.fit_spec_wave
        spec_flux = self.fit_spec_flux

        
        @numba.njit
        def func5577(p, spec_idx):
            fluxes = oxygen.create_fluxes(p)
            

        @numba.njit
        def func63xx(p, spec_idx):
            pass

        return func5577, func63xx
