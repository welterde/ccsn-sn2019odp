import collections
import numpy as np
import numba
import dynesty
from dynesty import utils as dyfunc
from multiprocessing import Pool
from astropy.cosmology import Planck15 as cosmology
import astropy.units as u
import astropy.constants as aconst
from scipy.special import voigt_profile

from .. import gaussian
from .. import cauchy
from .. import const
from .. import utils
from . import oxygen
from .. import spec as sutil


egauss = np.vectorize(gaussian.elongated_gaussian)

C_KM_S = aconst.c.to(u.km/u.s).value
def generation_func_63xx(amp_6300, ratio_6364, outer_shell_width, outer_shell_line_width, outer_shell_amplitude, continuum, wave_7774, flux_7774, wave_63xx):
    """
    Generate the 63xx feature.
    flux_7774 should already be continuum subtracted and total flux normalized to 1.0 (@6300)
    """
    
    # get projecting into the velocity space
    center_wave = 7774
    velocity = (wave_7774 - center_wave)/center_wave * C_KM_S
    
    # generate the matching wavelength grids for the velocities
    center_wave = 6300
    wave_6300 = velocity/C_KM_S * center_wave + center_wave
    center_wave = 6364
    wave_6364 = velocity/C_KM_S * center_wave + center_wave
    
    # re-project the 7774 flux onto the observed wavelength grid
    flux_6300 = np.interp(wave_63xx, wave_6300, flux_7774)*amp_6300
    flux_6364 = np.interp(wave_63xx, wave_6364, flux_7774)*amp_6300*ratio_6364
    
    # generate the continuum
    flux_cont = egauss(wave_63xx, 6300, outer_shell_amplitude, outer_shell_line_width, outer_shell_width) + egauss(wave_63xx, 6364, outer_shell_amplitude/3, outer_shell_line_width, outer_shell_width)
    
    flux = flux_6300 + flux_6364 + flux_cont + continuum
    return flux

def generation_func_5577(amp_6300, ratio_5577, outer_shell_width, outer_shell_line_width, outer_shell_amplitude, continuum, wave_7774, flux_7774, wave_5577):
    """
    Generate the 5577 feature.
    flux_7774 should already be continuum subtracted and total flux normalized to 1.0 (@6300)
    """
    
    # get projecting into the velocity space
    center_wave = 7774
    velocity = (wave_7774 - center_wave)/center_wave * C_KM_S
    
    # generate the matching wavelength grids for the velocities
    center_wave = 5577
    wave_5577_old = velocity/C_KM_S * center_wave + center_wave
    
    # re-project the 7774 flux onto the observed wavelength grid
    flux_5577 = np.interp(wave_5577, wave_5577_old, flux_7774)*amp_6300*ratio_5577
    
    # generate the outer shell continuum thingy
    flux_cont = egauss(wave_5577, 5577, outer_shell_amplitude * ratio_5577, outer_shell_line_width, outer_shell_width)
    
    #flux = flux_5577 + flux_cont + continuum
    flux = flux_5577 + continuum
    return flux

# function to pre-compute the 7774 flux
def premangle_7774_flux(spec, wave7774_min=7550, wave7774_max=8000, wave63xx_min=6050, wave63xx_max=6500):
    idx = np.logical_and(spec['restwave'] > wave7774_min, spec['restwave'] < wave7774_max)
    # remove those annyoing NaN
    idx = np.logical_and(idx, ~np.isnan(spec['flux']))
    wave = spec['restwave'][idx]
    flux = spec['flux'][idx]
    
    # create continuum subtracted flux
    flux_nocont = flux - np.nanpercentile(flux, 1)
    
    # reproject onto 6300 grid
    center_wave = 7774
    velocity = (wave - center_wave)/center_wave * C_KM_S

    #wave_grid = np.arange(6000, 6600, np.median(np.diff(wave))/2)
    idx = np.logical_and(spec['restwave'] > wave63xx_min, spec['restwave'] < wave63xx_max)
    # remove those annyoing NaN
    idx = np.logical_and(idx, ~np.isnan(spec['flux']))
    wave_grid = spec['restwave'][idx]
    
    center_wave = 6300
    wave_6300 = velocity/C_KM_S * center_wave + center_wave
    
    flux_6300 = np.interp(wave_grid, wave_6300, flux_nocont)
    
    # now calculate the flux of the reprojected line
    flux_int = np.trapz(flux_6300, wave_grid)
    #flux_oth = np.trapz(flux_nocont, wave_6300)
    #print(flux_int, flux_oth)
    
    # we want to normalize it 1.0
    norm_factor = 1.0/flux_int
    
    return wave, flux_nocont*norm_factor



LABELS = [
    'log_F6300',
    'R6364',
    'R5577',
    'ShWidth',
    'ShLineWidth',
    'log_ShAmp',
    'C63xx',
    'C5577'
]

PLOT_LABELS = [
    'log F6300',
    'Ratio 6364',
    'Ratio 5577',
    'Shell Width',
    'Shell Line Width',
    'log F6300[Shell]',
    'Continuum 63xx',
    'Continuum 5577'
]

@numba.njit
def prior(u, min_continuum, max_continuum, max_line_amp):
    p = np.empty_like(u)
    
    # 6300 log amplitude
    p[0] = np.log(max_line_amp) - 20*u[0]
    
    # 6364/6300 ratio
    p[1] = 1/(1+2*u[1])
    
    # 5577/6300 ratio
    p[2] = np.exp(-5*u[2])
    
    # outer shell width
    p[3] = 150 + 150*u[3]
    
    # outer shell line width
    p[4] = 50*u[4]
    
    # log outer shell amplitude
    p[5] = np.log(max_line_amp/10) - 20*u[5]
    
    # continuum 63xx
    #p[6] = np.log(max_continuum) - 10*u[6]
    p[6] = (max_continuum-min_continuum)*u[6]+min_continuum
    
    # continuum 5577
    #p[7] = np.log(max_continuum) - 10*u[7]
    p[7] = (max_continuum-min_continuum)*u[7]+min_continuum
    
    return p

def likelihood(p, wave_7774, flux_7774, wave_63xx, flux_63xx, wave_5577, flux_5577, scale_factor):
    amp_6300 = np.exp(p[0])
    ratio_6364 = p[1]
    ratio_5577 = p[2]
    outer_shell_width = p[3]
    outer_shell_line_width = p[4]
    outer_shell_amplitude = np.exp(p[5])
    continuum_63xx = p[6]
    continuum_5577 = p[7]
    
    model_flux_63xx = generation_func_63xx(amp_6300, ratio_6364, outer_shell_width, outer_shell_line_width, outer_shell_amplitude, continuum_63xx, wave_7774, flux_7774, wave_63xx)
    sigma2_63xx = 1/scale_factor**2 #+ model_flux_63xx ** 2 * np.exp(2*log_f)
    
    model_flux_5577 = generation_func_5577(amp_6300, ratio_5577, outer_shell_width, outer_shell_line_width, outer_shell_amplitude, continuum_5577, wave_7774, flux_7774, wave_5577)
    
    logl_63xx = -0.5*np.sum( (model_flux_63xx*scale_factor - flux_63xx*scale_factor)**2 )
    
    logl_5577 = -0.5*np.sum( (model_flux_5577*scale_factor - flux_5577*scale_factor)**2 )
    
    return logl_63xx+logl_5577

def render_func(p, wave_7774, flux_7774, wave_63xx, wave_5577):
    amp_6300 = np.exp(p[0])
    ratio_6364 = p[1]
    ratio_5577 = p[2]
    outer_shell_width = p[3]
    outer_shell_line_width = p[4]
    outer_shell_amplitude = np.exp(p[5])
    continuum_63xx = p[6]
    continuum_5577 = p[7]
    
    model_flux_63xx = generation_func_63xx(amp_6300, ratio_6364, outer_shell_width, outer_shell_line_width, outer_shell_amplitude, continuum_63xx, wave_7774, flux_7774, wave_63xx)
    
    model_flux_5577 = generation_func_5577(amp_6300, ratio_5577, outer_shell_width, outer_shell_line_width, outer_shell_amplitude, continuum_5577, wave_7774, flux_7774, wave_5577)
    
    return model_flux_63xx, model_flux_5577

def extract_region(spec, wave_min, wave_max):
    idx = np.logical_and(spec['restwave'] > wave_min, spec['restwave'] < wave_max)
    # remove those annyoing NaN
    idx = np.logical_and(idx, ~np.isnan(spec['flux']))
    wave = spec['restwave'][idx]
    flux = spec['flux'][idx]
    
    return wave, flux

def run_fit(spec, regions={}, dynesty_method='unif', dynesty_pfrac=0.5, nlive_init=3000, dlogz_init=0.1):
    wave_7774, flux_7774 = premangle_7774_flux(spec, wave7774_min=regions['7774_min'], wave7774_max=regions['7774_max'], wave63xx_min=regions['63xx_min'], wave63xx_max=regions['63xx_max'])
    
    # extract the 63xx region
    wave_63xx, flux_63xx = extract_region(spec, regions['63xx_min'], regions['63xx_max'])
    
    # extract the 5577 region
    wave_5577, flux_5577 = extract_region(spec, regions['5577_min'], regions['5577_max'])
    
    # use finite-difference estimate for uncertainty (scalefactor = 1/stddev)
    scale_factor = sutil.estimate_scalefactor(spec)/2
    
    prior_args = (np.nanpercentile(spec['flux'], 1), 2*np.nanpercentile(spec['flux'], 40), np.nanpercentile(spec['flux'], 95)*1e6)
    
    logl_args = (wave_7774, flux_7774, wave_63xx, flux_63xx, wave_5577, flux_5577, scale_factor)
    
    ndim = 8
    
    sampler = dynesty.DynamicNestedSampler(likelihood, prior, ndim,
                                           logl_args=logl_args,
                                           nlive=8000,
                                           ptform_args=prior_args,
                                           method=dynesty_method,
                                           slices=7,
                                           enlarge=3,
                                           walks=50)
    sampler.run_nested(nlive_init=nlive_init, dlogz_init=dlogz_init, wt_kwargs={'pfrac': dynesty_pfrac})
    results = sampler.results
    
    samples, weights = results.samples, np.exp(results.logwt - results.logz[-1])
    mean, cov = dyfunc.mean_and_cov(samples, weights)
    #if not quiet:
    print('\tMean: %s' % repr(mean))
    print('\n\t\t'.join(('\tCov: %s' % repr(cov)).split('\n')))

    new_samples = dyfunc.resample_equal(samples, weights)
    
    #return sampler, new_samples
    return new_samples, results, sampler
