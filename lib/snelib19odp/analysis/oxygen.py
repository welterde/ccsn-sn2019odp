"""
Oxygen mass analysis similar to Jerkstrand+2014

1) Measure flux ratio OI5577/(OI6300+OI6364) to estimate temperature assuming LTE (eqn 2)
2) Use it to estimate oxygen mass using eqn 3

Bayesian model:
- oxygen_mass
- temperature
- distance (nuisance parameter)

- line_width
- continuum_level[]
- continuum_slope[]

Virtual params:
line_flux[] <- model(oxygen_mass, temperature
amplitude[] <- gaussian(line_flux[], line_width[])

flux{} <- gaussian + continuum

"""
import collections
import numpy as np
import numba
import dynesty
from dynesty import utils as dyfunc
from multiprocessing import Pool
from astropy.cosmology import Planck15 as cosmology
import astropy.units as u
import astropy.constants as aconst
from scipy.special import voigt_profile

from .. import gaussian
from .. import cauchy
from .. import const
from .. import utils



FitSetup = collections.namedtuple('FitSetup', 'spec smp start5577 stop5577 start6300_6364 stop6300_6364 extra_lines max_width scale_factor max_continuum nlte_mode nlte_departure_max max_extra_line_amp multizone_lines multizone_max_width multizone_width_enable multizone_reverse_tau_enable multizone_blue_wing_enable multizone_reverse_tau_max_ratio multizone_thick_shell')


def make_setup(**kwargs):
    new_kw = kwargs.copy()
    if 'multizone_lines' not in kwargs:
        new_kw['multizone_lines'] = []
    if 'extra_lines' not in kwargs:
        new_kw['extra_lines'] = np.empty([])
    
    if 'multizone_max_width' not in kwargs:
        new_kw['multizone_width_enable'] = False
        new_kw['multizone_max_width'] = 0
    elif 'multizone_width_enable' not in kwargs:
        new_kw['multizone_width_enable'] = True
    
    if 'multizone_reverse_tau_enable' not in kwargs:
        new_kw['multizone_reverse_tau_enable'] = False
    if 'multizone_blue_wing_enable' not in kwargs:
        new_kw['multizone_blue_wing_enable'] = False
    if 'multizone_reverse_tau_max_ratio' not in kwargs:
        new_kw['multizone_reverse_tau_max_ratio'] = 1.0

    if 'multizone_thick_shell' not in kwargs:
        new_kw['multizone_thick_shell'] = np.array([])
    
    return FitSetup(**new_kw)


PARAMETER_LABELS = [
    'M_ox',
    'T',
    'line_width',
    'continuum5577',
    'slope5577',
    'continuum6300',
    'slope6300',
    'distance',
    'wave_offset',
    'optical_depth',
    'log_f',
    'beta_ratio'
]

@numba.njit
def prior(u, distance_min, distance_max, num_extra_lines, max_width, max_continuum, nlte_mode, nlte_departure_max, max_extra_line_amp, multizone_width_enable, multizone_max_width, multizone_reverse_tau_enable, multizone_reverse_tau_max_ratio):
    x = np.empty(len(u))

    # oxygen mass [Msol]
    #x[0] = 10*u[0]+1e-4
    # log oxygen mass [Msol]
    #x[0] = 30*u[0]-25
    x[0] = 6*u[0]

    # temperature [K]
    #x[1] = 1e4*u[1]+1000
    x[1] = np.exp(2.4*u[1]+6.9)

    # line width [A]
    x[2] = max_width/2 * (1+u[2])

    # continuum level 5577
    x[3] = 2*max_continuum*u[3]-max_continuum

    # continuum level 6300,6364
    x[4] = 2*max_continuum*u[4]-max_continuum

    # continuum slope 5577
    x[5] = 0.02*u[5]-0.01

    

    # continuum slope 6300,6364
    x[6] = 0.01*u[6]-0.005

    # distance
    x[7] = (distance_max-distance_min)*u[7] + distance_min

    # wavelength offset [A]
    x[8] = 8*u[8]-5

    # nuisance parameter: optical depth
    x[9] = np.exp(5*u[9] - 3)

    # nuisance parameter: log f
    x[10] = 12*u[10] - 10

    # nuisance parameter: NLTE departure coefficient
    if nlte_mode == 0:
        # running in LTE mode
        # this means the ratio is exactly the LTE ratio
        x[11] = 1.0
    else:
        # running in NLTE mode
        # in NLTE mode the ratio can fall below one
        # range is 0.05 - 1.0
        x[11] = np.exp(np.log(nlte_departure_max)*u[11])
    

    for i in range(num_extra_lines):
        x[12+i] = 2*np.log(max_extra_line_amp)*(u[12+i]-0.5)

    if multizone_width_enable:
        idx = 12+num_extra_lines
        x[idx] = multizone_max_width/2 * (1+u[idx])

    if multizone_reverse_tau_enable:
        idx = 13+num_extra_lines
        x[idx] = np.exp(-0.7*u[idx] + np.log(multizone_reverse_tau_max_ratio))
    
    return x


# from Jerkstrand+2017 table 1 for O I,6300
# A = 5.6e-3
# from https://physics.nist.gov/cgi-bin/ASD/lines1.pl?spectra=O+I&limits_type=0&low_w=&upp_w=&unit=1&submit=Retrieve+Data&de=0&format=0&line_out=0&en_unit=0&output=0&bibrefs=1&page_size=15&show_obs_wl=1&show_calc_wl=1&unc_out=1&order_out=0&max_low_enrg=&show_av=2&max_upp_enrg=&tsb_value=0&min_str=&A_out=0&intens_out=on&max_str=&allowed_out=1&forbid_out=1&min_accur=&min_intens=&conf_out=on&term_out=on&enrg_out=on&J_out=on
# M1 transition 630.0304 nm
RAD_DECAY_RATE_6300 = 5.63e-3
RAD_DECAY_RATE_6364 = 1.83e-3

RAD_DECAY_RATE_5577 = 1.26

def compute_pre_factor_63xx(wavelength):
    # M_ion / atomic_number / m_p
    N = u.solMass.to(u.g) / 16 / aconst.m_p.cgs.value
    
    if wavelength == 6300:
        A = RAD_DECAY_RATE_6300
    elif wavelength == 6364:
        A = RAD_DECAY_RATE_6364
    else:
        return np.nan

    # h*\nu
    h_nu = aconst.h.cgs.value * aconst.c.cgs.value / (wavelength*u.Angstrom.to(u.cm))
    
    # from Jerkstrand+2017 table 1 for O I,6300
    # upper state the same for 6300 and 6364
    g_u1 = 5

    # taken from Jerkstrand+2014 (page 3699 left)
    # approximation treating the split ground state in a combined way
    g_g = 9

    return N*A*h_nu*g_u1/g_g

LUM_6300_PRE_FACTOR = compute_pre_factor_63xx(6300)
LUM_6364_PRE_FACTOR = compute_pre_factor_63xx(6364)

# E_u / k_B
# 1.97 eV from Jerkstrand+2017 (page 17 tab 1)
LUM_63XX_TEMP_FACTOR = 1.97 / 8.617333262145e-5

@numba.njit(inline='always')
def calculate_line_luminosity_63xx(oxygen_mass, temperature, optical_depth, line):
    """
    Equation 3 from Jerkstrand+2014 (MNRAS439,3694–3703 (2014))
    Changed to be seperate for 6300 and 6364 (instead of combined)
    """

    if line == 6300:
        beta = (1-np.exp(-optical_depth))/optical_depth
        pre_factor = LUM_6300_PRE_FACTOR
    elif line == 6364:
        beta = 3*(1-np.exp(-optical_depth/3))/optical_depth
        pre_factor = LUM_6364_PRE_FACTOR
    else:
        beta = optical_depth * np.nan
        pre_factor = 1.0

    lum = pre_factor * beta * oxygen_mass / np.exp(LUM_63XX_TEMP_FACTOR / temperature)
    return lum

@numba.njit(inline='always')
def calculate_5577_optical_depth(tau_6300, temperature, nlte_departure_coefficient=1.0):
    # nlte_departure_coefficient denotes how much the n_2 (5577 is n_2 -> n_1) state deviates from LTE
    g_2 = 1
    g_1 = 5
    E2 = 48620.30533 # in K
    E1 = 22830.462395 # in K
    E0 = 0
    return tau_6300 * nlte_departure_coefficient * g_2/g_1 * RAD_DECAY_RATE_5577 / RAD_DECAY_RATE_6300 * 5577**3 / 6300**3 * np.exp(-E1/temperature) * (1-nlte_departure_coefficient*np.exp(-E2/temperature)/np.exp(-E1/temperature))/(1-np.exp(-E1/temperature))

@numba.njit(inline='always')
def calculate_optical_depth_lte_other(tau_6300, temp, line):
    """
    Calculate the optical depth of other O I lines in relation to the optical depth of the 6300 line
    """
    k_B = 8.617333262e-5 # eV/K
    if line == 7771:
        Ax = 3.69e7
        E0 = 9.1460911 # eV
        E1 = 10.7409314 # eV
        g0 = 5
        g1 = 7
    A6300 = 5.53e-3
    E6300 = 1.967 # eV
    g_g = 9 # ground state
    g_u1 = 5 # E6300 state

    pre_factor = (g1/g0)/(g_u1/g_g) * Ax / A6300 * (line)**3 / 6300**3
    return tau_6300 * pre_factor * g0/g_g * np.exp(-E0/k_B/temp) * (1-(np.exp(-E1/k_B/temp)/np.exp(-E0/k_B/temp)))/(1-np.exp(-E6300/k_B/temp))

@numba.njit(inline='always')
def optical_depth_from_line_volume(line, mass_ion, line_velocity, time, fill_factor):
    """
    m_ion mass in 1Msol
    line_velocity in 1km/s
    time in seconds
    """
    # epsilon factor used in eqn 39 of Jerkstrand+2017
    if line == 6300:
        eps = 0.21e14
    else:
        eps = np.nan

    return eps * mass_ion / (line_velocity/3000)**3 / fill_factor / time**2
    


@numba.njit(inline='always')
def tau_to_beta(tau):
    """ Convert (Sobolev) optical depth to escape probability """
    return (1-np.exp(-tau))/tau

@numba.njit(inline='always')
def calculate_line_luminosity_5577(oxygen_mass, temperature, optical_depth, nlte_departure_coefficient):
    g_u2 = 1
    g_u1 = 5

    pre_factor = g_u2/g_u1 * RAD_DECAY_RATE_5577 / RAD_DECAY_RATE_6300 * 6300/5577

    # tau6300 -> tau5577
    optical_depth_5577 = calculate_5577_optical_depth(optical_depth, temperature, nlte_departure_coefficient)
    beta_5577 = tau_to_beta(optical_depth_5577)
    beta_6300 = tau_to_beta(optical_depth)
    beta_ratio = beta_5577/beta_6300
    
    
    lum_6300 = calculate_line_luminosity_63xx(oxygen_mass, temperature, optical_depth, 6300)
    
    return lum_6300 * pre_factor * np.exp(-25790 / temperature) * beta_ratio * nlte_departure_coefficient

luminosity2flux = utils.luminosity2flux
redshift2distance = utils.redshift2distance

# light speed in km/s
C_KMS = 2.997925e5

@numba.njit(inline='always')
def velocity2line(lambda0, velocity):
    return lambda0*C_KMS/(velocity+C_KMS)

@numba.njit(inline='always')
def line2velocity(wave0, wave):
    return wave0*C_KMS/wave-C_KMS

@numba.njit(inline="always")
def generation_func_5577(wlen, continuum_level, continuum_slope, amplitude, line_width, extra_lines, extra_lines_amps, mz_line_width):
    dwlen = wlen - 5577
    accu = continuum_level
    for i in range(len(extra_lines)):
        accu += gaussian.gaussian(wlen, extra_lines[i], extra_lines_amps[i], mz_line_width)
    return accu - dwlen*continuum_slope + gaussian.gaussian(wlen, 5577, amplitude, line_width)

@numba.njit(inline="always")
def generation_func_6300_6364(wlen, continuum_level, continuum_slope, amplitude_6300, amplitude_6364, line_width, extra_lines, extra_lines_amps, multizone_lines, matching_negative_wavelengths6300, matching_negative_wavelengths6364, mz_line_width, mz_reverse_beta, mz_blue_wing_enable, mz_thick_shell):
    dwlen = wlen - 6330
    accu = continuum_level
    for i in range(len(extra_lines)):

        if mz_thick_shell[i] and multizone_lines[i]:
            velocity = np.abs(line2velocity(6300, extra_lines[i]))
            width_6300 = velocity2line(6300, -2*velocity)-6300
            width_6364 = velocity2line(6364, -2*velocity)-6364
            accu += gaussian.elongated_gaussian(wlen, 6300, extra_lines_amps[i], mz_line_width, width_6300)
            accu += gaussian.elongated_gaussian(wlen, 6364, extra_lines_amps[i]/3, mz_line_width, width_6364)
        else:
            accu += gaussian.gaussian(wlen, extra_lines[i], extra_lines_amps[i], mz_line_width)
        
        # in case of multizone lines we add an additional line that is offset 64A redwards and is optically thin
        # optically thin means flux ratio of 1/3
        if multizone_lines[i] and not mz_thick_shell[i]:
            accu += gaussian.gaussian(wlen, extra_lines[i]+64, extra_lines_amps[i]/3, mz_line_width)
            if mz_blue_wing_enable:
                accu += gaussian.gaussian(wlen, matching_negative_wavelengths6300[i], mz_reverse_beta*extra_lines_amps[i], mz_line_width)
                accu += gaussian.gaussian(wlen, matching_negative_wavelengths6364[i], mz_reverse_beta*extra_lines_amps[i]/3, mz_line_width)
        #accu += gaussian.flat_top_gaussian(wlen, extra_lines[i], extra_lines_amps[i], line_width)
    #return accu - dwlen*continuum_slope + cauchy.cauchy(wlen, 6300, amplitude_6300, line_width) + cauchy.cauchy(wlen, 6364, amplitude_6364, line_width)
    return accu - dwlen*continuum_slope + gaussian.gaussian(wlen, 6300, amplitude_6300, line_width) + gaussian.gaussian(wlen, 6364, amplitude_6364, line_width)
    #return accu - dwlen*continuum_slope + gaussian.flat_top_gaussian(wlen, 6300, amplitude_6300, line_width) + gaussian.flat_top_gaussian(wlen, 6364, amplitude_6364, line_width)
    


@numba.njit(inline='always')
def create_fluxes(p, distance_idx=7, tau_idx=9, nlte_idx=11):
    oxygen_mass = p[0]
    lte_temp = p[1]
    distance = p[distance_idx]
    optical_depth = p[tau_idx]
    if nlte_idx > 0:
        nlte_departure_coefficient = p[nlte_idx]
    else:
        nlte_departure_coefficient = 1.0

    # estimate line fluxes given oxygen mass and lte_temp
    flux_5577 = luminosity2flux(calculate_line_luminosity_5577(oxygen_mass, lte_temp, optical_depth, nlte_departure_coefficient), distance)
    flux_6300 = luminosity2flux(calculate_line_luminosity_63xx(oxygen_mass, lte_temp, optical_depth, 6300), distance)
    flux_6364 = luminosity2flux(calculate_line_luminosity_63xx(oxygen_mass, lte_temp, optical_depth, 6364), distance)
    
    r = np.empty(3)
    r[0] = flux_5577
    r[1] = flux_6300
    r[2] = flux_6364
    
    return r

@numba.njit
def likelihood(p, spec_wave, spec_flux, grp_5577, grp_6300_6364, extra_lines, scale_factor, multizone_lines, matching_negative_wavelengths6300, matching_negative_wavelengths6364, multizone_width_enable, multizone_reverse_tau_enable, multizone_blue_wing_enable, multizone_thick_shell):
    #oxygen_mass = np.exp(p[0])
    oxygen_mass = p[0]
    lte_temp = p[1]

    line_width = p[2]
    if multizone_width_enable:
        idx = 12+len(extra_lines)
        mz_line_width = p[idx]
    else:
        mz_line_width = line_width
    if multizone_reverse_tau_enable:
        idx = 13+len(extra_lines)
        mz_reverse_beta = p[idx]
    else:
        # actually already in beta and not tau
        mz_reverse_beta = 1.0

    continuum_level_5577 = p[3]/scale_factor
    continuum_level_6300_6364 = p[4]/scale_factor

    continuum_slope_5577 = p[5]/scale_factor
    continuum_slope_6300_6364 = p[6]/scale_factor

    distance = p[7]

    wlen_offset = p[8]

    optical_depth = p[9]

    log_f = p[10]

    #beta_ratio = np.exp(p[11])
    beta_ratio = p[11]

    # compute stuff for where the extra lines belong
    extra_lines_amps = np.exp(p[12:])/scale_factor
    extra_lines_idx_63xx = extra_lines > 6000 # they are in 63xx region
    extra_lines_idx_5577 = ~extra_lines_idx_63xx # they are in 5577 region

    fluxes = create_fluxes(p)
    flux_5577 = fluxes[0]
    flux_6300 = fluxes[1]
    flux_6364 = fluxes[2]

    # calculate amplitude from flux and line width
    # which is very easy since the gaussian we use is normalized!
    amplitude_5577 = flux_5577
    amplitude_6300 = flux_6300
    amplitude_6364 = flux_6364

    log_l = 0

    # TODO: integrate uncertainty from specs
    
    for i in numba.prange(len(spec_wave)):
        wlen, obs_flux = spec_wave[i], spec_flux[i]
        if grp_5577[i]:
            model_flux = generation_func_5577(wlen+wlen_offset, continuum_level_5577, continuum_slope_5577, amplitude_5577, line_width, extra_lines[extra_lines_idx_5577], extra_lines_amps[extra_lines_idx_5577], mz_line_width)
            sigma2 = 1/scale_factor**2 + model_flux ** 2 * np.exp(2*log_f)
            log_l += -0.5 * ((obs_flux - model_flux) ** 2 / sigma2 + np.log(sigma2))
        elif grp_6300_6364[i]:
            model_flux = generation_func_6300_6364(wlen+wlen_offset, continuum_level_6300_6364, continuum_slope_6300_6364, amplitude_6300, amplitude_6364, line_width, extra_lines[extra_lines_idx_63xx], extra_lines_amps[extra_lines_idx_63xx], multizone_lines[extra_lines_idx_63xx], matching_negative_wavelengths6300[extra_lines_idx_63xx], matching_negative_wavelengths6364[extra_lines_idx_63xx], mz_line_width, mz_reverse_beta, multizone_blue_wing_enable, multizone_thick_shell[extra_lines_idx_63xx])
            sigma2 = 1/scale_factor**2 + model_flux ** 2 * np.exp(2*log_f)
            log_l += -0.5 * ((obs_flux - model_flux) ** 2 / sigma2 + np.log(sigma2))

    return log_l



def run_dynesty_fit(cfg, dist_range=None, nlive_init=1000, dynesty_method='unif', dynesty_pfrac=0.5, return_sampler=False, quiet=False):
    spec_wave = cfg.spec['restwave']
    spec_flux = cfg.spec['flux']

    grp_5577 = np.logical_and(spec_wave > cfg.start5577, spec_wave < cfg.stop5577)
    grp_6300_6364 = np.logical_and(spec_wave > cfg.start6300_6364, spec_wave < cfg.stop6300_6364)

    if dist_range is not None:
        dist_min, dist_max = dist_range
    else:
        dist_min, dist_max = redshift2distance(const.redshift-const.redshift_err, const.redshift+const.redshift_err)

    multizone_lines = np.zeros(len(cfg.extra_lines), dtype=np.bool)
    multizone_thick_shell = np.zeros(len(cfg.extra_lines), dtype=np.bool)
    for i in range(len(cfg.extra_lines)):
        multizone_lines[i] = cfg.extra_lines[i] in cfg.multizone_lines
        multizone_thick_shell[i] = cfg.extra_lines[i] in cfg.multizone_thick_shell

    # calculate the negative velocity matches for the 63xx extra lines
    matching_negative_wavelengths6300 = np.zeros(len(cfg.extra_lines))
    matching_negative_wavelengths6364 = np.zeros(len(cfg.extra_lines))
    for i in range(len(cfg.extra_lines)):
        line_velocity = line2velocity(6300, cfg.extra_lines[i])
        matching_negative_wavelengths6300[i] = velocity2line(6300, -line_velocity)
        matching_negative_wavelengths6364[i] = velocity2line(6364, -line_velocity)

    print(cfg.extra_lines, multizone_lines, matching_negative_wavelengths6300, matching_negative_wavelengths6364)
        
    max_continuum = cfg.max_continuum*cfg.scale_factor
    prior_args = (dist_min, dist_max, len(cfg.extra_lines), cfg.max_width, max_continuum, cfg.nlte_mode, cfg.nlte_departure_max, cfg.max_extra_line_amp*cfg.scale_factor, cfg.multizone_width_enable, cfg.multizone_max_width, cfg.multizone_reverse_tau_enable, cfg.multizone_reverse_tau_max_ratio)
    logl_args = (spec_wave, spec_flux, grp_5577, grp_6300_6364, cfg.extra_lines, cfg.scale_factor, multizone_lines, matching_negative_wavelengths6300, matching_negative_wavelengths6364, cfg.multizone_width_enable, cfg.multizone_reverse_tau_enable, cfg.multizone_blue_wing_enable, multizone_thick_shell)

    # maybe run with multiple CPUs
    pool = None
    queue_size = None
    if cfg.smp > 1:
        pool = Pool(cfg.smp)
        queue_size = cfg.smp

    # extra prior params for extra features
    num_extra = 0
    if cfg.multizone_width_enable:
        num_extra += 1
        if cfg.multizone_reverse_tau_enable:
            num_extra += 1
    elif cfg.multizone_reverse_tau_enable:
        print('Invalid parameter combination. reverse tau requires width_enable')
        return None
    
    sampler = dynesty.DynamicNestedSampler(likelihood, prior, 12+len(cfg.extra_lines)+num_extra, logl_args=logl_args, nlive=12000, pool=pool, queue_size=queue_size, ptform_args=prior_args, method=dynesty_method, slices=7, enlarge=3, walks=50)

    sampler.run_nested(nlive_init=nlive_init, wt_kwargs={'pfrac': dynesty_pfrac})
    results = sampler.results

    samples, weights = results.samples, np.exp(results.logwt - results.logz[-1])
    mean, cov = dyfunc.mean_and_cov(samples, weights)
    if not quiet:
        print('\tMean: %s' % repr(mean))
        print('\n\t\t'.join(('\tCov: %s' % repr(cov)).split('\n')))

    new_samples = dyfunc.resample_equal(samples, weights)

    if not return_sampler:
        return results, new_samples
    else:
        return results, new_samples, sampler



@numba.njit(inline='always')
def render_func(wave, p, grp_5577, grp_6300_6364, extra_lines, scale_factor, multizone_lines, matching_negative_wavelengths6300, matching_negative_wavelengths6364, multizone_width_enable, multizone_reverse_tau_enable, multizone_blue_wing_enable, multizone_thick_shell):
    line_width = p[2]
    if multizone_width_enable:
        idx = 12+len(extra_lines)
        mz_line_width = p[idx]
    else:
        mz_line_width = line_width

    if multizone_reverse_tau_enable:
        idx = 13+len(extra_lines)
        mz_reverse_beta = p[idx]
    else:
        # actually already in beta and not tau
        mz_reverse_beta = 1.0

    continuum_level_5577 = p[3]/scale_factor
    continuum_level_6300_6364 = p[4]/scale_factor

    continuum_slope_5577 = p[5]/scale_factor
    continuum_slope_6300_6364 = p[6]/scale_factor

    distance = p[7]

    wlen_offset = p[8]
    
    # compute stuff for where the extra lines belong
    extra_lines_amps = np.exp(p[12:])/scale_factor
    extra_lines_idx_63xx = extra_lines > 6000 # they are in 63xx region
    extra_lines_idx_5577 = ~extra_lines_idx_63xx # they are in 5577 region

    fluxes = create_fluxes(p)
    flux_5577 = fluxes[0]
    flux_6300 = fluxes[1]
    flux_6364 = fluxes[2]

    # calculate amplitude from flux and line width
    # which is very easy since the gaussian we use is normalized!
    amplitude_5577 = flux_5577
    amplitude_6300 = flux_6300
    amplitude_6364 = flux_6364

    flux = np.empty_like(wave)
    
    for i in numba.prange(len(wave)):
        wlen = wave[i]
        if grp_5577[i]:
            flux[i] = scale_factor*generation_func_5577(wlen+wlen_offset, continuum_level_5577, continuum_slope_5577, amplitude_5577, line_width, extra_lines[extra_lines_idx_5577], extra_lines_amps[extra_lines_idx_5577], mz_line_width)
        elif grp_6300_6364[i]:
            flux[i] = scale_factor*generation_func_6300_6364(wlen+wlen_offset, continuum_level_6300_6364, continuum_slope_6300_6364, amplitude_6300, amplitude_6364, line_width, extra_lines[extra_lines_idx_63xx], extra_lines_amps[extra_lines_idx_63xx], multizone_lines[extra_lines_idx_63xx], matching_negative_wavelengths6300[extra_lines_idx_63xx], matching_negative_wavelengths6364[extra_lines_idx_63xx], mz_line_width, mz_reverse_beta, multizone_blue_wing_enable, multizone_thick_shell[extra_lines_idx_63xx])

    return flux

def render(cfg, p, wave):
    """
    Utility function for rendering the function on specified wavelength grid
    """

    # extract 
    scale_factor = cfg.scale_factor
    grp_5577 = np.logical_and(wave > cfg.start5577, wave < cfg.stop5577)
    grp_6300_6364 = np.logical_and(wave > cfg.start6300_6364, wave < cfg.stop6300_6364)

    multizone_lines = np.zeros(len(cfg.extra_lines), dtype=np.bool)
    multizone_thick_shell = np.zeros(len(cfg.extra_lines), dtype=np.bool)
    for i in range(len(cfg.extra_lines)):
        multizone_lines[i] = cfg.extra_lines[i] in cfg.multizone_lines
        multizone_thick_shell[i] = cfg.extra_lines[i] in cfg.multizone_thick_shell

    # calculate the negative velocity matches for the 63xx extra lines
    matching_negative_wavelengths6300 = np.zeros(len(cfg.extra_lines))
    matching_negative_wavelengths6364 = np.zeros(len(cfg.extra_lines))
    for i in range(len(cfg.extra_lines)):
        line_velocity = line2velocity(6300, cfg.extra_lines[i])
        matching_negative_wavelengths6300[i] = velocity2line(6300, -line_velocity)
        matching_negative_wavelengths6364[i] = velocity2line(6364, -line_velocity)
        
    return render_func(wave, p, grp_5577, grp_6300_6364, cfg.extra_lines, cfg.scale_factor, multizone_lines, matching_negative_wavelengths6300, matching_negative_wavelengths6364, cfg.multizone_width_enable, cfg.multizone_reverse_tau_enable, cfg.multizone_blue_wing_enable, multizone_thick_shell)

