import os, numba, dynesty, collections, tqdm
import numpy as np
import speclite
import speclite.filters
from astropy.table import Table
import astropy.units as u

from .. import cache
from .. import blackbody


FILTERCURVES_DIR = os.path.join(os.path.dirname(__file__), '../../../const/filters')


# TODO: move this to its own module
def load_filter(inst, band):
    data = Table.read(os.path.join(FILTERCURVES_DIR, '%s.%s.dat' % (inst, band)), format='ascii')
    wave = []
    transmission = []
    for entry in data:
        if len(wave) > 0 and wave[-1] == entry['col1']:
            continue
        wave.append(entry['col1'])
        if entry['col2'] < 0.01 or entry['col1'] < 3790:
            transmission.append(0.0)
        else:
            transmission.append(entry['col2'])
    wave = np.array(wave)*u.Angstrom
    transmission = np.array(transmission)
    
    # hack to make sure response goes to zero on both sides
    transmission[0] = 0
    transmission[-1] = 0

    f = speclite.filters.FilterResponse(wavelength=wave, response=transmission, meta=dict(group_name='ZTF', band_name=band))
    #print(f)
    return f



FILTERS = {
    'ZTF_g': load_filter('Palomar_ZTF', 'g'),
    'ZTF_r': load_filter('Palomar_ZTF', 'r'),
    'ZTF_i': load_filter('Palomar_ZTF', 'i'),
    'Bessel_B': load_filter('NOT_ALFOSC', 'Bes_B'),
    'Bessel_V': load_filter('NOT_ALFOSC', 'Bes_V'),
    'Bessel_R': load_filter('NOT_ALFOSC', 'Bes_R'),
    'Bessel_I': load_filter('NOT_ALFOSC', 'Bes_I'),
    'Cousins_Rc': load_filter('Generic_Cousins', 'R'),
    'Cousins_Ic': load_filter('Generic_Cousins', 'I'),
}
for band in 'grizJHK':
    FILTERS['GROND_' + band] = load_filter('LaSilla_GROND', band)



def sample_mags(ds, mjd, band, sample_num=100):
    # for now just use simple normal distribution
    sample_mags = np.empty(sample_num)

    for i in range(sample_num):
        sample_mags[i] = ds.get_interpolated(band, t_grid=mjd, parametric=False, sample='random')

    return np.nanmean(sample_mags), np.nanstd(sample_mags)



def make_funcs(bands, dist_min, dist_max):

    # setup the filter curves
    filter_curves = []
    for band in bands:
        if band in 'gri':
            filter_curves.append(FILTERS['ZTF_%s' % band])
        elif band in 'zJHK':
            filter_curves.append(FILTERS['GROND_%s' % band])
        elif band in ['Rc', 'Ic']:
            filter_curves.append(FILTERS['Cousins_%s' % band])
        else:
            filter_curves.append(FILTERS['Bessel_%s' % band])

    wave_grid = np.arange(3500, 25000, 1)*1.0

    # resample them onto the same grid
    

    
    
    #@numba.njit(inline='always')
    def likelihood(p, obs_mags, obs_mags_errs):
        temp, ln_radius, distance_cm = p

        sed = blackbody.bb(temp, np.exp(ln_radius), wave_grid)
        syn_mags = []
        for f in filter_curves:
            syn_mags.append(f.get_ab_magnitude(sed/4/np.pi/distance_cm**2, wave_grid*u.Angstrom))
            #print(syn_mags)
            #return
        sigma2 = obs_mags_errs ** 2
        return -0.5 * np.sum((syn_mags - obs_mags)**2 / sigma2)

    @numba.njit
    def prior(u, log_uniform=False):
        x = np.empty_like(u)
        # temperature [K]
        if log_uniform:
            x[1] = np.exp(3*u[0] + 6.9)
        else:
            x[0] = 2e4*u[0] + 1000
        
        # radius for the range 20km (NS) to a lot of Rsun
        # [log cm]
        x[1] = 25*u[1] + 15

        # distance [cm]
        x[2] = (dist_max-dist_min)*u[2] + dist_min
        return x

    return likelihood, prior, 3


FitResult = collections.namedtuple('FitResult', 'results obs_mags obs_mags_errs mjd')

def run_fit(ds, mjd, bands, func_setup, mode='nested', no_cache=False):
    mjd_key = str(int(mjd*100))
    cache_key = ds.name + '_'+ mjd_key + '_' + (''.join(bands))
    cached = cache.DEFAULT_CACHE.get_product('phot_bb', cache_key)
    if cached is not None and not no_cache:
        return cached
    
    # setup the observations
    obs_mags = np.empty(len(bands))
    obs_mags_errs = np.empty_like(obs_mags)
    for i, band in enumerate(bands):
        obs_mags[i], obs_mags_errs[i] = sample_mags(ds, mjd, band)

    likelihood, prior_transform, ndim = func_setup

    args = (obs_mags, obs_mags_errs)
    if mode == 'nested':
        sampler = dynesty.NestedSampler(likelihood, prior_transform, ndim, logl_args=args)
    elif mode == 'dynamic_nested':
        sampler = dynesty.DynamicNestedSampler(likelihood, prior_transform, ndim, logl_args=args)
    sampler.run_nested()
    results = sampler.results

    result = FitResult(results=results, obs_mags=obs_mags, obs_mags_errs=obs_mags_errs, mjd=mjd)
    cache.DEFAULT_CACHE.add_product('phot_bb', cache_key, result)
    return result



MPC_TO_CM = u.Mpc.to(u.cm)

def run_observation_grid(ds, bands, mjd_step_size=1, fine_pre_peak_grid=False, max_phase=80, mode='nested', no_cache=False, progressbar=True):
    dist_min, dist_max = MPC_TO_CM*(ds.transient.dist_mpc-ds.transient.dist_mpc_err), MPC_TO_CM*(ds.transient.dist_mpc+ds.transient.dist_mpc_err)

    # setup the functions (prior & likelihood)
    func_setup = make_funcs(bands=bands, dist_min=dist_min, dist_max=dist_max)
    
    # get the times of all observations in $bands
    lcs_per_band = [ds.get_combined_lc(band, include_upperlim=False) for band in bands]
    times_per_band = [x['mjd'] for x in lcs_per_band]
    all_mjds = np.concatenate(times_per_band)

    # filter out all that our outside max phase
    phase = all_mjds - ds.transient.prior_t0
    idx = phase < max_phase
    cut_mjds = all_mjds[idx]

    
    # minimum time is when all used bands have at least one detection
    # (otherwise we are extrapolating one or more bands)
    mjd_min = np.array([x.min() for x in times_per_band]).max()
    mjd_max = np.array([x.max() for x in times_per_band]).min()

    # now discretize that to 1d grid (and replace time with mean time of observations
    # FIXME: doesn't quite do the correct day clustering.. but good enough for now
    if not fine_pre_peak_grid:
        mjd_grid = np.arange(mjd_min, cut_mjds.max(), mjd_step_size)
    else:
        # this assumes all bands have a observation by peak
        mjd_grid = np.arange(max(mjd_min, ds.transient.prior_t0), min(mjd_max, cut_mjds.max()), mjd_step_size)
    for i, mjd in enumerate(mjd_grid):
        idx = np.abs(cut_mjds - mjd) < 0.5
        if np.count_nonzero(idx) == 0:
            mjd_grid[i] = np.nan
        else:
            mjd_grid[i] = np.mean(cut_mjds[idx])

    # cut days without close observations
    mjd_grid = mjd_grid[~np.isnan(mjd_grid)]

    if fine_pre_peak_grid:
        # now use all times we have from mjd_min to peak
        idx = np.logical_and(cut_mjds > mjd_min, cut_mjds < ds.transient.prior_t0)

        # discretize to 0.1d grid
        mjd_grid_fine = np.array(np.unique(np.array(cut_mjds[idx]*10, dtype=np.int64)), dtype=np.float)/10

        # prepend it
        mjd_grid = np.concatenate((mjd_grid_fine, mjd_grid))

    result = []
    for mjd in tqdm.tqdm(mjd_grid, disable=not progressbar):
        x = run_fit(ds, mjd, bands, func_setup, mode=mode, no_cache=no_cache)

        result.append(x)

    return result
