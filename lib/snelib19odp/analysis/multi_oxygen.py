"""
Multi-zone oxygen mass analysis based on Jerkstrand+2014

1) Measure flux ratio OI5577/(OI6300+OI6364) to estimate temperature assuming LTE (eqn 2)
2) Use it to estimate oxygen mass using eqn 3

Bayesian model (per zone):
- oxygen_mass
- temperature (smaller for outer zone than inner)
- optical_depth (must be smaller for outer zone than inner)

Global params:
- distance (nuisance parameter)

- line_width
- continuum_level[]
- continuum_slope[]

Virtual params:
line_flux[] <- model(oxygen_mass, temperature
amplitude[] <- gaussian(line_flux[], line_width[])

flux{} <- gaussian + continuum

"""

import collections
import numpy as np
import numba
import dynesty
from dynesty import utils as dyfunc
from multiprocessing import Pool
from astropy.cosmology import Planck15 as cosmology
import astropy.units as u
import astropy.constants as aconst
from scipy.special import voigt_profile

from .. import gaussian
from .. import cauchy
from .. import const
from .. import gaussian
from .. import utils



class MultizoneOxygen(object):

    def __init__(self, spec, zones, start5577, stop5577, start63xx, stop63xx, max_width, scale_factor, max_continuum, nlte_mode):
        self.spec = spec
        self.zones = zones
        assert len(self.zones) > 0
        self.start5577 = start5577
        self.stop5577 = stop5577
        self.start63xx = start63xx
        self.stop63xx = stop63xx
        self.max_width = max_width
        self.scale_factor = scale_factor
        self.max_continuum = max_continuum
        self.nlte_mode = nlte_mode

    def make_prior_func(self):
        num_zones = len(self.zones)

        @numba.njit
        def prior(u):
            pass

        return prior
