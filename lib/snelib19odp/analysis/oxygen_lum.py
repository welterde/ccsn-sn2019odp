"""
Oxygen analysis submodule focused on the physical model.
Only needs line luminosity traces as input.

"""
import collections
import numba
import numpy as np
import dynesty
from dynesty import utils as dyfunc
from scipy.stats import multivariate_normal as mv_normal
from scipy.stats import norm

from . import oxygen




LABELS = [
    'MOI',
    'Temp',
    'Dist',
    'Tau',
    'NLTE'
]

#@numba.njit
def prior(u, distance_min, distance_max, nlte_mode, nlte_departure_max, oxygen_mass_min, oxygen_mass_max, temp_min, temp_max, dist_gaussian):
    x = np.empty(len(u))

    # oxygen mass [Msol]
    x[0] = (oxygen_mass_max-oxygen_mass_min)*u[0]+oxygen_mass_min

    # temperature [K]
    x[1] = np.exp((np.log(temp_max)-np.log(temp_min))*u[1]+np.log(temp_min))

    # distance
    if dist_gaussian:
        x[2] = norm.ppf(u[2], loc=distance_min, scale=distance_max)
    else:
        x[2] = (distance_max-distance_min)*u[2] + distance_min
    
    # nuisance parameter: optical depth
    x[3] = np.exp(5*u[3] - 3)
    
    # nuisance parameter: NLTE departure coefficient
    if nlte_mode == 1:
        # running in NLTE mode
        # in NLTE mode the ratio can fall below one
        # range is 0.05 - 1.0
        x[4] = np.exp(np.log(nlte_departure_max)*u[4])

    return x

prior_mv_normal = prior


def likelihood_mv_normal(p, nlte_mode, obs_mean, obs_cov):
    """
    Likelihood function using the multivariate normal as likelihood
    """
    if nlte_mode == 1:
        fluxes = oxygen.create_fluxes(p, distance_idx=2, tau_idx=3, nlte_idx=4)
    else:
        fluxes = oxygen.create_fluxes(p, distance_idx=2, tau_idx=3, nlte_idx=-1)
    flux_5577 = fluxes[0]
    flux_6300 = fluxes[1]
    flux_6364 = fluxes[2]

    x = np.empty(3)
    x[0] = np.log(flux_6300)
    x[1] = flux_6364/flux_6300
    x[2] = flux_5577/flux_6300

    log_l = mv_normal.logpdf(x, mean=obs_mean, cov=obs_cov)

    return log_l


def preproc_dynesty_to_mv_normal(result, add_shell=False, use_shell=False):
    """
    Using a dynesty result structure calculate the mean and covariance matrix needed
    for the multivariate gaussian.
    """
    assert not (add_shell and use_shell)
    if isinstance(result, dyfunc.Results):
        if add_shell:
            raise NotImplemented()
        weights = np.exp(result.logwt - result.logz[-1])
        mean, cov = dyfunc.mean_and_cov(result.samples[:,(0,1,2)], weights)
    else:
        weights = result['weights']
        if add_shell:
            amp = np.log(np.exp(result['log_F6300'].data)+np.exp(result['log_ShAmp'].data))
            samples = np.column_stack((amp, result['R6364'].data, result['R5577'].data))
        elif use_shell:
            samples = np.column_stack((result['log_ShAmp'].data, result['R6364'].data, result['R5577'].data))
        else:
            samples = np.column_stack((result['log_F6300'].data, result['R6364'].data, result['R5577'].data))
        mean, cov = dyfunc.mean_and_cov(samples, weights.data)

    return mean, cov



FitSetup = collections.namedtuple('FitSetup', 'dist_range dist_gaussian mode line_profile_fit_result nlte_departure_max dynesty_method dynesty_nlive_init dynesty_pfrac dynesty_dlogz_init oxygen_mass_min oxygen_mass_max temp_min temp_max additive_shell')

DEFAULT_SETUP = {
    'dynesty_pfrac': 0.5,
    'dynesty_method': 'rwalk',
    'dynesty_nlive_init': 6000,
    'dynesty_dlogz_init': 0.01,
    'nlte_departure_max': 0.1,
    'oxygen_mass_min': 0,
    'oxygen_mass_max': 6,
    'temp_min': 1000,
    'temp_max': 10000,
    'additive_shell': False,
    'dist_gaussian': False
}

def make_setup(**kwargs):
    new_kw = DEFAULT_SETUP.copy()
    new_kw.update(kwargs)
    return FitSetup(**new_kw)
                                  

def run_dynesty_fit(cfg, target_samples=None):

    dist_min, dist_max = cfg.dist_range

    if cfg.mode == 'mv_normal':
        obs_mean, obs_cov = preproc_dynesty_to_mv_normal(cfg.line_profile_fit_result, cfg.additive_shell)
        
        prior_args = (dist_min, dist_max, 1, cfg.nlte_departure_max, cfg.oxygen_mass_min, cfg.oxygen_mass_max, cfg.temp_min, cfg.temp_max, cfg.dist_gaussian)
        logl_args = (1, obs_mean, obs_cov)

        ndim = 5
        sampler = dynesty.DynamicNestedSampler(likelihood_mv_normal, prior_mv_normal, ndim, logl_args=logl_args, nlive=12000, ptform_args=prior_args, method=cfg.dynesty_method, slices=7, enlarge=3, walks=50)

    kwargs = {}
    if target_samples is not None:
        kwargs['n_effective'] = target_samples
    sampler.run_nested(nlive_init=cfg.dynesty_nlive_init, wt_kwargs={'pfrac': cfg.dynesty_pfrac}, dlogz_init=cfg.dynesty_dlogz_init, **kwargs)
    results = sampler.results
    
    samples, weights = results.samples, np.exp(results.logwt - results.logz[-1])
    new_samples = dyfunc.resample_equal(samples, weights)

    return results, new_samples, sampler
