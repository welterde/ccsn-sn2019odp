import numpy
import numba






def vHe5876_to_vEpansion(velocity_he5876):
    """
    Use the relation from Dessart+2016 (MNRAS 458, 1618-1635) sect. 5.3
    to convert the maximum absorption velocity of 5876 line to the
    expansion velocity V_m = sqrt(2*E_kin/M_ej).

    Takes km/s as input and returns km/s
    """
    return (velocity_he5876/1000-2.64)/0.765*1000
    
vHe5876_to_vEpansion_disperson = 1370
