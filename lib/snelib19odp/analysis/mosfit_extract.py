import json
import numpy as np



def load_walkers(fpath):
    # load the json
    with open(fpath, mode='r', encoding='utf-8') as f:
        data = json.loads(f.read())
        if 'name' not in data:
            data = data[list(data.keys())[0]]

    # extract observed photometry
    photo = data['photometry']

    # extract fitted model
    model = data['models'][0]

    setup = model['setup']
    realizations = model['realizations']
    
