import numpy as np
import numba
import collections
from scipy.integrate import quad
import astropy.table as table
from scipy.optimize import curve_fit, differential_evolution
import tqdm
import dynesty
import astropy.units as u
import astropy.constants as aconst


from .. import utils
from . import late_nickel
from .. import noisy_likelihood











@numba.njit(inline='always')
def heating_nico(t, m_ni):
    return late_nickel.gamma_deposition(t, m_ni) + late_nickel.positron_deposition(t, m_ni)

@utils.jit_integrand_function_1d
def integrand_nico(t, p):
    m_ni = p[0]
    td = p[1]
    return t*heating_nico(t, m_ni)*np.exp(t**2 / td**2)

def luminosity_nico(t_mjd, m_ni, td, texpl):
    t = t_mjd - texpl
    pre_factor = 2/td**2 * np.exp(-t**2 / td**2)
    integral = quad(integrand_nico, 0, t, args=(m_ni, td))[0]
    return np.log(pre_factor * integral)



@numba.njit
def prior_nico(u, texpl_min, texpl_max):
    x = np.empty_like(u)
    # M_ni
    x[0] = 1.4*u[0] + 0.01
    # t_d
    x[1] = 50*u[1] + 5
    # texpl
    x[2] = (texpl_max-texpl_min)*u[2] + texpl_min

    return x

Msol_to_g = u.Msun.to(u.g)
lightspeed = aconst.c.cgs.value
kms_to_cms = u.km.to(u.cm)
s_to_day = u.s.to(u.day)

def luminosity_nico_alt(t_mjd, m_ni, m_ej, v_sc, texpl):
    """
    Alternative parametrization based on Mej and v_sc instead of t_d
    """
    opacity = 0.07 # cm^2 / g
    # calculate t_d from this
    t_d = np.sqrt(opacity * m_ej * Msol_to_g * 2 / 13.8 / lightspeed / (v_sc * kms_to_cms)) * u.s.to(u.day)

    # and now just use the usual eqn for this
    return luminosity_nico(t_mjd, m_ni, t_d, texpl)



@numba.njit
def prior_nico_alt(u, vsc_min, vsc_max, texpl_min, texpl_max):
    """
    Alternative parametrization based on Mej and v_sc instead of t_d
    """
    x = np.empty_like(u)
    # M_ni [Msol]
    x[0] = 1.4*u[0] + 0.01
    # M_ej [Msol]
    x[1] = (10-0.6)*u[1]+0.6
    # v_sc [km/s]
    x[2] = (vsc_max-vsc_min)*u[2] + vsc_min
    # texpl
    x[3] = (texpl_max-texpl_min)*u[3] + texpl_min

    return x




def luminosity_nico_r0(t_mjd, m_ni, td, texpl, r0, e_thermal, vsc):
    t = t_mjd - texpl
    pre_factor = 2/td**2 * np.exp(-t**2 / td**2)
    integral = quad(integrand_nico, 0, t, args=(m_ni, td))[0]
    return np.log(pre_factor * integral)






R_SUN_CM = 1.0 * u.Rsun.to(u.cm)
@numba.njit
def prior_nico_r0(u, texpl_min, texpl_max, vsc_min, vsc_max):
    x = np.empty_like(u)
    # M_ni [Msol]
    x[0] = 10*u[0] + 0.01
    # t_d [d]
    #x[1] = 95*u[1] + 5
    x[1] = 60*u[1] + 10
    # texpl [d]
    x[2] = (texpl_max-texpl_min)*u[2] + texpl_min
    # r0 [cm]
    x[3] = R_SUN_CM * np.exp(5*u[3] - 10)
    # E_thermal
    x[4] = np.exp(50*u[4]+1)
    # scale velocity [cm/s]
    x[5] = (vsc_max-vsc_min)*u[5] + vsc_min
    
    return x



def nested_fitting(ds, time_range, texpl_range, time_valid, bolometric_method='lyman', quiet=False, sampler='nested', **extra_kwargs):
    texpl_min, texpl_max = texpl_range
    prior_kwargs = {
        'texpl_min': texpl_min,
        'texpl_max': texpl_max
    }
    return noisy_likelihood.nested_fitting(ds,
                                           model_func=luminosity_nico,
                                           likelihood_func=noisy_likelihood.likelihood_t_mjd,
                                           prior_func=prior_nico,
                                           ndim=3,
                                           time_range=time_range,
                                           prior_kwargs=prior_kwargs,
                                           time_valid=time_valid,
                                           quiet=quiet,
                                           bolometric_method=bolometric_method,
                                           sampler=sampler,
                                           **extra_kwargs)

def nested_fitting_alt(ds, time_range, texpl_range, vsc_range, time_valid, bolometric_method='lyman', quiet=False, sampler='nested', **extra_kwargs):
    texpl_min, texpl_max = texpl_range
    vsc_min, vsc_max = vsc_range
    assert vsc_min > 0
    assert vsc_max > 0
    prior_kwargs = {
        'texpl_min': texpl_min,
        'texpl_max': texpl_max,
        'vsc_min': vsc_min,
        'vsc_max': vsc_max
    }
    return noisy_likelihood.nested_fitting(ds,
                                           model_func=luminosity_nico_alt,
                                           likelihood_func=noisy_likelihood.likelihood_t_mjd,
                                           prior_func=prior_nico_alt,
                                           ndim=4,
                                           time_range=time_range,
                                           prior_kwargs=prior_kwargs,
                                           time_valid=time_valid,
                                           quiet=quiet,
                                           bolometric_method=bolometric_method,
                                           sampler=sampler,
                                           **extra_kwargs)

def nested_fitting_nico_r0(ds, time_range, texpl_range, time_valid, vsc_range=(5000, 15000), bolometric_method='lyman', quiet=False, sampler='nested', **extra_kwargs):
    texpl_min, texpl_max = texpl_range
    vsc_min, vsc_max = vsc_range
    assert vsc_min > 0
    assert vsc_max > 0
    prior_kwargs = {
        'texpl_min': texpl_min,
        'texpl_max': texpl_max,
        'vsc_min': vsc_min,
        'vsc_max': vsc_max
    }
    return noisy_likelihood.nested_fitting(ds,
                                           model_func=luminosity_nico_r0,
                                           likelihood_func=noisy_likelihood.likelihood_t_mjd,
                                           prior_func=prior_nico_r0,
                                           ndim=6,
                                           time_range=time_range,
                                           prior_kwargs=prior_kwargs,
                                           time_valid=time_valid,
                                           quiet=quiet,
                                           bolometric_method=bolometric_method,
                                           sampler=sampler,
                                           **extra_kwargs)

# # TODO: refactor that stuff to common module (maybe noisy_likelihood_sampling ?)
# def nested_fitting(ds, time_range, prior_kwargs={}, num_pregen=12000, num_times=50, bolometric_method='lyman', time_valid=np.array([[1, 1e12]]), quiet=False, texpl_range=None):
#     t_min, t_max = time_range

#     # TODO: implement that once we prior_texpl
#     #if texpl_range is None:
#     #    texpl_range = (ds.transient.prior_t0-0.1, ds.transient.prior_t0+0.1)
#     texpl_min, texpl_max = texpl_range

#     # TODO: outsource this into utility function
#     # pre-generate bolometric lightcurves
#     # TODO: cache this somehow
#     pregen_t = np.empty((num_pregen, num_times), dtype=np.float)
#     pregen_lums = np.empty((num_pregen, num_times), dtype=np.float)
#     for i in tqdm.trange(num_pregen, disable=quiet):
#         #t_grid = np.sort(np.random.uniform(t_min, t_max, M))
#         t_grid = tigerfit.gen_times(num_times, time_valid, t_min, t_max)
#         lc = ds.sample_abs_lc(method=bolometric_method, t_grid=t_grid, tweaks=['correlated-error'])
#         lums = np.log(utils.convert_luminosity(lc))
#         #lums = utils.convert_luminosity(lc)/1e42
#         pregen_lums[i] = lums
#         pregen_t[i] = t_grid

#     logl_kwargs = {
#         'pregen_times': pregen_t,
#         'pregen_lums': pregen_lums,
#         'modfunc': np.vectorize(luminosity_nico),
#         'texpl_min': texpl_min,
#         'texpl_max': texpl_max
#     }
        
        
#     ndim = 2
#     #sampler = dynesty.NestedSampler(likelihood, prior, ndim, ptform_kwargs=prior_kwargs, logl_kwargs=logl_kwargs, method='unif', nlive=1000)
#     sampler = dynesty.DynamicNestedSampler(likelihood, prior, ndim, ptform_kwargs=prior_kwargs, logl_kwargs=logl_kwargs, method='unif', nlive_init=2000)
#     return sampler
