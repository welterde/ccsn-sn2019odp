import numpy as np
import numba
import dynesty
from dynesty import utils as dyfunc
from multiprocessing import Pool
from astropy.cosmology import Planck15 as cosmology
import astropy.units as u
import astropy.constants as aconst
from scipy.optimize import curve_fit, differential_evolution

from .. import utils
from .. import nucdata



# values from https://arxiv.org/pdf/2004.07244.pdf eqn 4 and eqn 5
Q_GAMMA_NI56 = 6.54e43 # erg/s
Q_GAMMA_CO56 = 1.38e43 # erg/2
#T_GAMMA_NI56 = 8.76 # d
T_GAMMA_NI56 = nucdata.NI56_LIFETIME.to(u.d).value
#T_GAMMA_CO56 = 111.4 #d
T_GAMMA_CO56 = nucdata.CO56_LIFETIME.to(u.d).value
Q_POS_CO56 = 4.64e41 # erg/s

@numba.njit(inline='always')
#def deposition_fraction(t, t0, n):
def deposition_fraction(t, t0):
    return 1-np.exp(-t0**2/t**2)
    #return 1/(1+(t/t0)**n)**(2/n)

@numba.njit(inline='always')
def gamma_deposition(t, m_ni):
    return m_ni * (Q_GAMMA_NI56*np.exp(-t/T_GAMMA_NI56) + Q_GAMMA_CO56*np.exp(-t/T_GAMMA_CO56))

@numba.njit(inline='always')
def positron_deposition(t, m_ni):
    return m_ni * Q_POS_CO56 * (np.exp(-t/T_GAMMA_CO56) - np.exp(-t/T_GAMMA_NI56))

#@numba.njit(inline='always')
#def total_deposition(t, t0, m_ni, n):
#    return deposition_fraction(t, t0, n) * gamma_deposition(t, m_ni) + positron_deposition(t, m_ni)

@numba.njit(inline='always')
def total_deposition(t, t0, m_ni):
    return deposition_fraction(t, t0) * gamma_deposition(t, m_ni) + positron_deposition(t, m_ni)


# dist in cm
def sample_fit(ds, bolometric_method, texpl_mjd, time_samples=40, t_min=30, t_max=None):
    bolometric = ds.get_bolometric(bolometric_method)

    t0 = ds.transient.prior_t0

    if t_max is None:
        _,t_max = bolometric.validity_range
        t_max -= t0

    #print('Sampling from %f to %f' % (t_min, t_max))
    
    t_grid = np.sort(np.random.uniform(t0+t_min, t0+t_max, time_samples))

    # sample absolute lc
    lc = ds.sample_abs_lc(method=bolometric_method, t_grid=t_grid, tweaks=['correlated-error'])

    # convert to lum
    lums = utils.convert_luminosity(lc)
    
    t_expl = t_grid - texpl_mjd

    bounds = [
        # T_0
        (10.0, 200.0),
        # M_ni in Msol
        (0.01, 5.0)
    ]
    #bounds_curvefit = ((15, 0.01, 1.9), (400.0, 5.0, 4))
    bounds_curvefit = ((30, 0.01), (400.0, 5.0))

    #guess = (100, 0.02, 2.5)
    #guess = (100, 0.02)
    guess = (np.random.uniform(40, 400), np.random.uniform(0.02, 1))

    model = np.vectorize(total_deposition)
    t = t_expl
    obs = lums

    p, pconv = curve_fit(model, t, obs, p0=guess, bounds=bounds_curvefit)

    return p, pconv


def sample_fit_integral(ds, bolometric_method, t_max=None, t_resolution=0.1):
    bolometric = ds.get_bolometric(bolometric_method)

    t0 = ds.transient.prior_t0

    if t_max is None:
        t_min,t_max = bolometric.validity_range
        t_max -= t0
    else:
        t_min,_ = bolometric.validity_range
        t_min -= t0

    t_grid = np.arange(t_min+t0, t_max+t0, t_resolution)

    # sample absolute lc
    lc = ds.sample_abs_lc(method=bolometric_method, t_grid=t_grid, tweaks=['correlated-error'])

    # convert to lum
    lums = utils.convert_luminosity(lc)

    # integrate the luminosity until t_max
    lums_int = np.trapz(lums, t_grid)
    
    
    
