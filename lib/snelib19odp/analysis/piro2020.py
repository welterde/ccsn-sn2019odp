import numpy as np
import numba
import astropy.constants as aconst

"""
Implementation of the shock cooling from Piro+2020

Parameters:
Re: Initial Radius
n: outer density power-law index
kappa: opacity
vt: expansion velocity
td: diffusion timescale
t: time
Me: envelope mass
"""


C_CGS = aconst.c.cgs

@numba.njit(inline='always')
def diffusion_timescale(opacity, envelope_mass, inner_density_index, outer_density_index, expansion_velocity):
    n = outer_density_index
    s = inner_density_index
    K = (n-3)*(3-s)/4/np.pi/(n-s)
    return np.sqrt(
        (3*opacity*K*envelope_mass)/((n-1)*expansion_velocity*C_CGS)
    )


@numba.njit(inline='always')
def lum_pre_td(t, t_diffusion, outer_density_index, envelope_radius, expansion_velocity, opacity):
    n = outer_density_index
    time_scale = (t_diffusion/t)**(4/(n-2))
    return np.pi*(n-1)/3/(n-5) * C_CGS * envelope_radius * expansion_velocity**2 / opacity * time_scale

@numba.njit(inline='always')
def lum_post_td(t, t_diffusion, outer_density_index, envelope_radius, expansion_velocity, opacity):
    n = outer_density_index
    e_th = np.pi*(n-1)/3/(n-5) * C_CGS * envelope_radius * expansion_velocity**2 / opacity * t_diffusion

    return e_th/t_diffusion * np.exp(
        -0.5 * (t**2/t_diffusion**2 - 1)
    )

@numba.njit(inline='always')
def lum(t, t_diffusion, outer_density_index, envelope_radius, expansion_velocity, opacity):
    if t > t_diffusion:
        return lum_post_td(t, t_diffusion, outer_density_index, envelope_radius, expansion_velocity, opacity)
    else:
        return lum_pre_td(t, t_diffusion, outer_density_index, envelope_radius, expansion_velocity, opacity)



@numba.njit(inline='always')
def photospheric_timescale(opacity, envelope_mass, inner_density_index, outer_density_index, expansion_velocity):
    """
    Photospheric timescale (t_ph in Piro2021, eqn 8)
    """
    n = outer_density_index
    s = inner_density_index
    K = (n-3)*(3-s)/4/np.pi/(n-s)
    return np.sqrt(
        (3*opacity*K*envelope_mass)/2/((n-1)*expansion_velocity**2)
    )

@numba.njit(inline='always')
def photospheric_radius_pre_tph(t, t_photospheric, outer_density_index, expansion_velocity):
    # only valid for t < t_photospheric
    n = outer_density_index
    return (t_photospheric/t) ** (2/(n-1)) * expansion_velocity * t

@numba.njit(inline='always')
def photospheric_radius_post_tph(t, t_photospheric, inner_density_index, outer_density_index, expansion_velocity):
    # only valid for t < t_photospheric
    n = outer_density_index
    s = inner_density_index
    pre_factor = (s-1)/(n-1)
    return ( pre_factor * (t**2 / t_photospheric**2 - 1) + 1 ) ** (-1/(s-1)) * expansion_velocity * t


@numba.njit(inline='always')
def photospheric_radius(t, t_photospheric, inner_density_index, outer_density_index, expansion_velocity):
    if t < t_photospheric:
        return photospheric_radius_pre_tph(t, t_photospheric, outer_density_index, expansion_velocity)
    else:
        return photospheric_radius_post_tph(t, t_photospheric, inner_density_index, outer_density_index, expansion_velocity)

