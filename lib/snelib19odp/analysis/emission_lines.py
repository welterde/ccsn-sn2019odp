import collections
import numpy as np
import numba
import dynesty
from dynesty import utils as dyfunc
from dynesty import plotting as dyplot

from .. import gaussian
from .. import cauchy
from .. import const
from .. import gaussian
from .. import utils


FitSetup = collections.namedtuple('FitSetup', 'spec model start5577 stop5577 start6300_6364 stop6300_6364 redshift redshift_err scale_factor')




@numba.njit(inline="always")
def generation_func_5577(wlen, continuum_level, continuum_slope, amplitude, line_width):
    dwlen = wlen - 5577
    return continuum_level - dwlen*continuum_slope + gaussian.flat_top_gaussian(wlen, 5577, amplitude, line_width)

@numba.njit(inline="always")
def generation_func_6300_6364(wlen, continuum_level, continuum_slope, amplitude_6300, amplitude_6364, line_width):
    dwlen = wlen - 6330
    return continuum_level - dwlen*continuum_slope + gaussian.gaussian(wlen, 6300, amplitude_6300, line_width) + gaussian.gaussian(wlen, 6364, amplitude_6364, line_width)


@numba.njit(inline='always')
def model1_free(p, wavelength, grp_5577, grp_63xx):
    continuum_level_5577 = p[0]
    continuum_level_6300_6364 = p[1]
    
    continuum_slope_5577 = p[2]
    continuum_slope_6300_6364 = p[3]
    
    line_width = p[4]
    
    amplitude_5577 = p[5]
    amplitude_6300 = p[6]
    amplitude_6364 = p[7]

    ret = np.empty_like(wavelength)
    ret[grp_5577] = generation_func_5577(wavelength[grp_5577], continuum_level_5577, continuum_slope_5577, amplitude_5577, line_width)
        
    ret[grp_63xx] = generation_func_6300_6364(wavelength[grp_63xx], continuum_level_6300_6364, continuum_slope_6300_6364, amplitude_6300, amplitude_6364, line_width)
    return ret


@numba.njit
def prior1_free(u, scale_factor):
    x = np.empty_like(u)
    # wavelength offset
    #x[8] = 20*u[8]-10
    x[0] = 10*u[0]-5

    # continuum level 5577
    x[1] = (300*u[1]-150)/scale_factor
    # continuum level 6300,6364
    x[2] = (300*u[2]-150)/scale_factor

    # continuum slope 5577
    x[3] = (0.02*u[3]-0.01)/scale_factor
    # continuum slope 6300,6364
    x[4] = (0.04*u[4]-0.02)/scale_factor

    # line width [A]
    #x[5] = 30*u[5] + 5
    x[5] = np.exp(3.5*u[5])
    #x[5] = 60*u[5] + 5

    # Line Amplitude
    x[6] = np.exp(15*u[6])/scale_factor
    x[7] = np.exp(15*u[7])/scale_factor
    x[8] = np.exp(15*u[8])/scale_factor

    # nuisance param: log f
    x[9] = 12*u[9] - 10

    # nuisance param: distance
    #x[10] = (distance_max-distance_min)*u[10] + distance_min

    return x


@numba.njit(inline='always')
def model2_free(p):
    """
    Model with 2 components and free ratio
    """

@numba.njit(inline='always')
def model2_fixed(p):
    """
    Model with 2 components and fixed ratio
    """

@numba.njit
def likelihood(p, spec_wave, spec_flux, modfunc_idx, scale_factor, grp5577, grp63xx):
    #print(p.shape)
    #print(spec_flux)
    wlen_offset = p[0]
    
    corrected_wlen = spec_wave + wlen_offset
    obs_flux = spec_flux * scale_factor
    
    model_flux = np.empty_like(spec_flux)
    ndim = 0
    if modfunc_idx == 1:
        model_flux[:] = model1_free(p[1:], corrected_wlen, grp5577, grp63xx)*scale_factor
        ndim = 8
    #elif modfunc_idx == 2:
    #    model_lum[:] = model2_free(p[1:], corrected_wlen)*scale_factor
    #elif modfunc_idx == 3:
    #    model_lum[:] = model2_fixed(p[1:], corrected_wlen)*scale_factor

    log_f = p[ndim+1]
    #dist = p[ndim+2]
    
    #model_flux = luminosity2flux(model_lum, dist) * scale_factor
    #print(model_flux)
    
    
    sigma2 = 1+model_flux**2 * np.exp(2*log_f)
    return -0.5 * np.sum(
        (obs_flux - model_flux)**2 / sigma2 + np.log(sigma2)
    )


luminosity2flux = utils.luminosity2flux
redshift2distance = utils.redshift2distance

def model2idx(model_name):
    if model_name == '1_free':
        return 1
    elif model_name == '2_free':
        return 2
    elif model_name == '2_fixed':
        return 3

def setup_dynesty_sampler(cfg):
    if cfg.model == '1_free':
        modidx = 1
        ndim = 10
        prior_func = prior1_free
    elif cfg.model == '2_free':
        modix = 2
    elif cfg.model == '2_fixed':
        modidx = 3
    
    spec_wave = np.array(cfg.spec['restwave'], copy=True)
    spec_flux = np.array(cfg.spec['flux'], copy=True)

    grp_5577 = np.logical_and(spec_wave > cfg.start5577, spec_wave < cfg.stop5577)
    grp_6300_6364 = np.logical_and(spec_wave > cfg.start6300_6364, spec_wave < cfg.stop6300_6364)
    idx = np.logical_or(grp_5577, grp_6300_6364)

    #dist_min, dist_max = redshift2distance(cfg.redshift-cfg.redshift_err/2, cfg.redshift+cfg.redshift_err/2)
    
    prior_args = (cfg.scale_factor,)
    logl_args = (spec_wave[idx], spec_flux[idx], modidx, cfg.scale_factor, grp_5577[idx], grp_6300_6364[idx])
    
    sampler = dynesty.NestedSampler(likelihood, prior_func, ndim, logl_args=logl_args, nlive=2000, ptform_args=prior_args)
    return sampler


def plot_comparison(ax, cfg, results, num_realizations=200):
    spec_wave = np.array(cfg.spec['restwave'], copy=True)
    spec_flux = np.array(cfg.spec['flux'], copy=True)

    grp_5577 = np.logical_and(spec_wave > cfg.start5577, spec_wave < cfg.stop5577)
    grp_63xx = np.logical_and(spec_wave > cfg.start6300_6364, spec_wave < cfg.stop6300_6364)
    idx = np.logical_or(grp_5577, grp_63xx)
    
    samples, weights = results.samples, np.exp(results.logwt - results.logz[-1])
    new_samples = dyfunc.resample_equal(samples, weights)

    model_flux = np.empty_like(spec_flux[idx])
    for i in range(num_realizations):
        p = new_samples[(i*38) % len(new_samples)]
        p = np.array(p, copy=True)
        
        wlen_offset = p[0]
    
        corrected_wlen = spec_wave[idx] + wlen_offset
        if cfg.model == '1_free':
            model_flux[:] = model1_free(p[1:], corrected_wlen, grp_5577[idx], grp_63xx[idx])*(cfg.scale_factor)
        ax.plot(spec_wave[idx], model_flux, alpha=0.1, color='green', lw=1)
