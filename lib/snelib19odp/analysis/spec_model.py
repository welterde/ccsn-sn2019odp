from .. import cache
from . import oxygen


class SpecModel(object):
    def __init__(self, spec, model, params, model_name, sampler_params={}):
        self.spec = spec
        self.model = model
        self.params = params
        self.name = model_name
        self.sampler_params = sampler_params

        self.model_run = False

    def run(self):
        if self.model_run:
            return
        
        if self.model == 'oxygen_gaussline':
            cfg = self.params._replace(spec=self.spec)
            self.results, self.samples, self.sampler = oxygen.run_dynesty_fit(cfg,
                                                                              nlive_init=self.sampler_params['nlive_points'],
                                                                              dynesty_method=self.sampler_params['dynesty_method'],
                                                                              return_sampler=True
                                                                              )

        self.model_run = True
        cache.DEFAULT_CACHE.add_product('spec_model', self.name, self)
    

    
def load_or_make(model_name, spec, model, params, sampler_params, cache_options=set(['same_release'])):
    cached = cache.DEFAULT_CACHE.get_product('spec_model', model_name, cache_options=cache_options)
    if cached is not None:
        return cached
    return SpecModel(spec, model, params, model_name, sampler_params)
