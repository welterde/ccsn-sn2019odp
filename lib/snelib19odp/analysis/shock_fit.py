import numpy as np
import numba
import collections
from scipy.integrate import quad
import astropy.table as table
from scipy.optimize import curve_fit, differential_evolution
import tqdm
import dynesty
import astropy.units as u
import astropy.constants as aconst


from .. import utils
from . import late_nickel
from .. import noisy_likelihood
from . import piro2020
from . import arnett



D_TO_S = u.d.to(u.s)
S_TO_D = u.s.to(u.d)
RSUN_TO_CM = u.Rsun.to(u.cm)
KMS_TO_CMS = u.km.to(u.cm)
C_CGS = aconst.c.cgs.value
MSOL_TO_G = u.Msun.to(u.g)

@numba.njit
def prior_shock_arnett_tdiff(u, vsc_min, vsc_max, texpl_min, texpl_max, r_env_min, r_env_max, t_diff_min, t_diff_max):
    """
    Variant of the prior function with Piro-tdiff as free parameter
    """
    x = np.empty_like(u)
    # M_ni [Msol]
    x[0] = 1.4*u[0] + 0.01
    # M_ej [Msol]
    x[1] = (10-0.6)*u[1]+0.6
    # v_sc [km/s]
    x[2] = (vsc_max-vsc_min)*u[2] + vsc_min
    # texpl [d]
    x[3] = (texpl_max-texpl_min)*u[3] + texpl_min
    # log R_e [Rsun]
    x[4] = (np.log(r_env_max)-np.log(r_env_min))*u[4] + np.log(r_env_min)
    # outer density index []
    x[5] = 15*u[5]+5.5
    # envelope diffusion time scale [d]
    x[6] = (t_diff_max-t_diff_min)*u[6] + t_diff_min
    

    return x

@numba.njit
def prior_shock_arnett_me(u, vsc_min, vsc_max, texpl_min, texpl_max, r_env_min, r_env_max):
    """
    Variant of the prior function with Piro-tdiff as free parameter
    """
    x = np.empty_like(u)
    # M_ni [Msol]
    x[0] = 1.4*u[0] + 0.01
    # M_ej [Msol]
    x[1] = (10-0.6)*u[1]+0.6
    # v_sc [km/s]
    x[2] = (vsc_max-vsc_min)*u[2] + vsc_min
    # texpl [d]
    x[3] = (texpl_max-texpl_min)*u[3] + texpl_min
    # log R_e [Rsun]
    x[4] = (np.log(r_env_max)-np.log(r_env_min))*u[4] + np.log(r_env_min)
    # outer density index []
    x[5] = 15*u[5]+5.5
    # envelope mass [Msun]
    #  fraction of M_ej
    me = x[1] * np.exp(-3*u[6])
    # convert to td
    vsc_cm = x[2] * KMS_TO_CMS
    n = x[5]
    opacity = 0.07
    K = (n-3)*(3-1.1)/4/np.pi/(n-1.1)
    x[6] = np.sqrt(3*opacity*K*me*MSOL_TO_G/(n-1)/vsc_cm/C_CGS) * S_TO_D
    

    return x


def luminosity_shock_arnett_tdiff(t_mjd, m_ni, m_ej, v_sc, texpl, log_r_env, outer_density_index, t_diff_env):
    """
    Variant of the luminosity function combining Arnett and Piro2020 shock cooling
    """
    r_env = np.exp(log_r_env)
    opacity = 0.07 # cm^2 / g

    # calculate the Arnett luminosity
    # TODO: change the nico_alt function to accept the opacity from here
    lum_arnett = arnett.luminosity_nico_alt(t_mjd, m_ni, m_ej, v_sc, texpl)

    # calculate the Piro2020 luminosity
    t_seconds = (t_mjd - texpl)*D_TO_S
    lum_shock = piro2020.lum(t_seconds, t_diff_env*D_TO_S, outer_density_index, r_env*RSUN_TO_CM, v_sc*KMS_TO_CMS, opacity)

    return np.log(np.exp(lum_arnett) + lum_shock)



def nested_fitting_shock_arnett_tdiff(ds, time_range, texpl_range, vsc_range, r_env_range, t_diff_env_range, time_valid, bolometric_method='lyman', quiet=False, sampler='nested', **extra_kwargs):
    texpl_min, texpl_max = texpl_range
    vsc_min, vsc_max = vsc_range
    r_env_min, r_env_max = r_env_range
    t_diff_env_min, t_diff_env_max = t_diff_env_range
    print(f"Texpl Range={texpl_range}")
    assert vsc_min > 0
    assert vsc_max > 0
    prior_kwargs = {
        'texpl_min': texpl_min,
        'texpl_max': texpl_max,
        'vsc_min': vsc_min,
        'vsc_max': vsc_max,
        'r_env_min': r_env_min,
        'r_env_max': r_env_max,
        't_diff_min': t_diff_env_min,
        't_diff_max': t_diff_env_max
    }
    return noisy_likelihood.nested_fitting(ds,
                                           model_func=luminosity_shock_arnett_tdiff,
                                           likelihood_func=noisy_likelihood.likelihood_t_mjd,
                                           prior_func=prior_shock_arnett_tdiff,
                                           ndim=7,
                                           time_range=time_range,
                                           prior_kwargs=prior_kwargs,
                                           time_valid=time_valid,
                                           quiet=quiet,
                                           bolometric_method=bolometric_method,
                                           sampler=sampler,
                                           **extra_kwargs)
def nested_fitting_shock_arnett_me(ds, time_range, texpl_range, vsc_range, r_env_range, time_valid, bolometric_method='lyman', quiet=False, sampler='nested', **extra_kwargs):
    texpl_min, texpl_max = texpl_range
    vsc_min, vsc_max = vsc_range
    r_env_min, r_env_max = r_env_range
    print(f"Texpl Range={texpl_range}")
    assert vsc_min > 0
    assert vsc_max > 0
    prior_kwargs = {
        'texpl_min': texpl_min,
        'texpl_max': texpl_max,
        'vsc_min': vsc_min,
        'vsc_max': vsc_max,
        'r_env_min': r_env_min,
        'r_env_max': r_env_max
    }
    return noisy_likelihood.nested_fitting(ds,
                                           model_func=luminosity_shock_arnett_tdiff,
                                           likelihood_func=noisy_likelihood.likelihood_t_mjd,
                                           prior_func=prior_shock_arnett_me,
                                           ndim=7,
                                           time_range=time_range,
                                           prior_kwargs=prior_kwargs,
                                           time_valid=time_valid,
                                           quiet=quiet,
                                           bolometric_method=bolometric_method,
                                           sampler=sampler,
                                           **extra_kwargs)
