import collections
import numpy as np
import numba
import dynesty



class SpectralModel(object):


    def __init__(self, spec, likelihood_mode='simple'):
        # filter out any NaN in the flux column
        spec = spec[~np.isnan(spec['flux'])]
        
        self.spec_wave = spec['restwave']
        self.spec_flux = spec['flux']
        
        self.logl_args = None
        self.ptform_args = None
        self.dynesty_options = {}
        self.dynesty_run_options = {}

        self.params = {}
        self.param_ctr = 0

        self.likelihood_mode = likelihood_mode
        if likelihood_mode == 'simple':
            self.add_param('log_f')
        else:
            raise ValueError('Unsupported likelihood mode "%s"' % likelihood_mode)
        

    def build_functions(self):
        """
        Construct the prior and model function
        Also returns the number of dimensions the prior function requires
        
        Returns:
        (prior_function, model_function, ndim)
        """
        raise NotImplementedError('Spectral Model subclass needs to implement this!')

    def make_prior_func(self):
        """
        Make the prior function with the needed parameters for the
        selected likelihood mode
        
        Returns:
        (prior_function, num_dimensions)
        """
        if self.likelihood_mode == 'simple':
            logf_idx = self.params['log_f']
            
            @numba.njit(inline='always')
            def prior_func(u):
                p = np.empty_like(u)
                
                # log_f
                p[logf_idx] = 3*u[logf_idx]
                return p

            return (prior_func, 1)
        elif self.likelihood_mode == 'gp':
            @numba.njit(inline='always')
            def prior_func(u):
                p = np.empty_like(u)

                # TODO: the model params
                
                return p

            return (prior_func, 2)

        else:
            raise ValueError('Invalid likelihood mode %s' % self.likelihood_mode)
        
        

    def make_likelihood_func(self, model_func, transform_func, model_param_names):
        spec_wave = self.fit_spec_wave
        spec_flux = self.fit_spec_flux

        scale_factor = self.spec_scale_factor
        
        if self.likelihood_mode == 'simple':
            logf_idx = self.params['log_f']
            
            # create a simple gaussian likelihood function
            @numba.njit(inline='always')
            def likelihood(p):
                log_f = p[logf_idx]

                params = transform_func(p)
                
                log_l = 0
                for i in numba.prange(len(spec_wave)):
                    wlen, obs_flux = spec_wave[i], scale_factor*spec_flux[i]
                    model_flux = scale_factor * model_func(params, i)
                    sigma2 = 1 + model_flux ** 2 * np.exp(2*log_f)
                    log_l += -0.5 * ((obs_flux - model_flux) ** 2 / sigma2 + np.log(sigma2))
                return log_l

            return likelihood
        elif self.likelihood_mode == 'gp':
            # create a model object and GP likelihood based function
            class GpModel(gmodel.Model):
                parameter_names = model_param_names

                def get_value(self, x):
                    return model_func(x)

            raise NotImplementedError('Still work in progress here')
        else:
            raise ValueError('Invalid likelihood mode %s' % self.likelihood_mode)


    def setup_dynesty(self):
        # create the needed functions
        prior_func, likelihood_func, ndim = self.build_functions()

        dynesty_options = {
            'nlive': 6000,
            'method': 'rwalk',
            'enlarge': 3
        }
        dynesty_options.update(self.dynesty_options)

        self.sampler = dynesty.DynamicNestedSampler(likelihood_func, prior_func, ndim, logl_args=self.logl_args, ptform_args=prior_args, **dynesty_options)

    def run_sampler(self, keep_sampler=False):
        self.sampler.run_nested(**self.dynesty_run_options)
        results = sampler.results

        samples, weights = results.samples, np.exp(results.logwt - results.logz[-1])
        new_samples = dyfunc.resample_equal(samples, weights)
        self.trace = new_samples
        self.result = results

        # null the sampler since we cannot cache the object otherwise
        # numba functions cannot be pickled (or the anonymous ones anyway)
        if not keep_sampler:
            self.sampler = None

    def add_param(self, name, alias=[]):
        assert name not in self.params.keys()
        idx = self.param_ctr
        self.param_ctr += 1
        assert name not in self.params
        self.params[name] = idx
        for a in alias:
            assert a not in self.params
            self.params[a] = idx


SpectralRegion = collections.namedtuple('SpectralRegion', 'min_wave max_wave func')


class MultiRegionSpectralModel(SpectralModel):

    def get_spectral_regions(self):
        raise NotImplementedError('Subclass needs to implement get_spectral_regions')

    def get_transform_func(self):
        raise NotImplementedError('Subclass needs to implement get_transform_func')

    def get_prior_func(self):
        raise NotImplementedError('Subclass needs to implement get_prior_func')
    
    def build_functions(self):
        regions = self.get_spectral_regions()

        # TODO: check that they are non-overlapping

        # cut down the spectra to the regions actually covered by 
        spec_covered = np.zeros(len(self.spec_wave), dtype=np.bool)
        for region in regions:
            idx = np.logical_and(self.spec_wave > region.min_wave, self.spec_wave < region.max_wave)
            spec_covered = np.logical_or(spec_covered, idx)

        self.fit_spec_wave = self.spec_wave[spec_covered]
        self.fit_spec_flux = self.spec_flux[spec_covered]
            
        region_idx = np.zeros(len(self.fit_spec_wave), dtype=np.int)

        for i, region in enumerate(regions):
            idx = np.logical_and(self.fit_spec_wave > region.min_wave, self.fit_spec_wave < region.max_wave)
            region_idx[idx] = i

        if len(regions) == 1:
            region_0_func = regions[0].func
            @numba.njit
            def combiner_func(p, spec_idx):
                return region_0_func(p, spec_idx) 
        elif len(regions) == 2:
            region_0_func = regions[0].func
            region_1_func = regions[1].func
            @numba.njit
            def combiner_func(p, spec_idx):
                region_id = region_idx[spec_idx]
                if region_id == 0:
                    return region_0_func(p, spec_idx)
                else:
                    return region_1_func(p, spec_idx)
        elif len(regions) == 3:
            region_0_func = regions[0].func
            region_1_func = regions[1].func
            region_2_func = regions[2].func
            @numba.njit
            def combiner_func(p, spec_idx):
                region_id = region_idx[spec_idx]
                if region_id == 0:
                    return region_0_func(p, spec_idx)
                elif region_id == 1:
                    return region_1_func(p, spec_idx)
                else:
                    return region_2_func(p, spec_idx)
        elif len(regions) == 4:
            region_0_func = regions[0].func
            region_1_func = regions[1].func
            region_2_func = regions[2].func
            region_3_func = regions[3].func
            @numba.njit
            def combiner_func(p, spec_idx):
                region_id = region_idx[spec_idx]
                if region_id == 0:
                    return region_0_func(p, spec_idx)
                elif region_id == 1:
                    return region_1_func(p, spec_idx)
                elif region_id == 2:
                    return region_2_func(p, spec_idx)
                else:
                    return region_3_func(p, spec_idx)
        else:
            raise NotImplementedError('5 and more regions are not supported yet')
        
        trans_func, ndim = self.get_transform_func()

        # TODO: better solution for that..
        model_param_names = self.param_names
        logl_func = self.make_likelihood_func(combiner_func, trans_func, model_param_names)
        prior_func = self.get_prior_func()
        
        return (prior_func, logl_func, ndim)
        
