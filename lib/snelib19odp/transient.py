import os
import numpy as np
import astropy.table as table



transient_data_file = os.path.join(os.path.dirname(__file__), '../../const/transients.txt')


class Transient(object):

    def __init__(self, name, data):
        self.name = name
        self.prior_t0 = data['prior_phase0'][0]
        self.prior_t0_band = data['band'][0]
        self.prior_t0_err = data['prior_phase0_err'][0]
        self.prior_texpl = data['prior_texpl'][0]
        self.redshift = data['redshift'][0]
        self.dist_mpc = data['dist_mpc'][0]
        self.dist_mpc_err = data['dist_mpc_err'][0]








def load_transient(name):
    dat = table.Table.read(transient_data_file, format='ascii')
    #print(dat)
    idx = dat['name'] == name
    assert np.count_nonzero(idx) == 1

    return Transient(name, dat[idx])
