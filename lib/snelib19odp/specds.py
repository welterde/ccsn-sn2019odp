import os
import re
import numpy as np
import astropy.table as table
import sexpdata
import logging

from . import transient
from . import dataloader
from . import extinction
from . import dataset
from . import utils
from . import speccal
from . import cache


BASE_DIR = os.path.join(os.path.dirname(__file__), '../..')
TRACE_BASE_DIR = os.path.join(os.path.dirname(__file__), '../../products/lc')
CFG_FILE = os.path.join(os.path.dirname(__file__), '../../config/spec_datasets.scm')
FILTERS_DIR = os.path.join(os.path.dirname(__file__), '../../const/filters')
PRODUCTS_DIR = os.path.join(os.path.dirname(__file__), '../../products')

# MVP Requirements
# - load all specs for our supernova
# - have the spec objects with restwave, obswave, etc.
# - extinction correction
# - add obs_time and phase info to meta dict
# - 



class SpecDataset(object):
    
    def __init__(self, name, cfg, skip_steps=None):
        self.log = logging.getLogger('specds.%s' % name)

        self.name = name
        self.rev = 1
        self.cfg = cfg

        self.flags = set()

        self.skip_steps = skip_steps
        if skip_steps is None:
            self.skip_steps = set()

        # init state
        self.specs = []

        for rule in self.cfg:
            self.eval_config_rule(rule)

    def eval_config_rule(self, rule):
        assert len(rule) > 0
        assert isinstance(rule[0], sexpdata.Symbol)
        cmd = rule[0].value()

        # allow skipping certain steps
        if cmd in self.skip_steps:
            return

        if cmd == 'transient':
            # sets the transient we are talking about
            assert len(rule) == 2
            self.log.debug('Loading transient info for %s', rule[1])
            self.transient = transient.load_transient(rule[1])
        elif cmd == 'rev':
            assert len(rule) == 2
            self.rev = rule[1]
        elif cmd == 'import':
            assert len(rule) == 2
            self.log.debug('Importing config from dataset %s', rule[1])
            
            sub_ds_cfg = get_dataset_config(rule[1])
            for sub_rule in sub_ds_cfg:
                self.eval_config_rule(sub_rule)
        elif cmd == 'load':
            assert len(rule) == 2
            fname = rule[1]

            self.log.debug('Loading file %s', fname)
            spec = dataloader.load_obs_spec(fname, fluxcal=False)
            meta = dataloader.get_meta(fname)
            spec.meta['obs_mjd'] = dataloader.get_spec_obs_mjd(fname)
            spec.meta['phase'] = spec.meta['obs_mjd'] - self.transient.prior_t0
            spec.meta['name'] = fname.split('.')[0]
            
            self.specs.append(spec)
        elif cmd == 'load:comparison':
            assert len(rule) >= 2
            fname = rule[1]

            redshift = None
            for load_subrule in rule[2:]:
                if load_subrule[0].value() == 'redshift':
                    redshift = load_subrule[1]

            # TODO: add support to exclude spectra
            self.log.debug('Loading comparison json file %s', fname)
            add_specs = dataloader.load_comparison_json_specs(fname, redshift=redshift)
            self.log.debug('Loaded %d additional spectra', len(add_specs))
            self.specs.extend(add_specs)
        elif cmd == 'extinction:mw':
            assert len(rule) > 1
            self.process_extinction(config=rule[1:], mw=True)
            self.flags.add('E:MW')
        elif cmd == 'extinction:host':
            assert len(rule) > 1
            self.process_extinction(config=rule[1:], mw=False)
            self.flags.add('E:H')
        elif cmd == 'fluxcal':
            assert len(rule) > 3
            self.process_fluxcal(config=rule[1:])
            self.flags.add('FC:P')


    def process_extinction(self, config, mw):
        val = config[0]

        self.log.debug('Correcting for extinction (ebv=%f MW=%s)', val, mw)
        
        new_specs = []
        for spec in self.specs:
            new_spec = extinction.spec_extinction_correct_tbl(spec, ebv=val, mw=mw)
            new_specs.append(new_spec)
        self.specs = new_specs

    def process_fluxcal(self, config):
        cfg = utils.convert_dict(config)
        ds = dataset.load_dataset(cfg['dataset'][0])
        band = cfg['band'][0]

        self.log.info('Performing fluxcal using interpolated %s-band and using %s filter curves', band, cfg['filter:system'][0])

        # load the filter curve
        # TODO: move this to dataloader package
        filter_curve = table.Table.read(os.path.join(FILTERS_DIR, cfg['filter:system'][0] + '.%s.dat' % band), format='ascii')
        filter_wave, filter_response = filter_curve['col1'], filter_curve['col2']

        # extra kwargs
        kwargs = {}
        if 'photometric:system' in cfg:
            kwargs['phot_system'] = cfg['photometric:system'][0]
        
        # now perform fluxcal for eac
        new_specs = []
        for spec in self.specs:
            self.log.debug('Doing fluxcal for %s', spec.meta['file_name'])
            new_spec = speccal.fluxcal_spectrum(spec, ds, band, filter_wave, filter_response, log=self.log, **kwargs)
            new_specs.append(new_spec)
        self.specs = new_specs
    

    def __getitem__(self, specname):
        # find the spectrum among the spectra
        for i in range(len(self.specs)):
            s = self.specs[i]
            if s.meta['name'] == specname:
                return s
        
        raise ValueError('Unknown spectrum %s' % specname)
        
    def get_absfit_path(self, specname, fit_rev, line):
        return os.path.join(PRODUCTS_DIR, 'absfit', self.name, f"{line}_{specname}_{fit_rev}.h5")
        
    @property
    def names(self):
        # TODO: cache?
        return list(map(lambda a: a.meta['name'], self.specs))
    
            
    









def get_dataset_config(name):
    # 1) load config file
    with open(CFG_FILE, 'r') as f:
        cfg = sexpdata.load(f)
        
    # 2) extract config for $name
    cfg_ds = None
    for entry in cfg:
        assert len(entry) > 0
        if entry[0] == name:
            cfg_ds = entry[1:]
    if cfg_ds is None:
        raise ValueError('Dataset %s not found' % repr(name))
    return cfg_ds
        

def load_dataset(name, disable_cache=(os.environ.get("SN_DISABLE_CACHE", "no") == "yes"), skip_steps=None):
    cfg_ds = get_dataset_config(name)

    # try to find the rev in the config
    rev = 1
    for entry in cfg_ds:
        if entry[0] == sexpdata.Symbol('rev'):
            rev = entry[1]

    # try to find it in the cache first
    ds = cache.DEFAULT_CACHE.get_dataset('specds', name, rev, set('same_release'))
    if ds is None or disable_cache or skip_steps is not None:
        # create and return dataset object with that config
        ds = SpecDataset(name, cfg_ds, skip_steps=skip_steps)
        
        # write the object to the cache
        if skip_steps is None:
            cache.DEFAULT_CACHE.add_dataset('specds', name, rev, ds)
    return ds


# -*- mode: python-mode; python-indent-offset: 4 -*-
