import os
import numpy as np
import astropy.table as table

from . import const




# TODO: refactor inplace.. we can replace that with a decorator!


def add_restframe(spec, redshift=const.redshift, inplace=False):
    restwave = spec['obswave'] / (1+redshift)
    if inplace:
        newspec = spec
    else:
        newspec = table.Table(spec, copy=True)
    
    newspec['restwave'] = restwave
    return newspec


def mask_telluric(spec, telluric_start=7580, telluric_end=7680, inplace=False):
    if 'mask' not in spec.colnames:
        raise ValueError('Spectrum not properly initialized. Missing mask column')

    idx = np.logical_and(spec['obswave'] > telluric_start, spec['obswave'] < telluric_end)
    if inplace:
        newspec = spec
    else:
        newspec = table.Table(spec, copy=True)
    
    newspec['mask'][idx] = False
    return newspec
    

extra_mask_file = os.path.join(os.path.dirname(__file__), '../../data/specs_extra_mask.txt')

# TODO: cache loading of that file
def mask_extra_regions(spec, fname, inplace=True):
    if 'mask' not in spec.colnames:
        raise ValueError('Spectrum not properly initialized. Missing mask column')
    
    if inplace:
        newspec = spec
    else:
        newspec = table.Table(spec, copy=True)

    extra_masks_tbl = table.Table.read(extra_mask_file, format='ascii')
    db_idx = extra_masks_tbl['filename'] == (os.path.basename(fname))
    
    for entry in extra_masks_tbl[db_idx]:
        range_start = entry['mask_start']
        range_end = entry['mask_end']
        idx = np.logical_and(spec['obswave'] > range_start, spec['obswave'] < range_end)
        newspec['mask'][idx] = False
    
    return newspec
    
def estimate_scalefactor(spec):
    idx = np.logical_and(spec['obswave'] > 3000, spec['obswave'] < 8000)
    spec_diff = np.diff(spec['flux'][idx])
    std = np.nanstd(spec_diff)/np.sqrt(2)
    return 1/std
