import numpy as np
import astropy.table as table
import extinction





# A/E(B-V) coefficient from S&F(2011)
# for R_V = 3.1
# URL: https://iopscience.iop.org/article/10.1088/0004-637X/737/2/103/pdf
EXTINCTION_COEFF = {
    'SDSS u': 4.239,
    'SDSS g': 3.303,
    'SDSS r': 2.285,
    'SDSS i': 1.698,
    'SDSS z': 1.263,
    'SDSS g\'': 3.303,
    'SDSS r\'': 2.285,
    'SDSS i\'': 1.698,
    'SDSS z\'': 1.263,
    
    '2MASS J': 0.723,
    '2MASS H': 0.460,
    '2MASS K': 0.310,
    
    'Landolt U': 4.334,
    'Landolt B': 3.626,
    'Landolt V': 2.742,
    'Landolt R': 2.169,
    'Landolt I': 1.505,
    
    'CTIO U': 4.107,
    'CTIO B': 3.641,
    'CTIO V': 2.682,
    'CTIO Rc': 2.119,
    'CTIO R': 2.119,
    'CTIO Ic': 1.516,
    'CTIO I': 1.516
}

def correct_extinction_rows(df, ebv, rv=3.1, filter_key='band', mag_key='mag', filter_prefix='', ignore_unknown=False):
    if np.abs(rv - 3.1) > 0.1:
        raise ValueError('Rv != 3.1 is not supported!')
    filter_idx = df.index.get_level_values(filter_key)
    bands = np.unique(filter_idx)
    for band in bands:
        filter_name = filter_prefix + band
        if filter_name not in EXTINCTION_COEFF and ignore_unknown:
            continue
        elif filter_name not in EXTINCTION_COEFF:
            raise ValueError('Unknown/Unsupported filter band "%s"' % filter_name)
        abs_extinction = EXTINCTION_COEFF[filter_name] * ebv
        df.loc[filter_idx == band, 'mag'] -= abs_extinction

def correct_extinction_columns(df, ebv, rv=3.1, column_band_mapping=None):
    if np.abs(rv - 3.1) > 0.1:
        raise ValueError('Rv != 3.1 is not supported!')

    for column_name, filter_name in column_band_mapping.items():
        if filter_name not in EXTINCTION_COEFF:
            raise ValueError('Unknown/Unsupported filter band "%s"' % filter_name)
        abs_extinction = EXTINCTION_COEFF[filter_name] * ebv
        df.loc[:, column_name] -= abs_extinction


def spec_extinction_correct(wavelength, flux, ebv):
    # TODO: double-check this..
    a_v = 3.2*ebv
    extinction_curve = extinction.ccm89(wavelength, a_v, 3.1)

    # de-apply extinction
    new_flux = extinction.remove(extinction_curve, flux)
    return new_flux

def spec_extinction_correct_tbl(spec, ebv, mw=True):
    # TODO: double-check this..
    a_v = 3.2*ebv
    if mw:
        extinction_curve = extinction.ccm89(spec['obswave'], a_v, 3.1)
    else:
        extinction_curve = extinction.ccm89(spec['restwave'], a_v, 3.1)

    # de-apply extinction
    new_flux = extinction.remove(extinction_curve, spec['flux'])

    # TODO: propagate error into flux_err ?
    ret = table.Table(spec, copy=True)
    ret['flux'] = new_flux
    
    return ret




class EbvSpec(object):
    def __init__(self, spec):
        parts = spec.split(':')
        self.distribution = parts[0]
        if parts[0] == 'uni':
            self.min_val = float(parts[1])
            self.max_val = float(parts[2])
        elif parts[1] == 'norm':
            self.mean_val = float(parts[1])
            self.std_val = float(parts[2])
        else:
            raise ValueError('invalid distribution type')

    



