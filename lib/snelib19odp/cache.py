import os, socket, pickle, time, threading, subprocess, logging


DEFAULT_CACHE_DIR = os.path.join(os.path.dirname(__file__), '../../.cache')




class Cache(object):

    def __init__(self, cache_dir=DEFAULT_CACHE_DIR):
        self.cache_dir = cache_dir
        self.hostname = socket.gethostname()
        self.git_rev = subprocess.check_output("git describe --tags", shell=True).decode('utf-8').strip()
        self.log = logging.getLogger('cache')
        self.log.debug('Cache set up: hostname=%s, git-rev=%s, cache-dir=%s', self.hostname, self.git_rev, self.cache_dir)
        self.ignore_cache = os.environ.get('SN_IGNORE_CACHE', 'False').lower() == 'true'

        # make sure the cache directory exists
        if not os.path.isdir(cache_dir):
            os.makedirs(cache_dir)


    def get_lookup_dir(self, lookup_key):
        return os.path.join(self.cache_dir, *list(map(str, lookup_key)))

    def lookup_newest_entry(self, lookup_key, cache_options):
        # cache is empty if user says it is
        if self.ignore_cache or 'ignore-cache' in cache_options:
            return None
        
        dir_path = self.get_lookup_dir(lookup_key)
        
        # check if nothing cached yet
        if not os.path.isdir(dir_path):
            return None

        entries = os.listdir(dir_path)
        self.log.debug('Contents of dir: %s', entries)

        # exclude different release (only first part of git-rev)
        if 'same_release' in cache_options:
            entries = filter(lambda a: a.split('_')[0].split('-')[0] == self.git_rev.split('_')[0].split('-')[0], entries)

        # exclude different git rev (if it's something semi-cheap that can easily change)
        if 'same_rev' in cache_options:
            entries = filter(lambda a: a.split('_')[0] == self.git_rev.split('_')[0], entries)

        # sort the entries by name
        entries = sorted(list(entries))
        self.log.debug('Entries after filtering: %s', entries)

        if len(entries) == 0:
            return None
        
        # last one is the newest
        return os.path.join(dir_path, entries[-1])

    def gen_filename(self):
        t = int(time.time())
        return f"{self.git_rev}_{t}_{self.hostname}.pickle"

    def get_dataset(self, ds_type, name, rev, cache_options=set()):
        entry = self.lookup_newest_entry(['ds', ds_type, name, rev], cache_options)
        if entry is None:
            return None

        # TODO: add error handling

        with open(entry, 'rb') as f:
            return pickle.load(f)

    def add_dataset(self, ds_type, name, rev, data):
        # TODO: unify with add_product
        dir_name = self.get_lookup_dir(['ds', ds_type, name, rev])
        if not os.path.isdir(dir_name):
            os.makedirs(dir_name)
        target_fname = os.path.join(dir_name, self.gen_filename())
        self.log.debug('Writing cache entry to %s', target_fname)
        with open(target_fname, 'wb') as f:
            pickle.dump(data, f)


    def get_product(self, product_type, product_key, cache_options=set()):
        entry = self.lookup_newest_entry(['product', product_type, product_key], cache_options)
        if entry is None:
            return None

        try:
            with open(entry, 'rb') as f:
                return pickle.load(f)
        except TypeError as e:
            # most likely reason is that the type was changed in the code
            # and the cached file is no invalid..
            # so return None and have it be regenerated
            self.log.exception('Got error while trying to unpickle cache file..')
            return None
        except EOFError as e:
            # most likely error occured when trying to save the cached element
            self.log.exception('Encountered corrupted cache entry..')
            return None

    def add_product(self, product_type, product_key, data):
        # TODO: unify with add_dataset
        dir_name = self.get_lookup_dir(['product', product_type, product_key])
        if not os.path.isdir(dir_name):
            os.makedirs(dir_name)
        target_fname = os.path.join(dir_name, self.gen_filename())
        self.log.debug('Writing cache entry to %s', target_fname)
        with open(target_fname, 'wb') as f:
            pickle.dump(data, f)
        
    
DEFAULT_CACHE = Cache()
