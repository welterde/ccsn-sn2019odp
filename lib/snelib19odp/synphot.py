import os
import numpy as np
import numba
import speclite
import speclite.filters
from astropy.table import Table
import astropy.constants as const
import astropy.units as u


filters_dir = os.path.join(os.path.dirname(__file__), '../../const/filters')

def load_filter(instrument, band, return_format='speclite'):
    data = Table.read(os.path.join(filters_dir, '%s.%s.dat' % (instrument, band)), format='ascii')
    wave = []
    transmission = []
    for entry in data:
        if len(wave) > 0 and wave[-1] == entry['col1']:
            continue
        wave.append(entry['col1'])
        if entry['col2'] < 0.01 or entry['col1'] < 3790:
            transmission.append(0.0)
        else:
            transmission.append(entry['col2'])
    wave = np.array(wave)*u.Angstrom
    transmission = np.array(transmission)
    transmission[0] = 0
    transmission[-1] = 0

    if return_format == 'speclite':
        f = speclite.filters.FilterResponse(wavelength=wave, response=transmission, meta=dict(group_name='ZTF', band_name=band))
        #print(f)
        return f
    elif return_format == 'table':
        t = Table({'wave': wave, 'transmission': transmission})
        t.meta['band'] = band
        t.meta['instrument'] = instrument
        return t
    else:
        raise ValueError('Unknown return_format %s' % return_format)



def spec2mag(spec, filter_curve):
    # unpack filter curve table
    filter_wave = filter_curve['wave']
    filter_response = filter_curve['transmission']
    
    # interpolate masked pixels in the observed spectrum
    # TODO: check if there is some kind of mask and include that
    spec_valid = ~np.isnan(spec['flux'])
    flux_fixed = np.interp(spec['obswave'], spec['obswave'][spec_valid], spec['flux'][spec_valid])

    # interpolate the filter response onto the observed spec grid
    filter_response_interp = np.interp(spec['obswave'], filter_wave, filter_response)

    pivot_factor2 = np.trapz(spec['obswave']*filter_response_interp, spec['obswave'])/np.trapz(filter_response_interp/spec['obswave'], spec['obswave'])

    spec_flux = np.trapz(spec['obswave']*flux_fixed*filter_response_interp, spec['obswave'])/np.trapz(spec['obswave']*filter_response_interp, spec['obswave'])

    mag_spec = -2.5*np.log10(spec_flux) - 2.5*np.log10(pivot_factor2/(const.c.to(u.Angstrom/u.s).value)) - 48.6

    return mag_spec

#@numba.njit
#def spec2mag_fast(spec_wave, spec_flux, filter_wave, filter_response):
    
