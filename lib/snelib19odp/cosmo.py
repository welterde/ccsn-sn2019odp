import numpy as np
import astropy.units as u
from astropy.cosmology import Planck15 as cosmology


from . import const


def compute_distance_samples_cm(samples):
    redshift = np.random.normal(const.redshift, const.redshift_err, size=samples)
    dist_cm = cosmology.luminosity_distance(redshift).to(u.cm).value
    return dist_cm

def compute_distance_samples_pc(samples):
    redshift = np.random.normal(const.redshift, const.redshift_err, size=samples)
    dist_pc = cosmology.luminosity_distance(redshift).to(u.pc).value
    return dist_pc

def compute_distance_samples_modulus(samples):
    redshift = np.random.normal(const.redshift, const.redshift_err, size=samples)
    dist_pc = cosmology.luminosity_distance(redshift).to(u.pc).value
    dist_mod = 5*np.log10(dist_pc) - 5
    return dist_mod

