{-# LANGUAGE DefaultSignatures, DeriveAnyClass, TypeFamilies, DeriveGeneric #-}
import System.Process (shell, readCreateProcess)
import System.Environment (lookupEnv)
import Control.Monad (when, mapM_)
import Data.Typeable (Typeable)
import Data.Hashable (Hashable(..))
import Data.Binary (Binary(..), Get, encode)
import Data.Word (Word8)
import Data.List (intercalate)
import Control.DeepSeq (NFData)
import GHC.Generics (Generic)
import Development.Shake
import Development.Shake.Command
import Development.Shake.FilePath
import Development.Shake.Util



data DatasetSpec = PhotDS String
                 | SpecDS String
  deriving (Show,Typeable,Eq,Generic,NFData, Binary, Hashable)

type instance RuleResult DatasetSpec = Int

getDatasetRev :: DatasetSpec -> Action Int
getDatasetRev (PhotDS name) = liftIO $ do
  revString <- readCreateProcess (shell $ "bin/dataset_info.py rev phot " ++ name) ""
  return $ read revString
getDatasetRev (SpecDS name) = liftIO $ do
  revString <- readCreateProcess (shell $ "bin/dataset_info.py rev spec " ++ name) ""
  return $ read revString

requireDataset :: DatasetSpec -> Action Int
requireDataset (PhotDS name) = do
  orderOnly [".dsstatus" </> name]
  askOracle $ (PhotDS name)
requireDataset (SpecDS name) = do
  askOracle $ (PhotDS name)

-- utility function to assess if this is a full build or quick build
isFullBuild :: Rules Bool
isFullBuild = liftIO $ do
  env <- lookupEnv "BUILD_TYPE"
  return $ case env of
             Just "full" -> True
             Just "quick" -> False
             Just _ -> False
             Nothing -> False


getSpecFiles :: Action [FilePath]
getSpecFiles = getDirectoryFiles "data/specs/preproc_marshall" ["*.ascii"]


touchDataset :: String -> [String] -> Rules ()
touchDataset name bands = do
  let bands' = intercalate "," bands
  (".dsstatus" </> name) %> \out -> do
    alwaysRerun
    cmd_ "bin/touch_dataset.py" name "--bands" bands' "--interpolated" 



lightcurveProcessing :: Rules ()
lightcurveProcessing = versioned 1 $ do
  -- ingest the raw marshal lightcurve
  "products/marshal_raw_lc.h5" %> \out -> do
    let src = "data/marshal_raw_lc.csv"
    need [src]
    cmd_ "gp3-lc-ingest" "--format=ZTF-marshal" src out
  
  -- interpolate the raw lightcurves
  "products/lc_interpolated_per_ins.h5" %> \out -> do
    let src = "products/marshal_raw_lc.h5"
    need [src]
    cmd_ "gp3-lc-interpolate" "--filter-psf" "--per-instrument" "--truncate-end" src out
  "products/lc_interpolated_combined.h5" %> \out -> do
    let src = "products/marshal_raw_lc.h5"
    need [src]
    cmd_ "gp3-lc-interpolate" "--filter-psf" src out
    
  


specFluxCal :: Rules ()
specFluxCal = versioned 1 $ do
  "products/specs/fluxcal/*.ascii" %> \out -> do
    let src = "data/specs/preproc_marshall" </> (takeFileName out)
    let photSrc = "products/lc_interpolated_per_ins.h5"
    need [src, photSrc, "bin/phot_fluxcal.py"]
    cmd_ "pipenv" "run" "python3" "bin/phot_fluxcal.py" "--overwrite" photSrc src out

specSeqProcessing :: Rules [String]
specSeqProcessing = versioned 3 $ do
  -- generate the combined hdf5 file with headers, etc.

  -- plot the spectral sequence
  "paper/plots/spec_seq.png" %> \out -> do
    need ["plots/spec/seq.py"]
    cmd_ "python3" "plots/spec/seq.py" "-o" out

  return $ ["paper/plots/spec_seq.png"]

bolLCComparison :: Rules [String]
bolLCComparison = versioned 1 $ do
  -- spectral integration qbol
  "products/lum_lc_spectral_integration.fits" %> \out -> do
    specFiles <- getSpecFiles
    let srcFiles = map (\x -> "products/specs/fluxcal" </> x) specFiles
    need $ srcFiles ++ ["bin/integrate_spec.py"]
    cmd_ "pipenv" "run" "bin/integrate_spec.py" "--overwrite" srcFiles "-o" out
  -- spectral integration too...?
  "products/lc/qbol_comp/spec_direct_int.fits" %> \out -> do
    specFiles <- getSpecFiles
    let srcFiles = map (\x -> "data/specs/preproc_marshall" </> x) specFiles
    --let srcFiles = map (\x -> "products/specs/fluxcal" </> x) specFiles
    --specFiles <- getDirectoryFiles "products/specs/fluxcal" ["*.ascii"]
    --let srcFiles = map (\x -> "products/specs/fluxcal" </> x) specFiles
    specFiles <- getDirectoryFiles "products/specs/fluxcal" ["*.ascii"]
    let srcFiles = map (\x -> "products/specs/fluxcal" </> x) specFiles
    need $ srcFiles ++ ["bin/integrate_spec.py"]
    cmd_ "pipenv" "run" "bin/integrate_spec.py" "--overwrite" "--qbol-format" srcFiles "-o" out
  -- GROND direct integration
  "products/lc/qbol_comp/grond_direct_int.fits" %> \out -> do
    let srcFile = "data/grond_lc/current.fits"
    need [srcFile, "bin/qbol_grond_direct_int.py"]
    cmd_ "pipenv" "run" "bin/qbol_grond_direct_int.py" "--overwrite" srcFile out
  -- GROND direct integration E(B-V)_host = 0.2 mag
  "products/lc/qbol_comp/grond_direct_int_0mag2.fits" %> \out -> do
    let srcFile = "data/grond_lc/current.fits"
    need [srcFile, "bin/qbol_grond_direct_int.py"]
    cmd_ "pipenv" "run" "bin/qbol_grond_direct_int.py" "--overwrite" "--host-ebv" "0.2" srcFile out

  -- GROND superbol
  "products/lc/qbol_comp/grond_lb_superbol.fits" %> \out -> do
    let srcFile = "data/grond_lc/current.fits"
    need [srcFile, "bin/qbol_grond_lb_superbol.py"]
    cmd_ "pipenv" "run" "bin/qbol_grond_lb_superbol.py" "--overwrite" srcFile out
    
  -- Lyman g-r based bolometric lightcurve
  "products/lc/qbol_comp/grond_lyman_gr.fits" %> \out -> do
    let srcFile = "data/grond_lc/current.fits"
    need [srcFile, "bin/qbol_grond_lyman.py"]
    cmd_ "pipenv" "run" "bin/qbol_grond_lyman.py" "--overwrite" "--variant" "bc_gr" srcFile out
  -- Lyman g-r based bolometric lightcurve E(B-V)_host = 0.2 mag
  "products/lc/qbol_comp/grond_lyman_gr_0mag2.fits" %> \out -> do
    let srcFile = "data/grond_lc/current.fits"
    need [srcFile, "bin/qbol_grond_lyman.py"]
    cmd_ "pipenv" "run" "bin/qbol_grond_lyman.py" "--overwrite" "--variant" "bc_gr" "--host-ebv" "0.2" srcFile out
  -- Lyman g-i based bolometric lightcurve
  "products/lc/qbol_comp/grond_lyman_gi.fits" %> \out -> do
    let srcFile = "data/grond_lc/current.fits"
    need [srcFile, "bin/qbol_grond_lyman.py"]
    cmd_ "pipenv" "run" "bin/qbol_grond_lyman.py" "--overwrite" "--variant" "bc_gi" srcFile out

  

  -- PLOT: qbol grond comparison plot
  "paper/plots/lc/qbol/comparison.png" %> \out -> do
    let srcFiles = ["products/lc/qbol_comp/grond_direct_int.fits", "products/lc/qbol_comp/grond_lyman_gr.fits", "products/lc/qbol_comp/grond_lyman_gr_0mag2.fits", "products/lc/qbol_comp/grond_lyman_gi.fits", "products/lc/qbol_comp/spec_direct_int.fits", "products/lc/qbol_comp/grond_direct_int_0mag2.fits", "products/lc/qbol_comp/grond_lb_superbol.fits"]
    need $ srcFiles ++ ["plots/qbol_comparison.py"]
    cmd_ "pipenv" "run" "plots/qbol_comparison.py" "-o" out "grond_direct_int" "grond_lb_superbol" "grond_lyman_gr" "grond_lyman_gi" "specs_direct_int" "--min-mjd" "58777" "--max-mjd" "58820"

  -- PLOT: qbol grond comparison plot
  "paper/plots/lc/qbol/ratio_comparison.png" %> \out -> do
    let srcFiles = ["products/lc/qbol_comp/grond_direct_int.fits", "products/lc/qbol_comp/grond_lyman_gr.fits", "products/lc/qbol_comp/grond_lyman_gr_0mag2.fits", "products/lc/qbol_comp/grond_lyman_gi.fits", "products/lc/qbol_comp/spec_direct_int.fits", "products/lc/qbol_comp/grond_direct_int_0mag2.fits", "products/lc/qbol_comp/grond_lb_superbol.fits"]
    need $ srcFiles ++ ["plots/qbol_comparison.py"]
    cmd_ "pipenv" "run" "plots/qbol_comparison.py" "--ratio-plot" "grond_lyman_gr" "-o" out "grond_direct_int" "grond_lb_superbol" "grond_lyman_gi"

  return $ ["paper/plots/lc/qbol/comparison.png", "paper/plots/lc/qbol/ratio_comparison.png"]

bolLC :: Rules [String]
bolLC = versioned 1 $ do
  -- generate the bolometric lightcurve we will use for all further analysis
  "products/lc/pbol/ztf_lyman_gr.fits" %> \out -> do
    let srcFile = "products/lc_interpolated_combined.h5"
    let bin = "bin/compute_lyman_bol.py"
    need [srcFile, bin]
    cmd_ "pipenv" "run" bin "--variant" "bc_gr" srcFile out

  "products/lc/pbol/ztf_lyman_gi.fits" %> \out -> do
    let srcFile = "products/lc_interpolated_combined.h5"
    let bin = "bin/compute_lyman_bol.py"
    need [srcFile, bin]
    cmd_ "pipenv" "run" bin "--variant" "bc_gi" srcFile out

  -- PLOT: plot the ZTF lyman g-r
  "paper/plots/lc/pbol/ztf_lyman_gr.png" %> \out -> do
    let srcFiles = ["products/lc/pbol/ztf_lyman_gr.fits", "products/lc/pbol/ztf_lyman_gi.fits"]
    need $ srcFiles ++ ["plots/qbol_comparison.py"]
    cmd_ "pipenv" "run" "plots/qbol_comparison.py" "-o" out "ztf_lyman_gr" "ztf_lyman_gi"

  return ["paper/plots/lc/pbol/ztf_lyman_gr.png"]


lcPlots :: Rules [String]
lcPlots = do
  fullBuild <- isFullBuild
  versioned 19 $ do
    "paper/plots/lc/overview.png" %> \out -> do
      requireDataset $ PhotDS "SN2019odp_phot"
      let bin = "plots/lc/overview.py"
      need [bin]
      cmd_ "python3" "plots/lc/overview.py" out

    "paper/plots/lc/early.png" %> \out -> do
      requireDataset $ PhotDS "SN2019odp_phot"
      let bin = "plots/lc/overview.py"
      need [bin]
      cmd_ "python3" "plots/lc/overview.py" "--early" out

    -- only build them in full build.. they take about an hour each!
    when fullBuild $ do
      "paper/plots/lc/color_evo_green_red.png" %> \out -> do
        mapM_ requireDataset [PhotDS "SN2019odp_phot", PhotDS "SN2008D", PhotDS "iPTF13bvn", PhotDS "SN2002ap", PhotDS "SN1998bw"]
        let bin = "plots/lc/color_evo.py"
        need [bin]
        cmd_ "python3" "plots/lc/color_evo.py" "-o" out "green_red"

      "paper/plots/lc/color_evo_blue_green.png" %> \out -> do
        mapM_ requireDataset [PhotDS "SN2019odp_phot", PhotDS "SN2008D", PhotDS "iPTF13bvn", PhotDS "SN2002ap", PhotDS "SN1998bw"]
        let bin = "plots/lc/color_evo.py"
        need [bin]
        cmd_ "python3" "plots/lc/color_evo.py" "-o" out "blue_green"

      "paper/plots/lc/color_evo_blue_red.png" %> \out -> do
        mapM_ requireDataset [PhotDS "SN2019odp_phot", PhotDS "SN2008D", PhotDS "iPTF13bvn", PhotDS "SN2002ap", PhotDS "SN1998bw"]
        let bin = "plots/lc/color_evo.py"
        need [bin]
        cmd_ "python3" "plots/lc/color_evo.py" "-o" out "blue_red"
      "paper/plots/lc/color_evo_combined.png" %> \out -> do
        mapM_ requireDataset [PhotDS "SN2019odp_phot", PhotDS "SN2008D", PhotDS "iPTF13bvn", PhotDS "SN2002ap", PhotDS "SN1998bw", SpecDS "SN2019odp"]
        let bin = "plots/lc/color_evo.py"
        need [bin]
        cmd_ "python3" "plots/lc/color_evo.py" "-o" out "blue_red"

    "paper/plots/lc/interp_vs_raw_r.png" %> \out -> do
      need ["products/lc_interpolated_combined.h5", "products/marshal_raw_lc.h5"]
      cmd_ "pipenv" "run" "python3" "plots/raw_lc_interp.py" out "\"r\"_global" "\"r\""
    "paper/plots/lc/interp_vs_raw_g.png" %> \out -> do
      need ["products/lc_interpolated_combined.h5", "products/marshal_raw_lc.h5"]
      cmd_ "pipenv" "run" "python3" "plots/raw_lc_interp.py" out "\"g\"_global" "\"g\""
    "paper/plots/lc/peakmag_comparison.png" %> \out -> do
      cmd_ "pipenv" "run" "plots/compare_peakmag.py" "PTF12os" "SN2019odp" "PTF13bvn" "--band r" "--taddia2019-sample" "SN1993J" "SN2008D" "--cristina2020-sample" "-o" out
  
    "paper/plots/lc/specs_optical_luminosity.png" %> \out -> do
      specFiles <- getSpecFiles
      let srcFiles = map (\x -> "products/specs/fluxcal" </> x) specFiles
      need srcFiles
      cmd_ "pipenv" "run" "bin/integrate_spec.py" "--plot" srcFiles "--plot-dest" out
    "paper/plots/lc/optical_luminosity_comparison.png" %> \out -> do
      need ["products/lum_lc_spectral_integration.fits", "products/lc_interpolated_combined.h5"]
      cmd_ "pipenv" "run" "plots/bolometric_comparison.py" "--luminosity" "PTF12os" "PTF13bvn" "19odp" "--use-phase=auto" "--log-luminosity" "19odp:spectral" "-o" out

  return $ ["paper/plots/lc/overview.png", "paper/plots/lc/early.png", "paper/plots/lc/interp_vs_raw_r.png", "paper/plots/lc/interp_vs_raw_g.png", "paper/plots/lc/peakmag_comparison.png", "paper/plots/lc/specs_optical_luminosity.png", "paper/plots/lc/color_evo_green_red.png", "paper/plots/lc/color_evo_blue_green.png", "paper/plots/lc/color_evo_blue_red.png", "paper/plots/lc/color_evo_combined.png"]


genTables :: (DatasetSpec -> Action Int) -> Rules [String]
genTables getDsRev = do
  versioned 2 $ do
    "paper/tabs/observations_specs.tex" %> \out -> do
      specFiles <- getSpecFiles
      need $ map (\x -> "data/specs/preproc_marshall" </> x) $ specFiles
      need ["bin/print_spec_obs_tbl.py", "data/specs_extra_metadata.txt"]
      cmd_ "bin/runpy" "bin/print_spec_obs_tbl.py" out

  versioned 20 $ do
    "paper/tabs/observations_phot.tex" %> \out -> do
      getDsRev $ PhotDS "SN2019odp_phot"
      need ["data/ztflc_forcefit.h5", "bin/print_phot_tbl.py"]
      cmd_ "bin/runpy" "bin/print_phot_tbl.py" "--min-mjd=58712" "--max-entries=20" out

  versioned 1 $ do
    "paper/tabs/nucdata.tex" %> \out -> do
      need ["tables/nuc_data.py", "lib/snelib19odp/nucdata.py"]
      cmd_ "bin/runpy" "tables/nuc_data.py" "-o" out

  return ["paper/tabs/observations_specs.tex", "paper/tabs/observations_phot.tex", "paper/tabs/nucdata.tex"]


--fitAbsLines :: String -> Rules [String]
--fitAbsLines dataset = versioned 1 $ do
  

config :: ShakeOptions
config = shakeOptions
  { shakeFiles="_build"
  , shakeThreads=0
  , shakeChange=ChangeModtimeOrDigest
  , shakeColor=True
  , shakeProgress=progressSimple
  }

main :: IO ()
main = shakeArgs config $ do
  -- setup special functions
  getDsRev' <- newCache $ getDatasetRev
  getDsRev <- addOracle $ getDsRev'

  -- touch the datasets
  touchDataset "SN2019odp_phot" ["u", "g", "r", "i", "z", "J", "H", "K"]
  touchDataset "SN2008D" ["B", "r", "i"]
  touchDataset "iPTF13bvn" ["g", "r", "i"]
  touchDataset "SN2002ap" ["B", "R", "I"]
  touchDataset "SN1998bw" ["B", "Rc", "Ic"]

  
  lightcurveProcessing

  specFluxCal
  plotsSpecSeq <- specSeqProcessing

  plotsLC <- lcPlots

  plotsBolComparison <- bolLCComparison
  plotsBol <- bolLC
  tables <- genTables getDsRev
  

  -- synthetic photometry
  "products/synphot/*.json" %> \out -> do
    let srcFile = "data/specs/preproc_marshall" </> ((takeFileName out) -<.> "ascii")
    need [srcFile]
    cmd_ "sh" "-c" ("python3 bin/synphot_spec.py --json " ++ srcFile ++ " > " ++ out)

  
    
  -- paper products: plots
  
  

  "paper/plots/specs/filter_comparison.png" %> \out -> do
    let src1 = "data/specs/preproc_marshall/ZTF19abqwtfu_20190827_P200_v1.ascii"
    let src2 = "data/specs/preproc_marshall/ZTF19abqwtfu_20191122_NOT_v1.ascii"
    need [src1, src2]
    filterCurves <- getDirectoryFiles "" ["const/filters/*.dat"]
    need filterCurves
    need ["plots/spec_phot_visual_comparison.py"]
    cmd_ "pipenv" "run" "python3" "plots/spec_phot_visual_comparison.py" "--spec" src1 "early" "--spec" src2 "late" "--scale-filter" "2.0" "ZTF_g" "ZTF_r" "GROND_r" "GROND_i" "GROND_z" "--output" out
  
  -- the final paper
  versioned 6 $ do
    "paper/main.pdf" %> \out -> do
      srcFiles <- getDirectoryFiles "" ["paper/*.tex"]
      allNeeded <- return $ concat $ [srcFiles, plotsBolComparison, plotsSpecSeq, plotsBol, plotsLC, tables]
      need allNeeded
      --need srcFiles
      command_ [Cwd "paper"] "latexmk" ["-pdf", "main.tex"]
