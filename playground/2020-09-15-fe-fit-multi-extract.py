#!/usr/bin/env python
import re, json, os, glob, json
import click
import numpy as np
import astropy.table as table
import matplotlib.pyplot as plt


PHASE_RE = re.compile(r'.*_([\-\d]+)\..*')

def extract(data_arr, data_quantiles, quantile):
    for d, q in zip(data_arr, data_quantiles):
        if q == quantile:
            return d
    raise ValueError('Quantile %f not found' % quantile)



def extract_dir(dirname, phase):
    obs_phase = phase
    result_files = glob.glob(os.path.join(dirname, '*.json'))

    # load template offset table
    tmpl_offsets = table.Table.read("playground/mean_FeII5169_vabs_comb.dat", format='ascii')
    

    offsets = {}
    phases = []
    for row in tmpl_offsets:
        fname = row["template_name"]
        phase = int(PHASE_RE.match(fname).groups(1)[0])
        phases.append(phase)
        offsets[phase] = (row['template_vel'], row['mean_vel_err'])
    tmpl_offsets['PHASE'] = phases
    #print(tmpl_offsets)

    rows = []
    for fname in result_files:
        # extract phase
        phase = int(PHASE_RE.match(fname).groups(1)[0])

        # load data
        with open(fname, 'r') as f:
            data = json.load(f)
            #print(data)

        fit_blueshift = extract(data['blueshift'], data['quantiles'], 50)
        fit_broadening = extract(data['broadening'], data['quantiles'], 50)

        template_offset = offsets[phase][0]

        rows.append((phase, 1000*fit_blueshift+np.abs(template_offset), 1000*fit_broadening))

    t = table.Table(rows=rows, names=('phase', 'velocity', 'broadening'))
    t.sort('phase')
    #if print_tab:
    #    print(t)
    idx = np.abs(t['phase'] - obs_phase) <= 6
    return obs_phase, np.mean(t['velocity'][idx]), np.std(t['velocity'][idx]), np.mean(t['broadening'][idx]), np.std(t['broadening'][idx])
        #print('\n\n\n')
        #print('Velocity: %f km/s' % np.mean(t['velocity'][idx]))
        #print('Uncertainty: %f km/s' % np.std(t['velocity'][idx]))

def main():
    rows = []
    rows.append(extract_dir("/home/welterde/mess/2020/35/SESNspectraLib/odp_20190827_-14", -12))
    rows.append(extract_dir("/home/welterde/mess/2020/35/SESNspectraLib/odp_20190830_-10", -10))
    rows.append(extract_dir("/home/welterde/mess/2020/35/SESNspectraLib/odp_20191003_23", 23))
    rows.append(extract_dir("/home/welterde/mess/2020/35/SESNspectraLib/odp_20191021_42", 42))
    print(rows)

    fig, (ax1,ax2) = plt.subplots(2, 1, sharex=True)
    
    phases = [x[0] for x in rows]
    y = [x[1] for x in rows]
    yerr = [x[2] for x in rows]
    y2 = [x[3] for x in rows]
    yerr2 = [x[4] for x in rows]
    ax1.errorbar(phases, y, yerr=yerr)
    ax1.set_xlabel('Phase [d]')
    ax1.set_ylabel('Velocity [km/s]')
    ax2.errorbar(phases, y2, yerr=yerr2)
    ax2.set_xlabel('Phase [d]')
    ax2.set_ylabel('Broadening [km/s]')
    plt.show()


if __name__ == '__main__':
    main()
