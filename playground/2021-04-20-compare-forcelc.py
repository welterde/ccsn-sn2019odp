import astropy.table as table
import numpy as np
import matplotlib.pyplot as plt

def main():
    t_grid = np.arange(2.458e6+700, 2.458e6+800, 1.0)
    cat = table.Table({'t': t_grid})
    for band in 'gri':
        for grp in ['ztflc', 'ipac']:
            cat['%s_%s' % (grp, band)] = np.zeros(len(t_grid), dtype=np.int)
    
    # the IPAC one
    x = table.Table.read('data/forcephot/mangled_forcedphotometry_req00009772_lc.txt', format='ascii.commented_header')

    fig, axs = plt.subplots(6, sharex=True)
    
    for i, band in enumerate('gri'):
        idx = np.logical_and(x['jd,'] > t_grid.min(), x['jd,'] < t_grid.max())
        idx = np.logical_and(idx, x['filter,'] == ('ZTF_%s' % band))
        axs[2*i].hist(x['jd,'][idx], bins=t_grid, label='IPAC %s' % band, alpha=0.5)
        #axs[0].plot(x['jd,'][idx], x['forcediffimfluxap,'][idx], 'o', label=band)

    #axs[0].legend()

    # the other one
    d = table.Table.read('data/ztflc_forcefit.h5', path='/lc')
    #x, y, err = d["obsmjd"] + 2400000.5, d["ampl"], d["ampl.err"]
    jd = d["obsmjd"] + 2400000.5
    #scalezp = 25
    
    #f0coef = ( 10 ** (-(d["magzp"] - scalezp) / 2.5)
    #           if scalezp is not None
    #           else 1
    #          )

    for i, band, band_ in [[0, 'g', "C0"], [1, 'r', "C2"], [2, 'i', "C1"]]:
        flag = np.asarray(d["filter"] == ('ZTF_%s' % band), dtype="bool")
        idx = np.logical_and(jd > t_grid.min(), jd < t_grid.max())
        idx = np.logical_and(flag, idx)
        axs[2*i+1].hist(jd[idx], bins=t_grid, label='ztflc %s' % band, alpha=0.5)
    for i in range(6):
        axs[i].legend()
    plt.show()

    

if __name__ == '__main__':
    main()

