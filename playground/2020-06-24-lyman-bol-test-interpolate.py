import sys
import numpy as np
import astropy.table as table


def main(infile, destfile):
    tab = table.Table.read(infile, format='ascii')
    jd = tab['col1']
    mag = tab['col2']
    magerr = tab['col3']
    band = tab['col4']

    t_grid = np.linspace(2455944, 2456147)
    mjd = t_grid - 2400000.5
    
    data = [mjd]
    names = ['mjd']
    for band_sel in 'gr':
        idx = band == band_sel

        mag_interp = np.interp(t_grid, jd[idx], mag[idx])
        magerr_interp = np.interp(t_grid, jd[idx], magerr[idx])

        data.append(mag_interp)
        data.append(magerr_interp)
        names.append('mag_"%s"_global' % band_sel)
        names.append('magerr_"%s"_global' % band_sel)
        
    t = table.Table(data, names=names)
    df = t.to_pandas().set_index('mjd')
    print(df)
    df.to_hdf(destfile, key='/lc')


if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])
