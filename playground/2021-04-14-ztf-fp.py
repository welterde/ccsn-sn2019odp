from ztflc import forcephotometry
fp = forcephotometry.ForcePhotometry.from_name('ZTF19abqwtfu')
fp.load_metadata()
fp.load_filepathes()
fp.run_forcefit(verbose=True)
#fp.store()
import astropy.table as table
x = table.Table.from_pandas(fp.data_forcefit)
del x['data_hasnan']
del x['filterid']
del x['fieldid']
del x['status']
del x['ccdid']
del x['amp_id']
del x['qid']
del x['rcid']

x.write('data/ztflc_forcefit.h5', path='/lc')
fp.show_lc()
import matplotlib.pyplot as plt
plt.show()
