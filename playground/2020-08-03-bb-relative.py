import sys
import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u
import astropy.constants as const






if __name__ == '__main__':
    temps = np.linspace(2e3, 2e4, 100)

    l_bol = const.sigma_sb.cgs.value * temps**4
    
    contribution = np.empty(len(temps))
    wlen = np.linspace(4000, 9000, 99999)*u.Angstrom.to(u.cm)
    #wlen_start = 4000*u.Angstrom.to(u.cm)
    #wlen_stop = 9000*u.Angstrom.to(u.cm)
    #sed = np.empty(wlen.shape)
    #flux_factor = 1.1910429526245744e-05

    flux_factor = 2*(const.h.cgs.value)*(const.c.cgs.value)**2
    
    #x_const = 1.4387773538277204
    x_const = (const.h.cgs.value)*(const.c.cgs.value)/(const.k_B.cgs.value)
    #idx = np.logical_and(wlen > wlen_start, wlen < wlen_stop)
    for i,temp in enumerate(temps):
        sed = flux_factor / wlen**5 / (np.exp(x_const/wlen/temp) - 1.0)
        full_int = np.trapz(sed, wlen)
        #opt_int = np.trapz(sed[idx], wlen[idx])
        #contribution[i] = opt_int/full_int
        contribution[i] = np.pi*full_int/l_bol[i]
    #plt.plot(temps, contribution*100)
    plt.plot(temps, np.log10(contribution), label='Optical (4000-9000A)')

    wlen = np.linspace(3700, 24500, 99999)*u.Angstrom.to(u.cm)
    for i,temp in enumerate(temps):
        sed = flux_factor / wlen**5 / (np.exp(x_const/wlen/temp) - 1.0)
        full_int = np.trapz(sed, wlen)
        #opt_int = np.trapz(sed[idx], wlen[idx])
        #contribution[i] = opt_int/full_int
        contribution[i] = np.pi*full_int/l_bol[i]
    #plt.plot(temps, contribution*100)
    plt.plot(temps, np.log10(contribution), label='Optical+NIR (3700-24500A)')
    
    plt.ylabel('log10 Contribution')
    plt.xlabel('Temperature [K]')
    plt.legend()
    plt.grid()
    if len(sys.argv) > 1:
        plt.savefig(sys.argv[1])
    else:
        plt.show()
