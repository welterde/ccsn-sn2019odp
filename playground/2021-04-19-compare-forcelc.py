import matplotlib.pyplot as plt
import astropy.table as table
import numpy as np

def main():
    # the IPAC one
    x = table.Table.read('data/forcephot/mangled_forcedphotometry_req00009772_lc.txt', format='ascii.commented_header')

    fig, axs = plt.subplots(2, sharex=True)
    
    for band in 'gri':
        idx = x['filter,'] == ('ZTF_%s' % band)
        axs[0].plot(x['jd,'][idx], x['forcediffimfluxap,'][idx], 'o', label=band)

    axs[0].legend()

    # the other one
    d = table.Table.read('data/ztflc_forcefit.h5', path='/lc')
    x, y, err = d["obsmjd"] + 2400000.5, d["ampl"], d["ampl.err"]

    scalezp = 25
    
    f0coef = ( 10 ** (-(d["magzp"] - scalezp) / 2.5)
               if scalezp is not None
               else 1
              )

    for band, band_ in [['g', "C0"], ['i', "C2"], ['r', "C1"]]:
        flag = np.asarray(d["filter"] == ('ZTF_%s' % band), dtype="bool")
    
        prop = {**dict(marker="o", color=band_, zorder=5, label=band)}
        axs[1].scatter(x[flag], (y * f0coef)[flag], **prop)
        prop["zorder"] = prop.get("zorder", 5) - 1
        axs[1].errorbar(
            x[flag],
            (y * f0coef)[flag],
            yerr=err[flag],
            ls="None",
            ecolor="0.6",
            **prop
        )
        
    axs[1].axhline(0, ls="--", color="0.5")
    #axs[1].legend()
    
    #print(y)
    #print(y.colnames)
    #print(y['filtercode'])
    #for band in 'gri':
    #    idx = y['filtercode'] == ('z%s' % band)
    #    axs[1].plot(y['hjd'][idx], y['mag'][idx], 'o', label=band)
    axs[1].legend()
    #axs[1].invert_yaxis()
    
    #axs[0].set_yrange(
    plt.show()

    

if __name__ == '__main__':
    main()

