import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import astropy.table as table



def main():
    ####### 19odp
    #lyman_lc = table.Table.read('2020-06-23-lyman-bol-lc-bc.fits')
    #lyman_lc_bc_c = table.Table.read('2020-06-23-lyman-bol-lc-bc-cooling.fits')
    lyman_lc = table.Table.read('2020-06-25-lyman-bol-lc-bc.fits')

    specs_lc = table.Table.read('2020-06-23-specs-bol-lc-v2.txt', format='ascii')

    # estimate peak
    t_peak_odp = specs_lc['mjd'][np.argmax(specs_lc['luminosity'])]


    ###### ph12os
    ph12_lyman = table.Table.read('2020-06-25-lyman-bol-test-ph12os-bolout.fits')
    t_peak_phos = ph12_lyman['mjd'][np.argmax(ph12_lyman['luminosity'])]

    plt.plot(lyman_lc['mjd'] - t_peak_odp, lyman_lc['luminosity'], label='19odp Phot (Lyman BC)')
    #plt.plot(lyman_lc_bc_c['mjd'] - t_peak_odp, lyman_lc_bc_c['luminosity'], label='19odp Phot (Lyman BC Cooling)')
    #plt.plot(lyman_lc['mjd'] - t_peak_odp, lyman_lc_p['luminosity'], label='19odp Phot (Lyman pBC)')
    plt.plot(specs_lc['mjd'] - t_peak_odp, specs_lc['luminosity']/4/np.pi, label='19odp Specs (Integration)')

    plt.plot(ph12_lyman['mjd'] - t_peak_phos, ph12_lyman['luminosity']*2*np.pi, label='PTF12os (Lyman pBC)')
    
    #plt.xlabel('MJD [d]')
    plt.xlabel('Phase [d]')
    #plt.ylabel('Luminosity [erg/s]')
    plt.ylabel('Luminosity [erg/s]')
    
    plt.legend()
    
    plt.show()

if __name__ == '__main__':
    main()
    
