import sys
import numpy as np
import astropy.table as table
import matplotlib.pyplot as plt

def main():
    tab = table.Table.read('2020-06-24-lyman-bol-test-ph12os.dat', format='ascii')
    #tab = table.Table.read('2020-06-24-lyman-bol-test-ph13bvn.dat', format='ascii')
    jd = tab['col1']
    mag = tab['col2']
    magerr = tab['col3']
    band = tab['col4']

    idx = band == 'r'

    #dist_mod = 31.48 # mine
    dist_mod = 32.14 # paper

    #dust = 0.19
    extra = 2.8*0.29 + 0.0437*2.8
    
    plt.plot(jd[idx] - 2400000.5, mag[idx] - dist_mod - extra, label='r')
    plt.legend()
    plt.gca().invert_yaxis()
    plt.ylabel('Absolute Magnitude')
    plt.xlabel('MJD [d]')

    plt.show()


if __name__ == '__main__':
    main()
