"""
Oxygen mass analysis similar to Jerkstrand+2014

1) Measure flux ratio OI5577/(OI6300+OI6364) to estimate temperature assuming LTE (eqn 2)
2) Use it to estimate oxygen mass using eqn 3

Bayesian model:
- oxygen_mass
- temperature
- distance (nuisance parameter)

- line_width
- continuum_level[]
- continuum_slope[]

Virtual params:
line_flux[] <- model(oxygen_mass, temperature
amplitude[] <- gaussian(line_flux[], line_width[])

flux{} <- gaussian + continuum

"""
import collections
import numpy as np
import numba
import dynesty
from dynesty import utils as dyfunc
from multiprocessing import Pool
from astropy.cosmology import Planck15 as cosmology
import astropy.units as u
import astropy.constants as aconst
from scipy.special import voigt_profile

from .. import gaussian
from .. import cauchy
from .. import const
from .. import gaussian
from .. import utils

FitSetup = collections.namedtuple('FitSetup', 'spec smp start5577 stop5577 start6300_6364 stop6300_6364 extra_lines')


PARAMETER_LABELS = [
    'M_ox',
    'T',
    'line_width',
    'continuum5577',
    'slope5577',
    'continuum6300',
    'slope6300',
    'distance',
    'wave_offset',
    'optical_depth',
    'log_f',
    'beta_ratio'
]

@numba.njit
def prior(u, distance_min, distance_max, num_extra_lines):
    x = np.empty(len(u))

    # oxygen mass [Msol]
    #x[0] = 10*u[0]+1e-4
    # log oxygen mass [Msol]
    #x[0] = 30*u[0]-25
    x[0] = 5*u[0]

    # temperature [K]
    x[1] = 1e4*u[1]+1000

    # line width [A]
    #x[2] = 30*u[2] + 5
    x[2] = np.exp(3.5*u[2])
    #x[2] = 60*u[2] + 5

    # continuum level 5577
    x[3] = 30*u[3]-15

    # continuum slope 5577
    x[4] = 0.02*u[4]-0.01

    # continuum level 6300,6364
    x[5] = 30*u[5]-15

    # continuum slope 6300,6364
    x[6] = 0.04*u[6]-0.02

    # distance
    x[7] = (distance_max-distance_min)*u[7] + distance_min

    # wavelength offset
    #x[8] = 20*u[8]-10
    x[8] = 10*u[8]-5
    #x[8] = 5*u[8]-2.5

    # nuisance parameter: optical depth
    x[9] = np.exp(6*u[9] - 5)

    # nuisance parameter: log f
    x[10] = 12*u[10] - 10

    # nuisance parameter: 5577 to 6300 escape probability
    # beta_5577 always bigger than beta_6300, so the ratio has to be above 1
    # However in NLTE the ratio can fall below one!
    #x[11] = 3*u[11] + 1
    #x[11] = 6.5*u[11] - 6
    x[11] = np.exp(13.5*u[11] - 13)

    for i in range(num_extra_lines):
        x[12+i] = 8*u[12+i] - 4
    
    return x


def compute_pre_factor_63xx(wavelength):
    # M_ion / atomic_number / m_p
    N = u.solMass.to(u.g) / 16 / aconst.m_p.cgs.value
    
    if wavelength == 6300:
        # from Jerkstrand+2017 table 1 for O I,6300
        # A = 5.6e-3
        # from https://physics.nist.gov/cgi-bin/ASD/lines1.pl?spectra=O+I&limits_type=0&low_w=&upp_w=&unit=1&submit=Retrieve+Data&de=0&format=0&line_out=0&en_unit=0&output=0&bibrefs=1&page_size=15&show_obs_wl=1&show_calc_wl=1&unc_out=1&order_out=0&max_low_enrg=&show_av=2&max_upp_enrg=&tsb_value=0&min_str=&A_out=0&intens_out=on&max_str=&allowed_out=1&forbid_out=1&min_accur=&min_intens=&conf_out=on&term_out=on&enrg_out=on&J_out=on
        # M1 transition 630.0304 nm
        A = 5.63e-3
    elif wavelength == 6364:
        # using A_6300 = 3*A_6364 from Elmhamdi+2011
        #A = 5.6e-3 / 3
        # from NIST too
        # M1 transition  636.3776 nm (matches that /3 method.. solid!)
        A = 1.83e-3
    else:
        return np.nan

    # h*\nu
    h_nu = aconst.h.cgs.value * aconst.c.cgs.value / (wavelength*u.Angstrom.to(u.cm))
    
    # from Jerkstrand+2017 table 1 for O I,6300
    # upper state the same for 6300 and 6364
    g_u = 5

    # taken from Jerkstrand+2014 (page 3699 left)
    # approximation treating the split ground state in a combined way
    g_g = 9

    return N*A*h_nu*g_u/g_g

LUM_6300_PRE_FACTOR = compute_pre_factor_63xx(6300)
LUM_6364_PRE_FACTOR = compute_pre_factor_63xx(6364)

# E_u / k_B
# 1.97 eV from Jerkstrand+2017 (page 17 tab 1)
LUM_63XX_TEMP_FACTOR = 1.97 / 8.617333262145e-5

@numba.njit(inline='always')
def calculate_line_luminosity_63xx(oxygen_mass, temperature, optical_depth, line):
    """
    Equation 3 from Jerkstrand+2014 (MNRAS439,3694–3703 (2014))
    """

    if line == 6300:
        beta = (1-np.exp(-optical_depth))/optical_depth
        pre_factor = LUM_6300_PRE_FACTOR
    elif line == 6364:
        beta = 3*(1-np.exp(-optical_depth/3))/optical_depth
        pre_factor = LUM_6364_PRE_FACTOR
    else:
        beta = optical_depth * np.nan
        pre_factor = 1.0

    lum = pre_factor * beta * oxygen_mass / np.exp(LUM_63XX_TEMP_FACTOR / temperature)
    return lum

@numba.njit(inline='always')
def calculate_line_luminosity_5577(oxygen_mass, temperature, optical_depth, beta_ratio):
    # Assuming a ratio of 1.5 of beta_5577/beta_6300
    #beta_6300 = (1-np.exp(-optical_depth))/optical_depth
    #beta_5577 = 1.5*beta_6300
    
    # use the 5577/6300,6364 ratio from equation 2 from Jerkstrand+2014 (MNRAS439,3694–3703 (2014))
    lum_6300 = calculate_line_luminosity_63xx(oxygen_mass, temperature, optical_depth, 6300)
    return lum_6300 * 38 * np.exp(-25790 / temperature) * beta_ratio

luminosity2flux = utils.luminosity2flux
redshift2distance = utils.redshift2distance

@numba.njit(inline="always")
def generation_func_5577(wlen, continuum_level, continuum_slope, amplitude, line_width):
    dwlen = wlen - 5577
    return continuum_level - dwlen*continuum_slope + gaussian.flat_top_gaussian(wlen, 5577, amplitude, line_width)

@numba.njit(inline="always")
def generation_func_6300_6364(wlen, continuum_level, continuum_slope, amplitude_6300, amplitude_6364, line_width, extra_lines, extra_lines_amps):
    dwlen = wlen - 6330
    accu = continuum_level
    for i in range(len(extra_lines)):
        #accu += cauchy.cauchy(wlen, extra_lines[i], extra_lines_amps[i], line_width)
        accu += gaussian.gaussian(wlen, extra_lines[i], extra_lines_amps[i], line_width)
        #accu += gaussian.flat_top_gaussian(wlen, extra_lines[i], extra_lines_amps[i], line_width)
    #return accu - dwlen*continuum_slope + cauchy.cauchy(wlen, 6300, amplitude_6300, line_width) + cauchy.cauchy(wlen, 6364, amplitude_6364, line_width)
    return accu - dwlen*continuum_slope + gaussian.gaussian(wlen, 6300, amplitude_6300, line_width) + gaussian.gaussian(wlen, 6364, amplitude_6364, line_width)
    #return accu - dwlen*continuum_slope + gaussian.flat_top_gaussian(wlen, 6300, amplitude_6300, line_width) + gaussian.flat_top_gaussian(wlen, 6364, amplitude_6364, line_width)
    


@numba.njit(inline='always')
def create_fluxes(p):
    oxygen_mass = p[0]
    lte_temp = p[1]
    distance = p[7]
    optical_depth = np.exp(p[9])
    beta_ratio = p[11]

    # estimate line fluxes given oxygen mass and lte_temp
    flux_5577 = luminosity2flux(calculate_line_luminosity_5577(oxygen_mass, lte_temp, optical_depth, beta_ratio), distance)
    flux_6300 = luminosity2flux(calculate_line_luminosity_63xx(oxygen_mass, lte_temp, optical_depth, 6300), distance)
    flux_6364 = luminosity2flux(calculate_line_luminosity_63xx(oxygen_mass, lte_temp, optical_depth, 6364), distance)
    
    r = np.empty(3)
    r[0] = flux_5577
    r[1] = flux_6300
    r[2] = flux_6364
    
    return r

@numba.njit
def likelihood(p, spec_wave, spec_flux, grp_5577, grp_6300_6364, extra_lines):
    #oxygen_mass = np.exp(p[0])
    oxygen_mass = p[0]
    lte_temp = p[1]

    line_width = p[2]

    continuum_level_5577 = p[3]/1e17
    continuum_level_6300_6364 = p[4]/1e17

    continuum_slope_5577 = p[5]/1e17
    continuum_slope_6300_6364 = p[6]/1e17

    distance = p[7]

    wlen_offset = p[8]

    optical_depth = p[9]

    log_f = p[10]

    #beta_ratio = np.exp(p[11])
    beta_ratio = p[11]

    extra_lines_amps = np.exp(p[12:])/1e17

    fluxes = create_fluxes(p)
    flux_5577 = fluxes[0]
    flux_6300 = fluxes[1]
    flux_6364 = fluxes[2]

    # calculate amplitude from flux and line width
    # which is very easy since the gaussian we use is normalized!
    amplitude_5577 = flux_5577
    amplitude_6300 = flux_6300
    amplitude_6364 = flux_6364

    log_l = 0

    # TODO: integrate uncertainty from specs
    
    for i in numba.prange(len(spec_wave)):
        wlen, obs_flux = spec_wave[i], 1e18*spec_flux[i]
        if grp_5577[i]:
            model_flux = 1e18*generation_func_5577(wlen+wlen_offset, continuum_level_5577, continuum_slope_5577, amplitude_5577, line_width)
            sigma2 = 1 + model_flux ** 2 * np.exp(2*log_f)
            log_l += -0.5 * ((obs_flux - model_flux) ** 2 / sigma2 + np.log(sigma2))
        elif grp_6300_6364[i]:
            model_flux = 1e18*generation_func_6300_6364(wlen+wlen_offset, continuum_level_6300_6364, continuum_slope_6300_6364, amplitude_6300, amplitude_6364, line_width, extra_lines, extra_lines_amps)
            sigma2 = 1 + model_flux ** 2 * np.exp(2*log_f)
            log_l += -0.5 * ((obs_flux - model_flux) ** 2 / sigma2 + np.log(sigma2))

    return log_l







def run_dynesty_fit(cfg):
    spec_wave = cfg.spec['restwave']
    spec_flux = cfg.spec['flux']

    grp_5577 = np.logical_and(spec_wave > cfg.start5577, spec_wave < cfg.stop5577)
    grp_6300_6364 = np.logical_and(spec_wave > cfg.start6300_6364, spec_wave < cfg.stop6300_6364)

    dist_min, dist_max = redshift2distance(const.redshift-const.redshift_err, const.redshift+const.redshift_err)
    
    prior_args = (dist_min, dist_max, len(cfg.extra_lines))
    logl_args = (spec_wave, spec_flux, grp_5577, grp_6300_6364, cfg.extra_lines)

    # maybe run with multiple CPUs
    pool = None
    queue_size = None
    if cfg.smp > 1:
        pool = Pool(cfg.smp)
        queue_size = cfg.smp
    
    sampler = dynesty.DynamicNestedSampler(likelihood, prior, 12+len(cfg.extra_lines), logl_args=logl_args, nlive=12000, pool=pool, queue_size=queue_size, ptform_args=prior_args, method='unif')

    sampler.run_nested(nlive_init=1000)
    results = sampler.results

    samples, weights = results.samples, np.exp(results.logwt - results.logz[-1])
    mean, cov = dyfunc.mean_and_cov(samples, weights)
    print('\tMean: %s' % repr(mean))
    print('\n\t\t'.join(('\tCov: %s' % repr(cov)).split('\n')))

    new_samples = dyfunc.resample_equal(samples, weights)

    return results, new_samples
