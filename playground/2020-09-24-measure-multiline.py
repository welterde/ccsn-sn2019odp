import os,re,h5py,click
import numpy as np
import astropy.time as time
from astropy.table import Table
from scipy.ndimage import gaussian_filter1d
import matplotlib.pyplot as plt
from scipy.optimize import differential_evolution

#REDSHIFT = 0.014353
REDSHIFT = 0.0143

CONFIG = [
    (5876, (-500, 100), 'HeI/'),
    (7774, (-500, 100), 'OI/')
]


@click.command()
@click.argument('sseq_file')
def main(sseq_file):
    f = h5py.File(sseq_file, 'r')
    specs = []
    
    for x in f:
        for y in f[x]:
            spec = Table.read(sseq_file, format='hdf5', path='%s/%s' % (x,y))
            phase = time.Time(spec.meta.get('DATE-OBS', spec.meta['date'])).jd - 2458735.8720
            spec.meta['phase'] = phase
            spec.meta['jd'] = time.Time(spec.meta.get('DATE-OBS', spec.meta['date'])).jd
            #if phase > max_phase:
            #    continue
            specs.append(spec)

    

    fig, (ax1, ax2, ax3) = plt.subplots(3, sharex=True)
    
    for line_center, wlen_range_rel, line_name in CONFIG:
        wlen_range = (line_center + wlen_range_rel[0], line_center + wlen_range_rel[1])

        offset_estimate = []
        center_fit = []
        sigma_fit = []
        amplitude_fit = []
        eq_width = []
        
        for spec in specs:
            wave = spec['wavelength']/(1+REDSHIFT)
            flux = spec['flux']*1e16
            flux_smoothed = gaussian_filter1d(flux, 3)
            
            # detect line..
            idx = np.logical_and(wave > wlen_range[0], wave < wlen_range[1])
            
            min_idx2 = np.argmin(flux_smoothed[idx])
            min_wlen = wave[idx][min_idx2]
            #print(spec.meta['phase'], min_wlen)
            offset_estimate.append(min_wlen)

            # try to fit it
            def model(continuum, amplitude, sigma, center):
                x = wave[idx]
                return continuum - amplitude/sigma/np.sqrt(2*np.pi)*np.exp(-(x - center)**2/2/sigma**2)
            #def func(x, continuum, amplitude, sigma, center):
            def func(p):
                continuum, amplitude, sigma, center = p[0], p[1], p[2], p[3]
                x = wave[idx]
                model = continuum - amplitude/sigma/np.sqrt(2*np.pi)*np.exp(-(x - center)**2/2/sigma**2)
                return np.sum(np.abs(model - flux[idx])**2)

            #p0 = (np.max(flux_smoothed[idx]), # continuum
            #      10, # amplitude
            #      10, # sigma
            #      min_wlen # center
            #      )
            bounds = [
                (1, 1000),
                (0, 100000),
                (10, 90),
                wlen_range
            ]
            #print(p0)
            # try:
            #     popt, pcov = curve_fit(func, wave[idx], flux[idx], p0=p0)
            # except RuntimeError:
            #     center_fit.append(np.nan)
            #     sigma_fit.append(np.nan)
            #     continue
            result = differential_evolution(func, bounds)
            print(result)

            center_fit.append(result.x[3])
            sigma_fit.append(result.x[2])
            amplitude_fit.append(result.x[1])

            continuum = result.x[0]
            idx2 = np.logical_and(idx, flux < continuum)
        
            wavelength_element = np.mean(np.diff(wave[idx]))
            eqw = np.sum(1 - flux[idx2]/continuum)*wavelength_element
            if np.count_nonzero(idx2) > 5:
                eq_width.append(eqw)
            else:
                eq_width.append(np.nan)
                
            if False:
                plt.plot(wave[idx], flux[idx])
                plt.plot(wave[idx], flux_smoothed[idx])
                plt.plot(wave[idx], model(*result.x))
                plt.show()
        
        

        t = Table({
            'phase': [spec.meta['phase'] for spec in specs],
            'offset': offset_estimate,
            'center_fit': center_fit,
            'amplitude_fit': amplitude_fit,
            'sigma_fit': sigma_fit,
            'eq_width': eq_width})
        t.sort('phase')
    

        t['width_velocity'] = t['sigma_fit']/t['center_fit']*300e3
        print(t)
    
        velocity = (t['center_fit'] - line_center)/line_center*300e3*(-1)
        amplitude = t['amplitude_fit']
        width = t['width_velocity']
    
        idx = t['sigma_fit'] > 15
        #plt.plot(t['phase'][idx], t['center_fit'][idx])
        #plt.plot(t['phase'][idx], t['eq_width'][idx])
        
        ax1.plot(t['phase'][idx], velocity[idx], label='%s%d' % (line_name, line_center))
        ax1.set_xlabel('Phase [d]')
        ax1.set_ylabel('Velocity [km/s]')
        ax1.legend()
        ax2.plot(t['phase'][idx], amplitude[idx], label='%s%d' % (line_name, line_center))
        ax2.set_ylabel('Amplitude')
        ax2.set_xlabel('Phase [d]')
        ax2.legend()
        ax3.plot(t['phase'][idx], width[idx], label='%s%d' % (line_name, line_center))
        ax3.set_ylabel('Width [km/s]')
        ax3.set_xlabel('Phase [d]')
        ax3.legend()
        #plt.plot(t['phase'][idx], t['width_velocity'][idx])
    plt.show()





if __name__ == '__main__':
    main()
