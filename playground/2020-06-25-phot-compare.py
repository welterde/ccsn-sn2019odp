import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import astropy.table as table
from astropy.cosmology import Planck15 as cosmology
import astropy.units as u

def plot(df, obj_label, key, band_label, z, extra=0.0):
    label = '%s %s-band' % (obj_label, band_label)

    #print(df.index)
    mjd = df.index.get_level_values('mjd')
    #print(np.argmin(df[key]))
    #print(df[key].idxmin())
    t_peak = df[key].idxmin()

    dist = cosmology.luminosity_distance(z).to(u.pc).value
    dist_mod = 5*np.log10(dist) - 5

    abs_mag = df[key] - dist_mod
    
    plt.plot(mjd - t_peak, abs_mag + extra, label=label)

def main():
    # 
    #lyman_lc_p = table.Table.read('2020-06-23-lyman-bol-lc-pbc.fits')
    odp_lc = pd.read_hdf('../products/lc_interpolated_combined.h5')
    # PTF12os
    #ph12_lyman = table.Table.read('2020-06-24-lyman-bol-test-ph12os-bolout.fits')

    ptf12os_lc = pd.read_hdf('2020-06-24-lyman-bol-test-ph12os.h5')

    plot(odp_lc, 'SN2019odp', 'mag_"r"_global', 'r', z=0.014353)
    plot(ptf12os_lc, 'PTF12os', 'mag_"r"_global', 'r', z=0.00449, extra=-0.66)

    plt.legend()
    plt.xlabel('Phase [d]')
    plt.ylabel('Absolute Magnitude')
    plt.gca().invert_yaxis()
    plt.show()

if __name__ == '__main__':
    main()
    
