import matplotlib.pyplot as plt
import numpy as np


# how does pure constant v_ej expansion look like?
# ie. R = v_ej * t

# [day]
t_grid = np.linspace(0, 200)

# vej in km/s
for vej in [1000, 2000, 5000, 10000]:
    radius_km = vej*t_grid*3600*24 + 100e6
    log_radius = np.log(radius_km)
    plt.plot(t_grid, log_radius, label='vej=%f km/s' % vej)
plt.legend()
plt.xlabel('Time [d]')
plt.ylabel('log R')
plt.show()
