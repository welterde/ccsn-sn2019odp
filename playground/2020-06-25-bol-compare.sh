#!/bin/sh
rm playground/2020-06-25-lyman-bol-test-ph12os-bolout.fits
rm playground/2020-06-25-lyman-bol-lc-bc.fits
pipenv run python3 bin/compute_lyman_bol.py --mw-extinction 0.168 0.116 --distance-file playground/2020-06-24-lyman-bol-test-distance playground/2020-06-24-lyman-bol-test-ph12os.h5 playground/2020-06-25-lyman-bol-test-ph12os-bolout.fits
#pipenv run python3 bin/compute_lyman_bol.py --distance-file playground/2020-06-24-lyman-bol-test-distance playground/2020-06-24-lyman-bol-test-ph12os.h5 playground/2020-06-25-lyman-bol-test-ph12os-bolout.fits
#pipenv run python3 bin/compute_lyman_bol.py products/lc_interpolated_combined.h5 playground/2020-06-25-lyman-bol-lc-bc.fits
pipenv run python3 bin/compute_lyman_bol.py --mw-extinction 0.64 0.442 products/lc_interpolated_combined.h5 playground/2020-06-25-lyman-bol-lc-bc.fits
