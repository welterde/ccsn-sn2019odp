#!/usr/bin/env python3
import click
import sys, os, functools
import numpy as np
import astropy.units as u
from astropy.table import Table
import json as jsonlib
import numba
import tqdm
from scipy import optimize
import matplotlib.pyplot as plt
import astropy.time as time
import time as ostime
import h5py
import dynesty
from dynesty import plotting as dyplot
from dynesty import utils as dyfunc

A_TO_CM = u.Angstrom.to(u.cm)

@numba.njit
def bb(temp, normalization, wavs):
    wavs_cm = wavs*A_TO_CM
    flux_factor = 1.1910429526245744e-05
    x_const = 1.4387773538277204
    pre_factor = flux_factor * normalization
    return pre_factor / wavs_cm**5 / (np.exp(x_const / wavs_cm / temp) - 1.0)


@numba.njit
def likelihood(p, wave, obs_spec):
    #ln_temp, ln_radius = p
    temp, ln_radius = p

    #sed = bb(np.exp(ln_temp), np.exp(ln_radius), wave)/1e20
    sed = bb(temp, np.exp(ln_radius), wave)

    #return -0.5 * np.nansum((sed - obs_spec)**2)
    return -0.5 * np.nansum(np.abs(sed - obs_spec))


def dynesty_fit(wave, flux):
    flux = flux/np.nanmean(flux)

    args = (wave, flux)
    sampler = dynesty.NestedSampler(likelihood, prior_transform, 2, logl_args=args, nlive=4000)
    #sampler = dynesty.DynamicNestedSampler(likelihood, prior_transform, 2, logl_args=args)
    sampler.run_nested()
    results = sampler.results

    
    samples, weights = results.samples, np.exp(results.logwt - results.logz[-1])
    mean, cov = dyfunc.mean_and_cov(samples, weights)
    print('\tMean: %s' % repr(mean))
    print('\n\t'.join(('Cov: %s' % repr(mean)).split('\n')))

    new_samples = dyfunc.resample_equal(samples, weights)
    temp_q = np.quantile(new_samples[:,0], [0.025, 0.975])
    print('\tQuantiles T[K]: %s' % repr(temp_q))
    

    def plot_spec():
        plt.plot(wave[idx], flux[idx], label='Observed')
        sed_mean = bb(mean[0], np.exp(mean[1]), wave[idx])/1e20
        plt.plot(wave[idx], sed_mean, label='Fit (mean)')
        sed_low =  bb(temp_q[0], np.exp(mean[1]), wave[idx])/1e20
        plt.plot(wave[idx], sed_low, label='Fit (low)', alpha=0.5)
        sed_high =  bb(temp_q[1], np.exp(mean[1]), wave[idx])/1e20
        plt.plot(wave[idx], sed_high, label='Fit (high)', alpha=0.5)
        for i in range(50):
            i = np.random.randint(10, new_samples.shape[0])
            sed = bb(new_samples[i,0], np.exp(new_samples[i,1]), wave[idx])/1e20
            plt.plot(wave[idx], sed, alpha=0.1, color='grey')
        plt.legend()
        plt.xlabel('Wavelength [A]')
        plt.ylabel('Flux')
    #plot_fig(plot_spec, 'comparison')
    
    #return mean, cov, temp_q, results.logz[-1]
    return {
        'mean': mean,
        'cov': cov,
        'temp_q': temp_q,
        'logz': results.logz[-1]
    }
    
    
@numba.njit
def prior_transform(u):
    x = np.empty(2)
    x[0] = 1e4*u[0] + 300
    #x[0] = 3.5*u[0] + 5.5
    x[1] = 52*u[1] - 46
    return x


def gendata(temp, ln_radius, wave_grid, redshift, snr):
    sed = bb(temp, np.exp(ln_radius), wave_grid/(1+redshift))
    sed /= np.nanmean(sed)

    return np.random.normal(sed, sed/snr)


def main():
    temp = 3000
    ln_radius = 1.0
    wave_grid = np.linspace(4000, 9000, 250)

    for snr in [5, 10, 20]:
        sed = gendata(temp, ln_radius, wave_grid, 0.0, snr)
        plt.plot(wave_grid, sed, label='SNR %d' % snr, alpha=0.8)
        result = dynesty_fit(wave_grid, sed)
        #sed = bb(np.exp(result['mean'][0]), np.exp(result['mean'][1]), wave_grid)
        sed = bb(result['mean'][0], np.exp(result['mean'][1]), wave_grid)
        plt.plot(wave_grid, sed/np.nanmean(sed), label='SNR %d (Fit)' % snr)

    plt.legend()
    plt.xlabel('Wavelength [A]')
    plt.ylabel('Flux')
    plt.show()

if __name__ == '__main__':
    main()
    
    
    
