import matplotlib.pyplot as plt
import astropy.table as table
import sys



t = table.Table.read(sys.argv[1])
temp_mean = t['temp_mean']
temp_q5 = t['temp_q5']
temp_q95 = t['temp_q95']

plt.figure(figsize=(11,8))
ax1=plt.subplot(311)

ax1.errorbar(t['phase'], t['temp_mean'], yerr=(temp_mean-temp_q5, temp_q95-temp_mean))
ax1.set_ylabel('Temperature [K]')
plt.setp(ax1.get_xticklabels(), visible=False)

norm_mean = t['norm_mean']
norm_q5 = t['norm_q5']
norm_q95 = t['norm_q95']


ax2 = plt.subplot(312, sharex=ax1)
ax2.errorbar(t['phase'], t['norm_mean'], yerr=(norm_mean-norm_q5, norm_q95-norm_mean))
#ax2.plot(t['phase'], t['norm_mean'])
plt.setp(ax2.get_xticklabels(), visible=False)
ax2.set_ylabel('log N')

ax3 = plt.subplot(313, sharex=ax1)
ax3.plot(t['phase'], t['logz'])
ax3.set_ylabel('log z')
ax3.set_xlabel('Phase [d]')

if len(sys.argv) > 2:
    plt.savefig(sys.argv[2])
else:
    plt.show()
