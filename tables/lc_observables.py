import os, sys, warnings
import click
import numpy as np
import jinja2
import matplotlib
matplotlib.use('Agg')
try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../lib'))
from snelib19odp.dataset import load_dataset
import snelib19odp.lc.observables as observables
import snelib19odp.dataloader as dataloader
import snelib19odp.lc.peak_observables as peak_observables

def setup_jinja_env():
    env = jinja2.Environment(
        loader=jinja2.PackageLoader("snelib19odp"),
        autoescape=jinja2.select_autoescape()
    )
    return env



TABLE_CONFIG = [
    [
        {
            'dataset': 'SN2019odp_phot',
            'bands': ['g', 'r', 'i', 'z']
        }
    ],
    [
        {
            'dataset': 'SN1998bw',
            'bands': ['B', 'V', 'Rc', 'Ic']
        },
        {
            'dataset': 'SN2002ap',
            'bands': ['B', 'V', 'R', 'I']
        },
        {
            'dataset': 'SN2008D',
            'bands': ['r', 'i', 'B', 'V']
        },
        {
            'dataset': 'iPTF13bvn',
            'bands': ['g', 'r', 'i']
        }
    ]
]

# small macro for grouped config
def mds(dataset_name, *bands):
    return {
        'dataset': dataset_name,
        'bands': bands
    }

TABLE_CONFIG_GROUPED = [
    [
        mds('SN2019odp_phot', 'g'),
        mds('SN1998bw', 'V'),
        mds('SN2002ap', 'V'),
        mds('SN2008D', 'V'),
        mds('iPTF13bvn', 'g')
    ],
    [
        mds('SN2019odp_phot', 'r'),
        mds('SN1998bw', 'Rc'),
        mds('SN2002ap', 'R'),
        mds('SN2008D', 'r'),
        mds('iPTF13bvn', 'r')
    ],
    [
        mds('SN2019odp_phot', 'i'),
        mds('SN1998bw', 'Ic'),
        mds('SN2002ap', 'I'),
        mds('SN2008D', 'i'),
        mds('iPTF13bvn', 'i')
    ]
]

def percentiles(samples, digits):
    x = np.percentile(samples, [32, 50, 68])
    def f(x):
        return ('%%.%df' % digits) % x
    return {'p32': f(x[0]), 'p50': f(x[1]), 'p68': f(x[2]), 'dneg': f(x[1]-x[0]), 'dpos': f(x[2]-x[1])}

def format_entry(samples, digits, limit, upperlimit=True, digits_limit=1, invert=False):
    x = np.percentile(samples, [32, 50, 68])
    def f(x):
        return ('%%.%df' % digits) % x
    def fl(x):
        return ('%%.%df' % digits_limit) % x
    err = x[2] - x[0]
    if err > limit:
        if upperlimit:
            if not invert:
                return '< %s' % fl(x[2])
            else:
                return '> %s' % fl(x[2])
        else:
            if not invert:
                return '> %s' % fl(x[0])
            else:
                return '< %s' % fl(x[0])
    pos_diff = x[2] - x[1]
    neg_diff = x[1] - x[0]
    return '$%s_{ -%s }^{ +%s }$' % (f(x[1]), f(neg_diff), f(pos_diff))


@click.command()
@click.argument('output')
@click.option('--grouped', is_flag=True)
def main(output, grouped):
    jenv = setup_jinja_env()
    tmpl = jenv.get_template('table_lc_observables.j2')

    measurements = []
    cfg = TABLE_CONFIG
    if grouped:
        cfg = TABLE_CONFIG_GROUPED
    for grp in cfg:
        rows = []
        for sn in grp:
            ds = load_dataset(sn['dataset'])
            obs = observables.Observables(ds)
            
            name = ds.transient.name
            for band in sn['bands']:
                # compute the abs mag correction
                combined_lc = ds.get_combined_lc(band)
                d_absmag = np.nanmean(combined_lc['absmag'] - combined_lc['mag'])
                d_absmag_err = np.nanmean(combined_lc['mag_err_correlated'])
                
                basic = obs.basic(band)
                peak_obs = peak_observables.peak_observables(ds, band).parameter_trace_table
                print(name, band, format_entry(peak_obs['peak_mag']+peak_obs['gaussian_amplitude'], 2, 1))
                print(name, band, format_entry(peak_obs['gaussian_t0'], 1, 1))

                formatted_name = name
                if name.startswith('SN'):
                    formatted_name = '\supernova{%s}' % name[2:]
                row = {
                    'sne_name': formatted_name,
                    'band': band,
                    'delta_m15': format_entry(basic['delta_m15'], digits=2, limit=1.0),
                    'delta_mn10': format_entry(basic['delta_m-10'], digits=2, limit=1.0),
                    'linear_slope': format_entry(basic['linear_slope'], digits=1, limit=100),
                    'm_max': format_entry(basic['peak_mag'] + np.random.normal(d_absmag, d_absmag_err, size=len(basic)), digits=2, limit=2),
                    'abs_plateau_level': '',
                    'rel_plateau_level': '',
                    'peak_width': '',
                    'rise_timescale': ''
                }
                if 'plateau_level' in basic.colnames:
                    row['abs_plateau_level'] = format_entry(basic['plateau_level'] + np.random.normal(d_absmag, d_absmag_err, size=len(basic)), digits=1, limit=1)
                    row['rel_plateau_level'] = format_entry(basic['plateau_level'] - basic['peak_mag'], digits=2, limit=1, upperlimit=False, invert=True)
                if 'peak_width' in basic.colnames:
                    row['peak_width'] = format_entry(basic['peak_width'], digits=1, limit=5)
                
                if 'rise_timescale' in basic.colnames:
                    row['rise_timescale'] = format_entry(basic['rise_timescale'], digits=1, limit=5)
                rows.append(row)

        measurements.append(rows)
    
    rendered = tmpl.render(measurements=measurements)
    if output:
        if os.path.dirname(output) != '' and not os.path.isdir(os.path.dirname(output)):
            os.makedirs(os.path.dirname(output))
        with open(output, 'w') as f:
            f.write(rendered)
    else:
        print(rendered)
        

if __name__ == '__main__':
    main()
