import os, sys, warnings, collections
import click
import jinja2
try:
    import snelib19odp
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), '../lib'))
import snelib19odp.nucdata as nucdata





Row = collections.namedtuple('Row', 'descr symbol value unit')


def setup_jinja_env():
    env = jinja2.Environment(
        loader=jinja2.PackageLoader("snelib19odp"),
        autoescape=jinja2.select_autoescape()
    )
    return env


SECTIONS = [
    [
        Row('$\ion{Ni}{56}$ Lifetime', r'\tau_{Ni}', '%.2f' % nucdata.NI56_LIFETIME.value, nucdata.NI56_LIFETIME.unit),
        Row('$\ion{Co}{56}$ Lifetime', r'\tau_{Co}', '%.2f' % nucdata.CO56_LIFETIME.value, nucdata.CO56_LIFETIME.unit),
        Row('$\ion{Ni}{56}$ Specific Heating Rate', r'\epsilon_{Ni}', '%.2f' % nucdata.NI56_HEATING_RATE.value, nucdata.NI56_HEATING_RATE.unit),
        Row('$\ion{Co}{56}$ Specific Heating Rate', r'\epsilon_{Co}', '%.2f' % nucdata.CO56_HEATING_RATE.value, nucdata.CO56_HEATING_RATE.unit),
    ]
]


@click.command()
@click.option('-o', '--output')
def main(output):
    jenv = setup_jinja_env()
    tmpl = jenv.get_template('nucdata.j2')

    rendered = tmpl.render(sections=SECTIONS)
    if output:
        if not os.path.isdir(os.path.dirname(output)):
            os.makedirs(os.path.dirname(output))
        with open(output, 'w') as f:
            f.write(rendered)
    else:
        print(rendered)
        

if __name__ == '__main__':
    main()
