# save this as shell.nix
{ pkgs ? import <nixpkgs> {}}:

pkgs.mkShell rec {
  dynesty = pkgs.python3Packages.callPackage nix/dynesty.nix { };
  extinction = pkgs.python3Packages.callPackage nix/extinction.nix { };
  george = pkgs.python3Packages.callPackage nix/george.nix { };
  nativeBuildInputs = [
    (pkgs.python3.withPackages (ps: with ps; with pkgs.python3Packages; [
      jupyter
      ipython
      numpy
      matplotlib
      numba
      tqdm
      click
      astropy
      scipy
      dynesty
      pandas
      seaborn
      extinction
      sexpdata
      george
      h5py
    ]))
    (pkgs.haskellPackages.ghcWithPackages (p: [p.shake]))
  ];
}
